var LANG, DEF_LANG, BASE_URL, $black, dealer_limit = 0, wholesale_limit = 0, price_type = false,
	emailRegex = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,6}$/i,
	ruleRegex = /^(.+)$/;

jQuery.fn.placeholder = function (label) {
	return this.each(function () {

		if ($.trim($(this).val()) === '') {
			$(this).val(label);
		} else {
			$(this).attr('title', label);
		}

		$(this)
			.on('click focus', function () {
				if ($.trim($(this).val()) === label) {
					$(this).val('').attr('title', label);
				}
			})
			.on('keyup', function () {
				$(this).removeClass('error');
			})
			.on('blur', function () {
				if ($.trim($(this).val()) === '') {
					$(this).val(label).removeAttr('title');
				} else {
					$(this).attr('title', label);
					$(this).removeClass('error');
				}
			});
	});
};

jQuery.fn.dropdown = function (options) {

	var settings = $.extend({
		arrow: '',
		prevent: true,
		def_value: true,
		hide_selected: true,
		onChange: ''
	}, options);

	return this.each(function () {
		var $this = $(this);

		if ($this.find('.selected').length > 0) {
			if (settings.def_value) {
				$this.find('span:eq(0)').html($this.find('.selected').html() + settings.arrow).end().find('input').val($this.find('.selected a').data('value'));
			}

			if (settings.def_value) {
				$this.find('.selected').closest('li').hide();
			}
		}

		$this.on('click', 'span:eq(0)', function (e) {
			e.preventDefault();
			if (!$(this).closest('.dropdown').hasClass('dropdown-open')) {
				$.when($('.dropdown').each(function () { $(this).removeClass('dropdown-open').find('ul:eq(0)').slideUp(); })).then(function () {$this.addClass('dropdown-open').find('ul:eq(0)').slideDown();});
			} else {
				$this.removeClass('dropdown-open').find('ul:eq(0)').slideUp();
			}
		});

		$this.find('ul').eq(0).on('click', 'a', function (e) {
			e.preventDefault();

			if (!$(this).hasClass('disabled')) {
				if (settings.prevent === false) {
					window.location.href = $(this).attr('href');
				} else {
					$(this).closest('ul').find('.selected').removeClass('selected').show().end().end().closest('li').addClass('selected').hide();
					$this.find('span:eq(0)').html($(this).text() + settings.arrow).end().find('input').val($(this).data('value'));

					$this.removeClass('dropdown-open').find('ul:eq(0)').slideUp();
					if ($.isFunction(settings.onChange)) settings.onChange($(this));
				}
			}
		});
	});
};

jQuery.fn.filter_dropdown = function (options) {

	var settings = $.extend({
		onChange: ''
	}, options);

	return this.each(function () {
		var $this = $(this);

		$this.find('span').eq(0).on('click', function (e) {
			e.preventDefault();

			if ($this.hasClass('dropdown-open')) {
				$this.removeClass('dropdown-open').find('ul').eq(0).stop().slideUp();
			} else {
				//$.when($('.filter-dropdown').each(function () { $(this).removeClass('dropdown-open').find('ul').eq(0).stop().slideUp(); }))
				// .then(function () { $this.addClass('dropdown-open').find('ul').eq(0).stop().slideDown(); });
				$this.addClass('dropdown-open').find('ul').eq(0).stop().slideDown();
			}
		});

		$this.find('ul').eq(0).on('click', 'a', function (e) {
			e.preventDefault();

			if (!$(this).hasClass('filter-disabled')) {
				$(this).hasClass('filter-selected') ? $(this).removeClass('filter-selected') : $(this).addClass('filter-selected');
				if ($.isFunction(settings.onChange)) settings.onChange($(this));
			}
		});
	});
};

function setcookie(name, value, expires, path, domain, secure) {
	expires instanceof Date ? expires = expires.toGMTString() : typeof(expires) === 'number' && (expires = (new Date(+(new Date) + expires * 1e3)).toGMTString());
	var r = [name + "=" + value], s, i;
	for(i in s = {expires: expires, path: path, domain: domain}) s[i] && r.push(i + "=" + s[i]);
	return secure && r.push("secure"), document.cookie = r.join(";"), true;
}

function full_url(uri) {
	return window.location.protocol + '//' + window.location.hostname + (LANG === DEF_LANG ? '' : '/' + LANG) + '/' + (uri !== '' ? uri + '/' : '');
}

function base_url(uri) {
	return window.location.protocol + '//' + window.location.hostname + '/' + uri;
}

function roundPlus(x, n) {
	if (isNaN(x) || isNaN(n)) return false;
	var m = Math.pow(10,n);
	return Math.round(x*m)/m;
}

function mini_cart(data) {
	//var cart_content = '';
	//
	//if (data[0] > 0) {
	//	var grn = '';
	//	if (LANG == 'ua') grn = 'грн';
	//	if (LANG == 'ru') grn = 'грн';
	//	if (LANG == 'en') grn = 'uah';
	//
	//	var go = '';
	//	if (LANG == 'ua') go = 'Перейти до кошика';
	//	if (LANG == 'ru') go = 'Перейти в корзину';
	//	if (LANG == 'en') go = 'Skip to cart';
	//
	//	var goods = '';
	//	if (LANG == 'ua') goods = 'товарів';
	//	if (LANG == 'ru') goods = 'товаров';
	//	if (LANG == 'en') goods = 'goods';
	//
	//	var last_products = '';
	//	for (var i = 0; i < data[2].length; i++) last_products += '<div class="fm mw_one"><div class="fm mw_photo"><div><a href="' + data[2][i]['url'] + '">' + (data[2][i]['image'] != '' ? '<img src="' + data[2][i]['image'] + '" alt="">' : '') + '</a></div></div><div class="fm mw_name"><a href="' + data[2][i]['url'] + '">' + data[2][i]['title'] + '</a><div class="fm mw_price"><b>' + data[2][i]['price'] + '</b> ' + grn + '</div></div></div>';
	//
	//	cart_content = '<div class="mc_window"><div class="fm inner_window"><div class="fm mw_all">' + last_products + '</div><a href="#" class="fm mw_see_all show_cart_form">' + go + '</a></div></div><a href="#" class="fm mc_icon show_cart_form"><b></b></a><a href="#" class="fm mc_number show_cart_form"><b>' + data[0] + '</b> ' + goods + ' &nbsp; <b>' + data[1] + '</b> ' + grn + '</a>';
	//} else {
	//	var empty_cart = '';
	//	if (LANG == 'ua') empty_cart = 'Кошик порожній';
	//	if (LANG == 'ru') empty_cart = 'Корзина пуста';
	//	if (LANG == 'en') empty_cart = 'Cart is empty';
	//
	//	cart_content = '<span class="fm mc_icon"><b></b></span><span class="fm mc_number">' + empty_cart + '</span>';
	//}
	//
	//$('#mini_cart').html(cart_content);
}

$(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 600) {
            $('.page-up').addClass('is-show');
        } else {
            $('.page-up').removeClass('is-show');
        }
    });

    $('.page-up').on("click", function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 700);
    });

    $('.print_icon').on('click', function () {
        window.open(full_url('printing/index') + '?module=' + $(this).data('module') + '&id=' + $(this).data('id'), '', 'width=800,height=600,scrollbars=yes,status=no,resizable=yes,screenx=0,screeny=0');
    });

    $('.phone_mask').mask('+38 (099) 999-99-99');

    //--------------------------------------------------------------------------------------------------------------------------------------

    $(document).on('click', function (e) {
        if ($(e.target).hasClass('dropdown') || $(e.target).closest('.dropdown').length > 0) return false;
        if ($(e.target).hasClass('header-contacts_list') || $(e.target).closest('.header-contacts_list').length > 0) return false;

        $('.dropdown').map(function () {
            if ($(this).find('.dropdown_wrapper').length > 0) {
                $(this).removeClass('dropdown-open').find('.dropdown_wrapper').eq(0).stop().slideUp();
            } else {
                $(this).removeClass('dropdown-open').find('ul').eq(0).stop().slideUp();
            }
        });


        $('.header-contacts_list').hide();

        return true;
    });

    //--------------------------------------------------------------------------------------------------------------------------------------

    $('#language_dropdown').dropdown({
        prevent: false,
        def_value: false
    });

    //--------------------------------------------------------------------------------------------------------------------------------------

    $black = $('<div/>', {class: 'black'}).fadeTo(20, 0.6).hide();
    $('body').append($black);

    $black.on('click', function () {
        $('#cart').add('.catalog_modal').add($black).hide();
        $('.zoom_window').hide();
        $('.sale_form').hide();
        $('.price_form').hide();
    });

    //--------------------------------------------------------------------------------------------------------------------------------------

    var search_placeholder = '';
    if (LANG === 'ua') search_placeholder = 'Пошук...';
    if (LANG === 'ru') search_placeholder = 'Поиск...';
    if (LANG === 'en') search_placeholder = 'Search...';

    function search(query) {
        query = $.trim(query);
        if (query !== '' && query !== search_placeholder) window.location.href = full_url('search/index') + '?query=' + query;
    }

    $('#search_form')
        .find('input')
        .placeholder(search_placeholder)
        .on('keydown', function (e) {
            if (e.keyCode === 13) search($(this).val());
        })
        .end()
        .find('a').on('click', function (e) {
        e.preventDefault();
        search($(this).prev('input').eq(0).val());
    });

    //--------------------------------------------------------------------------------------------------------------------------------------

    $('.header-contacts_dropdown_link').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.header-contacts_list').toggle();
    });

    //--------------------------------------------------------------------------------------------------------------------------------------

    var $mm = $('.main-nav_wrapper');

    if ($mm.find('.main-nav_more').length > 0) {
        var $wm = $('.with-menu'),
            mh = $wm.outerHeight(),
            h = 0;

        $mm.find('.main-nav').find('ul').eq(0).children().map(function () {
            h += $(this).height();

            if (h + 44 > mh) {
                $(this).addClass('hidden mh');
            }
        });

        $mm.find('.main-nav_more').on('click', 'a', function (e) {
            e.preventDefault();

            $mm.find('.main-nav').find('.mh').toggleClass('hidden');

            $(this).toggleClass('active');
            $(this).find('span').text($(this).hasClass('active') ? '-' : '+');
        });
    }

    $mm.on('click', '.catalog-btn', function (e) {
        e.preventDefault();
        $mm.find('.main-nav').toggle();
    });

    //--------------------------------------------------------------------------------------------------------------------------------------

    $('.footer-nav_title').on('click', function () {
        if (!$(this).closest('.footer-nav').find('ul').is(':visible')) {
            $(this).closest('.footer-nav').find('ul').show();
            $(this).addClass('open');
        } else {
            if ($(this).hasClass('open')) {
                $(this).closest('.footer-nav').find('ul').hide();
                $(this).removeClass('open');
            }
        }
    });

    //--------------------------------------------------------------------------------------------------------------------------------------

    if ($('.main-slider').length > 0) {
        $('.main-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            autoplay: true,
            autoplaySpeed: 3000
        });
    }

    if ($('.categories-slider').length > 0) {
        $('.categories-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            autoplay: true,
            autoplaySpeed: 3000
        });

        $('.categories-list')
            .on('mouseenter', 'a', function () {
                if ($('#figure-' + $(this).data('menu-id')).length > 0) {
                    $('.categories-slider').addClass('hidden');
                    $('#figure-' + $(this).data('menu-id')).removeClass('hidden');
                }
            })
            .on('mouseleave', 'a', function () {
                if ($('#figure-' + $(this).data('menu-id')).length > 0) {
                    $('#figure-' + $(this).data('menu-id')).addClass('hidden');
                    $('.categories-slider').removeClass('hidden');
                }
            });

        $('.more-categories').on('click', function (e) {
            e.preventDefault();

            $('.categories-list').find('.more-category').toggleClass('hidden');
        });
    }

    //--------------------------------------------------------------------------------------------------------------------------------------

    if ($('.our_brands').length > 0) {
        $.ajax(
            base_url('js/components/brands.js'),
            {
                dataType: 'script',
                cache: true
            }
        );
    }

    //--------------------------------------------------------------------------------------------------------------------------------------

    $('.sale_form_open').on('click', function (e) {
        e.preventDefault();
        $('.sale_form').show();
        $black.show();
    });

    $('.sale_form_close').on('click', function (e) {
        e.preventDefault();
        $('.sale_form').hide();
        $black.hide();
    });

    $('.sale_send').on('click', function (e) {
        e.preventDefault();

        var $box = $(this).closest('.input-place'),
            $name = $box.find('.sale_name'),
            $email = $box.find('.sale_email'),
            $phone = $box.find('.sale_phone'),
            send_message, success_message;

        $name.add($phone).add($email).on('keyup paste blur', function () {
            $(this).removeClass('error');
        });

        if (!ruleRegex.test($name.val())) {
            $name.addClass('error');
            return false;
        }

        if (!ruleRegex.test($phone.val())) {
            $phone.addClass('error');
            return false;
        }

        /*
        if (!emailRegex.test($email.val())) {
            $email.addClass('error');
            return false;
        }
        */

        var request = {
            name: $name.val(),
            phone: $phone.val(),
            email: $email.length > 0 ? $email.val() : ''
        };
        var msg_1 = '';
        if (LANG == 'ua') msg_1 = 'Відправка';
        if (LANG == 'ru') msg_1 = 'Отправка';
        if (LANG == 'en') msg_1 = 'Sending';
        var msg_2 = '';
        if (LANG == 'ua') msg_2 = 'Повідомлення надіслано.<br><br>Наш менеджер сконтактується з Вами.';
        if (LANG == 'ru') msg_2 = 'Message sent.<br><br>Our manager will contact you.';
        if (LANG == 'en') msg_2 = 'Сообщение отправлено.<br><br>Наш менеджер свяжется с Вами.';
        send_message = '<div class="fm sending">' + msg_1 + '</div>';
        success_message = msg_2;

        $box.find('.sale_form').html('<p class="loader">' + send_message + '</p>');

        $.post(
            full_url('feedback/sale'),
            request,
            function (response) {
                if (response.error === 0) {
                    $box.html('<p class="success">' + success_message + '</p>');
                    setTimeout(function () {
                        $box.remove();
                    }, 2000);
                }
            },
            'json'
        );

        return true;
    });

    //--------------------------------------------------------------------------------------------------------------------------------------

    $('.price_form_open').on('click', function (e) {
        e.preventDefault();

        var partno = $(this).closest('tr').find('td').eq(0).text();
        $('.price_form').find('[name="partno"]').val(partno);
        $('.price_form').find('.product_partno').text(partno);

        var title = $.trim($(this).closest('tr').find('td').eq(1).text());
        $('.price_form').find('[name="title"]').val(title);
        $('.price_form').find('.product_title').text(title);

        $('.price_form').css('top', $(document).scrollTop() + 100).show();
        $black.show();
    });

    $('.price_form_close').on('click', function (e) {
        e.preventDefault();
        $('.price_form').hide();
        $black.hide();
    });

    $('.price_send').on('click', function (e) {
        e.preventDefault();

        var $self = $(this),
            $box = $self.closest('.price_form'),
            $name = $box.find('.price_name'),
            $email = $box.find('.price_email'),
            $phone = $box.find('.price_phone'),
            $total = $box.find('.price_total_items'),
            send_message, success_message;

        $name.add($phone).add($email).on('keyup paste blur', function () {
            $(this).removeClass('error');
        });

        if (!ruleRegex.test($name.val())) {
            $name.addClass('error');
            return false;
        }

        if (!ruleRegex.test($phone.val())) {
            $phone.addClass('error');
            return false;
        }

        if (!emailRegex.test($email.val())) {
            $email.addClass('error');
            return false;
        }

        if (!$.isNumeric($total.val()) || $total.val() == 0) {
            $total.addClass('error');
            return false;
        }

        var request = {
            name: $name.val(),
            phone: $phone.val(),
            email: $email.length > 0 ? $email.val() : '',
            partno: $box.find('[name="partno"]').val(),
            title: $box.find('[name="title"]').val(),
            total: $box.find('.price_total_items').val()
        };

        var msg_1 = '';
        if (LANG == 'ua') msg_1 = 'Відправка';
        if (LANG == 'ru') msg_1 = 'Отправка';
        if (LANG == 'en') msg_1 = 'Sending';
        var msg_2 = '';
        if (LANG == 'ua') msg_2 = 'Повідомлення надіслано.<br><br>Наш менеджер сконтактується з Вами.';
        if (LANG == 'ru') msg_2 = 'Сообщение отправлено.<br><br>Наш менеджер свяжется с Вами.';
        if (LANG == 'en') msg_2 = 'Message sent.<br><br>Our manager will contact you.';
        send_message = '<div class="fm sending">' + msg_1 + '</div>';
        success_message = msg_2;

        $box.hide();
        $box.find('.product_title').html('');
        $box.find('.product_partno').html('');
        $box.find('input').val('');

        $('.popup-manager')
            .html('<p class="loader">' + send_message + '</p>')
            .css('top', $(document).scrollTop() + 300)
            .show();

        $.post(
            full_url('feedback/price'),
            request,
            function (response) {
                if (response.error === 0) {
                    $('.popup-manager')
                        .html('<div class="popup-manager__inner"><a href="#" class="sf_close"></a>' + success_message + '</div>')
                        .css('top', $(document).scrollTop() + 300);

                    $black.add($('.popup-manager').find('.sf_close')).one('click', function (e) {
                        e.preventDefault();
                        $('.popup-manager').hide();
                        $black.hide();
                    });

                    setTimeout(function () {
                        $('.popup-manager').hide();
                        $black.hide();
                    }, 5000);
                }
            },
            'json'
        );

        return true;
    });

    //--------------------------------------------------------------------------------------------------------------------------------------

    if (jQuery().raty) {
        $('.raty').raty({
            readOnly: true,
            path: 'js/raty/images',
            score: function () {
                return $(this).attr('data-score');
            },
            number: function () {
                return $(this).attr('data-number');
            }
        });
    }

    //--------------------------------------------------------------------------------------------------------------------------------------

    $('#mini_cart')
        .on('mouseenter', function () {
            $(this).find('.mc_window').stop().slideDown();
        })
        .on('mouseleave', function () {
            $(this).find('.mc_window').stop().slideUp();
        });

    var $cart = $('#cart');

    $(document).on('click', '.show_cart_form', function (e) {
        e.preventDefault();

        $('.catalog_modal').hide();

        var loading = '';
        if (LANG == 'ua') loading = 'завантаження...';
        if (LANG == 'ru') loading = 'загрузка...';
        if (LANG == 'en') loading = 'loading...';

        $cart.css('top', $(document).scrollTop() + 20).text(loading);
        $black.add($cart).show();

        $.post(
            full_url('cart/form'),
            function (response) {
                $cart.html(response.form).trigger('change_cart');

            },
            'json'
        );
    });

    $cart
        .on('change_cart', function () {
            var total_price = 0,
                total_weight = 0;

            $(this).find('.tab_row_product').map(function () {
                var price = $(this).data('price'),
                    price_1 = $(this).data('price-1'),
                    limit_1 = $(this).data('limit-1'),
                    price_2 = $(this).data('price-2'),
                    limit_2 = $(this).data('limit-2'),
                    box = parseInt($(this).data('box')),
                    total = parseInt($(this).find('.ct_total').val());

                total = Math.ceil(total / box);
                total_weight += total * parseFloat($(this).data('weight'));

                var row_price = roundPlus(total * price, 2);
                $(this).find('.row_total_retail').text(row_price);

                var row_price_1 = roundPlus(total * price_1, 2);
                $(this).find('.row_total_1').text(row_price_1);

                var row_price_2 = roundPlus(total * price_2, 2);
                $(this).find('.row_total_2').text(row_price_2);

                if (total * box >= limit_2 && limit_2 > 0) {
                    $(this).find('.price_retail').add($(this).find('.price_1')).hide();
                    $(this).find('.price_2').show();
                    total_price += row_price_2;
                } else if (total * box >= limit_1 && limit_1 > 0) {
                    $(this).find('.price_retail').add($(this).find('.price_2')).hide();
                    $(this).find('.price_1').show();
                    total_price += row_price_1;
                } else {
                    $(this).find('.price_1').add($(this).find('.price_2')).hide();
                    $(this).find('.price_retail').show();
                    total_price += row_price;
                }
            });

            $(this).find('.total_cost').find('b').text(total_price.toFixed(2));
            $(this).find('.total_weight').find('strong').text(total_weight.toFixed(3));
        })
        .on('click', '.ct_plus', function (e) {
            e.preventDefault();

            var product_id = $(this).closest('.tab_row_product').data('product-id'),
                variant_id = $(this).closest('.tab_row_product').data('variant-id'),
                box = parseInt($(this).closest('.tab_row_product').data('box')),
                total = parseInt($(this).closest('.tab_row_product').find('.ct_total').val());

            if (total !== box && box > 0) {
                total = Math.ceil(total / box) * box;
            }

            total += box;

            $(this).closest('.tab_row_product').find('.ct_total').val(total);
            $cart.trigger('change_cart');

            $.post(
                full_url('cart/cart_put'),
                {
                    product_id: product_id,
                    variant_id: variant_id,
                    total: total
                },
                function (response) {
                    mini_cart(response);
                },
                'json'
            );
        })
        .on('click', '.ct_minus', function (e) {
            e.preventDefault();

            var product_id = $(this).closest('.tab_row_product').data('product-id'),
                variant_id = $(this).closest('.tab_row_product').data('variant-id'),
                box = $(this).closest('.tab_row_product').data('box'),
                total = parseInt($(this).closest('.tab_row_product').find('.ct_total').val()) - 1;

            if (total !== box && box > 0) {
                total = Math.ceil(total / box) * box;
            }

            total -= box;

            if (total <= 0) {
                total = box;
            }

            $(this).closest('.tab_row_product').find('.ct_total').val(total);
            $cart.trigger('change_cart');

            if (total >= box) {
                $.post(
                    full_url('cart/cart_put'),
                    {
                        product_id: product_id,
                        variant_id: variant_id,
                        total: total
                    },
                    function (response) {
                        mini_cart(response);
                    },
                    'json'
                );
            }
        })
        .on('blur', '.ct_total', function (e) {
            e.preventDefault();

            var product_id = $(this).closest('.tab_row_product').data('product-id'),
                variant_id = $(this).closest('.tab_row_product').data('variant-id'),
                box = $(this).closest('.tab_row_product').data('box'),
                total = parseInt($(this).val());

            if ($.isNumeric(total)) {
                if (total !== box && box > 0) {
                    total = Math.ceil(total / box) * box;
                    $(this).val(total);
                }
            } else {
                total = 0;
            }

            $(this).closest('.tab_row_product').find('.ct_total').val(total);
            $cart.trigger('change_cart');

            if (total > 0 && total >= box) {
                $.post(
                    full_url('cart/cart_put'),
                    {
                        product_id: product_id,
                        variant_id: variant_id,
                        total: total
                    },
                    function (response) {
                        mini_cart(response);
                    },
                    'json'
                );
            }
        })
        .on('click', '.ct_delete', function (e) {
            e.preventDefault();

            var product_id = $(this).closest('.tab_row_product').data('product-id'),
                variant_id = $(this).closest('.tab_row_product').data('variant-id');

            $(this).closest('.tab_row_product').remove();

            $.post(
                full_url('cart/delete_item'),
                {
                    product_id: product_id,
                    variant_id: variant_id
                },
                function (response) {

                    if (response[0] == 0) {
                        $cart.add($black).hide();
                    }

                    mini_cart(response);
                },
                'json'
            );

            $cart.trigger('change_cart');
        })
        .on('click', '.cart_close_link', function (e) {
            e.preventDefault();

            $cart.add($black).hide();
        });

    //--------------------------------------------------------------------------------------------------------------------------------------

    var $articles = $('.article_open'), read_more, read_less;

    if ($articles.length > 0) {

        if (LANG == 'ua') {
            read_more = 'Читати більше';
            read_less = 'Згорнути';
        }

        if (LANG == 'ru') {
            read_more = 'Читать больше';
            read_less = 'Свернуть';
        }

        if (LANG == 'en') {
            read_more = 'Read more';
            read_less = 'Close';
        }

        $articles.map(function () {
            $(this).append(' <a href="#" class="fmr read_more">' + read_more + '</a>');
            $(this).next('.article_close').append(' <a href="#" class="fmr read_less">' + read_less + '</a>');

            $(this).find('.read_more').on('click', function (e) {
                e.preventDefault();

                $(this).closest('.article_open').next('.article_close').stop().slideDown();
                $(this).hide();
            });

            $(this).next('.article_close').find('.read_less').on('click', function (e) {
                e.preventDefault();

                $(this).closest('.article_close').stop().slideUp();
                $(this).closest('.article_close').prev('.article_open').find('.read_more').show();
            });
        });
    }
});

$(function () {
	function supports_storage() {
		try {
			return 'localStorage' in window && window['localStorage'] !== null;
		} catch (e) {
			return false;
		}
	}

	function get_storage() {
		return window['localStorage'];
	}

	if (supports_storage()) {
		var storage = get_storage(),
			favorite_products,
			compare_products;

		function get_storage_products() {
			favorite_products = storage.getItem('favorite_products');
			compare_products = storage.getItem('compare_products');

			if (favorite_products === null || favorite_products === '') {
				favorite_products = [];
			} else {
				if (favorite_products.substring(0, 1) === ',') {
					favorite_products = favorite_products.substring(1, favorite_products.length - 1);
				}

				favorite_products = favorite_products.split(',');
			}

			if (favorite_products.length > 0) {
				$('#favorite_link').find('b').text(favorite_products.length).removeClass('hidden');

				$(document).find('.select_favorite').removeClass('active');
				$.each(favorite_products, function(i, v) {
					$(document).find('.select_favorite[data-product-id="' + v + '"]').addClass('active');
				});
			} else {
				$(document).find('.select_favorite').removeClass('active');
				$('#favorite_link').find('b').text(favorite_products.length).addClass('hidden');
			}

			if (compare_products === null || compare_products === '') {
				compare_products = [];
			} else {
				if (compare_products.substring(0, 1) === ',') {
					compare_products = compare_products.substring(1, compare_products.length - 1);
				}

				compare_products = $.trim(compare_products, ',').split(',');
			}

			if (compare_products.length > 0) {
				$('#compare_link').find('b').text(compare_products.length).removeClass('hidden');

				$(document).find('.select_compare').removeClass('active');
				$.each(compare_products, function(i, v) {
					$(document).find('.select_compare[data-product-id="' + v + '"]').addClass('active');
				});
			} else {
				$(document).find('.select_compare').removeClass('active');
				$('#compare_link').find('b').text(compare_products.length).addClass('hidden');
			}
		}

		get_storage_products();

		setInterval(function () {
			get_storage_products();
		}, 5000);

		$(document)
			.on('click', '.select_favorite', function (e) {
				e.preventDefault();

				if (supports_storage()) {
					var product_id = $(this).data('product-id');

					$(this).toggleClass('active');

					if ($(this).hasClass('active')) {
						favorite_products.push(product_id);
					} else {
						var _selected_products = favorite_products;
						favorite_products = [];

						$.each(_selected_products, function (i, v) {
							if (product_id != v) {
								favorite_products.push(v);
							}
						});

						if ($(this).hasClass('rem')) {
							$(this).closest('.col-md-4').remove();

							history.pushState('', document.title, full_url('profile/favorite') + '?products=' + favorite_products.join(','));
						}
					}

					if (favorite_products.length > 0) {
						$('#favorite_link').find('b').text(favorite_products.length).removeClass('hidden');
					} else {
						//$('#favorite_link').addClass('hidden');
					}

					storage.setItem('favorite_products', favorite_products.join(','));
				}
			})
			.on('click', '.select_compare', function (e) {
				e.preventDefault();

				if (supports_storage()) {
					var product_id = $(this).data('product-id');

					$(this).toggleClass('active');

					if ($(this).hasClass('active')) {
						compare_products.push(product_id);
					} else {
						var _selected_products = compare_products;
						compare_products = [];

						$.each(_selected_products, function (i, v) {
							if (product_id != v) {
								compare_products.push(v);
							}
						});

						if ($(this).hasClass('rem')) {
							$(this).closest('.col-md-4').remove();

							history.pushState('', document.title, full_url('profile/compare') + '?products=' + compare_products.join(','));
						}
					}

					if (compare_products.length > 0) {
						$('#compare_link').find('b').text(compare_products.length).removeClass('hidden');
					} else {
						//$('#compare_link').addClass('hidden');
					}

					storage.setItem('compare_products', compare_products.join(','));
				}
			});

		$('#compare_link').on('click', function (e) {
			e.preventDefault();

			if (compare_products.length > 0) {
				window.location.href = $(this).attr('href') + '?products=' + compare_products.join(',');
			}
		});

		$('#favorite_link').add('#favorite_link_2').on('click', function (e) {
			e.preventDefault();

			if (favorite_products.length > 0) {
				window.location.href = $(this).attr('href') + '?products=' + favorite_products.join(',');
			}
		});
	}
});

$(function () {
	var s_timer;

	$(window).scroll(function(){
			if ( $(this).scrollTop() > 34 ) {
					$('.header-main').addClass('fix');
			} else {
					$('.header-main').removeClass('fix');
			}
	})

	$(document).on('click', function (event) {
		if ($(event.target).hasClass('sq') || $(event.target).closest('.sq').length > 0) return false;
		$('.search_variants').removeClass('active').hide();
		return true;
	});

	$('#search_form').find('[type="text"]').prop('autocomplete', 'off');

	$('#search_form').on('keyup', '[type="text"]', function (e) {
		if (e.keyCode != 13 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40) {
			clearTimeout(s_timer);

			var v = $.trim($(this).val());

			if (v.length >= 3) {
				$('.search_variants').removeClass('active').find('ul').hide().empty();

				s_timer = setTimeout(function () {
					$.post(
						full_url('search/autocomplete'),
						{
							query: v
						},
						function (response) {
							if (response.length > 0) {
								var slist = '';

								$.each(response, function (i, val) {
                                    if (val.hasOwnProperty('all')) {
                                        slist += '<li><a href="/search/index/?query=' + v + '&mode=' + val.all + '" class="all_results">' + (LANG === 'ua' ? 'всі результати' : 'все результаты') + ' (' + val.total + ')</a>';
									} else {
                                        if (val.hasOwnProperty('url')) {
                                            slist += '<li><a href="' + val.url + '">' + val.title + '</a></li>';
                                        } else {
                                            slist += '<li>' + val.title + '</li>';
                                        }
									}
								});

								$('.search_variants').find('ul').html(slist).show();
								$('.search_variants').addClass('active').show();
							}
						},
						'json'
					);
				}, 200);
			}
		}
	});

	$(document).keydown(function(e){

		if (e.keyCode == 13) {
			if ($('.search_variants').hasClass('active') && $('.search_variants').find('li.active').length > 0) {
				e.preventDefault();
				window.location.href = $('.search_variants').find('li.active').find('a').attr('href');
			}
		}

		if (e.keyCode == 38) {

			var sv = $('.search_variants').find('li').length,
				si = $('.search_variants').find('li.active').length > 0 ? $('.search_variants').find('li.active').index() - 1 : sv - 1;

			if ($('.search_variants').hasClass('active')) {
				e.preventDefault();

				if (si >= 0) {
					$('.sq').blur();
					$('.search_variants').find('li').removeClass('active');
					$('.search_variants').find('li').eq(si).addClass('active');
				} else {
					$('.sq').focus();
					$('.search_variants').find('li').removeClass('active');
				}
			}
		}

		if (e.keyCode == 40) {

			var sv = $('.search_variants').find('li').length,
				si = $('.search_variants').find('li.active').length > 0 ? $('.search_variants').find('li.active').index() + 1 : 0;

			if ($('.search_variants').hasClass('active')) {
				e.preventDefault();

				if (si < sv) {
					$('.sq').blur();
					$('.search_variants').find('li').removeClass('active');
					$('.search_variants').find('li').eq(si).addClass('active');
				} else {
					$('.sq').focus();
					$('.search_variants').find('li').removeClass('active');
				}
			}
		}
	});
});

$(function () {
	var msg_1 = '';
	if (LANG == 'ua') msg_1 = 'Відправка';
	if (LANG == 'ru') msg_1 = 'Отправка';
	if (LANG == 'en') msg_1 = 'Sending';

	var msg_2 = '';
	if (LANG == 'ua') msg_2 = 'Надіслано';
	if (LANG == 'ru') msg_2 = 'Sent';
	if (LANG == 'en') msg_2 = 'Отправлено';

	var $subscribe_form = $(document).find('.subscribe-form');

	$subscribe_form
		.on('submit', function (e) {
			e.preventDefault();

			var $form = $(this);

			if (!$form.hasClass('disabled')) {
				if (!emailRegex.test($form.find('[type="email"]').val())) {
					$form.find('[type="email"]').addClass('error');
				} else {
					$form.addClass('disabled').find('[type="submit"]').val(msg_1);

					$.post(
						full_url('subscribe/send'),
						{
							email: $form.find('[type="email"]').val()
						},
						function (response) {
							if (response.hasOwnProperty('success') && response.success) {
								$form
									.removeClass('disabled')
									.find('[type="email"]').val('')
									.end()
									.find('[type="submit"]').val(msg_2);
							}
						},
						'json'
					);
				}
			}
		})
		.map(function () {
			var $form = $(this);

			$form
				.on('keyup paste blur', '[type="email"]', function (e) {
					if ($.trim($(this).val()) !== '') {
						$(this).removeClass('error');

						if (e.hasOwnProperty('keyCode') && e.keyCode === 13) {
							$form.trigger('submit');
						}
					}
				});
		});
});