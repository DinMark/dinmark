var component_id = 0,
	components = '',
	filters = {},

	component_filters = {},
	catalog_check_filters = 0,

	catalog_page = 1,
	catalog_per_page = 33,
	catalog_get_total = 0,
	catalog_total_products = 0,

	slide_timer,
	catalog_get_limits = 0,
	min_price = 0, current_min_price = 0,
	max_price = 0, current_max_price = 0,

	$catalog_filters,
	$catalog_pagination;

$(function () {
	$catalog_filters = $('#catalog_filters');
	$catalog_pagination = $('#products_pagination');

	function set_selected_filters() {
		var selected_filters = '';

		if (current_min_price !== min_price || current_max_price !== max_price) {
			selected_filters += '<div>Ціна <p><a href="#" class="deactivate-filter" data-filter="price">x</a> <span style="font-weight: normal">' + current_min_price + ' грн &dash; ' + current_max_price + ' грн</span></p></div>';
		}

		$catalog_filters.find('.filter-dropdown').each(function () {
			if ($(this).find('.filter-selected').length > 0) {
				selected_filters += '<div>' + $(this).find('span').eq(0).text() + '<br>';

				$(this).find('.filter-selected').each(function () {
					selected_filters += '<p><a href="#" class="deactivate-filter" data-filter="option" data-value="' + $(this).data('value') + '">x</a> <span style="font-weight: normal">' + $(this).text() + '</span></p>';
				});

				selected_filters += '</div>';
			}
		});

		$('#selected_filters_list').html(selected_filters);

		if (selected_filters !== '') {
			$('#selected_filters').removeClass('hidden');
		} else {
			$('#selected_filters').addClass('hidden');
		}
	}

	function set_pagination(total) {
		$catalog_pagination.html('');

		if (total > 0) {
			$catalog_pagination.pagination(total, {
				items_per_page: catalog_per_page,
				current_page: catalog_page - 1,
				callback: function (page) {
					$('html, body').animate({ scrollTop: $('.main_col').offset()['top']});

					catalog_page = page + 1;

					catalog_get_total = 0;
					catalog_get_limits = 0;

					filtration();

					return false;
				}
			});
		}
	}

	function set_slider() {
		$('#price_with').text(min_price + ' грн');
		$('#price_to').text(max_price + ' грн');

		current_min_price = min_price;
		current_max_price = max_price;

		if (min_price === max_price) {
			$('#filter_prices').hide();
		} else {
			$('#filter_prices').show();

			$('#price_slider')
				.html('')
				.noUiSlider({
					range: [min_price, max_price],
					start: [min_price, max_price],
					serialization: {
						to: ['', ''],
						resolution: 1
					},
					slide: function () {
						var values = $(this).val();

						current_min_price = values[0];
						current_max_price = values[1];

						$('#price_with').text(current_min_price + ' грн');
						$('#price_to').text(current_max_price + ' грн');

						clearTimeout(slide_timer);

						slide_timer = setTimeout(function () {
							if ($catalog_filters.length > 0) {
								var _filter_id;
								component_filters = [];

								$catalog_filters.find('.filter-dropdown').each(function (i, val) {
									_filter_id = $(this).data('filter-id');
									var values = [];

									$(this).find('.filter-selected').each(function () { values.push($(this).data('value')); });
									filters[_filter_id] = values;
								});
							}

							catalog_check_filters = 1;
							catalog_get_limits = 0;
							catalog_get_total = 1;
							catalog_page = 1;

							filtration();
						}, 200);
					}
				});
		}
	}

	function filtration() {

		//set_selected_filters();
		filters = {};

		if ($catalog_filters.length > 0) {
			$catalog_filters.find('.filter-box').each(function () {
				var _filter_id = $(this).data('filter-id'),
					values = [];

				if ($(this).find('.selected').length > 0) {
					$(this).find('.selected').each(function () { values.push($(this).data('value-id')); });
					filters[_filter_id] = values;
				}
			});
		}

		if (current_min_price !== min_price || current_max_price !== max_price) catalog_get_limits = 0;

		$.post(
			full_url('catalog/filter'),
			{
				component_id: component_id,
				components: components,
				get_total: catalog_get_total,
				page: catalog_page,

				get_limits: catalog_get_limits,
				min_price: current_min_price,
				max_price: current_max_price,

				filters: filters,

				check_filters: catalog_check_filters,
				component_filters: catalog_check_filters === 1 ? component_filters : {}
			},
			function (response) {
				if (response.success) {

					$('#products_list').html(response.products);

					if (catalog_get_total === 1) {
						set_pagination(response.total);
						catalog_get_total = 0;
					}

					if (catalog_check_filters === 1) {
						$.each(response.filters, function (i, val) {
							if (val[1] === 0) {
								$('#value_' + val[0]).addClass('filter-disabled');
							} else {
								$('#value_' + val[0]).removeClass('filter-disabled');
							}
						});
						catalog_check_filters = 0;
					}

					if (catalog_get_limits === 1) {
						min_price = response.price_limits['min'];
						max_price = response.price_limits['max'];
						set_slider();
						catalog_get_limits = 0;
					}

					/*
					$('.raty').raty({
						readOnly: true,
						path: 'js/raty/images',
						score: function() { return $(this).attr('data-score'); },
						number: function() { return $(this).attr('data-number'); }
					});
					*/
				}
			},
			'json'
		);
	}

	set_pagination(catalog_total_products);
	set_slider();

	$catalog_filters
		.on('click', '.filter-box_title', function (e) {
			e.preventDefault();

			$(this).closest('.filter-box').find('.filter-box_content').toggle();
		})
		.on('click', 'a', function(e) {
			e.preventDefault();

			$(this).toggleClass('selected');

            catalog_get_total = 1;
           // catalog_check_filters = 1;

			filtration();
		});

	$('.sort-dropdown').filter_dropdown({
		onChange: function (obj) {
			obj.closest('ul').find('.filter-selected').removeClass('filter-selected');
			obj.addClass('filter-selected');

			setcookie('ds_catalog_sort', obj.data('value'), 86400, '/');

			catalog_get_limits = 0;
			catalog_get_total = 0;
			catalog_check_filters = 0;

			filtration();
		}
	});
});