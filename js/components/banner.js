var $slides = $('.main-slider_slide'), $pimps = $('.banner_pag').find('a'), slide_index = 0, slide_interval;

function sliding() {
	slide_interval = setInterval(function () {
		$slides.eq(slide_index).removeClass('active').fadeTo(500, 0);
		$pimps.eq(slide_index).removeClass('active');

		slide_index++;
		if (slide_index > $slides.length - 1) slide_index = 0;
		$slides.eq(slide_index).addClass('active').fadeTo(500, 1);
		$pimps.eq(slide_index).addClass('active');
	}, 3000);
}

if ($slides.length > 1) {
	sliding();

	$pimps.on('click', function (e) {
		e.preventDefault();

		clearInterval(slide_interval);

		$slides.eq(slide_index).removeClass('active').fadeTo(500, 0);
		$pimps.eq(slide_index).removeClass('active');

		slide_index = $(this).index();
		$slides.eq(slide_index).addClass('active').fadeTo(500, 1);
		$pimps.eq(slide_index).addClass('active');

		sliding();
	});
}