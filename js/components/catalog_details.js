$(function () {
	$('.product-buy').on('click', function (e) {
		e.preventDefault();

		$('html, body').animate({'scrollTop': $('.details-tabs').offset()['top'] - 70});
	});

	$('.catalog_table')
		.on('click', '.product-table_minus', function (e) {
			e.preventDefault();

			var box = parseInt($(this).closest('tr').find('.cart_total').data('box')),
                total = $.isNumeric($(this).closest('tr').find('.cart_total').val()) ? parseInt($(this).closest('tr').find('.cart_total').val()) : 0;

			if (total - box > 0) {
                $(this).closest('tr').find('.cart_total').val(total - box);
			}
		})
        .on('click', '.product-table_plus', function (e) {
            e.preventDefault();

            var box = parseInt($(this).closest('tr').find('.cart_total').data('box')),
                total = $.isNumeric($(this).closest('tr').find('.cart_total').val()) ? parseInt($(this).closest('tr').find('.cart_total').val()) : 0;

            $(this).closest('tr').find('.cart_total').val(total + box);
        })
		.on('keyup', '.cart_total', function () {
			$(this).removeClass('error');
		})
		.on('blur', '.cart_total', function () {
			var v = parseInt($(this).val()),
				box = parseInt($(this).data('box'));

			if ($.isNumeric(v)) {
				if ((v / box != Math.ceil(v / box)) || v < 1) {
					if (box > 0) {
						$(this).val(Math.ceil(v / box) * box);
						$(this).addClass('error');
					}

					$(this).addClass('error');
				} else {
					$(this).removeClass('error');
				}
			}
		})
		.on('click', '.to_cart', function (e) {
			e.preventDefault();

			var $link = $(this),
				box = $link.closest('tr').find('.cart_total').data('box'),
				total = parseInt($link.closest('tr').find('.cart_total').val());

			if ($.isNumeric(total) && total > 0 && total % box === 0) {
				$link.closest('tr').find('.cart_total').removeClass('error');
				$link.find('i').text('...');

				$.post(
					full_url('cart/cart_put'),
					{
						product_id: $link.closest('tr').data('product-id'),
						variant_id: $link.closest('tr').data('variant-id'),
						total: total
					},
					function (response) {
						$link.addClass('show_cart_form');

						if (LANG == 'ua') $link.replaceWith('В кошику');
						if (LANG == 'ru') $link.replaceWith('В корзине');
						if (LANG == 'en') $link.replaceWith('In cart');

						//mini_cart(response);
						$('#mini_cart').find('b').removeClass('hidden').text(response[0]);
					},
					'json'
				);
			} else {
				$link.closest('tr').find('.cart_total').addClass('error');
			}
		});
});

$(function () {
	$('#delivery_details').on('click', function (e) {
		e.preventDefault();
		$(document).scrollTop($('#product_delivery').offset()['top']);
	});

	$('.new_tab_base')
		.on('mouseenter', 'td', function () {
			var index = $(this).addClass('crnt').index() - 1;

			$(this).closest('table').find('tr').each(function () {
				if (!$(this).find('td').eq(index).hasClass('crnt')) {
					$(this).find('td').eq(index).addClass('td_hover');
				}
			});
		})
		.on('mouseleave', 'td', function () {
			$('.td_hover').removeClass('td_hover');
			$('.crnt').removeClass('crnt');
		});
});

$(function () {
	var $images_slider = $('.small_photos'),
		images_length = $images_slider.find('.sp_all').find('a').length,
		image_width = $images_slider.find('.sp_all').find('a').eq(0).outerWidth(true),
		image_index = 0;

	$images_slider.find('.sp_all').css('width', images_length * image_width);

	$images_slider.on('click', '.dt_ar_left', function (e) {
		e.preventDefault();
		if (!$(this).hasClass('invisible')) {
			image_index--;

			if (image_index == 0) $(this).addClass('invisible');
			$images_slider.find('.dt_ar_right').removeClass('invisible');
			$images_slider.find('.sp_all').stop().animate({'left': -Math.abs(image_index * image_width)}, 300);
		}
	});

	$images_slider.on('click', '.dt_ar_right', function (e) {
		e.preventDefault();
		if (!$(this).hasClass('invisible')) {
			image_index++;

			if (image_index + 2 == images_length - 1) $(this).addClass('invisible');
			$images_slider.find('.dt_ar_left').removeClass('invisible');
			$images_slider.find('.sp_all').stop().animate({'left': -Math.abs(image_index * image_width)}, 300);
		}
	});
});

$(function () {
	jQuery.fn.photo_gallery = function (options) {

		var msg_6 = '', msg_7 = '';

		if (LANG == 'ua') msg_6 = 'Купити';
		if (LANG == 'ru') msg_6 = 'Купить';
		if (LANG == 'en') msg_6 = 'Buy';

		if (LANG == 'ua') msg_7 = 'грн';
		if (LANG == 'ru') msg_7 = 'грн';
		if (LANG == 'en') msg_7 = 'uah';

		return this.each(function () {
			var settings = $.extend({
					group: '',
					price: 0,
					product_id: 0,
					total_photos: 0,
					current_index: 0
				}, options),
				_window = '<div id="zoom_window_' + settings.group + '" class="zoom_window"><div class="zoom_inner">{content}</div></div>',
				header = '<div class="fm zoom_top"><table align="center"><tbody><tr><td align="center">{thumbs}</td></tr></tbody></table><a href="#" class="z_close sf_close"></a></div>',
				content = '	<div class="fm for_zoom"><a href="#" class="fm zoom_left invisible"></a><div class="fm zoom_place"><div>{image}</div></div><a href="#" class="fm zoom_right"></a></div>',
				footer = '<div class="fm zoom_price"><table align="center"><tbody><tr><td align="center"><a href="#" class="fm buy to_cart" data-id="{product-id}">' + msg_6 + '</a><div class="fm z_price">{price} ' + msg_7 + '</div></td></tr></tbody></table></div>',
				$this = $(this),
				n = 0;

			$this.find('a').each(function () { if ($(this).find('img').length > 0) { $(this).addClass('gallery-item').data('index', n); n++; } });
			settings.total_photos = $this.find('.gallery-item').length;

			if ($('body').find('#zoom_window_' + settings.group).length == 0) {
				var thumbs = '';

				$this.find('.gallery-item').each(function () {
					$(this).data('index');
					thumbs += '<a href="#"><span><img src="' + $(this).find('img').attr('src') + '" alt=""></span></a>';
				});
				header = header.replace('{thumbs}', thumbs);

				var src = $this.find('.gallery-item').eq(settings.current_index).attr('href');
				content = content.replace('{image}', '<img src="' + src + '" alt="">');

				footer = footer.replace('{product-id}', settings.product_id);
				footer = footer.replace('{price}', settings.price);

				_window = _window.replace('{content}', header + content + footer);
				$('body').append(_window);
			}

			var $zoom = $('#zoom_window_' + settings.group);

			$zoom.find('.zoom_top').find('table').find('a').eq(settings.current_index).addClass('active');

			$zoom.find('.z_close').on('click', function (e) {
				e.preventDefault();

				$black.hide();
				$zoom.hide();
			});

			$this.find('.gallery-item').on('click', function (e) {
				e.preventDefault();

				settings.current_index = $(this).data('index');

				if (settings.current_index == 0) {
					$zoom.find('.zoom_left').addClass('invisible');
				} else {
					$zoom.find('.zoom_left').removeClass('invisible');
				}

				if (settings.current_index == settings.total_photos - 1) {
					$zoom.find('.zoom_right').addClass('invisible');
				} else {
					$zoom.find('.zoom_right').removeClass('invisible');
				}

				$(this).addClass('active').siblings().removeClass('active');

				var src = $(this).attr('href');
				$zoom.find('.zoom_place').find('img').attr('src', src);

				$black.show();
				$zoom.css('top', $(document).scrollTop() + 100).show();
			});

			$zoom.find('.zoom_top').find('table').on('click', 'a', function (e) {
				e.preventDefault();

				if (!$(this).hasClass('active')) {
					settings.current_index = $(this).index();

					if (settings.current_index == 0) $zoom.find('.zoom_left').addClass('invisible');
					if (settings.current_index == settings.total_photos - 1) $zoom.find('.zoom_right').addClass('invisible');

					$(this).addClass('active').siblings().removeClass('active');

					var src = $this.find('.gallery-item').eq(settings.current_index).attr('href');

					$zoom.find('.zoom_place').find('img').stop().fadeTo(500, 0, function () {
						$(this).attr('src', src).load(function () {
							$(this).stop().fadeTo(500, 1);
						});
					});
				}
			});

			$zoom.on('click', '.zoom_left', function (e) {
				e.preventDefault();
				if (!$(this).hasClass('invisible')) {
					settings.current_index--;

					if (settings.current_index >= 0) {
						if (settings.current_index == 0) $(this).addClass('invisible');
						$zoom.find('.zoom_right').removeClass('invisible');
						$zoom.find('.zoom_top').find('table').find('a').eq(settings.current_index).addClass('active').siblings().removeClass('active');

						var src = $this.find('.gallery-item').eq(settings.current_index).attr('href');

						$zoom.find('.zoom_place').find('img').stop().fadeTo(500, 0, function () {
							$(this).attr('src', src).load(function () {
								$(this).stop().fadeTo(500, 1);
							});
						});
					}
				}
			});

			$zoom.on('click', '.zoom_right', function (e) {
				e.preventDefault();
				if (!$(this).hasClass('invisible')) {
					settings.current_index++;

					if (settings.current_index <= settings.total_photos - 1) {

						if (settings.current_index == settings.total_photos - 1) $(this).addClass('invisible');

						$zoom.find('.zoom_left').removeClass('invisible');
						$zoom.find('.zoom_top').find('table').find('a').eq(settings.current_index).addClass('active').siblings().removeClass('active');

						var src = $this.find('.gallery-item').eq(settings.current_index).attr('href');

						$zoom.find('.zoom_place').find('img').stop().fadeTo(500, 0, function () {
							$(this).attr('src', src).load(function () {
								$(this).stop().fadeTo(500, 1);
							});
						});
					}
				}
			});
		});
	};
});

$(function () {
	Array.prototype.diff = function (a) {
		return this.filter(function (i) {
			return a.indexOf(i) >= 0;
		});
	};

	var $filters = $('.filters-box'),
		$variants = $('.catalog_table');

	$filters
		.on('click', '.filter-box_title', function (e) {
			e.preventDefault();

			$(this).toggleClass('active');
			$(this).closest('.filter-box').find('.filter-box_content').toggle();
		})
		.on('click', '.filter-value', function (e) {
			e.preventDefault();

			$(this).toggleClass('selected');
			$variants.trigger('filtration');
		})
		.on('click', '.common-btn--invert', function (e) {
			e.preventDefault();

			$filters.find('.selected').removeClass('selected');
			$variants.trigger('filtration');
		});

	$variants
		.on('filtration', function (e) {
			e.preventDefault();

			var filters = {},
				filter_id;

			$filters.find('.selected').map(function () {
				filter_id = $(this).closest('.filter-box').data('filter-id');

				if (!filters.hasOwnProperty(filter_id)) {
					filters[filter_id] = [];
				}

				filters[filter_id].push($(this).data('value-id').toString());
			});

			if (Object.keys(filters).length === 0) {
				$variants.find('tbody').find('tr').show();
				$filters.find('.common-btn--invert').addClass('hidden');
			} else {
				$variants.find('tbody').find('tr').map(function () {
					var $row = $(this),
						row_filters = 0,
						row_values = [];

					$.each(filters, function (filter_id, values) {
						if ($row.data('filter-' + filter_id) && $row.data('filter-' + filter_id).toString().split(',').diff(values).length > 0) {
							row_filters++;
						}
					});

					if (Object.keys(filters).length === row_filters) {
						$row.show();
					} else {
						$row.hide();
					}
				});

				$filters.find('.common-btn--invert').removeClass('hidden');
			}
		});
});

$(function () {
	var msg_1 = '';
	if (LANG == 'ua') msg_1 = 'Відправка';
	if (LANG == 'ru') msg_1 = 'Отправка';
	if (LANG == 'en') msg_1 = 'Sending';

	var msg_2 = '';
	if (LANG == 'ua') msg_2 = 'Надіслано';
	if (LANG == 'ru') msg_2 = 'Sent';
	if (LANG == 'en') msg_2 = 'Отправлено';

	var $form = $(document).find('.order-call_form');

    $form.find('[type="tel"]').mask('+38(099)999-99-99');

	$form
		.on('submit', function (e) {
			e.preventDefault();

			if (!$form.hasClass('disabled')) {
				if ($form.find('[type="tel"]').val() === '') {
					$form.find('[type="tel"]').addClass('error');
				} else {
					$form.addClass('disabled').find('[type="submit"]').val(msg_1);

					$.post(
						full_url('feedback/call'),
						{
							phone: $form.find('[type="tel"]').val()
						},
						function (response) {
							if (response.hasOwnProperty('success') && response.success) {
								alert('Наш менеджер з Вами зв”яжеться!');

								$form
									.removeClass('disabled')
									.find('[type="tel"]').val('')
									.end()
									.find('[type="submit"]').val(msg_2);
							}
						},
						'json'
					);
				}
			}
		})
		.map(function () {
			var $form = $(this);

			$form
				.on('keyup paste blur', '[type="tel"]', function (e) {
					if ($.trim($(this).val()) !== '') {
						$(this).removeClass('error');

						if (e.hasOwnProperty('keyCode') && e.keyCode === 13) {
							$form.trigger('submit');
						}
					}
				});
		});
});

$(function () {
    $('.raty_active').raty({
        targetScore : '#comment_stars',
        path: 'js/raty/images',
        score: function() { return $(this).attr('data-score'); },
        number: function() { return $(this).attr('data-number'); }
    });

    $('.raty').raty({
        readOnly: true,
        path: 'js/raty/images',
        score: function() { return $(this).attr('data-score'); },
        number: function() { return $(this).attr('data-number'); }
    });

    $('#comment_send').on('click', function (e) {
        e.preventDefault();

        var $name = $('#comment_name'),
            $email = $('#comment_email'),
            $comment = $('#comment_text'),
            $stars = $('#comment_stars');

        $name.add($email).add($comment).on('keyup paste blur', function () { $(this).removeClass('error'); });

        if (!ruleRegex.test($name.val())) {
            $name.addClass('error');
            return false;
        }

        if (!emailRegex.test($email.val())) {
            $email.addClass('error');
            return false;
        }

        if (!ruleRegex.test($comment.val())) {
            $comment.addClass('error');
            return false;
        }

        var request = {
            name: $name.val(),
            email: $email.val(),
            comment: $comment.val(),
            stars: $stars.val(),
            product_id: $(e.target).data('product-id')
        };

        var msg_4 = '';

        if (LANG == 'ua') msg_4 = 'Відправка...';
        if (LANG == 'ru') msg_4 = 'Отправка...';
        if (LANG == 'en') msg_4 = 'Sending...';

        $(e.target).text(msg_4);

        $.post(
            (LANG == DEF_LANG ? '' : '/' + LANG) + '/catalog/send_comment/',
            request,
            function (response) {
                if (response.success) {
                    var row = '<div class="fm dc_one"><div class="fm dc_title"><span>' + request.name + '</span><div class="raty-' + response.comment_id + '" data-number="5" data-score="' + request.stars + '"></div><span>' + response.date + '</span></div><div class="fm dc_txt">' + request.comment + '</div></div>';

                    $('#comments_list').append(row);

                    $('.raty-' + response.comment_id).raty({
                        readOnly: true,
                        path: 'js/raty/images',
                        score: function() { return $(this).attr('data-score'); },
                        number: function() { return $(this).attr('data-number'); }
                    });
                    var msg_5 = '';
                    if (LANG == 'ua') msg_5 = 'додати відгук';
                    if (LANG == 'ru') msg_5 = 'добавить отзыв';
                    if (LANG == 'en') msg_5 = 'Add a review';
                    $(e.target).text(msg_5);
                    $('.raty_active').raty('reload');
                    $name.add($email).add($comment).val('');
                }
            },
            'json'
        );

        return true;
    });
});