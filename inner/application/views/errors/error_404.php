<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	if (!defined('LANG')) define('LANG', 'ua');
	if (!defined('DEF_LANG')) define('DEF_LANG', 'ua');
?><!DOCTYPE html>
<html lang="uk">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<link href="/css/base.css" rel="stylesheet">
	<script type="text/javascript" src="<?=base_url('js/jquery.js');?>"></script>
	<script type="text/javascript">var LANG = '<?=LANG;?>', emailRegex = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,6}$/i;</script>
</head>
<body>
	<div class="fm page_404">
		<div class="fm er_logo">
			<a href="/<?=(LANG != DEF_LANG ? LANG . '/' : '');?>" class="<?=LANG;?>"><img src="/images/logo.png" alt=""></a>
		</div>
		<div class="centre">
			<div class="fm text_404">
				<figure>
					<img src="<?=base_url('images/img-404.png');?>" alt="">
					<figcaption>
						<div class="title_404"><?php if(LANG=='ua')echo'<b>ОПаньки!</b> Такої сторінки не знайдено...';if(LANG=='ru')echo'<b>Опаньки!</b> Такой страницы не найдено ...';if(LANG=='en')echo'<b>Wow!</b> This page was not found ...'; ?></div>
						<a href="/<?=(LANG != DEF_LANG ? LANG . '/' : '');?>" class="common-btn"><? if(LANG=='ua')echo'повернутися на головну';if(LANG=='ru')echo'вернуться на главную';if(LANG=='en')echo'back home';?></a>
						<p><small>або</small> ПІдписатись на розсилку</p>
						<form action="#" class="subscribe_404">
							<input type="email" placeholder="вкажіть свій е-мейл">
							<input type="submit" value="ПІДПИСАТИСЬ">
						</form>
					</figcaption>
				</figure>
			</div>
		</div>
	</div>
	<div class="popup-manager"></div>
    <div class="black"></div>
	<script type="text/javascript">
		$(function () {
			var msg_1 = '';
			if (LANG == 'ua') msg_1 = 'Відправка';
			if (LANG == 'ru') msg_1 = 'Отправка';
			if (LANG == 'en') msg_1 = 'Sending';

			var msg_2 = '';
			if (LANG == 'ua') msg_2 = 'Ви підписані на новини компанії DinMark';
			if (LANG == 'ru') msg_2 = 'Ви підписані на новини компанії DinMark';
			if (LANG == 'en') msg_2 = 'Ви підписані на новини компанії DinMark';

			var msg_3 = '';
			if (LANG == 'ua') msg_3 = 'Дана пошта вже зареєстрована';
			if (LANG == 'ru') msg_3 = 'Дана пошта вже зареєстрована';
			if (LANG == 'en') msg_3 = 'Дана пошта вже зареєстрована';

			var $subscribe_form = $(document).find('.subscribe_404');

			$subscribe_form
				.on('submit', function (e) {
					e.preventDefault();

					var $form = $(this);

					if (!$form.hasClass('disabled')) {
						if (!emailRegex.test($form.find('[type="email"]').val())) {
							$form.find('[type="email"]').addClass('error');
						} else {
							$form.addClass('disabled').find('[type="submit"]').text(msg_1);

							$.post(
								'/subscribe/send/',
								{
									email: $form.find('[type="email"]').val()
								},
								function (response) {
									if (response.hasOwnProperty('success')) {
										$form
											.removeClass('disabled')
											.find('[type="email"]').val('');

										if (response.subscribe == 1) {
											$('.popup-manager')
												.html('<div class="popup-manager__inner"><a href="#" class="sf_close"></a>' + msg_2 + '</div>')
                                                .show();
										} else {
											$('.popup-manager')
												.html('<div class="popup-manager__inner"><a href="#" class="sf_close"></a>' + msg_3 + '</div>')
                                                .show();
										}

										$('.black').show();
									}
								},
								'json'
							);
						}
					}
				})
				.map(function () {
					var $form = $(this);

					$form
						.on('keyup paste blur', '[type="email"]', function (e) {
							if ($.trim($(this).val()) !== '') {
								$(this).removeClass('error');

								if (e.hasOwnProperty('keyCode') && e.keyCode === 13) {
									$form.trigger('submit');
								}
							}
						});
				});

			$('.popup-manager').on('click', '.sf_close', function (e) {
				e.preventDefault();
                $('.popup-manager').hide();
                $('.black').hide();
			});
		});
	</script>
</body>
</html>