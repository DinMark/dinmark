<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_css('admin.css');
	$this->template_lib->set_js('admin/plugins.js');

	$top_level = isset($active['top_level']) ? $active['top_level'] : '';
	$sub_level = isset($active['sub_level']) ? $active['sub_level'] : '';

	$_menu_id = $menu_id;
	if ($menu_id == 0) $menu_id = $r = $this->db->select('id')->where('main', 1)->get('menu')->row('id');

	$is_main = $this->init_model->is_main();
	$page_uri = $this->init_model->get_link($menu_id, '{URL}');
?>
<?php if (isset($admin_menu['root']) AND count($admin_menu['root']) > 0): ?>
<div class="centre">
<div class="admin_base">
	<div class="fm main_panel">
		<div class="fm top_info_part">
			<div class="fm tip_admin">Головний адміністратор</div>
			<div class="tip_logo"></div>
			<a href="<?=$this->uri->full_url('admin/login/logout');?>" class="fmr tip_exit">Вийти</a>
		</div>
		<div class="fm middle_buttons_part">
			<div class="mbp_width">
				<?php foreach ($admin_menu['root'] as $item): ?>
				<a href="<?=($item['url'] == '' ? $page_uri : $this->uri->full_url($item['url'] . 'menu_id=' . $menu_id));?>"<?php if ($top_level === $item['code']) echo ' class="active"'; ?>><b></b><?=$item['name'];?></a>
				<?php endforeach; ?>
			</div>
		</div>
		<?php if (isset($admin_menu[$top_level]) AND $sub_level != ''): ?>
		<div class="fm bottom_links_part" id="components_controls">
			<?php foreach ($admin_menu[$top_level] as $item): ?>
				<?php if (!$is_main OR ($is_main AND (isset($item['on_main'])) OR $top_level != 'components')): ?>
				<a href="<?=!isset($item['url']) ? '#' : $this->uri->full_url($item['url'] . 'menu_id=' . $menu_id);?>" class="<?=$item['class'];?><?=($top_level == $item['module'] AND $sub_level == $item['index']) ? ' active' : '';?>" data-module="<?=$item['module'];?>" data-method="<?=$item['index'];?>" data-config="<?=$item['config'];?>"<?php if (isset($item['check'])) if ($this->init_model->check_component(($item['check'] == 'on_page' ? $menu_id : ''), $item['module'], $item['index'])) echo ' style="display: none"'; ?>><b></b><?=$item['name'];?></a>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
	<div id="admin_components_list"><?=$page_content;?></div>
</div>
</div>
<?php endif; ?>
<script type="text/javascript">
//<![CDATA[
	var delete_alert = <?=$this->config->item('delete_alert') == 1 ? 'true': 'false';?>, menu_id = <?=$menu_id;?>;

	function component_position(callback) {
		var components = [];

		$('.admin_component').each(function (i, val) {
			components[i] = $(val).data('component-id');
		});

		if (components.length > 0) {
			$.post(
				full_url('admin/components/update_position'),
				{
					components : components
				},
				function () {
					if ($.type('callback') === 'function') callback();
					component_loader_hide($('.component_loader'), '');
				},
				'json'
			);
		}
	}

	$(function () {

		component_controls();

		$('#components_controls').on('click', '.component_add', function (e) {
			e.preventDefault();

			var request = {
					menu_id: menu_id,
					module: $(this).data('module'),
					method: $(this).data('method'),
					config: $(this).data('config')
				},
				reload = false;

			if (!$(this).hasClass('com_article') && !$(this).hasClass('google_map')) {
				$(this).hide();
				reload = true;
			}

			$.post(
				full_url('admin/components/insert'),
				request,
				function (response) {
					if (response.error === 0) {
						if (reload) {
							window.location.reload();
						} else {
							$('#admin_components_list').prepend(response.component);
							component_controls();
						}
					}
				},
				'json'
			);
		});
	});
//]]>
</script>