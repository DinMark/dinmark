<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	$is_main = $this->init_model->is_main();
	$menu_id = $this->init_model->get_menu_id();
	$languages = $this->config->item('database_languages');

	$cpage = (int)$this->input->get('page');
	$mpage = '';

	if ($cpage > 1) {
		$mpage = (LANG === 'ua' ? '. Сторінка ' : '. Страница ') . $cpage;
	}
?>
<!DOCTYPE html>
<html lang="<?=(LANG == 'ua') ? 'uk' : LANG;?>">
<head>
	<?php if (isset($data_layer)): ?><script type="text/javascript"><?=$data_layer;?></script><?php endif; ?>
	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NJDPR6J');</script>
    <!-- End Google Tag Manager -->
	
	<script type='text/javascript'>
(function() {
var widgetId = 'd99e0652c1ff6548ff25215295d91f3a';
var s = document.createElement('script');
s.type = 'text/javascript';
s.charset = 'utf-8';
s.async = true;
s.src = '//callme.voip.com.ua/lirawidget/script/'+widgetId;
var ss = document.getElementsByTagName('script')[0];
ss.parentNode.insertBefore(s, ss);}
)();
</script>

	<base href="<?=base_url();?>">
	<meta charset="utf-8">

	<title><?php if (isset($page_title)) echo $page_title . $mpage; ?></title>
	<?php if (isset($page_keywords) AND $page_keywords != ''): ?><meta name="keywords" content="<?=$page_keywords;?>"><?php endif; ?>
	<?php if (isset($page_description) AND $page_description != ''): ?><meta name="description" content="<?=$page_description . $mpage;?>"><?php endif; ?>

	<meta name="viewport" content="initial-scale=1.0, width=device-width, minimum-scale=1.0, maximum-scale=1.0">

	<?php if (count($languages) > 1): ?><?php foreach ($languages as $language): ?>
		<link rel="alternate" hreflang="<?=($language['code'] === 'ua' ? 'uk' : $language['code']);?>" href="<?=(isset($language_links[$language['code']]) ? $language_links[$language['code']] : $this->init_model->get_link($menu_id, '{URL}', FALSE, FALSE, $language['url']));?>">
		<?php if ($language['code'] === LANG): ?><link rel="canonical" href="<?=(isset($language_links[$language['code']]) ? $language_links[$language['code']] : $this->init_model->get_link($menu_id, '{URL}', FALSE, FALSE, $language['url']));?>"><?php endif; ?>
	<?php endforeach; ?><?php endif; ?>

	<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700&subset=latin,cyrillic" rel="stylesheet" type="text/css">
	<link href="<?=base_url('css/bootstrap.css');?>" rel="stylesheet">
	<link href="<?=base_url('css/font-awesome.css');?>" rel="stylesheet">
	<link href="<?=base_url('css/slick.css');?>" rel="stylesheet">
	<link href="<?=base_url('css/base.css');?>" rel="stylesheet">
	<link href="<?=base_url('css/adaptive.css');?>" rel="stylesheet">
	<?php if (isset($page_css)) echo $page_css; ?>
	<?php if (isset($slider['styles'])): ?><style type="text/css"><?=$slider['styles'];?></style><?php endif; ?>
	<?php if (isset($menu_styles)): ?><style type="text/css"><?=$menu_styles;?></style><?php endif; ?>

    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

	<!--[if IE]><script src="<?=base_url('js/html5.js');?>"></script><![endif]-->
	<script type="text/javascript" src="<?=base_url('js/jquery.js');?>"></script>
	<script type="text/javascript" src="<?=base_url('js/slick.min.js');?>"></script>
	<script type="text/javascript" src="<?=base_url('js/jquery.matchHeight-min.js');?>"></script>
	<script type="text/javascript" src="<?=base_url('js/jquery.maskedinput.min.js');?>"></script>
	<?php if (isset($page_javascript)) echo $page_javascript; ?>
	<script type="text/javascript" src="<?=base_url('js/scripts.js?v=1.2');?>"></script>
	<script type="text/javascript">LANG = '<?=LANG;?>'; DEF_LANG = '<?=$this->config->item('def_lang');?>';</script>
	<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/3dee989cceb11054a69a48e5b/725601213092d5a5253dde373.js");</script>
</head>
<body<?php if (!$is_main): ?> class="inner-page"<?php endif; ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJDPR6J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="fm for_footer_bottom">
	<?php if (isset($catalog_modal)) echo $catalog_modal; ?>
	<!--a href="<?=$this->init_model->get_component_link('feedback');?>" class="ask_us <?=LANG;?>"></a-->
	<header class="header-wrapper fm">
		<div class="top-line fm">
			<div class="container">
				<div class="header-menu fm"><?php if (isset($header_menu)): ?><?=$header_menu;?><?php endif; ?></div>
				<div class="header-reg fmr">
					<?php if ($this->init_model->is_user()): ?>
						<a href="<?=$this->uri->full_url('profile/data');?>" class="header-reg_user"><i class="fa fa-user" aria-hidden="true"></i><?=$this->session->userdata('user_name');?></a>
						<a href="<?=$this->uri->full_url('profile/logout');?>" class="header-reg_link"><? if(LANG=='ua')echo'Вийти';if(LANG=='ru')echo'Выйти';if(LANG=='en')echo'Logout';?></a>
					<?php else: ?>
						<a href="<?=$this->uri->full_url('profile/registration');?>" class="header-reg_link header-reg_link--invert"><? if(LANG=='ua')echo'Стати партнером';if(LANG=='ru')echo'Стать партнером';if(LANG=='en')echo'Partner with';?></a>
						<a href="<?=$this->uri->full_url('profile/login');?>" class="header-reg_link"><? if(LANG=='ua')echo'Вхід';if(LANG=='ru')echo'Вход';if(LANG=='en')echo'Login';?></a>
					<?php endif; ?>
				</div>
				<?php if (count($languages) > 1): ?>
					<div class="header-lang fmr">
						<?php foreach ($languages as $language): ?>
							<a href="<?=(isset($language_links[$language['code']]) ? $language_links[$language['code']] : $this->init_model->get_link($menu_id, '{URL}', FALSE, FALSE, $language['url']));?>" class="<?=($language['code'] . (LANG === $language['code'] ? ' active' : ''));?>"><?=mb_substr($language['name'], 0, 3)?></a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="header-main fm">
			<div class="container">
				<div class="vrt-c">
					<div class="logo-wrapper" itemscope itemtype = "http://schema.org/Organization">
						<a href="<?=$this->uri->full_url();?>" class="logo" itemprop="url">
							<img src="<?=base_url('images/logo.png');?>" alt="<? if(LANG=='ua')echo'Експерт з якісного кріплення';if(LANG=='ru')echo'Эксперт из качественного крепления';if(LANG=='en')echo'Еxpert on qualitative fastening';?>" itemprop="logo">
							<strong><? if(LANG=='ua')echo'Експерт з якісного кріплення';if(LANG=='ru')echo'Эксперт из качественного крепления';if(LANG=='en')echo'Еxpert on qualitative fastening';?></strong>
						</a>
					</div>
					<div class="header-contacts">
						<div class="header-contacts_dropdown">
							<div class="header-contacts_dropdown_link">
								<i class="fa fa-mobile" aria-hidden="true"></i>
								<span class="ct-phone">(096) 011-01-03</span>
								<i class="fa fa-angle-down" aria-hidden="true"></i>
							</div>
							<ul class="header-contacts_list">
								<li>
									<div class="header-contacts_list_title"><? if(LANG=='ua')echo'Відділ продажів';if(LANG=='ru')echo'Отдел продаж';if(LANG=='en')echo'Sales team';?>:</div>
									<span class="header-tel">
										<i class="fa fa-phone" aria-hidden="true"></i> 
										<span class="ct-phone">(044) 49 99 252</span>
									</span>
									<span class="header-tel">
										<i class="fa fa-phone" aria-hidden="true"></i>
										<span class="ct-phone">(096) 011 01 03</span>
									</span>
									<span class="header-email">
										<i class="fa fa-envelope-o" aria-hidden="true"></i>
										info@dinmark.com.ua
									</span>
								</li>
								<li>
									<div class="header-contacts_list_title"><? if(LANG=='ua')echo'Відділ оптових продажів';if(LANG=='ru')echo'Отдел оптовых продаж';if(LANG=='en')echo'Wholesale team';?>:</div>
									<span class="header-email">
										<i class="fa fa-envelope-o" aria-hidden="true"></i>
										masha.dinmark@gmail.com
									</span>
								</li>
								<li>
									<div class="header-contacts_list_title"><? if(LANG=='ua')echo'Графік роботи менеджерів';if(LANG=='ru')echo'График работы менеджеров';if(LANG=='en')echo'Managers schedule';?>:</div>
									<span><? if(LANG=='ua')echo'Пн-Пт';if(LANG=='ru')echo'Пн-Пт';if(LANG=='en')echo'Mon-Fr';?>: 9:00 - 18:00</span>
									<span><? if(LANG=='ua')echo'Сб-Нд';if(LANG=='ru')echo'Сб-Нд';if(LANG=='en')echo'St-Su';?>: 9:00 - 17:00</span>
								</li>
							</ul>
						</div>
						<div class="sale_form_box">
							<a href="#" class="header-callback sale_form_open"><? if(LANG=='ua')echo'зателефонувати вам';if(LANG=='ru')echo'позвонить вам';if(LANG=='en')echo'call you';?>?</a>
						</div>
					</div>
					<div class="header-search">
						<div id="search_form" class="search-input">
							<input type="text" value="">
							<a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
							<div class="search_variants"><ul></ul>
                                <a href="#" class="sf_close"></a></div>
						</div>
					</div>
					<div class="header-icons">
						<div id="mini_cart" class="cart-icon<?php if (defined('CART_PAYMENT')): ?> hidden<?php endif; ?>">
							<a href="#" class="cart-icon_wrapper show_cart_form">
								<i class="fa fa-shopping-cart" aria-hidden="true"></i>
								<span><? if(LANG=='ua')echo'Кошик';if(LANG=='ru')echo'Корзина';if(LANG=='en')echo'Cart';?></span>
								<b<?php if (isset($mini_cart[0]) and $mini_cart[0] == 0): ?> class="hidden"<?php endif; ?>><?php if (isset($mini_cart[0])): ?><?=$mini_cart[0];?><?php endif; ?></b>
							</a>
						</div>
						<a id="favorite_link" href="<?=$this->uri->full_url('profile/favorite');?>" class="header-favorite">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span><? if(LANG=='ua')echo'Улюблені';if(LANG=='ru')echo'Избранные';if(LANG=='en')echo'Favorites';?></span>
                            <b class="hidden">0</b>
						</a>
						<a id="compare_link" href="<?=$this->uri->full_url('profile/compare');?>" class="header-compare">
							<i class="fa fa-balance-scale" aria-hidden="true"></i>
							<span><? if(LANG=='ua')echo'Порівняння';if(LANG=='ru')echo'Сравнение';if(LANG=='en')echo'Comparison';?></span>
                            <b class="hidden">0</b>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="menu-line fm">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-7">
						<div class="main-nav_wrapper fm">
							<div class="catalog-btn_wrapper">
								<a href="#" class="catalog-btn">
									<b><i class="fa fa-bars" aria-hidden="true"></i></b>
									<? if(LANG=='ua')echo'Вибір кріплення';if(LANG=='ru')echo'Выбор крепежа';if(LANG=='en')echo'All categories';?>
									<span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
								</a>
							</div>
							<nav class="main-nav">
								<?php if (isset($catalog_menu)): ?><?=$catalog_menu;?><?php endif; ?>
								<?php if ($is_main): ?><div class="main-nav_more fm"><a href="#"><span>+</span><? if(LANG=='ua')echo'Більше категорій';if(LANG=='ru')echo'Больше категорий';if(LANG=='en')echo'More categories';?></a></div><?php endif; ?>
							</nav>
						</div>
					</div>

					<div class="col-md-9 col-sm-6 col-xs-5">
						<nav class="top-nav fm">
							<a href="#" class="top-nav_btn">
								<b><? if(LANG=='ua')echo'Меню';if(LANG=='ru')echo'Меню';if(LANG=='en')echo'Menu';?></b>
								<span class="burger">
									<span></span>
									<span></span>
									<span></span>
								</span>
							</a>
							<?php if (isset($top_menu)): ?><?=$top_menu;?><?php endif; ?>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Content -->
	<main class="fm content main_col">
		<?php if ($is_main AND (isset($slider['slides']) AND $slider['slides'] != '')): ?>
		<section class="with-menu fm">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 col-lg-offset-3 col-md-12 col-sm-12 col-xs-12">
						<div class="main-slider fm">
							<?=$slider['slides'];?>
						</div>
						<?php if ($is_main): ?>
						<div class="shop-advantages fm">
							<div class="shop-advantages_item">
								<a href="<?=$this->init_model->get_link(1452, '{URL}');?>">
									<span class="shop-advantages_item_icon icon_1"></span>
									<span class="shop-advantages_item_text"><? if(LANG=='ua')echo'Повернення коштів';if(LANG=='ru')echo'Возврат денежных средств';if(LANG=='en')echo'Refunds';?></span>
								</a>
							</div>
							<div class="shop-advantages_item">
								<a href="<?=$this->init_model->get_link(1451, '{URL}');?>">
									<span class="shop-advantages_item_icon icon_2"></span>
									<span class="shop-advantages_item_text"><? if(LANG=='ua')echo'Швидка доставка';if(LANG=='ru')echo'Быстрая доставка';if(LANG=='en')echo'Fast delivery';?></span>
								</a>
							</div>
							<div class="shop-advantages_item">
								<a href="<?=$this->init_model->get_link(459, '{URL}');?>">
									<span class="shop-advantages_item_icon icon_3"></span>
									<span class="shop-advantages_item_text"><? if(LANG=='ua')echo'Найнижчі ціни';if(LANG=='ru')echo'Самые низкие цены';if(LANG=='en')echo'Lowest prices';?></span>
								</a>
							</div>
							<div class="shop-advantages_item">
								<a href="<?=$this->init_model->get_link(1440, '{URL}');?>">
									<span class="shop-advantages_item_icon icon_4"></span>
									<span class="shop-advantages_item_text"><? if(LANG=='ua')echo'Гарантія якості';if(LANG=='ru')echo'Гарантия качества';if(LANG=='en')echo'Quality guarantee';?></span>
								</a>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
		<?php endif; ?>
		<?php if (!$is_main AND (isset($bread_crumbs['navigation']))): ?>
		<div class="fm clickpath">
			<div class="container">
				<?=implode('<b> / </b>', $bread_crumbs['navigation']);?><b> / </b><span><?=$bread_crumbs['current_name'];?></span>
			</div>
		</div>
		<?php endif; ?>
		<?php if (isset($page_content)) echo $page_content; ?>
	</main>
</div>
<div class="fm fullwidth-box">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-12 divider">
				<div class="social-networks">
					<div class="social-networks_title"><?php if (LANG == 'ua') echo 'Залишайтесь на зв’язку!'; if (LANG == 'ru') echo 'Оставайтесь на связи!'; ?></div>
					<ul class="social-networks_list">
						<li><a href="https://www.facebook.com/dinMarkUA"  rel="nofollow" target="_blank"><i class="icon icon-facebook"></i>Facebook</a></li>
						<li><a href="https://aboutme.google.com/b/115500355298424659746"  rel="nofollow" target="_blank"><i class="icon icon-google-plus"></i>Google+</a></li>
						<li><a href="https://www.youtube.com/channel/UCaW1nX048qzfDJx6qe-5CgA"  rel="nofollow" target="_blank"><i class="icon icon-youtube"></i>YouTube</a></li>
						<li><a href="https://twitter.com/DinmarkInfo"  rel="nofollow" target="_blank"><i class="icon icon-twitter"></i>Twitter</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-7 col-sm-7 col-xs-12 divider divider-invert">
				<form action="#" class="subscribe-form">
					<input type="email" placeholder="<?php if (LANG == 'ua') echo 'Вкажіть Ваш e-mail, щоб слідкувати за новинами Dinmark '; if (LANG == 'ru') echo 'Укажите Ваш e-mail, чтоб следить за Dinmark'; ?>">
					<input type="submit" value="<?php if (LANG == 'ua') echo 'Підписатися'; if (LANG == 'ru') echo 'Подписаться'; ?>" class="common-btn">
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Footer -->
<footer class="footer-wrapper fm">
	<div class="footer-main fm">
		<div class="container">
			<div class="row">
				<?php if (isset($bottom_menu)): ?><?=$bottom_menu;?><?php endif; ?>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="footer-contacts">
						<div class="footer-contacts_title"><? if(LANG=='ua')echo'Контактна інформація';if(LANG=='ru')echo'Контактная информация';if(LANG=='en')echo'Contact Information';?><i class="fa fa-angle-down" aria-hidden="true"></i></div>
						<div class="footer-contacts_wrapper" itemscope itemtype = "http://schema.org/Organization">
							<?php if ($this->config->item('address_' . LANG) !== ''): ?>
								<div class="footer-contacts_item address">
									<div class="footer-contacts_icon">
										<i class="fa fa-map-marker" aria-hidden="true"></i>
									</div>
									<div class="footer-contacts_text" itemprop = "address" itemscope itemtype = "http://schema.org/PostalAddress"><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?>: <a href="https://www.google.com.ua/maps/place/Dinmark/@49.857034,24.0179068,17z/data=!3m1!4b1!4m5!3m4!1s0x473adda64a9c2887:0xf18e2aa9f76c2723!8m2!3d49.857034!4d24.0200955" target="_blank"><?=stripslashes($this->config->item('address_' . LANG));?></a></div>
								</div>
							<?php endif; ?>
							<?php if ($this->config->item('site_email') !== ''): ?>
								<div class="footer-contacts_item email">
									<div class="footer-contacts_icon">
										<i class="fa fa-envelope-o" aria-hidden="true"></i>
									</div>
									<div class="footer-contacts_text">
										<span>Email: <a href="mailto:<?=$this->config->item('site_email');?>" itemprop = "email"><?=$this->config->item('site_email');?></a></span>
									</div>
								</div>
							<?php endif; ?>
							<?php if (trim($this->config->item('site_phones')) !== ''): ?>
								<div class="footer-contacts_item phones">
									<div class="footer-contacts_icon">
										<i class="fa fa-phone" aria-hidden="true"></i>
									</div>
									<div class="footer-contacts_text">
										<span><? if(LANG=='ua')echo'Номер телефону';if(LANG=='ru')echo'Номер телефона';if(LANG=='en')echo'Phone number';?>: <?=$this->config->item('site_phones');?></span>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom fm">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="fm copyright">&copy; Din Mark <?=date('Y');?>. <? if(LANG=='ua')echo'Всі права захищені';if(LANG=='ru')echo'Все права защищены';if(LANG=='en')echo'All right reserved';?></div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="development">
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="footer-payment">
						<a href="#"><i class="fa fa-cc-paypal" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-cc-mastercard" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-cc-visa" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<a href="#" class="page-up">
	<i class="fa fa-angle-up" aria-hidden="true"></i>
	<span><? if(LANG=='ua')echo'Вгору';if(LANG=='ru')echo'Вверх';if(LANG=='en')echo'Up';?></span>
</a>

<div class="black"></div>
<div class="popup-manager"></div>
<div id="cart" class="cart_window"></div>
<div class="standart_form sale_form" style="display:none;">
	<a href="#" class="sf_close sale_form_close"></a>
	<div class="fm sf_title"><? if(LANG=='ua')echo'Замовлення дзвінка';if(LANG=='ru')echo'Заказ звонка';if(LANG=='en')echo'Request a call';?></div>
	<div class="input-place">
		<div class="input-item">
			<input class="sale_name" type="text" placeholder="<? if(LANG=='ua')echo'Ваше ім’я';if(LANG=='ru')echo'Ваше имя';if(LANG=='en')echo'Your name';?>">
		</div>
		<div class="input-item">
			<input class="sale_phone phone_mask" type="text" placeholder="<? if(LANG=='ua')echo'Номер телефону';if(LANG=='ru')echo'Номер телефона';if(LANG=='en')echo'Phone number';?>">
		</div>
		<div class="box-c">
			<a href="#" class="common-btn sale_send"><? if(LANG=='ua')echo'Зателефонуйте мені';if(LANG=='ru')echo'Позвоните мне';if(LANG=='en')echo'Call me';?></a>
		</div>
	</div>
</div>

<div class="standart_form price_form" style="display:none;">
	<input type="hidden" name="partno">
	<input type="hidden" name="title">
	<a href="#" class="sf_close price_form_close"></a>
	<div class="fm sf_title"><? if(LANG=='ua')echo'Уточнити ціну';if(LANG=='ru')echo'Уточнить цену';if(LANG=='en')echo'Refine the price';?></div>
	<div class="input-place">
        <div class="input-item product_title"></div>
        <div class="input-item product_partno"></div>
        <div class="input-item">
			<input class="price_name" type="text" placeholder="<? if(LANG=='ua')echo'Ваше ім’я';if(LANG=='ru')echo'Ваше имя';if(LANG=='en')echo'Your name';?>">
		</div>
		<div class="input-item">
			<input class="price_phone phone_mask" type="text" placeholder="<? if(LANG=='ua')echo'Номер телефону';if(LANG=='ru')echo'Номер телефона';if(LANG=='en')echo'Phone number';?>">
		</div>
		<div class="input-item">
			<input class="price_email" type="text" placeholder="E-mail">
		</div>
        <div class="input-item">
            <input class="price_total_items" type="text" placeholder="<? if(LANG=='ua')echo'Кількість одиниць товару';if(LANG=='ru')echo'Количество единиц товара';if(LANG=='en')echo'Number of units';?>">
        </div>
        <div class="box-c">
			<a href="#" class="common-btn price_send"><? if(LANG=='ua')echo'Уточнити';if(LANG=='ru')echo'Уточнить';if(LANG=='en')echo'Refine';?></a>
		</div>
	</div>
</div>
<!--Mailchimp-->
	<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us16.list-manage.com","uuid":"3dee989cceb11054a69a48e5b","lid":"a193f09e00"}) })</script>
<!--End Mailchimp-->
<script type="text/javascript">(function (w, d) {w.amo_pixel_token = 'p1Ip4iB8yqaEyXU9HPmcTN8oexiJROuOSAZnLl6M0GYxIAhsHNVy6n8+AHsewtvO';var s = document.createElement('script'), f = d.getElementsByTagName('script')[0];s.id = 'amo_pixel_js';s.type = 'text/javascript';s.async = true;s.src = 'https://piper.amocrm.ru/pixel/js/tracker/pixel.js?token=' + w.amo_pixel_token;f.parentNode.insertBefore(s, f);})(window, document);</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-20155892-8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-20155892-8');
</script>
<meta name="wot-verification" content="d746a863a300c0ff5cdf"/>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/597b2cb30d1bb37f1f7a6453/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<!-- BEGIN CALL2ME CODE {literal} -->
<script type='text/javascript'>
(function() {
var widgetId = 'd99e0652c1ff6548ff25215295d91f3a';
var s = document.createElement('script');
s.type = 'text/javascript';
s.charset = 'utf-8';
s.async = true;
s.src = '//callme.voip.com.ua/lirawidget/script/'+widgetId;
var ss = document.getElementsByTagName('script')[0];
ss.parentNode.insertBefore(s, ss);}
)();
</script>
<!-- {/literal} END CALL2ME CODE -->
</body>
</html>