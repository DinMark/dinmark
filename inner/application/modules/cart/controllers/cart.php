<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Cart
	 *
	 * 0 - ID продукту
	 * 1 - кількість
	 *
	 * @property Cart_model $cart_model
	 */
	class Cart extends MX_Controller
	{
		/**
		 * Додавання товару в кошик
		 *
		 * @return string
		 */
		public function cart_put()
		{
			$this->load->helper('form');

			$product_id = intval($this->input->post('product_id'));
			$variant_id = intval($this->input->post('variant_id'));
			$total = intval($this->input->post('total'));

			$this->load->model('cart_model');
			$result = $this->cart_model->cart_put($product_id, $variant_id, $total);

			return json_encode($result);
		}

		/**
		 * Вивід кошику
		 */
		public function form()
		{
			$this->load->model('cart_model');

			$template_data = array(
				'products' => $this->cart_model->get_cart_products(),
			);

			$response = array(
				'form' => $this->load->view('cart_form_tpl', $template_data, TRUE),
			);

			return json_encode($response);
		}

		/**
		 * Видалення товару з кошику
		 *
		 * @return string
		 */
		public function delete_item()
		{
			$response = array();

			$product_id = intval($this->input->post('product_id'));
			$variant_id = intval($this->input->post('variant_id'));

			if ($product_id > 0 AND $variant_id > 0)
			{
				$this->load->model('cart_model');
				$response = $this->cart_model->delete_item($product_id, $variant_id);
			}

			return json_encode($response);
		}

		/**
		 * Вибір способу оплати та доставки
		 *
		 */
		public function payment()
		{
			define('CART_PAYMENT', true);

			$this->template_lib->set_title('Оплата та доставка');

			$this->load->model('cart_model');
			$this->load->model('catalog_help/catalog_help_model');

			$template_data = array(
				'cart' => $this->cart_model->get_mini_cart(),
				'products' => $this->cart_model->get_cart_products(),
				'helps' => $this->catalog_help_model->get_helps(),
				'delivery' => array(),
			);

			if ($this->init_model->is_user())
			{
				$this->load->model('profile/profile_model');

				$user_id = $this->session->userdata('user_id');
				$user = $this->profile_model->get_user($user_id, null, 'delivery');
				$delivery = (isset($user['delivery']) and $user['delivery'] !== '') ? json_decode(stripslashes($user['delivery']), true) : array();
				$template_data['delivery'] = $delivery;

				if (isset($delivery['np_type']) and (int)$delivery['np_type'] === 1) {
					$this->load->model('cart/cart_model');

					if (isset($delivery['np_city']) and $delivery['np_city'] !== '') {
						$city = $this->cart_model->get_city($delivery['np_city']);

						if ($city !== null) {
							$template_data['np_city'] = $city['name'];
							$template_data['np_warehouses'] = $this->cart_model->get_warehouses($delivery['np_city']);
						}
					}
				}
			}

			$this->template_lib->set_content($this->load->view('cart_payment_tpl', $template_data, TRUE));
		}

		/**
		 * Відправка замовлення
		 *
		 * @return string
		 */
		public function send()
		{
			$response = array('success' => FALSE);

			$this->load->library('form_validation');

			$this->form_validation->set_rules('name', '', 'required|xss_clean|strip_tags');
			$this->form_validation->set_rules('phone', '', 'required|xss_clean|strip_tags');
			$this->form_validation->set_rules('email', '', 'required|valid_email|xss_clean|strip_tags');

			$this->load->model('cart_model');

			$mini_cart = $this->cart_model->get_mini_cart();
			$cart = $this->cart_model->get_cart_products();

			if ($this->form_validation->run() AND $mini_cart[0] > 0)
			{
				$name = $this->input->post('name');
				$phone = $this->input->post('phone');
				$email = $this->input->post('email');
				$additional = strip_tags($this->input->post('additional', TRUE));
				$payment = intval($this->input->post('payment'));
				$delivery = strip_tags($this->input->post('delivery'));

				$order_id = $this->cart_model->insert($mini_cart, $name, $phone, $email, $payment, $delivery);

                $mini_cart[0] = 0;

				foreach ($cart as &$v)
				{
                    $mini_cart[0] += $v['total'];

                    if ($v['total'] >= $v['limit_2'] and $v['limit_2'] > 0) {
                        $v['price'] = $v['price_2'];
                        $v['cost'] = $this->init_model->price_to_box($v['price_2'], $v['box']) * ($v['total'] / $v['box']);
                    } elseif ($v['total'] >= $v['limit_1'] and $v['limit_1'] > 0) {
                        $v['price'] = $v['price_1'];
                        $v['cost'] = $this->init_model->price_to_box($v['price_1'], $v['box']) * ($v['total'] / $v['box']);
                    } else {
                        $v['cost'] = $this->init_model->price_to_box($v['price'], $v['box']) * ($v['total'] / $v['box']);
                    }

					$set = array(
						'order_id' => $order_id,
						'product_id' => $v['product_id'],
						'variant_id' => $v['variant_id'],
						'price' => $v['price'],
						'total' => $v['total'],
						'cost' => $v['cost'],
						'title' => $v['title'],
						'articul' => $v['articul'],
					);
					$this->cart_model->insert_cart_product($set);
				}

				$template_data = array(
					'cart' => $cart,
					'mini_cart' => $mini_cart,
					'name' => $name,
					'phone' => $phone,
					'email' => $email,
					'additional' => $additional,
					//'address' => $address,
					'payment' => $payment,
					'delivery' => $delivery,
				);

				$this->load->library('Email_lib');

				// Відправка листа адміну

				$this->email_lib->SetFrom('info@' . str_replace('www.', '', $_SERVER['HTTP_HOST']), str_replace('www.', '', $_SERVER['HTTP_HOST']));
				//$this->email_lib->AddAddress($this->config->item('site_email'));
				$this->email_lib->AddAddress('order@dinmark.com.ua');

				$template_data['subject'] = 'Нове замовлення з сайту ' . $this->config->item('site_name_' . LANG);

				$this->email_lib->Subject = $template_data['subject'];
				$this->email_lib->Body = $this->load->view('admin_letter_tpl', $template_data, TRUE);

				$this->email_lib->Send();
				$this->email_lib->ClearAddresses();

				// Відправка листа користувачу

				$this->email_lib->SetFrom('info@' . str_replace('www.', '', $_SERVER['HTTP_HOST']), $this->config->item('site_name_' . LANG));
				//$this->email_lib->AddAddress($this->input->post('email'));
				$this->email_lib->AddAddress($this->input->post('email'));

				$template_data['subsject'] = 'Замовлення. ' . $this->config->item('site_name_' . LANG);

				$this->email_lib->Subject = $template_data['subsject'];
				$this->email_lib->Body = $this->load->view('user_letter_tpl', $template_data, TRUE);

				$this->email_lib->Send();

				if ((int)$this->input->post('subscribe') === 1) {
					$this->load->model('subscribe/subscribe_model');
					$this->subscribe_model->insert($this->input->post('email'));
				}

				$this->cart_model->delete_cart();

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Формування коду перевірки
		 *
		 * @return mixed
		 */
		public function captcha()
		{
			$this->config->set_item('is_ajax_request', TRUE);

			$this->load->library('captcha_lib');
			$response = $this->captcha_lib->get_image('cart');

			return $response;
		}

		/**
		 * Отримання списка міст області
		 *
		 * @return string
		 */
		public function get_cities()
		{
			$response = array(
				'success' => false,
				'query' => '',
				'suggestions' => array(
					// [value => '', data => '']
				),
			);

			$query = mb_strtolower(mb_substr((string)$this->input->post('query', true), 0, 5, 'UTF-8'), 'UTF-8');

			if (mb_strlen($query, 'UTF-8') >= 2) {
				$this->load->model('cart/cart_model');

				$response['success'] = true;
				$response['query'] = $query;

				$result =  $this->cart_model->get_cities($query);

				foreach ($result as $v) {
					$response['suggestions'][] = array(
						'value' => $v['name'],
						'data' => $v['ref'],
					);
				}
			}

			return json_encode($response);
		}

		/**
		 * Отримання відділень міста
		 *
		 */
		public function get_warehouses()
		{
			$response = array(
				'success' => false,
			);

			$ref = form_prep($this->input->post('ref', true));

			if ($ref !== '') {
				$this->load->model('cart/cart_model');

				$response['success'] = true;
				$response['warehouses'] = $this->cart_model->get_warehouses($ref);
			}

			return json_encode($response);
		}
	}