<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Cart_model extends CI_Model
	{
		protected $cart_id = NULL;

		/**
		 * Отримання даних з кошику
		 *
		 * @return array
		 */
		public function get_cart()
		{
			$cart = array();
			$cart_id = $this->cart_id !== NULL ? $this->cart_id : $this->input->cookie($this->config->item('cookie_prefix') . 'cart_id');

			if (is_string($cart_id) AND mb_strlen($cart_id) == 32)
			{
				$products = $this->db->select('products')->where('cart_id', $cart_id)->get('cart')->row('products');

				if (is_string($products) AND $products != '')
				{
					$cart = unserialize($products);
				}
			}

			return $cart;
		}

		/**
		 * Збереження кошику
		 *
		 * @param $cart
		 */
		public function save_cart($cart)
		{
			$create_cart = TRUE;
			$cart_id = $this->input->cookie($this->config->item('cookie_prefix') . 'cart_id');

			if (is_string($cart_id) AND mb_strlen($cart_id) == 32)
			{
				if ($this->db->where('cart_id', $cart_id)->count_all_results('cart') > 0)
				{
					$create_cart = FALSE;
				}
			}

			if ($create_cart)
			{
				$cart_id = md5(time() . rand(0, 1000) . rand(1000, 2000));
				$this->cart_id = $cart_id;

				$cookie = array(
					'name' => 'cart_id',
					'value' => $cart_id,
					'expire' => 86400 * 14,
					'path' => '/',
					'httponly' => TRUE,
				);
				$this->input->set_cookie($cookie);

				$set = array(
					'cart_id' => $cart_id,
					'update_time' => time(),
				);
				$this->db->insert('cart', $set);
			}

			$set = array(
				'products' => serialize($cart),
				'update_time' => time(),
			);
			$this->db->update('cart', $set, array('cart_id' => $cart_id));
		}

		/**
		 * Видалення корзини
		 */
		public function delete_cart()
		{
			$cart_id = $this->input->cookie($this->config->item('cookie_prefix') . 'cart_id');

			if (is_string($cart_id) AND mb_strlen($cart_id) == 32)
			{
				$cookie = array(
					'name' => 'cart_id',
					'value' => '',
					'expire' => time() - 86400,
					'path' => '/',
					'httponly' => TRUE,
				);
				$this->input->set_cookie($cookie);

				$this->db->delete('cart', array('cart_id' => $cart_id));
			}
		}

		/**
		 * Додавання товару в корзину
		 *
		 * @param int $product_id
		 * @param int $variant_id
		 * @param int $total
		 *
		 * @return array|bool
		 */
		public function cart_put($product_id, $variant_id, $total)
		{
			$cart = $this->get_cart();

			if ($product_id > 0 AND $variant_id > 0 AND $total > 0)
			{
				$this->db->where('catalog_variants.variant_id', $variant_id);
				$this->db->where('catalog_variants.product_id', $product_id);

				$this->db->join('catalog', 'catalog.product_id=catalog_variants.product_id');
				$this->db->where('catalog.hidden', 0);

				$this->db->join('menu', 'catalog.menu_id = menu.id');
				$this->db->where('menu.hidden', 0);

				$this->db->join('components', 'catalog.component_id = components.component_id');
				$this->db->where('components.hidden', 0);

				if ($this->db->count_all_results('catalog_variants') > 0)
				{
					$cart[$product_id][$variant_id] = $total;
					$this->save_cart($cart);
				}
			}

			return $this->get_mini_cart($cart);
		}

		/**
		 * Видалення тогвару з кошика
		 *
		 * @param int $product_id
		 * @param int $variant_id
		 * @return array
		 */
		public function delete_item($product_id, $variant_id)
		{
			$cart = $this->get_cart();

			if (isset($cart[$product_id][$variant_id]))
			{
				if (count($cart[$product_id]) == 1)
				{
					unset($cart[$product_id]);
				}
				else
				{
					unset($cart[$product_id][$variant_id]);
				}

				$this->save_cart($cart);
			}

			return $this->get_mini_cart($cart);
		}

		/**
		 * Отримання даних для міні-кошику
		 *
		 * @param $cart
		 * @return array
		 */
		public function get_mini_cart($cart = NULL)
		{
			if ($cart === NULL)
			{
				$cart = $this->get_cart();
			}

			$update_cart = FALSE;
			$_total = 0;
			$_price = 0;
			$_products = array();

			$prefix = $this->db->dbprefix;

			foreach ($cart as $product_id => $variants)
			{
				foreach ($variants as $variant_id => $total)
				{
					$this->db->select('cv.title_' . LANG . ' as title, cv.box, cv.price, cv.currency');
					$this->db->select('(select `' . $this->db->dbprefix . 'catalog_images`.`image` from `' . $this->db->dbprefix . 'catalog_images` where `' . $this->db->dbprefix . 'catalog_images`.`product_id` = cv.`product_id` order by `' . $this->db->dbprefix . 'catalog_images`.`position` asc limit 1) as image', FALSE);

					$this->db->where('cv.variant_id', $variant_id);

					$this->db->join('`' . $prefix . 'catalog`', '`' . $prefix . 'catalog`.product_id=cv.product_id', '', false);
					$this->db->where('catalog.hidden', 0);

					$this->db->join('menu', 'catalog.menu_id = menu.id');
					$this->db->where('menu.hidden', 0);

					$this->db->join('components', 'catalog.component_id = components.component_id');
					$this->db->where('components.hidden', 0);

					$result = $this->db->get('catalog_variants as cv')->row_array();

					if (!isset($result['title']))
					{
						$update_cart = TRUE;

						if (count($cart[$product_id]) == 1)
						{
							unset($cart[$product_id]);
						}
						else
						{
							unset($cart[$product_id][$variant_id]);
						}

						continue;
					}

                    $total = ($result['box'] > 0) ? ceil($total/$result['box']) : $total;
                    //$_total += $total;
                    $_total++;

                    $price = $this->init_model->get_price($result['price'], $result['currency']);

					$price_1 = $this->init_model->get_price($result['price'], $result['currency'], 3);
					$limit_1 = $this->init_model->get_limit_items($result['box'], 3);
					$price_2 = $this->init_model->get_price($result['price'], $result['currency'], 5);
					$limit_2 = $this->init_model->get_limit_items($result['box'], 5);

					if ($total >= $limit_2 and $limit_2 > 0) {
					    $price = $this->init_model->price_to_box($price_2, $result['box']) * $total;
                    } elseif ($total >= $limit_1 and $limit_1 > 0) {
					    $price = $this->init_model->price_to_box($price_1, $result['box']) * $total;
                    } else {
					    $price = $this->init_model->price_to_box($price, $result['box']) * $total;
                    }

					$_products[] = array(
						'title' => $result['title'],
						'price' => $price,
						'url' => $this->uri->full_url($product_id),
						'image' => $result['image'] != '' ? base_url('upload/catalog/' . $this->init_model->dir_by_id($product_id) . '/' . $product_id . '/t_' . $result['image']) : '',
					);

					//$_total += $total;
					$_price += $price;
				}
			}

			if ($update_cart)
			{
				$this->save_cart($cart);
			}

			return array($_total, $_price, $_products);
		}

		/**
		 * Отримання товарів для кошику
		 *
		 * @return array
		 */
		public function get_cart_products($cart = NULL)
		{
			if ($cart === NULL)
			{
				$cart = $this->get_cart();
			}

			$update_cart = FALSE;
			$products = array();

			$prefix = $this->db->dbprefix;

			foreach ($cart as $product_id => $variants)
			{
				foreach ($variants as $variant_id => $total)
				{
					$this->db->select('catalog_variants.articul, catalog_variants.title_' . LANG . ' as title, catalog_variants.url_' . LANG . ' as url, catalog_variants.box, catalog_variants.weight, catalog_variants.price, catalog_variants.currency');
					$this->db->select('(select `' . $prefix . 'catalog_images`.`image` from `' . $prefix . 'catalog_images` where `' . $prefix . 'catalog_images`.`product_id` = `' . $prefix . 'catalog_variants`.`product_id` order by `' . $prefix . 'catalog_images`.`position` asc limit 1) as image', FALSE);

					$this->db->where('catalog_variants.variant_id', $variant_id);

					$this->db->join('catalog', 'catalog.product_id=catalog_variants.product_id');
					$this->db->where('catalog.hidden', 0);

					$this->db->join('menu', 'catalog.menu_id = menu.id');
					$this->db->where('menu.hidden', 0);

					$this->db->join('components', 'catalog.component_id = components.component_id');
					$this->db->where('components.hidden', 0);

					$result = $this->db->get('catalog_variants')->row_array();

					if (!isset($result['title']))
					{
						$update_cart = TRUE;

						if (count($cart[$product_id]) == 1)
						{
							unset($cart[$product_id]);
						}
						else
						{
							unset($cart[$product_id][$variant_id]);
						}

						continue;
					}

					$products[] = array(
						'product_id' => $product_id,
						'variant_id' => $variant_id,
						'total' => ($total % $result['box'] != 0 and $result['box'] > 0) ? ceil($total/$result['box']) * $result['box'] : $total,
						'title' => $result['title'],
						'articul' => $result['articul'],
						'box' => $result['box'],
						'weight' => $result['weight'],
						'price' => $this->init_model->get_price($result['price'], $result['currency']),
						'price_1' => $this->init_model->get_price($result['price'], $result['currency'], 3),
						'limit_1' => $this->init_model->get_limit_items($result['box'], 3),
						'price_2' => $this->init_model->get_price($result['price'], $result['currency'], 5),
						'limit_2' => $this->init_model->get_limit_items($result['box'], 5),
						'url' => $this->uri->full_url($result['url']),
						'image' => $result['image'] != '' ? base_url('upload/catalog/' . $this->init_model->dir_by_id($product_id) . '/' . $product_id . '/t_' . $result['image']) : '',
					);
				}
			}

			if ($update_cart)
			{
				$this->save_cart($cart);
			}

			return $products;
		}

		/**
		 * Збереження кошику
		 *
		 * @param array $cart
		 * @param string $name
		 * @param string $phone
		 * @param string $email
		 * @param int $payment
		 * @param int $delivery
		 *
		 * @return int
		 */
		public function insert($cart, $name, $phone, $email, $payment, $delivery)
		{
			$name = $this->db->escape_str($name);
			$phone = $this->db->escape_str($phone);
			$email = $this->db->escape_str($email);

			$set = array(
				'user_id' => $this->init_model->is_user() ? $this->session->userdata('user_id') : 0,
				'total' => $cart[0],
				'cost' => $cart[1],
				'name' => $name,
				'email' => $email,
				'phone' => $phone,
				'payment' => $payment,
				'delivery' => $delivery,
				'order_date' => time(),
			);
			$this->db->insert('orders', $set);

			return (int) $this->db->insert_id();
		}

		/**
		 * Додавання товару в замовлення
		 *
		 * @param array $set
		 */
		public function insert_cart_product($set)
		{
			$this->db->insert('orders_products', $set);
		}

		/**
		 * Отримання списку міст
		 *
		 * @param string $query
		 * @return array
		 */
		public function get_cities($query)
		{
			return $this->db
				->select('npc.ref, if(npc.name_' . LANG . '!="",npc.name_' . LANG . ',npc.name_' . DEF_LANG . ') as name', false)
				->like('npc.name_' . LANG, $query, 'after')
				->order_by('npc.name_' . LANG)
				->limit(10)
				->get('np_cities as npc')
				->result_array();
		}

		/**
		 * Отримання міста
		 *
		 * @param string $query
		 * @return array
		 */
		public function get_city($ref)
		{
			return $this->db
				->select('if(npc.name_' . LANG . '!="",npc.name_' . LANG . ',npc.name_' . DEF_LANG . ') as name', false)
				->where('npc.ref', $ref)
				->get('np_cities as npc')
				->row_array();
		}

		/**
		 * Отримання відділень
		 *
		 * @param string $ref
		 * @return array|null
		 */
		public function get_warehouses($ref)
		{
			return $this->db
				->select('ref, name_' . (LANG !== 'ua' ? 'ru' : 'ua') . ' as name, weight')
				->where('city_ref', $ref)
				->order_by('cast(`' . $this->db->dbprefix('np_warehouses') . '`.`name_' . (LANG !== 'ua' ? 'ru' : 'ua') . '` as char character set cp1251)', 'asc', false)
				->get('np_warehouses')
				->result_array();
		}
	}