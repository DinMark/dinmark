<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<section class="fm order_place">
	<div class="centre">
		<div class="fm op_title">

		</div>
		<article>

        </article>
    </div>
</section>

<section class="sect-article fm">
	<div class="container">
		<div class="article-wrapper fm">
			<article>
				<header>
					<?php if (LANG === 'ua'): ?>Замовлення надіслано<?php endif; ?>
					<?php if (LANG === 'ru'): ?>Заказ отправлено<?php endif; ?>
				</header>
				<div class="article_text">
					<?php if (LANG === 'ua'): ?><?php endif; ?>
					<?php if (LANG === 'ru'): ?><?php endif; ?>
				</div>
			</article>
		</div>
	</div>
</section>