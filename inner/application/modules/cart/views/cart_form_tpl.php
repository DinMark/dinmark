<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ini_set('display_errors', true); ?>
<div class="fm cart_inner">
	<a href="#" class="sf_close cart_close_link"></a>
	<?php if (count($products) === 0): ?>
		<p><?php if (LANG == 'ua') echo 'Кошик порожній'; if (LANG == 'ru') echo 'Корзина пуста'; if (LANG == 'en') echo 'Empty cart'; ?></p>
	<?php else: ?>
	<div class="fm cart_title"><?php if (LANG == 'ua') echo 'Кошик'; if (LANG == 'ru') echo 'Корзина'; if (LANG == 'en') echo 'Cart'; ?></div>
	<div class="fm st_tab_place">
		<div class="fm tab_row th">
			<div class="fm tab_cell cc_1"><span>№</span></div>
			<div class="fm tab_cell cc_2"><span><? if(LANG=='ua')echo'Зображення';if(LANG=='ru')echo'Изображение';if(LANG=='en')echo'Image';?></span></div>
			<div class="fm tab_cell cc_3"><span><? if(LANG=='ua')echo'Назва';if(LANG=='ru')echo'Название';if(LANG=='en')echo'Name';?></span></div>
			<div class="fm tab_cell cc_4"><span><? if(LANG=='ua')echo'Кількість,<br>шт./уп.';if(LANG=='ru')echo'Количество,<br>шт./уп.';if(LANG=='en')echo'Number of<br>pcs./pack.';?></span></div>
			<div class="fm tab_cell cc_5"><span><? if(LANG=='ua')echo'Ціна,<br>грн/100шт';if(LANG=='ru')echo'Цена,<br>грн/100шт';if(LANG=='en')echo'Price,<br>100pcs./uah';?></span></div>
			<div class="fm tab_cell cc_6"><span><? if(LANG=='ua')echo'Кількість шт.';if(LANG=='ru')echo'Количество шт.';if(LANG=='en')echo'Quantity pcs.';?></span></div>
			<div class="fm tab_cell cc_7"><span><? if(LANG=='ua')echo'Вага кг.';if(LANG=='ru')echo'Вес кг.';if(LANG=='en')echo'Weight kg.';?></span></div>
			<div class="fm tab_cell cc_8"><span><? if(LANG=='ua')echo'Загальна ціна,<br>грн';if(LANG=='ru')echo'Общая цена, <br> грн';if(LANG=='en')echo'Total price, <br> uah';?></span></div>
			<div class="fm tab_cell cc_9">&nbsp;</div>
		</div>
		<?php $i = 1; foreach ($products as $product): ?>
		<div class="fm tab_row tab_row_product" data-product-id="<?=$product['product_id'];?>" data-variant-id="<?=$product['variant_id'];?>" data-price="<?=$this->init_model->price_to_box($product['price'], $product['box']);?>" data-price-1="<?=$this->init_model->price_to_box($product['price_1'], $product['box']);?>" data-limit-1="<?=$product['limit_1'];?>" data-price-2="<?=$this->init_model->price_to_box($product['price_2'], $product['box']);?>" data-limit-2="<?=$product['limit_2'];?>" data-box="<?=$product['box'];?>" data-weight="<?=$product['weight'];?>">
			<div class="fm tab_cell cc_1"><span><?=$i;?></span></div>
			<div class="fm tab_cell cc_2"><?php if ($product['image'] !== ''): ?><img src="<?=$product['image'];?>" alt="<?=$product['title'];?>"><?php endif; ?></div>
			<div class="fm tab_cell cc_3"><span><a href="<?=$product['url'];?>"><?=$product['title'];?></a></span></div>
			<div class="fm tab_cell cc_4"><span><?=$product['box'];?></span></div>
			<div class="fm tab_cell cc_5">
				<span>
					<i class="price_retail"><?=number_format($product['price'], 2, '.', '');?></i>
					<i class="price_1" style="display: none;"><?=number_format($product['price_1'], 2, '.', '');?></i>
					<i class="price_2" style="display: none;"><?=number_format($product['price_2'], 2, '.', '');?></i>
				</span>
			</div>
			<div class="fm tab_cell cc_6">
				<a href="#" class="ct_minus">-</a>
				<input type="text" class="ct_total" value="<?=$product['total'];?>">
				<a href="#" class="ct_plus">+</a>
			</div>
			<div class="fm tab_cell cc_7"><span><?=number_format($product['weight'], 3, '.', '');?></span></div>
			<div class="fm tab_cell cc_8">
				<span>
					<i class="price_retail row_total_retail"></i>
					<i class="price_1 row_total_1" style="display:none;"></i>
					<i class="price_2 row_total_2" style="display:none;"></i>
				</span>
			</div>
			<div class="fm tab_cell cc_9"><span><a href="#" class="ct_delete"></a></span></div>
		</div>
	<?php $i++; endforeach; ?>
	<div class="fm cart_row total">
		<span class="total_weight"><span><?php if (LANG == 'ua') echo 'Загальна вага:'; if (LANG == 'ru') echo 'Общий вес:'; if (LANG == 'en') echo 'Total weight:'; ?></span> <strong></strong> <? if(LANG=='ua')echo'кг';if(LANG=='ru')echo'кг';if(LANG=='en')echo'kg';?></span>
		<span class="total_cost"><?php if (LANG == 'ua') echo 'Загальна вартість'; if (LANG == 'ru') echo 'Общая стоимость'; if (LANG == 'en') echo 'Total cost'; ?>: <b></b> <? if(LANG=='ua')echo'грн';if(LANG=='ru')echo'грн';if(LANG=='en')echo'uah';?></span>
	</div>
	</div>
	<div class="fm cw_bottom">
		<a href="#" class="fm cart_back cart_close_link"><? if(LANG=='ua')echo'Повернутись до покупок';if(LANG=='ru')echo'Вернуться к покупкам';if(LANG=='en')echo'Back to shopping';?></a>
		<a href="<?=$this->uri->full_url('cart/payment');?>" class="fmr main_order"><?php if (LANG == 'ua') echo 'оформити замовлення'; if (LANG == 'ru') echo 'оформить заказ'; if (LANG == 'en') echo 'place your order'; ?></a>
	</div>
	<?php endif; ?>
</div>