<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_js('autocomplete/jquery.autocomplete.min.js');
?>
<section class="order_page">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="op_title"><? if(LANG=='ua')echo'Оформлення замовлення';if(LANG=='ru')echo'Оформление заказа';if(LANG=='en')echo'Checkout';?></div>
			</div>
			<div class="col-sm-5">
				<!-- Контактні дані -->
				<div id="contacts_step" class="contacts_step">
					<div class="box_title"><? if(LANG=='ua')echo'Контактні дані';if(LANG=='ru')echo'Контактные данные';if(LANG=='en')echo'Contact information';?></div>
					<form action="#" id="contacts_form" class="border_box">
						<a href="<?=$this->uri->full_url('profile/login');?>" class="fm common-btn"><?php if (LANG == 'ua') echo 'Вже зареєстровані?'; if (LANG == 'ru') echo 'Уже зарегестрированы?'; if (LANG == 'en') echo 'Already registered?'; ?></a>
						<div class="fm link_wrapper">
							<a href="<?=$this->uri->full_url('profile/login');?>"><?php if (LANG == 'ua') echo 'Якщо Ви бажаєте стати нашим партнером, бажаєте отримати знижку, яка буде постійно збільшуватись, перейдіть на сторінку “Стати партнером”'; if (LANG == 'ru') echo 'Если Вы хотите стать нашим партнером, желаете получать скидку, которая будет постоянно увеличиваться, перейдите на  страницу “Стать партнером”'; if (LANG == 'en') echo 'If you want to become our partner, wish to receive a discount that will be constantly increasing, go to the "Become a partner" page.'; ?></a>
						</div>
						<p class="field req"><label for="order_name"><span>*</span><?php if(LANG=='ua')echo'Ім’я';if(LANG=='ru')echo'Имя';if(LANG=='en')echo'Name';?></label><input id="order_name" type="text" value="<?php if ($this->init_model->is_user()) echo $this->session->userdata('user_name'); ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
						<p class="field req"><label for="order_phone"><span>*</span><?php if(LANG=='ua')echo'Ваш телефон';if(LANG=='ru')echo'Ваш телефон';if(LANG=='en')echo'Your phone';?></label><input id="order_phone" type="text" value="<?php if ($this->init_model->is_user()) echo $this->session->userdata('user_phone'); ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
						<p class="field req"><label for="order_email"><span>*</span><?php if(LANG=='ua')echo'Ваш';if(LANG=='ru')echo'Ваш';if(LANG=='en')echo'Your';?> e-mail</label><input id="order_email" type="text" value="<?php if ($this->init_model->is_user()) echo $this->session->userdata('user_email'); ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
					</form>
				</div>
				<!-- Спосіб доставки -->
				<div id="delivery_form" class="delivery_form">
					<div class="box_title"><? if(LANG=='ua')echo'Спосіб доставки';if(LANG=='ru')echo'Способ доставки';if(LANG=='en')echo'Shipping Method';?></div>
					<div class="border_box">
						<div class="field">
							<input type="radio" name="delivery_selector" id="delivery_selector_2" value="2"<?php if (!isset($delivery['type']) or $delivery['type'] === 2): ?> checked="checked"<?php endif; ?>>
                            <label for="delivery_selector_2"><? if(LANG=='ua')echo'Нова пошта';if(LANG=='ru')echo'Новая почта';if(LANG=='en')echo'Nova Poshta';?><span class="error_message hidden"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></label>
							<div class="delivery-type<?php if (isset($delivery['type']) and $delivery['type'] !== 2): ?> hidden<?php endif; ?>">
								<div class="field">
									<input type="radio" name="np_selector" id="department" value="1"<?php if (!isset($delivery['np_type']) or $delivery['np_type'] === 1): ?> checked="checked"<?php endif; ?>>
                                    <label for="department" class="custom-label"><? if(LANG=='ua')echo'на відділення';if(LANG=='ru')echo'доставка на отделение';if(LANG=='en')echo'to the office';?></label>
									<div class="fbox<?php if (isset($delivery['np_type']) and $delivery['np_type'] !== 1): ?> hidden<?php endif; ?>">
										<p><?=$helps['new_post_warehouse'];?></p>
										<input type="text" name="np_city" placeholder="Місто" value="<?php if (isset($np_city)): ?><?=$np_city;?><?php endif; ?>">
										<select name="np_warehouse"<?php if (!isset($np_warehouses)): ?> disabled="disabled"<?php endif; ?>>
                                            <option value=""><? if(LANG=='ua')echo'Відділення';if(LANG=='ru')echo'Отделение';if(LANG=='en')echo'Department';?></option>
											<?php if (isset($np_warehouses) and count($np_warehouses) > 0): ?>
												<?php foreach ($np_warehouses as $v): ?>
                                                    <option value="<?=$v['name'];?>"<?php if (isset($delivery['np_warehouse']) and $delivery['np_warehouse'] === $v['ref']): ?> selected="selected"<?php endif; ?>><?=$v['name'];?></option>
												<?php endforeach; ?>
											<?php endif; ?>
                                        </select>
									</div>
								</div>
								<div class="field">
									<input type="radio" name="np_selector" id="courier" value="2"<?php if (isset($delivery['np_type']) and $delivery['np_type'] === 2): ?> checked="checked"<?php endif; ?>>
                                    <label for="courier" class="custom-label"><? if(LANG=='ua')echo'кур`єр';if(LANG=='ru')echo'курьер';if(LANG=='en')echo'courier';?></label>
									<div class="fbox<?php if (!isset($delivery['np_type']) or (isset($delivery['np_type']) and $delivery['np_type'] !== 2)): ?> hidden<?php endif; ?>">
										<p><?=$helps['new_post_courier'];?></p>
										<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 1: </label><input name="delivery_address_1" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 2 and isset($delivery['np_type']) and $delivery['np_type'] === 2 and isset($delivery['address_1'])): ?><?=$delivery['address_1'];?><?php endif; ?>"></p>
										<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 2: </label><input name="delivery_address_2" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 2 and isset($delivery['np_type']) and $delivery['np_type'] === 2 and isset($delivery['address_2'])): ?><?=$delivery['address_2'];?><?php endif; ?>"></p>
										<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Область';if(LANG=='ru')echo'Область';if(LANG=='en')echo'Region';?>: </label><input name="delivery_region" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 2 and isset($delivery['np_type']) and $delivery['np_type'] === 2 and isset($delivery['region'])): ?><?=$delivery['region'];?><?php endif; ?>"></p>
										<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Місто';if(LANG=='ru')echo'Город';if(LANG=='en')echo'City';?>: </label><input name="delivery_city" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 2 and isset($delivery['np_type']) and $delivery['np_type'] === 2 and isset($delivery['city'])): ?><?=$delivery['city'];?><?php endif; ?>"></p>
									</div>
								</div>
							</div>
						</div>
						<div class="field"><input type="radio" name="delivery_selector" id="delivery_selector_3" value="3"<?php if (isset($delivery['type']) and $delivery['type'] === 3): ?> checked="checked"<?php endif; ?>>
                            <label for="delivery_selector_3">ІнТайм</label>
							<div class="fbox<?php if (!isset($delivery['type']) or $delivery['type'] !== 3): ?> hidden<?php endif; ?>">
								<p><?=$helps['intime'];?></p>
								<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 1: </label><input name="delivery_address_1" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 3 and isset($delivery['address_1'])): ?><?=$delivery['address_1'];?><?php endif; ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
								<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 2: </label><input name="delivery_address_2" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 3 and isset($delivery['address_2'])): ?><?=$delivery['address_2'];?><?php endif; ?>"></p>
								<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Область';if(LANG=='ru')echo'Область';if(LANG=='en')echo'Region';?>: </label><input name="delivery_region" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 3 and isset($delivery['region'])): ?><?=$delivery['region'];?><?php endif; ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
								<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Місто';if(LANG=='ru')echo'Город';if(LANG=='en')echo'City';?>: </label><input name="delivery_city" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 3 and isset($delivery['city'])): ?><?=$delivery['city'];?><?php endif; ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
							</div>
						</div>
						<div class="field">
							<input type="radio" name="delivery_selector" id="delivery_selector_4" value="4"<?php if (isset($delivery['type']) and $delivery['type'] === 4): ?> checked="checked"<?php endif; ?>>
                            <label for="delivery_selector_4">Деливери</label>
							<div class="fbox<?php if (!isset($delivery['type']) or $delivery['type'] !== 4): ?> hidden<?php endif; ?>">
								<p><?=$helps['delivery'];?></p>
								<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 1: </label><input name="delivery_address_1" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 4 and isset($delivery['address_1'])): ?><?=$delivery['address_1'];?><?php endif; ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
								<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 2: </label><input name="delivery_address_2" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 4 and isset($delivery['address_2'])): ?><?=$delivery['address_2'];?><?php endif; ?>"></p>
								<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Область';if(LANG=='ru')echo'Область';if(LANG=='en')echo'Region';?>: </label><input name="delivery_region" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 4 and isset($delivery['region'])): ?><?=$delivery['region'];?><?php endif; ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
								<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Місто';if(LANG=='ru')echo'Город';if(LANG=='en')echo'City';?>: </label><input name="delivery_city" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 4 and isset($delivery['city'])): ?><?=$delivery['city'];?><?php endif; ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
							</div>
						</div>
						<div class="field">
							<input type="radio" name="delivery_selector" id="delivery_selector_5" value="5"<?php if (isset($delivery['type']) and $delivery['type'] === 5): ?> checked="checked"<?php endif; ?>>
                            <label for="delivery_selector_5">Транспортна компанія</label>
							<div class="fbox<?php if (!isset($delivery['type']) or $delivery['type'] !== 5): ?> hidden<?php endif; ?>">
								<p><?=$helps['transport_company'];?></p>
								<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 1: </label><input name="delivery_address_1" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 5 and isset($delivery['address_1'])): ?><?=$delivery['address_1'];?><?php endif; ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
								<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 2: </label><input name="delivery_address_2" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 5 and isset($delivery['address_2'])): ?><?=$delivery['address_2'];?><?php endif; ?>"></p>
								<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Область';if(LANG=='ru')echo'Область';if(LANG=='en')echo'Region';?>: </label><input name="delivery_region" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 5 and isset($delivery['region'])): ?><?=$delivery['region'];?><?php endif; ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
								<p class="field req"><label><span>*</span><? if(LANG=='ua')echo'Місто';if(LANG=='ru')echo'Город';if(LANG=='en')echo'City';?>: </label><input name="delivery_city" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 5 and isset($delivery['city'])): ?><?=$delivery['city'];?><?php endif; ?>"><span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span></p>
							</div>
						</div>
						<div class="field">
							<input type="radio" name="delivery_selector" id="delivery_selector_1" value="1"<?php if (isset($delivery['type']) and $delivery['type'] === 1): ?> checked="checked"<?php endif; ?>>
                            <label for="delivery_selector_1"><? if(LANG=='ua')echo'Самовивіз';if(LANG=='ru')echo'Самовывоз';if(LANG=='en')echo'Pickup';?></label>
							<div class="fbox<?php if (!isset($delivery['type']) or $delivery['type'] !== 1): ?> hidden<?php endif; ?>">
								<p><?=$helps['self_delivery'];?></p>
							</div>
						</div>
					</div>
				</div>
				<!-- Спосіб оплати доставки -->
				<div id="payment_delivery_form">
					<div class="box_title"><?php if (LANG == 'ua') echo 'Оплата доставки'; if (LANG == 'ru') echo 'Оплата доставки'; if (LANG == 'en') echo 'Payment delivery'; ?></div>
					<div class="border_box">
						<div class="field">
							<input type="radio" name="payment_delivery_selector" id="payment_delivery_selector_1" value="1" checked="checked"> <label
									for="payment_delivery_selector_1"><?php if (LANG == 'ua') echo 'Готівкою'; if (LANG == 'ru') echo 'Наличными
'; if (LANG=='en') echo'In cash'; ?></label>
						</div>
						<div class="field">
							<input type="radio" name="payment_delivery_selector" id="payment_delivery_selector_2" value="2"> <label
									for="payment_delivery_selector_2"><?php if (LANG == 'ua') echo 'Безготівковий розрахунок'; if (LANG == 'ru') echo 'Безналичный расчет'; if (LANG=='en') echo'Cashless payments'; ?></label>
						</div>
					</div>
				</div>
				<!-- Спосіб оплати -->
				<div id="payment_form">
					<div class="box_title"><? if(LANG=='ua')echo'Оплата';if(LANG=='ru')echo'Оплата';if(LANG=='en')echo'Payment';?></div>
					<div class="border_box">
						<div class="field">
							<input type="radio" name="payment_selector" id="payment_selector_1" value="1" checked="checked"> <label
									for="payment_selector_1"><? if(LANG=='ua')echo'Оплата при отриманні';if(LANG=='ru')echo'Оплата при получении';if(LANG=='en')echo'Payment on receipt';?></label>
							<div class="fbox"><p><?=$helps['receipt_payment'];?></p></div>
						</div>
						<div class="field">
							<input type="radio" name="payment_selector" id="payment_selector_2" value="2"> <label for="payment_selector_2"><? if(LANG=='ua')echo'Банківський переказ';if(LANG=='ru')echo'Банковский перевод';if(LANG=='en')echo'Bank Transfer';?></label>
							<div class="fbox hidden"><p><?=$helps['bank'];?></p></div>
						</div>
						<div class="field">
							<input type="radio" name="payment_selector" id="payment_selector_3" value="3"> <label for="payment_selector_3"><? if(LANG=='ua')echo'Безготівковий переказ для ЮО';if(LANG=='ru')echo'Безналичный перевод для ЮЛ';if(LANG=='en')echo'Non-cash transfer for LE';?></label>
							<div class="fbox hidden"><p><?=$helps['non_cash'];?></p></div>
						</div>
						<div class="field">
							<input type="radio" name="payment_selector" id="payment_selector_4" value="4"> <label for="payment_selector_4"><? if(LANG=='ua')echo'Приват';if(LANG=='ru')echo'Приват';if(LANG=='en')echo'Privat';?> 24</label>
							<div class="fbox hidden"><p><?=$helps['privat_24'];?></p></div>
						</div>
					</div>
				</div>
                <div class="order_btn_wrapper">
                    <div class="field">
                        <span class="req">*</span> <? if(LANG=='ua')echo'Сума доставки розраховується окремо і не входить в дану суму. Деталі узгодить менеджер';if(LANG=='ru')echo'Сумма доставки рассчитывается отдельно и не входит в данную сумму. Детали согласует менеджер';if(LANG=='en')echo'The amount of delivery is calculated separately and is not included in this amount. The details will be agreed by the manager';?>
                    </div>
                </div>
			</div>
			<div class="col-sm-7">
				<div class="box_title"><? if(LANG=='ua')echo'Загальна інформація';if(LANG=='ru')echo'Общая информация';if(LANG=='en')echo'General information';?></div>
				<div class="order_item">
					<div class="row_top">
						<?php $i = 1; foreach ($products as $product): ?>
						<figure data-product-id="<?=$product['product_id'];?>" data-variant-id="<?=$product['variant_id'];?>" data-price="<?=$this->init_model->price_to_box($product['price'], $product['box']);?>" data-price-1="<?=$this->init_model->price_to_box($product['price_1'], $product['box']);?>" data-limit-1="<?=$product['limit_1'];?>" data-price-2="<?=$this->init_model->price_to_box($product['price_2'], $product['box']);?>" data-limit-2="<?=$product['limit_2'];?>" data-box="<?=$product['box'];?>" data-weight="<?=$product['weight'];?>">
							<span class="num"><?=$i;?></span>
							<?php if ($product['image'] !== ''): ?><img src="<?=$product['image'];?>" alt=""><?php endif; ?>
							<figcaption>
								<div class="title"><a href="<?=$product['url'];?>"><?=$product['title'];?></a></div>
								<div class="info-table">
									<div class="col col-1">
										<div class="info-table_title"><? if(LANG=='ua')echo'Кількість, шт./уп.';if(LANG=='ru')echo'Количество, шт./уп.';if(LANG=='en')echo'Quantity, pcs./pack.';?></div>
										<div class="info-table_value"><?=$product['box'];?></div>
									</div>
									<div class="col col-2">
										<div class="info-table_title"><? if(LANG=='ua')echo'Кількість шт.';if(LANG=='ru')echo'Количество шт.';if(LANG=='en')echo'Quantity pcs.';?></div>
										<div class="counter">
											<a href="#" class="ct_minus">-</a>
											<input type="text" class="ct_total" value="<?=$product['total'];?>">
											<a href="#" class="ct_plus">+</a>
										</div>
									</div>
									<div class="col col-3">
										<div class="info-table_title"><? if(LANG=='ua')echo'Ціна, грн/100шт';if(LANG=='ru')echo'Цена, грн/100шт';if(LANG=='en')echo'Price, uah/100 pcs';?></div>
										<div class="info-table_value">
											<span class="price_retail"><?=number_format($product['price'], 2, '.', '');?></span>
											<span class="price_1" style="display: none;"><?=number_format($product['price_1'], 2, '.', '');?></span>
											<span class="price_2" style="display: none;"><?=number_format($product['price_2'], 2, '.', '');?></span>
										</div>
									</div>
									<div class="col col-4">
										<div class="info-table_title"><? if(LANG=='ua')echo'Вага кг.';if(LANG=='ru')echo'Вес кг.';if(LANG=='en')echo'Weight kg';?></div>
										<div class="info-table_value"><?=number_format($product['weight'], 3, '.', '');?></div>
									</div>
									<div class="col col-5">
										<div class="info-table_title"><? if(LANG=='ua')echo'Загальна ціна, грн';if(LANG=='ru')echo'Общая цена, грн';if(LANG=='en')echo'Total price, uah';?></div>
										<div class="info-table_value">
											<span class="total_retail"></span>
											<span class="total_1" style="display: none"></span>
											<span class="total_2" style="display: none"></span>
										</div>
									</div>
								</div>
							</figcaption>
							<a href="#" class="ct_delete"></a>
						</figure>
						<?php $i++; endforeach; ?>
					</div>
					<div class="row_bottom">
						<span class="total order_cost"><? if(LANG=='ua')echo'Сума замовлення';if(LANG=='ru')echo'Cумма заказа';if(LANG=='en')echo'Order amount';?>: <b></b> <? if(LANG=='ua')echo'грн';if(LANG=='ru')echo'грн';if(LANG=='en')echo'uah';?></span>
						<span class="total bank_cost hidden"><? if(LANG=='ua')echo'Комісія банку (0,5%)';if(LANG=='ru')echo'Комиссия банка (0,5%)';if(LANG=='en')echo'Bank Fee (0.5%)';?>: <b></b> <? if(LANG=='ua')echo'грн';if(LANG=='ru')echo'грн';if(LANG=='en')echo'uah';?></span>
						<span class="total pdv_cost hidden"><? if(LANG=='ua')echo'ПДВ (20%)';if(LANG=='ru')echo'НДС (20%)';if(LANG=='en')echo'VAT (20%)';?>: <b></b> <? if(LANG=='ua')echo'грн';if(LANG=='ru')echo'грн';if(LANG=='en')echo'uah';?></span>
						<span class="total total_cost"><? if(LANG=='ua')echo'Загальна вартість';if(LANG=='ru')echo'Общая стоимость';if(LANG=='en')echo'Total cost';?>: <b></b> <? if(LANG=='ua')echo'грн';if(LANG=='ru')echo'грн';if(LANG=='en')echo'uah';?></span>
						<span class="total total_weight"><span class="req">*</span><span><?php if (LANG == 'ua') echo 'Загальна вага:'; if (LANG == 'ru') echo 'Общий вес:'; if (LANG == 'en') echo 'Total weight:'; ?></span> <strong></strong> <? if(LANG=='ua')echo'кг';if(LANG=='ru')echo'кг';if(LANG=='en')echo'kg';?></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-5"></div>
            <div class="col-md-7">
                <div class="rules_wrapper">
                    <div class="field">
                        <input type="checkbox" id="order_subscribe" name="order_subscribe" value="1">
                        <label for="order_subscribe">
                            <? if(LANG=='ua')echo'Я хочу підписатися на розсилку новин від Dinmark';if(LANG=='ru')echo'Я хочу подписаться на рассылку новостей от Dinmark';if(LANG=='en')echo'I want to subscribe to the newsletter from Dinmark';?>
                        </label>
                    </div>
                    <div class="field">
                        <input type="checkbox" id="order_rules" name="order_rules" value="1">
                        <label for="order_rules">
                            <? if(LANG=='ua')echo'Я прочитав і погоджуюся з <a href="' . $this->init_model->get_link(1440, '{URL}') . '" target="_blank"><strong>Правила та умови</strong></a>';if(LANG=='ru')echo'Я прочитал и согласен с <a href="' . $this->init_model->get_link(1440, '{URL}') . '" target="_blank"><strong>Правила и условия</strong></a>';if(LANG=='en')echo'I have read and agree to the <a href="' . $this->init_model->get_link(1440, '{URL}') . '" target="_blank"><strong>Terms and Conditions</strong></a>';?><span class="req">*</span>
                        </label>
                        <span class="error_message"><?php if(LANG=='ua')echo'Заповніть поле';if(LANG=='ru')echo'Заполните поле';if(LANG=='en')echo'Fill the field';?></span>
                    </div>
                    <a href="#" class="fmr common-btn common-btn--invert order_btn"><? if(LANG=='ua')echo'Оформити замовлення';if(LANG=='ru')echo'Оформить заказ';if(LANG=='en')echo'Send';?></a>
                </div>
            </div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$(function () {
		var $cart = $('.order_item');

		$cart
			.on('change_cart', function () {
				var total_price = 0,
					total_weight = 0;

				$(this).find('figure').map(function () {
					var price = $(this).data('price'),
						price_1 = $(this).data('price-1'),
						limit_1 = $(this).data('limit-1'),
						price_2 = $(this).data('price-2'),
						limit_2 = $(this).data('limit-2'),
						box = parseInt($(this).data('box')),
						total = parseInt($(this).find('.ct_total').val());

					total = Math.ceil(total/box);
					total_weight += total * parseFloat($(this).data('weight'));

					var row_price = total * price;
					$(this).find('.total_retail').text(row_price.toFixed(2));

					var row_price_1 = total * price_1;
					$(this).find('.total_1').text(row_price_1.toFixed(2));

					var row_price_2 = total * price_2;
					$(this).find('.total_2').text(row_price_2.toFixed(2));

					if (total * box >= limit_2 && limit_2 > 0) {
						$(this).find('.price_retail').add($(this).find('.price_1')).hide();
						$(this).find('.price_2').show();
						total_price += row_price_2;
					} else if (total * box >= limit_1 && limit_1 > 0) {
						$(this).find('.price_retail').add($(this).find('.price_2')).hide();
						$(this).find('.price_1').show();
						total_price += row_price_1;
					} else {
						$(this).find('.price_1').add($(this).find('.price_2')).hide();
						$(this).find('.price_retail').show();
						total_price += row_price;
					}
				});

				$(this).find('.order_cost').find('b').text(total_price.toFixed(2));

				var payment = parseInt($('[name="payment_selector"]:checked').val()),
					payment_cost = 0;

				if (payment === 2) {
					payment_cost = 0.5 * total_price / 100;
					total_price += payment_cost;
					$(this).find('.bank_cost').removeClass('hidden').find('b').text(payment_cost.toFixed(2));
				} else {
					$(this).find('.bank_cost').addClass('hidden');
				}

				if (payment === 3) {
					payment_cost = 20 * total_price / 100;
					total_price += payment_cost;
					$(this).find('.pdv_cost').removeClass('hidden').find('b').text(payment_cost.toFixed(2));
				} else {
					$(this).find('.pdv_cost').addClass('hidden');
				}

				$(this).find('.total_cost').find('b').text(total_price.toFixed(2));
				$(this).find('.total_weight').find('strong').text(total_weight.toFixed(3));
			})
			.on('click', '.ct_plus', function (e) {
				e.preventDefault();

				var product_id = $(this).closest('figure').data('product-id'),
					variant_id = $(this).closest('figure').data('variant-id'),
					box = parseInt($(this).closest('figure').data('box')),
					total = parseInt($(this).closest('figure').find('.ct_total').val());

				if (total !== box && box > 0) {
					total = Math.ceil(total/box) * box;
				}

				total += box;

				$(this).closest('figure').find('.ct_total').val(total);
				$cart.trigger('change_cart');

				$.post(
					full_url('cart/cart_put'),
					{
						product_id: product_id,
						variant_id: variant_id,
						total: total
					},
					function (response) {
						mini_cart(response);
					},
					'json'
				);
			})
			.on('click', '.ct_minus', function (e) {
				e.preventDefault();

				var product_id = $(this).closest('figure').data('product-id'),
					variant_id = $(this).closest('figure').data('variant-id'),
					box = $(this).closest('figure').data('box'),
					total = parseInt($(this).closest('figure').find('.ct_total').val()) - 1;

				if (total !== box && box > 0) {
					total = Math.ceil(total/box) * box;
				}

				total -= box;

				if (total <= 0) {
					total = box;
				}

				$(this).closest('figure').find('.ct_total').val(total);
				$cart.trigger('change_cart');

				if (total >= box) {
					$.post(
						full_url('cart/cart_put'),
						{
							product_id: product_id,
							variant_id: variant_id,
							total: total
						},
						function (response) {
							mini_cart(response);
						},
						'json'
					);
				}
			})
			.on('blur', '.ct_total', function (e) {
				e.preventDefault();

				var product_id = $(this).closest('figure').data('product-id'),
					variant_id = $(this).closest('figure').data('variant-id'),
					box = $(this).closest('figure').data('box'),
					total = parseInt($(this).val());

				if ($.isNumeric(total)) {
					if (total !== box && box > 0) {
						total = Math.ceil(total/box) * box;
						$(this).val(total);
					}
				} else {
					total = 0;
				}

				$(this).closest('figure').find('.ct_total').val(total);
				$cart.trigger('change_cart');

				if (total > 0 && total >= box) {
					$.post(
						full_url('cart/cart_put'),
						{
							product_id: product_id,
							variant_id: variant_id,
							total: total
						},
						function (response) {
							mini_cart(response);
						},
						'json'
					);
				}
			})
			.on('click', '.ct_delete', function (e) {
				e.preventDefault();

				var product_id = $(this).closest('figure').data('product-id'),
					variant_id = $(this).closest('figure').data('variant-id');

				$(this).closest('figure').remove();

				$.post(
					full_url('cart/delete_item'),
					{
						product_id: product_id,
						variant_id: variant_id
					},
					function (response) {

						if (response[0] == 0) {
							window.location.href = full_url('');
						}

						mini_cart(response);
					},
					'json'
				);

				$cart.trigger('change_cart');
			})
			.trigger('change_cart');

		var $delivery = $('#delivery_form');

		$delivery
			.on('change', '[name="delivery_selector"]', function () {
				$delivery
					.find('.field').find('div').not('.field').addClass('hidden')
					.end().end().end()
					.find('textarea').val('')
					.end()
					.find('[type="text"]').val('').removeClass('error');

				$delivery.find('[name="np_selector"]').prop('checked', false);
				$(this).closest('.field').find('div').eq(0).removeClass('hidden');

				if ($(this).val() == 1) {
					$('#payment_delivery_form').addClass('hidden');
				} else {
					$('#payment_delivery_form').removeClass('hidden');
				}

				$delivery.find('.error_message').addClass('hidden');
			});

		$delivery
			.on('change', '[name="np_selector"]', function () {
				$(this).parents('div').eq(1)
					.find('div').not('.field').addClass('hidden');

				$delivery
					.find('textarea').val('')
					.end()
					.find('[type="text"]').val('')
					.end()
					.find('select').val('');

				$(this).closest('.field').find('div').eq(0).removeClass('hidden');

                $delivery.find('.error_message').addClass('hidden');
			});

		$delivery.find('[name="np_city"]')
			.on('keydown', function () {
				$delivery.find('[name="np_warehouse"]').val('').prop('disabled', true);
				$delivery.find('[name="delivery"]').val('');
			})
			.autocomplete({
				minChars: 2,
				ajaxSettings: {
					method: 'POST'
				},
				serviceUrl: full_url('cart/get_cities'),
				onSelect: function (suggestion) {
					$.post(
						full_url('cart/get_warehouses'),
						{
							ref: suggestion.data
						},
						function (response) {
							if (response.success) {
								var options = '<option value="">' + $delivery.find('[name="np_warehouse"]').find('option').eq(0).text() + '</option>';

								$.each(response.warehouses, function (i, v) {
									options += '<option value="' + v.name + '">' + v.name + '</option>';
								});

								$delivery.find('[name="np_warehouse"]').html(options).prop('disabled', false);
                                $delivery.find('.error_message').addClass('hidden');
							}
						},
						'json'
					);
				}
			});

		var $payment = $('#payment_form');

		$payment
			.on('change', '[name="payment_selector"]', function () {
				$payment.find('.fbox').addClass('hidden');
				$(this).closest('.field').find('.fbox').removeClass('hidden');
				$cart.trigger('change_cart');
			});

        $('#order_rules').on('change', function () {
            $('#order_rules').closest('.field').find('label').removeAttr('style');
        });

		$('.order_btn').on('click', function (e) {
			e.preventDefault();

			var $order_name = $('#order_name'),
				$order_phone = $('#order_phone'),
				$order_email = $('#order_email'),
				$delivery_selector = $('[name="delivery_selector"]:checked'),
				$np_selector = $('[name="np_selector"]:checked'),
				request = {
					name: $.trim($order_name.removeClass('error').val()),
					phone: $.trim($order_phone.removeClass('error').val()),
					email: $.trim($order_email.removeClass('error').val()),
					payment: $('[name="payment_selector"]:checked').val(),
					payment_delivery: $('[name="payment_delivery_selector"]:checked').val(),
					delivery: '',
                    subscribe: $('#order_subscribe').prop('checked') ? 1 : 0
				},
				order_error = false;

			if (!$('#order_rules').prop('checked')) {
				$('#order_rules').closest('.field').find('label').addClass('error');
				$('#order_rules').closest('.field').find('.error_message').removeClass('hidden');
			    order_error = true;
			}

			if (!ruleRegex.test(request.name)) {
				$order_name.addClass('error').closest('.field').find('.error_message').removeClass('hidden');
				order_error = true;
			}
			if (!ruleRegex.test(request.phone)) {
				$order_phone.addClass('error').closest('.field').find('.error_message').removeClass('hidden');
				order_error = true;
			}
			if (!emailRegex.test(request.email)) {
				$order_email.addClass('error').closest('.field').find('.error_message').removeClass('hidden');
				order_error = true;
			}

			var delivery_type = $delivery_selector.val(),
				np_type = 0,
				region = '',
				city = '',
				warehouse = '',
				address_1 = '',
				address_2 = '';

			if (delivery_type == 1) {
				request.delivery += 'Самовивіз';
				request.payment_delivery = 0;
			}

			if (delivery_type == 2) {
				request.delivery += 'Нова Пошта, ';

				np_type = $('[name="np_selector"]:checked').val();

				if (np_type == 1) {
					request.delivery += 'на відділення, ';

					city = $('[name="np_selector"]:checked').closest('div').find('[name="np_city"]').val();
					warehouse = $('[name="np_selector"]:checked').closest('div').find('[name="np_warehouse"]').val();

					if (city === '') {
						$('[name="np_selector"]:checked').closest('div').find('[name="np_city"]').addClass('error');
						$('[for="delivery_selector_2"]').find('.error_message').removeClass('hidden');
						order_error = true;
					} else {
						request.delivery += city + ', ';
					}

					if (warehouse === '') {
						$('[name="np_selector"]:checked').closest('div').find('[name="np_warehouse"]').addClass('error');
						$('[for="delivery_selector_2"]').find('.error_message').removeClass('hidden');
						order_error = true;
					} else {
						request.delivery += warehouse;
					}
				}

				if (np_type == 2) {
					request.delivery += 'кур”єр, ';
				}
			}

			if (delivery_type == 3) {
				request.delivery += 'ІнТайм, ';
			}

			if (delivery_type == 4) {
				request.delivery += 'Деливери, ';
			}

			if (delivery_type == 5) {
				request.delivery += 'Транспортна компанія, ';
			}

			if ((delivery_type == 2 && np_type == 2) || delivery_type == 3 || delivery_type == 4 || delivery_type == 5) {
				region = $('[name="delivery_selector"]:checked').closest('div').find('[name="delivery_region"]').val();
				city = $('[name="delivery_selector"]:checked').closest('div').find('[name="delivery_city"]').val();
				address_1 = $('[name="delivery_selector"]:checked').closest('div').find('[name="delivery_address_1"]').val();
				address_2 = $('[name="delivery_selector"]:checked').closest('div').find('[name="delivery_address_2"]').val();

				if (region === '') {
					$('[name="delivery_selector"]:checked').closest('div').find('[name="delivery_region"]').addClass('error');
					$('[for="delivery_selector_' + delivery_type + '"]').find('.error_message').removeClass('hidden');
					order_error = true;
				} else {
					request.delivery += region + ', ';
				}

				if (city === '') {
					$('[name="delivery_selector"]:checked').closest('div').find('[name="delivery_city"]').addClass('error');
					$('[for="delivery_selector_' + delivery_type + '"]').find('.error_message').removeClass('hidden');
					order_error = true;
				} else {
					request.delivery += city + ', ';
				}

				if (address_1 === '') {
					$('[name="delivery_selector"]:checked').closest('div').find('[name="delivery_address_1"]').addClass('error');
					$('[for="delivery_selector_' + delivery_type + '"]').find('.error_message').removeClass('hidden');
					order_error = true;
				} else {
					request.delivery += address_1;
				}

				if (address_2 !== '') {
					request.delivery += ' | ' + address_2;
				}
			}

			if (request.payment_delivery == 1) {
				request.delivery += '. Оплата доставки: готівкою';
			}

			if (request.payment_delivery == 2) {
				request.delivery += '. Оплата доставки: безготівковий розрахунок';
			}

			if (!order_error) {
				var msg_1 = '';
				if (LANG == 'ua') msg_1 = 'Відправка замовлення';
				if (LANG == 'ru') msg_1 = 'Отправка заказа';
				if (LANG == 'en') msg_1 = 'Sending orders';
				$(this).text(msg_1);

				$.post(
					full_url('cart/send'),
					request,
					function (response) {
						if (response.success) {
							$(document).scrollTop(0);
							var msg_2 = '';
							if (LANG == 'ua') msg_2 = 'Замовлення надіслано';
							if (LANG == 'ru') msg_2 = 'Заказ отправлено';
							if (LANG == 'en') msg_2 = 'Orders sent';
							$('.order_page').find('.container').eq(0).html('<div class="container"><div class="fm order_sent">' + msg_2 + '</div></div>');
						}
					},
					'json'
				);
			}
		});
	});
</script>