<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title><?=$subject;?></title>
	<style type="text/css">
		body, table tr td, table tr th {font: 12px/18px Verdana, Arial, Tahoma, sans-serif;}
		table tr td, table tr th {padding: 5px 0 5px 0;empty-cells: show;}
	</style>
</head>
<body>
<table align="center" width="900" cellpadding="10" cellspacing="0" border="0" style="border-collapse: collapse;">
	<tr>
		<td align="left" colspan="2" style="padding: 30px 0 30px 0; border-top: 5px solid #f4f4f4;">
			Ваш e-mail був використаний при замовленні на сайті <a href="<?php echo base_url(); ?>" style="color: #693319 !important; text-decoration: underline;"><span style="color:#693319;"><?php echo $this->config->item('site_name_' . LANG); ?></span></a>.
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			<span style="font-size: 14px">При замовленні було вказано наступні дані:</span>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2" style="padding-bottom: 30px">
			<table align="left" width="100%" cellpadding="5" cellspacing="5" border="0" style="border-collapse: collapse;">
				<tr>
					<td align="left" width="170">Ім’я:</td>
					<td align="left"><?php echo $name; ?></td>
				</tr>
				<tr>
					<td align="left" width="170" style="border-top: 1px solid #f4f4f4;">Телефон:</td>
					<td align="left" style="border-top: 1px solid #f4f4f4;"><?php echo $phone; ?></td>
				</tr>
				<tr>
					<td align="left" width="170" style="border-top: 1px solid #f4f4f4;">E-mail:</td>
					<td align="left" style="border-top: 1px solid #f4f4f4;"><?php echo $email; ?></td>
				</tr>
				<?php if ($additional != ''): ?>
					<tr>
						<td align="left" width="170" style="border-top: 1px solid #f4f4f4;">Додатково:</td>
						<td align="left" style="border-top: 1px solid #f4f4f4;"><?php echo $additional; ?></td>
					</tr>
				<?php endif; ?>
				<tr>
					<td align="left" width="170" style="border-top: 1px solid #f4f4f4;">Доставка:</td>
					<td align="left" style="border-top: 1px solid #f4f4f4;"><?=$delivery;?></td>
				</tr>
				<tr>
					<td align="left" width="170" style="border-top: 1px solid #f4f4f4;">Оплата:</td>
					<td align="left" style="border-top: 1px solid #f4f4f4;">
						<?php if ($payment == 1): ?>оплати при отриманні<?php endif; ?>
						<?php if ($payment == 2): ?>банкіський переказ<?php endif; ?>
						<?php if ($payment == 3): ?>безготівковий переказ для ЮО<?php endif; ?>
						<?php if ($payment == 4): ?>Приват 24<?php endif; ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			<span style="font-size: 14px">Склад замовлення:</span>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2" style="padding-bottom: 30px">
			<table align="left" width="100%" cellpadding="10" cellspacing="0" border="0" style="border-collapse: collapse;">
				<tr>
					<th align="left" style="border-bottom: 2px solid #f4f4f4;"><strong>Назва товару</strong></th>
					<th align="left" style="border-bottom: 2px solid #f4f4f4;"><strong>Ціна/100 шт.</strong></th>
					<th align="left" style="border-bottom: 2px solid #f4f4f4;"><strong>Кількість</strong></th>
					<th align="left" style="border-bottom: 2px solid #f4f4f4;"><strong>Вартість</strong></th>
				</tr>
				<?php foreach ($cart as $row): ?>
					<tr>
						<td align="left" valign="top" style="border-top: 1px solid #f4f4f4;">
							<a href="<?=$row['url'];?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319;"><?=$row['title'];?></span></a>
							<?php if ($row['articul'] != ''): ?><br>Артикул: <?=$row['articul'];?><?php endif; ?>
						</td>
						<td align="left" valign="top" style="border-top: 1px solid #f4f4f4;">
							<?=$row['price'];?> грн
						</td>
						<td align="left" valign="top" style="border-top: 1px solid #f4f4f4;"><?=$row['total'];?> шт.</td>
						<td align="left" valign="top" style="border-top: 1px solid #f4f4f4;"><?=$row['cost'];?> грн</td>
					</tr>
				<?php endforeach; ?>
                <?php if ($payment == 2): ?>
                    <tr>
                        <td align="left" style="border-top: 2px solid #f4f4f4;"><strong>Комісія банку (0,5%):</strong></td>
                        <td align="left" style="border-top: 2px solid #f4f4f4;"></td>
                        <td align="left" style="border-top: 2px solid #f4f4f4;"><strong></strong></td>
                        <td align="left" style="border-top: 2px solid #f4f4f4;">
                            <strong>
                                <?php
                                    $bank_fee = round(0.5 * $mini_cart[1] / 100, 2);
	                                $mini_cart[1] += $bank_fee;
                                    echo $bank_fee;
                                ?> грн
                            </strong>
                        </td>
                    </tr>
                <?php endif; ?>
				<?php if ($payment == 3): ?>
                    <tr>
                        <td align="left" style="border-top: 2px solid #f4f4f4;"><strong>ПДВ (20%):</strong></td>
                        <td align="left" style="border-top: 2px solid #f4f4f4;"></td>
                        <td align="left" style="border-top: 2px solid #f4f4f4;"><strong></strong></td>
                        <td align="left" style="border-top: 2px solid #f4f4f4;">
                            <strong>
								<?php
									$pdv = round(20 * $mini_cart[1] / 100, 2);
									$mini_cart[1] += $pdv;
									echo $pdv;
								?> грн
                            </strong>
                        </td>
                    </tr>
				<?php endif; ?>
				<tr>
					<td align="left" style="border-top: 2px solid #f4f4f4;"><strong>Всього:</strong></td>
					<td align="left" style="border-top: 2px solid #f4f4f4;"></td>
					<td align="left" style="border-top: 2px solid #f4f4f4;"><strong><?=$mini_cart[0];?> шт.</strong></td>
					<td align="left" style="border-top: 2px solid #f4f4f4;"><strong><?=$mini_cart[1];?> грн</strong></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			З повагою, <a href="<?= $this->uri->full_url(); ?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319;"><?= $this->config->item('site_name_' . LANG); ?></span></a>!
		</td>
	</tr>
</table>
</body>
</html>