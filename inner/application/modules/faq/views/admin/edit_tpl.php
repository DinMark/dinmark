<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	$this->template_lib->set_js('admin/ckeditor/ckeditor.js');
	$this->template_lib->set_js('admin/jquery.form.js');
?>
<div class="admin_component" id="news_component">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="faq"></div>
		</div>
		<div class="fm component_edit_links">
			<a href="#" class="fm save"><b></b>Зберегти</a>
			<a href="#" class="fm apply"><b></b>Застосувати</a>
			<a href="<?=$this->init_model->get_link($menu_id, '{URL}');?>" class="fm cancel"><b></b>До списку питань</a>
		</div>
		<?php if (count($languages) > 1): ?>
			<div class="fmr component_lang">
				<?php foreach ($languages as $key => $val): ?>
					<a href="#" class="flags <?=$key;?><?=(($key == LANG) ? ' active' : '');?>" data-language="<?=$key;?>"><img src="img/flags_<?=$key;?>.png"></a>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	<form id="faq_form" action="<?=$this->uri->full_url('admin/faq/save');?>" method="post">
		<input type="hidden" name="item_id" value="<?=$item_id;?>">
		<?php foreach ($languages as $key => $val): ?>
		<div id="faq_tab_<?=$key;?>" class="faq_tab"<?php if (LANG != $key) echo ' style="display: none"'; ?>>
			<div class="evry_title">
				<label class="block_label">Питання:</label>
				<input type="text" name="question[<?=$key;?>]" value="<?=$item['question_' . $key];?>">
			</div>
			<div class="evry_title">
				<label class="block_label">Відповідь:</label>
				<div class="no_float"><textarea class="answer_text" name="answer[<?=$key;?>]"><?=stripslashes($item['answer_' . $key]);?></textarea></div>
			</div>
		</div>
		<?php endforeach; ?>
		<div class="fm for_sucsess">
			<div class="fmr save_links">
				<a href="#" class="fm save_adm"><b></b>Зберегти</a>
				<a href="#" class="fm apply_adm"><b></b>Застосувати</a>
				<a href="<?=$this->init_model->get_link($menu_id, '{URL}');?>" class="fm cansel_adm"><b></b>До списку питань</a>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
//<![CDATA[
	$(function () {
		$('.answer_text').ckeditor();

		$('.save, .save_adm, .apply, .apply_adm').on('click', function (e) {
			e.preventDefault();

			var redirect = ($(this).hasClass('apply_adm') || $(this).hasClass('apply')) ? false : true;

			$('.answer_text').ckeditor({action: 'update'});

			$('#faq_form').ajaxSubmit({
				beforeSubmit: function () {
					component_loader_show($('.component_loader'));
				},
				success: function (response) {
					if (response.success) {
						component_loader_hide($('.component_loader'));
						if (redirect) window.location.href = $('.cansel_adm').attr('href');
					}
				},
				dataType: 'json'
			});
		});

		$('.component_lang').on('click', 'a', function (e) {
			e.preventDefault();

			$('.faq_tab').hide();
			$('#faq_tab_' + $(this).data('language')).show();
			$(this).addClass('active').siblings().removeClass('active');
		});
	});
//]]>
</script>