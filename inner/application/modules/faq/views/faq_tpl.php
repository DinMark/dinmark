<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if (count($items) > 0): ?>
<section class="fm faq">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="fm faq_title"><? if(LANG=='ua')echo'Питання';if(LANG=='ru')echo'Вопросы';if(LANG=='en')echo'Faq';?></div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="fm faq_all">
					<?php foreach ($items as $row): ?>
					<div class="fm faq_one">
						<a href="#" class="question"><?=$row['question'];?><i class="faq_arrow"></i></a>
						<div class="fm answer"><?=stripslashes($row['answer']);?></div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$(function () {
		$('.faq').on('click', '.question', function (e) {
			e.preventDefault();
			$(this).closest('.faq_one').toggleClass('active');
		})
	});
</script>
<?php endif; ?>