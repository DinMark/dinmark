<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_faq
	 *
	 * @property Admin_faq_model $admin_faq_model
	 */
	class Admin_faq extends MX_Controller
	{
		/**
		 * Додавання питання
		 *
		 * @return int|string
		 */
		public function insert()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE, 'item_id' => 0);

			$component_id = intval($this->input->post('component_id'));
			$menu_id = intval($this->input->post('menu_id'));

			if ($component_id > 0 AND $menu_id > 0)
			{
				$this->load->model('admin_faq_model');

				$db_set = array(
					'component_id' => $component_id,
					'menu_id' => $menu_id,
					'position' => $this->admin_faq_model->get_position($component_id),
				);

				$response['success'] = TRUE;
				$response['item_id'] = $this->admin_faq_model->insert($db_set);

				$this->session->set_userdata('last_faq', $response['item_id']);
			}

			return json_encode($response);
		}

		/**
		 * Відображення/приховування питання
		 *
		 * @return string
		 */
		public function visible()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$item_id = intval($this->input->post('item_id'));
			$status = intval($this->input->post('status'));

			if ($item_id > 0 AND in_array($status, array(0, 1)))
			{
				$this->load->model('admin_faq_model');

				$set = array('hidden' => $status);

				$this->admin_faq_model->update($item_id, $set);

				$this->session->set_userdata('last_faq', $item_id);
				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Редагування питання
		 */
		public function edit()
		{
			$this->init_model->is_admin('redirect');

			$menu_id = intval($this->input->get('menu_id'));
			$item_id = intval($this->input->get('item_id'));

			if ($menu_id > 0 AND $item_id > 0)
			{
				$this->init_model->set_menu_id($menu_id, TRUE);
				$this->session->set_userdata('last_faq', $item_id);

				$this->load->model('admin_faq_model');

				$tpl_data = array(
					'item' => $this->admin_faq_model->get($item_id),
					'item_id' => $item_id,
					'menu_id' => $menu_id,
					'languages' => $this->config->item('languages'),
				);

				$content = $this->load->view('admin/edit_tpl', $tpl_data, TRUE);
				$this->template_lib->set_content($content);

				$this->template_lib->set_title('Редагування питання');

				$this->template_lib->set_admin_menu_active('components');
				$this->template_lib->set_admin_menu_active('', 'sub_level');
			}
		}

		/**
		 * Збереження питання
		 */
		public function save()
		{
			$this->init_model->is_admin('json');

			$this->load->model('admin_faq_model');
			$this->load->helper('form');

			$item_id = intval($this->input->post('item_id'));

			$question = $this->input->post('question');
			$answer = $this->input->post('answer');

			$set = array();

			foreach ($question as $key => $val)
			{
				$set['question_' . $key] = form_prep($val);

				$_answer = str_replace(array("\r", "\n", "\t"), '', $answer[$key]);
				$set['answer_' . $key] = $this->db->escape_str($_answer);
			}

			$this->admin_faq_model->update($item_id, $set);

			$this->session->set_userdata('last_faq', $item_id);

			return json_encode(array('success' => TRUE));
		}

		/**
		 * Видалення питання
		 *
		 * @return string
		 */
		public function delete()
		{
			$this->init_model->is_admin();

			$response = array('success' => FALSE);
			$item_id = intval($this->input->post('item_id'));

			if ($item_id > 0)
			{
				$this->load->model('admin_faq_model');
				$this->admin_faq_model->delete($item_id);
				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Збереження порядку сортування питань
		 */
		public function save_position()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$item_id = $this->input->post('item_id');
			$position = $this->input->post('position');

			if (is_array($position))
			{
				$this->load->model('admin_faq_model');

				foreach ($position as $key => $val)
				{
					$set = array('position' => $key);
					$this->admin_faq_model->update($val, $set);
				}

				$response['success'] = TRUE;
				$this->session->set_userdata('last_faq', $item_id);
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту
		 *
		 * @return string
		 */
		public function delete_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1, 'success' => false);
			$component_id = intval($this->input->post('component_id'));

			if ($component_id > 0)
			{
				$this->load->model('admin_faq_model');

				$this->admin_faq_model->delete_component($component_id);

				$response['error'] = 0;
				$response['success'] = true;
			}

			return json_encode($response);
		}
	}