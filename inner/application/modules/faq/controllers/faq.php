<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Faq
	 *
	 * @property Admin_faq_model $admin_faq_model
	 * @property Faq_model $faq_model
	 */
	class Faq extends MX_Controller
	{
		/**
		 * Вивід списка питань
		 *
		 * @param int $menu_id
		 * @param int $component_id
		 * @param int $hidden
		 * @param array $config
		 */
		public function index($menu_id, $component_id, $hidden, $config = array())
		{
			$template_data = array(
				'component_id' => $component_id,
				'menu_id' => $menu_id,
				'hidden' => $hidden,
			);

			if ($this->init_model->is_admin())
			{
				$this->load->model('admin_faq_model');

				$template_data['items'] = $this->admin_faq_model->get_items($component_id);
				$template_data['last'] = $this->session->userdata('last_faq');

				$this->load->view('admin/faq_tpl', $template_data);
			}
			else
			{
				$this->load->model('faq_model');

				$template_data['items'] = $this->faq_model->get_items($component_id);

				$this->load->view('faq_tpl', $template_data);
			}
		}
	}