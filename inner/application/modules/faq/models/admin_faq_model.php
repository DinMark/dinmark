<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_faq_model
	 */
	class Admin_faq_model extends CI_Model
	{
		/**
		 * Загальна кількість записів компоненті
		 *
		 * @param int $component_id
		 * @return int
		 */
		public function count_items($component_id)
		{
			return (int)$this->db
				->where('component_id', $component_id)
				->count_all_results('faq');
		}

		/**
		 * Отримання всіх записів компоненту
		 *
		 * @param int $component_id
		 * @param int $page
		 * @return null
		 */
		public function get_items($component_id)
		{
			return $this->db
				->select('faq.item_id, faq.hidden, faq.question_' . LANG . ' as question')
				->where('faq.component_id', $component_id)
				->order_by('faq.position', 'asc')
				->get('faq')
				->result_array();
		}

		/**
		 * Додавання запису
		 *
		 * @param array $set
		 * @return int
		 */
		public function insert($set)
		{
			$this->db->insert('faq', $set);
			return (int)$this->db->insert_id();
		}

		/**
		 * Отримання запису
		 *
		 * @param $item_id
		 */
		public function get($item_id)
		{
			return $this->db
				->where('item_id', $item_id)
				->get('faq')
				->row_array();
		}

		/**
		 * Оновлення запису
		 *
		 * @param int $item_id
		 * @param array $set
		 */
		public function update($item_id, $set)
		{
			$this->db
				->set($set)
				->where('item_id', $item_id)
				->update('faq');
		}

		/**
		 * Отримання останньої позиції в компоненті
		 *
		 * @param int $component_id
		 * @return int
		 */
		public function get_position($component_id)
		{
			$this->db
				->set('`position`', '`position` + 1', false)
				->where('component_id', $component_id)
				->update('faq');

			return 0;
		}

		/**
		 * Видалення запису
		 *
		 * @param int $item_id
		 */
		public function delete($item_id)
		{
			$this->db
				->where('item_id', $item_id)
				->delete('faq');
		}

		/**
		 * Видалення компоненту
		 *
		 * @param int $component_id
		 */
		public function delete_component($component_id)
		{
			$this->db
				->where('component_id', $component_id)
				->delete('faq');

			$this->db
				->where('component_id', $component_id)
				->delete('components');
		}
	}

