<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Faq_model
	 */
	class Faq_model extends CI_Model
	{
		/**
		 * @param $component_id
		 * @param $page
		 * @return mixed
		 */
		public function get_items($component_id)
		{
			return $this->db
				->select('item_id, question_' . LANG . ' as question, answer_' . LANG . ' as answer')
				->where('component_id', $component_id)
				->where('hidden', 0)
				->order_by('position', 'asc')
				->get('faq')
				->result_array();
		}
	}