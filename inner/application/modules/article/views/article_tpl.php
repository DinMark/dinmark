<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<?php if ($article['wide'] == 0): ?>
<section class="sect-article fm">
	<div class="container">
		<div class="article-wrapper fm">
			<article>
				<?php if (isset($article) AND count($article) > 0): ?>
				<header>
					<?php if (!$h1): ?><h1><?=$article['title'];?></h1><?php else: ?><h2><?=$article['title'];?></h2><?php endif; ?>
					<?php if ($this->config->item('print_icon') == 1): ?><span class="print_icon" data-module="article" data-id="<?=$component_id;?>"></span><?php endif; ?>
				</header>
				<div class="article_text">
					<?=stripslashes($article['text']);?>
				</div>
			<?php else: ?>
				<header><h2><? if(LANG=='ua')echo'Нова стаття';if(LANG=='ru')echo'Новая статья';if(LANG=='en')echo'New Article';?></h2></header>
			<?php endif; ?>
			</article>
		</div>
	</div>
</section>
<?php else: ?>
<section class="about_us fm">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="article-wrapper fm">
					<article>
						<header>
							<h2><?=$article['title'];?></h2>
						</header>
						<?=stripslashes($article['text']);?>
					</article>
				</div>
			</div>
			<div class="col-md-6">
				<?=Modules::run('news/last_news_block');?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>