<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	if (isset($menu) and count($menu) > 0)
	{
		echo '<ul>';
		foreach ($menu as $v)
		{
			$url = '';
			$target = '';
			extract(link_attributes($v['url'], $v['static_url'], $v['main'], $v['target']));

			$class = array();
			if (in_array($v['id'], $parents)) $class[] = 'active';
			if ($v['main'] == 1 AND $is_main) $class[] = 'active';

			$name = stripslashes($v['name']);
			$class = (count($class) > 0) ? ' class="' . implode(' ', $class) . '"' : '';

			echo '<li' . (isset($menu[$v['id']]) ? ' class="has-drop"' : '') . '>';

			if (in_array($v['id'], $seo_link))
			{
				echo '<!--noindex--><a href="' . $url . '"' . $class . $target . ' rel="nofollow">' . $name . '</a><!--/noindex-->';
			}
			else
			{
				echo '<a href="' . $url . '"' . $class . $target . '>' . $name . '</a>';
			}

			echo '</li>';
		}
		echo '</ul>';
	}