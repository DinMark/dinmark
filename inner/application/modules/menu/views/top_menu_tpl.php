<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	if (isset($menu[0]) and count($menu[0]) > 0)
	{
		echo '<ul>';
		foreach ($menu[0] as $v)
		{
			$url = '';
			$target = '';
			extract(link_attributes($v['url'], $v['static_url'], $v['main'], $v['target']));

			$class = array();
			if (in_array($v['id'], $parents)) $class[] = 'active';
			if ($v['main'] == 1 AND $is_main) $class[] = 'active';

			$name = stripslashes($v['name']);
			$class = (count($class) > 0) ? ' class="' . implode(' ', $class) . '"' : '';

			echo '<li' . (isset($menu[$v['id']]) ? ' class="has-drop"' : '') . '>';

			if (in_array($v['id'], $seo_link))
			{
				echo '<!--noindex--><a href="' . $url . '"' . $class . $target . ' rel="nofollow">' . $name . '</a><!--/noindex-->';
			}
			else
			{
				echo '<a href="' . $url . '"' . $class . $target . '>' . $name . '</a>';
			}

			if (isset($menu[$v['id']])) {
				echo '<div class="top-nav_submenu fm">';

				foreach ($menu[$v['id']] as $_k => $_v)
				{
					if ($_k === 0 or $_k % 2 === 0) {
						echo '<div class="top-nav_col"><ul>';
					}

					$url = '';
					$target = '';
					extract(link_attributes($_v['url'], $_v['static_url'], $_v['main'], $_v['target']));

					$class = array();
					if (in_array($_v['id'], $parents)) $class[] = 'active nra';

					$class = (count($class) > 0) ? ' class="' . implode(' ', $class) . '"' : '';

					echo '<li>';

					if (in_array($_v['id'], $seo_link))
					{
						echo '<!--noindex--><a href="' . $url . '"' . $class . $target . ' rel="nofollow">' . $_v['name'] . '</a><!--/noindex-->';
					}
					else
					{
						echo '<a href="' . $url . '"' . $class . $target . '>' . $_v['name'] . '</a>';
					}

					if (isset($menu[$_v['id']])) {
						echo '<ul>';

						foreach ($menu[$_v['id']] as $__k => $__v) {
							$url = '';
							$target = '';
							extract(link_attributes($__v['url'], $__v['static_url'], $__v['main'], $__v['target']));

							$class = array();
							if (in_array($__v['id'], $parents)) {
								$class[] = 'active nra';
							}

							$class = (count($class) > 0) ? ' class="' . implode(' ', $class) . '"' : '';

							echo '<li>';

							if (in_array($__v['id'], $seo_link)) {
								echo '<!--noindex--><a href="' . $url . '"' . $class . $target . ' rel="nofollow">' . $__v['name'] . '</a><!--/noindex-->';
							} else {
								echo '<a href="' . $url . '"' . $class . $target . '>' . $__v['name'] . '</a>';
							}
						}

						echo '</ul>';
					}

					echo '</li>';

					if ($_k === count($menu[$v['id']]) - 1 or $_k % 2 === 1) {
						echo '</ul></div>';
					}
				}

				echo '</div>';
			}

			echo '</li>';
		}
		echo '</ul>';
	}