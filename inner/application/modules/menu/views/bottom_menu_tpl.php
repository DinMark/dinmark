<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	if (isset($menu[0]))
	{
		foreach ($menu[0] as $v)
		{
			echo '<div class="col-md-3 col-sm-3 col-xs-12"><nav class="footer-nav"><div class="footer-nav_title">' . $v['name'] . '<i class="fa fa-angle-down" aria-hidden="true"></i></div>';

			if (isset($menu[$v['id']]))
			{
				echo '<ul>'; // Start 2-nd level

				foreach ($menu[$v['id']] as $_v)
				{
					$url = '';
					$target = '';
					extract(link_attributes($_v['url'], $_v['static_url'], $_v['main'], $_v['target']));

					$class = array();
					if (in_array($_v['id'], $parents)) $class[] = 'active';
					if ($_v['main'] == 1 AND $is_main) $class[] = 'active';

					$name = stripslashes($_v['name']);
					$class = (count($class) > 0) ? ' class="' . implode(' ', $class) . '"' : '';

					echo '<li>';

					if (in_array($_v['id'], $seo_link))
					{
						echo '<!--noindex--><a href="' . $url . '"' . $class . $target . ' rel="nofollow"><span>&raquo;</span>' . $name . '</a><!--/noindex-->';
					}
					else
					{
						echo '<a href="' . $url . '"' . $class . $target . '><span>&raquo;</span>' . $name . '</a>';
					}

					echo '</li>';
				}
				echo '</ul>';
			}

			echo '</nav></div>'; // End 1-st level
		}
	}