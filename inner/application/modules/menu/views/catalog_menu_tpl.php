<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	if (isset($parents[0]) AND $parents[0] == 0)
	{
		unset($parents[0]);
		$parents = array_values($parents);
	}

	if (isset($menu[0]))
	{
		echo '<ul>';

		foreach ($menu[0] as $k => $v)
		{
			$url = '';
			$target = '';
			extract(link_attributes($v['url'], $v['static_url'], $v['main'], $v['target']));

			$class = array();

			if (in_array($v['id'], $parents) OR ($v['main'] == 1 AND $is_main))
			{
				$class[] = 'active nra';
				$open = TRUE;
			}
			else
			{
				if ($k == 0 AND count($parents) == 0)
				{
					$class[] = 'active nra';
					$open = TRUE;
				}
				else
				{
					$open = FALSE;
				}
			}

//			if (isset($parents[0]) AND $k == 0)
//			{
//				if (!in_array($parents[0], $root))
//				{
//					$class[] = 'active nra';
//					$open = TRUE;
//				}
//			}

			$class = (count($class) > 0) ? ' class="' . implode(' ', $class) . '"' : '';

			if (isset($menu[$v['id']]))
			{
				$arrow = '<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>';
			}
			else
			{
				$arrow = '';
			}

			if ($v['bg'] !== '')
			{
				$icon = '<img src="' . base_url('upload/menu/' . $v['id'] . '/' . $v['bg']) . '" alt="' . $v['name'] . '">';
			}
			else
			{
				$icon = '';
			}

			echo '<li>';

			if (in_array($v['id'], $seo_link))
			{
				echo '<!--noindex--><a href="' . $url . '"' . $class . $target . ' rel="nofollow">' . $icon . $v['name'] . $arrow . '</a><!--/noindex-->';
			}
			else
			{
				echo '<a href="' . $url . '"' . $class . $target . '>' . $icon . $v['name']. $arrow . '</a>';
			}

			if (isset($menu[$v['id']]))
			{
				echo '<div class="main-nav_drop fm">';

				foreach ($menu[$v['id']] as $_k => $_v)
				{
					//if ($_k === 0 or $_k % 3 === 0) {
						echo '<div class="main-nav_col fm"><ul class="main-nav_submenu">';
					///}

					$url = '';
					$target = '';
					extract(link_attributes($_v['url'], $_v['static_url'], $_v['main'], $_v['target']));

					$class = array();
					if (in_array($_v['id'], $parents)) $class[] = 'active nra';

					$class = (count($class) > 0) ? ' class="' . implode(' ', $class) . '"' : '';

					echo '<li>';

					if (in_array($_v['id'], $seo_link))
					{
						echo '<!--noindex--><a href="' . $url . '"' . $class . $target . ' rel="nofollow">' . $_v['name'] . '</a><!--/noindex-->';
					}
					else
					{
						echo '<a href="' . $url . '"' . $class . $target . '>' . $_v['name'] . '</a>';
					}

					if (isset($menu[$_v['id']]))
					{
						echo '<ul>';

						foreach ($menu[$_v['id']] as $__k => $__v)
						{
							$url = '';
							$target = '';
							extract(link_attributes($__v['url'], $__v['static_url'], $__v['main'], $__v['target']));

							$class = array();
							if (in_array($__v['id'], $parents)) $class[] = 'active nra';

							$class = (count($class) > 0) ? ' class="' . implode(' ', $class) . '"' : '';

							echo '<li>';

							if (in_array($__v['id'], $seo_link))
							{
								echo '<!--noindex--><a href="' . $url . '"' . $class . $target . ' rel="nofollow">' . $__v['name'] . '</a><!--/noindex-->';
							}
							else
							{
								echo '<a href="' . $url . '"' . $class . $target . '>' . $__v['name'] . '</a>';
							}

							if (isset($menu[$__v['id']]))
							{
								echo '<ul>';

								foreach ($menu[$__v['id']] as $___v)
								{
									$url = '';
									$target = '';
									extract(link_attributes($___v['url'], $___v['static_url'], $___v['main'], $___v['target']));

									$class = array();
									if (in_array($___v['id'], $parents)) $class[] = 'active';
									if ($___v['main'] == 1 AND $is_main) $class[] = 'active';

									$class = (count($class) > 0) ? ' class="' . implode(' ', $class) . '"' : '';

									echo '<li>';

									if (in_array($___v['id'], $seo_link))
									{
										echo '<!--noindex--><a href="' . $url . '"' . $class . $target . ' rel="nofollow">' . $___v['name'] . '</a><!--/noindex-->';
									}
									else
									{
										echo '<a href="' . $url . '"' . $class . $target . '>' . $___v['name'] . '</a>';
									}

									echo '</li>';
								}

								echo '</ul>';
							}

							echo '</li>';
						}

						echo '</ul>';
					}

					echo '</li>';

					//if ($_k === count($menu[$v['id']]) - 1 or $_k % 3 === 2) {
						echo '</ul></div>';
					//}
				}

				echo '</div>';
			}

			echo '</li>';
		}

		echo '</ul>';
	}