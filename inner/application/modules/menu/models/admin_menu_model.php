<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_menu_model extends CI_Model
	{

		private $paths_cache = array();

		/**
		 * Повний ID шлях до пункту меню
		 * @var array
		 */
		private $url_path_id = array();

		/**
		 * Повний url до пункту меню
		 * @var array
		 */
		private $url_path = array();

		/**
		 * Отримання списку пунктів меню
		 *
		 * @param int $menu_index
		 * @param null $language
		 *
		 * @return array|null
		 */
		public function get_menu($menu_index, $language = NULL)
		{
			if (is_null($language)) $language = LANG;

			$this->db->select('id, parent_id, position, hidden, main, target, name_' . $this->config->item('language') . ' as def_name, name_' . $language . ' as name, url_path_' . $language . ' as url, static_url_' . $language . ' as static_url, image, bg');
			$this->db->where('id !=', 196);
			$this->db->where('menu_index', $menu_index);
			$this->db->order_by('parent_id, position');
			$result = $this->db->get('menu')->result_array();

			if (count($result) > 0)
			{
				$menu = array(
					'root' => array(),
					'children' => array()
				);

				foreach ($result as $row)
				{
					if ($row['parent_id'] == 0)
					{
						$menu['root'][] = $row;
					}
					else
					{
						$menu['children'][$row['parent_id']][] = $row;
					}
				}

				return $menu;
			}

			return NULL;
		}

		/**
		 * Додавання нового пункту меню
		 *
		 * @param int $menu_index
		 * @param int $parent_id
		 *
		 * @return int|null
		 */
		public function menu_add($menu_index = 0, $parent_id = 0)
		{
			$set = array(
				'parent_id' => $parent_id,
				'menu_index' => $menu_index,
				'update' => time()
			);
			$this->db->insert('menu', $set);

			$menu_id = $this->db->insert_id();

			$set = array(
				'item_id' => $menu_id,
				'menu_id' => $menu_id,
				'module' => 'components',
				'method' => 'get_components'
			);
			$this->db->insert('site_links', $set);

			return $menu_id;
		}

		/**
		 * Оновлення інформації про пункт меню
		 *
		 * @param int $id
		 * @param array $set
		 * @param bool $update_uri
		 * @param null|string $language
		 * @return object
		 */
		public function menu_update($id, $set = array(), $update_uri = FALSE, $language = NULL)
		{
			$this->db->update('menu', $set, array('id' => $id));

			if ($update_uri)
			{
				if ($language === NULL)
				{
					$language = LANG;
				}

				$this->db->select('url_' . $language . ' as url, static_url_' . $language . ' as static_url');
				$this->db->where('id', $id);
				$menu = $this->db->get('menu')->row_array();

				$uri = $menu['static_url'] != '' ? $menu['static_url'] : $menu['url'];

				$link = $this->db
					->select('item_id, menu_id, module')
					->where('item_id !=', $id)
					->where('menu_id !=', $id)
					->where('hash_' . $language, md5($uri))
					->get('site_links')
					->row_array();

				if (isset($link['item_id'])) {
					$uri .= '-' . $id;
					$this->db->update('menu', array('url_' . $language => $uri), array('id' => $id));
				}

				$this->db->set('hash_' . $language, md5($uri));
				$this->db->where('item_id', $id);
				$this->db->where('menu_id', $id);
				$this->db->where('module', 'components');
				$this->db->update('site_links');
			}
		}

		/**
		 * Збереження порядку сортування пунктів меню
		 *
		 * @param $items
		 */
		public function update_position($items)
		{
			if (is_array($items))
			{
				$set = array();

				foreach ($items as $val)
				{
					if (isset($val['id']) AND $val['id'] > 0)
					{
						$set[] = array(
							'id' => $val['id'],
							'parent_id' => intval($val['parent_id']),
							'level' => intval($val['level']),
							'position' => intval($val['position'])
						);

						//$this->menu_update($val['id'], $set);
					}

					if (count($set) == 50) {
						$this->db->update_batch('menu', $set, 'id');
						$set = array();
					}
				}

				if (count($set) > 0) $this->db->update_batch('menu', $set, 'id');
			}
		}

		/**
		 * Встановлення пункту меню головним
		 *
		 * @param $id
		 */
		public function set_main($id)
		{
			$this->db->update('menu', array('main' => 0), array('main' => 1));
			$this->db->update('menu', array('main' => 1), array('id' => $id));
		}

		/**
		 * Видалення пункту меню
		 *
		 * @param $id
		 */
		public function menu_delete($id)
		{
			// Видалення компонентів пункту меню

			$this->db->delete('site_links', array('item_id' => $id, 'component_id' => 0));
			$this->db->delete('components', array('menu_id' => $id));
			$this->db->delete('component_article', array('menu_id' => $id));
			$this->db->delete('google_map', array('menu_id' => $id));
			$this->db->delete('feedback', array('menu_id' => $id));
			$this->db->delete('seo_tags', array('component_id' => 0, 'menu_id' => $id));
			$this->db->delete('seo_link', array('menu_id' => $id));

			// Видалення каталогу
			$this->db->select('product_id, component_id');
			$this->db->where('menu_id', $id);

			$result = $this->db->get('catalog');

			if ($result->num_rows() > 0)
			{
				$result_array = $result->result_array();
				foreach ($result_array as $row)
				{
					$dir = ROOT_PATH . 'upload/catalog/' . $row['component_id'] . '/';
					if (file_exists($dir)) delete_files($dir, TRUE, FALSE, 1);

					$this->db->delete('catalog_product_bind_components', array('product_id' => $row['product_id']));
					$this->db->delete('catalog', array('product_id' => $row['product_id']));
					$this->db->delete('catalog_variants', array('product_id' => $row['product_id']));
					$this->db->delete('catalog_images', array('product_id' => $row['product_id']));
					$this->db->delete('catalog_product_delivery', array('product_id' => $row['product_id']));
					$this->db->delete('site_links', array('item_id' => $row['product_id'], 'module' => 'catalog'));
					$this->db->delete('seo_tags', array('item_id' => $row['product_id'], 'module' => 'catalog'));
				}
			}

			// Видалення новин
			$this->db->select('news_id, component_id');
			$this->db->where('menu_id', $id);

			$result = $this->db->get('news');

			if ($result->num_rows() > 0)
			{
				$result_array = $result->result_array();
				foreach ($result_array as $row)
				{
					$dir = ROOT_PATH . 'upload/news/' . $row['component_id'] . '/';
					if (file_exists($dir)) delete_files($dir, TRUE, FALSE, 1);

					$this->db->delete('site_links', array('item_id' => $row['news_id'], 'module' => 'news'));
					$this->db->delete('seo_tags', array('item_id' => $row['news_id'], 'module' => 'news'));
				}
			}

			$this->db->delete('news', array('menu_id' => $id));
			$this->db->delete('news_images', array('menu_id' => $id));

			// Видалення пункту меню
			$this->db->delete('menu', array('id' => $id));

			$dir = ROOT_PATH . 'upload/menu/' . $id . '/';
			if (file_exists($dir)) delete_files($dir, TRUE, FALSE, 1);

			// Видалення дочірніх пунктів меню
			$result = $this->db->select('id')->where('parent_id', $id)->get('menu');
			if ($result->num_rows() > 0)
			{
				$result_array = $result->result_array();
				foreach ($result_array as $row)
				{
					$this->menu_delete($row['id']);
				}
			}
		}

		/**
		 * Оновлення посилань пунктів меню
		 *
		 * @param int $menu_id
		 */
		public function update_paths($menu_id = NULL, $current = true)
		{
			$this->db->select('id, parent_id, main');

			if ($menu_id !== null) {
				//$this->db->where('id', $menu_id);
				$this->db->where('parent_id', $menu_id);
			}

			$r = $this->db->get('menu');

			$menu_set = array();

			foreach ($r->result_array() as $key => $row)
			{
				if (($current === true and $menu_id == $row['id']) or ($menu_id != $row['id']))
				{
					$this->url_path_id = array();
					$paths = $this->get_paths($row['parent_id']);

					$menu_set[$key] = array(
						'id' => $row['id'],
						'url_path_id' => $paths['id']
					);

					$this->update_paths($row['id'], FALSE);
				}
			}

			if (count($menu_set) > 0) $this->db->update_batch('menu', $menu_set, 'id');
		}

		/**
		 * Рекурсивне отримання посилань
		 *
		 * @param int $id
		 */
		private function set_paths($id)
		{
			$this->db->select('parent_id');
			$this->db->where('id', $id);

			$r = $this->db->get('menu');

			if ($r->num_rows() > 0)
			{
				$row = $r->row_array();

				array_unshift($this->url_path_id, $id);

				if ($row['parent_id'] > 0) $this->set_paths($row['parent_id']);
			}
		}

		/**
		 * Отримання сформованого шляху до пункту меню
		 *
		 * @param $id
		 * @return string
		 */
		private function get_paths($id)
		{
			if (!isset($this->paths_cache[$id]))
			{
				$this->set_paths($id);
				$this->paths_cache[$id]['id'] = $this->url_path_id;
			}
			else
			{
				$this->url_path_id = $this->paths_cache[$id]['id'];
			}

			$paths = array('id' => count($this->url_path_id) > 0 ? ('.' . implode('.', $this->url_path_id) . '.') : '');

			return $paths;
		}
	}