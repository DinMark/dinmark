<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<?php if (LANG=='ua') $title = 'Зворотній зв`язок'; if (LANG == 'ru') $title = 'Обратная связь'; if (LANG == 'en') $title = 'Feedback'; ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<article>
				<header><?php if (!$h1): ?><h1><?=$title;?></h1><?php else: ?><h2><?=$title;?></h2><?php endif; ?></header>
				<div class="fm feedback" id="feedback_<?php echo $component_id; ?>">
					<div class="evry_title">
						<label for="feedback_name_<?php echo $component_id; ?>"><? if(LANG=='ua')echo'П.І.П.';if(LANG=='ru')echo'Ф.И.О.';if(LANG=='en')echo'Full Name';?>:</label>
						<input type="text" id="feedback_name_<?php echo $component_id; ?>" name="name" value="">
					</div>
					<div class="evry_title">
						<label for="feedback_email_<?php echo $component_id; ?>">E-mail:</label>
						<input type="text" id="feedback_email_<?php echo $component_id; ?>" name="email" value="">
					</div>
					<div class="evry_title">
						<label for="feedback_phone_<?php echo $component_id; ?>" class="no_active"><? if(LANG=='ua')echo'Телефон';if(LANG=='ru')echo'Телефон';if(LANG=='en')echo'Phone';?>:</label>
						<input type="text" id="feedback_phone_<?php echo $component_id; ?>" name="phone" value="">
					</div>
					<div class="evry_title">
						<label for="feedback_theme_<?php echo $component_id; ?>" class="no_active"><? if(LANG=='ua')echo'Тема';if(LANG=='ru')echo'Тема';if(LANG=='en')echo'Subject';?>:</label>
						<input type="text" id="feedback_theme_<?php echo $component_id; ?>" name="theme" value="">
					</div>
					<div class="evry_title">
						<label for="feedback_message_<?php echo $component_id; ?>"><? if(LANG=='ua')echo'Повідомлення';if(LANG=='ru')echo'Сообщение';if(LANG=='en')echo'Message';?>:</label>
						<textarea id="feedback_message_<?php echo $component_id; ?>" name="message"></textarea>
					</div>
					<div class="evry_title">
						<label></label>
						<div class="fm captcha">
							<input type="text" id="feedback_code_<?php echo $component_id; ?>" name="code" value="" maxlength="4">
							<div class="captcha_place" id="feedback_captcha_<?php echo $component_id; ?>">
								<img src="<?php echo $this->uri->full_url('feedback/captcha'); ?>" height="37">
								<a href="#" class="reload" id="feedback_recaptcha_<?php echo $component_id; ?>"></a>
							</div>
						</div>
					</div>
					<a href="#" class="common-btn" id="feedback_send_<?php echo $component_id; ?>"><? if(LANG=='ua')echo'Надіслати';if(LANG=='ru')echo'Отправить';if(LANG=='en')echo'Send';?></a>
				</div>
				<div id="feedback_alert_<?php echo $component_id; ?>" class="fm mail_sent" style="display:none;"></div>
			</article>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {

		$('#feedback_name_<?php echo $component_id; ?>,#feedback_email_<?php echo $component_id; ?>,#feedback_message_<?php echo $component_id; ?>').on('keyup blur paste', function () {
			var $field = $(this).parents('.evry_title:eq(0)');
			$field.removeClass('wrong');
		});

		$('#feedback_code_<?php echo $component_id; ?>').on('keyup blur paste', function () {
			$(this).closest('.evry_title').removeClass('wrong');
		});

		$('#feedback_send_<?php echo $component_id; ?>').on('click', function (event) {
			event.preventDefault();

			var request = {
					component_id: <?php echo $component_id; ?>,
					menu_id: <?php echo $menu_id; ?>,
					name:$.trim($('#feedback_name_<?php echo $component_id; ?>').val()),
					email:$.trim($('#feedback_email_<?php echo $component_id; ?>').val()),
					phone:$.trim($('#feedback_phone_<?php echo $component_id; ?>').val()),
					theme:$.trim($('#feedback_theme_<?php echo $component_id; ?>').val()),
					message:$.trim($('#feedback_message_<?php echo $component_id; ?>').val()),
					code:$.trim($('#feedback_code_<?php echo $component_id; ?>').val())
				},
				$error = $('#feedback_alert_<?php echo $component_id; ?>');

			if (request.name === '') {
				var $field = $('#feedback_name_<?php echo $component_id; ?>').closest('.evry_title').addClass('wrong');
				return false;
			}

			var email_test = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,6}$/i;
			if (!email_test.test(request.email)) {
				var $field = $('#feedback_email_<?php echo $component_id; ?>').closest('.evry_title').addClass('wrong');
				return false;
			}

			if (request.message === '') {
				var $field = $('#feedback_message_<?php echo $component_id; ?>').closest('.evry_title').addClass('wrong');
				return false;
			}

			if (request.code === '') {
				$('#feedback_code_<?php echo $component_id; ?>').closest('.evry_title').addClass('wrong');
				return false;
			}

			$.post(
				'<?php echo $this->uri->full_url('feedback/send'); ?>',
				request,
				function (response) {
					if (response.error === 0) {
						$('#feedback_<?php echo $component_id; ?>').fadeTo(500, 0).slideUp(function () {
							$(this).remove();
							$error.stop().fadeTo(50, 0).html('<? if(LANG=='ua')echo'Повідомлення відправлено';if(LANG=='ru')echo'Cообщение отправлено';if(LANG=='en')echo'Message sent';?>!').fadeTo(500, 1);
						});
					}

					if (response.error === 1) {
						$('#feedback_code_<?php echo $component_id; ?>').closest('.evry_title').addClass('wrong');
						$('#feedback_recaptcha_<?php echo $component_id; ?>').click();
					}
				},
				'json'
			);
		});

		$('#feedback_recaptcha_<?php echo $component_id; ?>').on('click', function (event) {
			event.preventDefault();
			$('#feedback_captcha_<?php echo $component_id; ?>').find('img').fadeTo(200, 0, function () {
				$(this).attr('src', '<?php echo $this->uri->full_url('feedback/captcha'); ?>#' + Math.random()).ready(function () {
					$('#feedback_captcha_<?php echo $component_id; ?>').find('img').fadeTo(1000, 1);
				});
			});
		});

	});
</script>