<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_feedback
	 *
	 * @property Admin_feedback_model $admin_feedback_model
	 */

	class Admin_feedback extends MX_Controller {

		private $per_page = 10;

		public function get()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$page = intval($this->input->post('page'));

			if ($page >= 0)
			{
				$this->load->model('admin_feedback_model');

				$response['error'] = 0;
				$response['messages'] = $this->load->view('admin/list_tpl', array('messages' => $this->admin_feedback_model->get_messages(0, $page, $this->per_page)), TRUE);
			}

			return json_encode($response);
		}

		/**
		 * Видалення повідомлення
		 */
		public function delete()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);

			$message_id = $this->input->post('message_id');

			if ($message_id > 0)
			{
				$this->load->model('admin_feedback_model');
				$this->admin_feedback_model->delete($message_id);

				$response['error'] = 0;
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту
		 */
		public function delete_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$component_id = $this->input->post('component_id');

			if ($component_id > 0)
			{
				$this->load->model('admin_feedback_model');
				$this->admin_feedback_model->delete_component($component_id);

				$response['error'] = 0;
			}

			return json_encode($response);
		}

	}