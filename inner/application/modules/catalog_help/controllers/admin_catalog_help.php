<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_catalog_help
	 *
	 * @property Admin_catalog_help_model $admin_catalog_help_model
	 */
	class Admin_catalog_help extends MX_Controller {

		/**
		 * Редагування інформації
		 */
		public function index()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Редагування підказок');

			$this->template_lib->set_admin_menu_active('catalog_config');
			$this->template_lib->set_admin_menu_active('catalog_help', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->template_lib->set_bread_crumbs('', 'Редагування підказок');

			$this->load->model('admin_catalog_help_model');

			$template_data = array(
				'menu_id' => $menu_id,
				'help' => $this->admin_catalog_help_model->get_help(),
				'languages' => $this->config->item('languages'),
			);
			$this->template_lib->set_content($this->load->view('admin/catalog_help_tpl', $template_data, TRUE));
		}

		/**
		 * Збереження інформації
		 */
		public function update()
		{
			$this->init_model->is_admin('json');
			$response = array('success' => FALSE);

			$this->load->model('admin_catalog_help_model');

			$help = $this->input->post('help');

			foreach ($help as $k => $data)
			{
				foreach ($data as $language => $v) {
					$this->admin_catalog_help_model->update($k, $v, $language);
				}
			}

			$response['success'] = TRUE;
			return json_encode($response);
		}
	}