<?php
	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_js('admin/ckeditor/ckeditor.js');
	$this->template_lib->set_js('admin/jquery.form.js');
?>
<div class="admin_component">
	<div class="component_loader"></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component delivery">
			<div class="article"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Редагування підказок</div></div>
			<a href="#" class="fm save"><b></b>Зберегти</a>
		</div>
		<?php if (count($languages) > 1): ?>
		<div class="fmr component_lang">
			<?php foreach ($languages as $key => $val): ?>
				<a href="#" class="flags <?=$key;?><?=(($key == LANG) ? ' active' : '');?>" data-language="<?=$key;?>"><img src="img/flags_<?=$key;?>.png"></a>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
	<div class="fm admin_view_article">
		<form id="component_help_form" action="<?=$this->uri->full_url('admin/catalog_help/update');?>" method="post">
			<?php foreach ($languages as $key => $val): ?>
			<div id="help_tab_<?=$key;?>" class="help_tab"<?=(($key != LANG) ? ' style="display: none"' : ''); ?>>
				<div class="evry_title">
					<label for="self_delivery_<?=$key;?>" class="block_label">Самовивіз:</label>
					<div class="no_float"><textarea id="self_delivery_<?=$key;?>" name="help[self_delivery][<?=$key;?>]"><?=stripslashes($help['self_delivery']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="new_post_warehouse_<?=$key;?>" class="block_label">Нова Пошта на відділення:</label>
					<div class="no_float"><textarea id="new_post_warehouse_<?=$key;?>" name="help[new_post_warehouse][<?=$key;?>]"><?=stripslashes($help['new_post_warehouse']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="new_post_courier_<?=$key;?>" class="block_label">Нова Пошта кур’єр:</label>
					<div class="no_float"><textarea id="new_post_courier_<?=$key;?>" name="help[new_post_courier][<?=$key;?>]"><?=stripslashes($help['new_post_warehouse']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="intime_<?=$key;?>" class="block_label">ІнТайм:</label>
					<div class="no_float"><textarea id="intime_<?=$key;?>" name="help[intime][<?=$key;?>]"><?=stripslashes($help['intime']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="delivery_<?=$key;?>" class="block_label">Деливери:</label>
					<div class="no_float"><textarea id="delivery_<?=$key;?>" name="help[delivery][<?=$key;?>]"><?=stripslashes($help['delivery']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="transport_company_<?=$key;?>" class="block_label">Транспортна компанія:</label>
					<div class="no_float"><textarea id="transport_company_<?=$key;?>" name="help[transport_company][<?=$key;?>]"><?=stripslashes($help['transport_company']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="receipt_payment_<?=$key;?>" class="block_label">Оплати при отриманні:</label>
					<div class="no_float"><textarea id="receipt_payment_<?=$key;?>" name="help[receipt_payment][<?=$key;?>]"><?=stripslashes($help['receipt_payment']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="bank_<?=$key;?>" class="block_label">Оплати при отриманні:</label>
					<div class="no_float"><textarea id="bank_<?=$key;?>" name="help[bank][<?=$key;?>]"><?=stripslashes($help['bank']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="non_cash_<?=$key;?>" class="block_label">Безготівковий переказ для ЮО:</label>
					<div class="no_float"><textarea id="non_cash_<?=$key;?>" name="help[non_cash][<?=$key;?>]"><?=stripslashes($help['non_cash']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="privat_24_<?=$key;?>" class="block_label">Приват 24:</label>
					<div class="no_float"><textarea id="privat_24_<?=$key;?>" name="help[privat_24][<?=$key;?>]"><?=stripslashes($help['privat_24']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="call_form_<?=$key;?>" class="block_label">Форма передзвоніть:</label>
					<div class="no_float"><textarea id="call_form_<?=$key;?>" name="help[call_form][<?=$key;?>]" class="text_editor"><?=stripslashes($help['call_form']['value_' . $key]);?></textarea></div>
				</div>
				<div class="evry_title">
					<label for="registration_<?=$key;?>" class="block_label">Реєстрація:</label>
					<div class="no_float"><textarea id="registration_<?=$key;?>" name="help[registration][<?=$key;?>]" class="text_editor"><?=stripslashes($help['registration']['value_' . $key]);?></textarea></div>
				</div>
                <div class="evry_title">
                    <label for="_registration_<?=$key;?>" class="block_label">Після реєстрації:</label>
                    <div class="no_float"><textarea id="_registration_<?=$key;?>" name="help[_registration][<?=$key;?>]" class="text_editor"><?=stripslashes($help['_registration']['value_' . $key]);?></textarea></div>
                </div>
			</div>
			<?php endforeach; ?>
			<div class="fm for_sucsess">
				<div class="fmr save_links">
					<a href="#" class="fm save_adm"><b></b>Зберегти</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">

	$(function () {
        $('.text_editor').ckeditor({height: 300});

		$('.component_lang').find('a').on('click', function (e) {
			e.preventDefault();

			$(this).closest('div').find('.active').removeClass('active');
			$(this).addClass('active');

			$('.help_tab').hide();
			$('#help_tab_' + $(this).data('language')).show();
		});

		$('.component_edit_links .save, .for_sucsess .save_adm').on('click', function (e) {
			e.preventDefault();

			component_loader_show($('.component_loader'), '');

			$('.text_editor').ckeditor({action: 'update'});

			$('#component_help_form').ajaxSubmit({
				success:function (response) {
					if (response.success) component_loader_hide($('.component_loader'), '');
				},
				dataType: 'json'
			});
		});

	});
</script>