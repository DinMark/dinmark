<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Catalog_help_model extends CI_Model
	{
		/**
		 * Отримання підказок
		 *
		 * @return array
		 */
		public function get_helps()
		{
			$help = array();

			$result = $this->db
				->select('key, value_' . LANG . ' as value')
				->get('catalog_help')
				->result_array();

			foreach ($result as $v)
			{
				$help[$v['key']] = stripslashes($v['value']);
			}

			return $help;
		}

		/**
		 * Отримання підказки
		 *
		 * @return array
		 */
		public function get_help($key)
		{
			$result = $this->db
				->select('value_' . LANG . ' as value')
				->where('key', $key)
				->get('catalog_help')
				->row_array();

			return $result !== null ? stripslashes($result['value']) : '';
		}
	}