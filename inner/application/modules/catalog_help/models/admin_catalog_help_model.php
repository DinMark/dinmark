<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_catalog_help_model extends CI_Model
	{
		/**
		 * Отримання інформації
		 *
		 * @return array
		 */
		public function get_help()
		{
			$help = array();

			$result = $this->db->get('catalog_help')->result_array();

			foreach ($result as $v)
			{
				$help[$v['key']] = $v;
			}

			return $help;
		}

		/**
		 * Збереження інформації
		 *
		 * @param string $key
		 * @param string $value
		 * @param string $language
		 */
		public function update($key, $value, $language)
		{
			$value = str_replace(array("\n", "\r", "\t"), '', $value);
			$value = $this->db->escape_str($value);

			$this->db->update('catalog_help', array('value_' . $language => $value), array('key' => $key));
		}
	}