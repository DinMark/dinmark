<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_components_model extends CI_Model {

		/**
		 * Додавання нового компоненту
		 *
		 * @param int $menu_id
		 * @param string $module
		 * @param string $method
		 * @param string $config
		 *
		 * @return mixed
		 */
		public function insert_component($menu_id, $module, $method, $config = '')
		{
			$this->shift_components($menu_id);

			$set = array(
				'menu_id' => $menu_id,
				'position' => 0,
				'module' => $module,
				'method' => $method,
				'config' => $config
			);
			$this->db->insert('components', $set);
			$component_id = $this->db->insert_id();

			// Оновлення посилань компоненту новин
			if ($module == 'news')
			{
				$languages = array_keys($this->config->item('languages'));

				foreach ($languages as $language) $this->db->select('menu.url_path_' . $language);
				$this->db->where('id', $menu_id);
				$menu = $this->db->get('menu')->row_array();

				foreach ($languages as $val)
				{
					$result = $this->db->select('news_id, url_' . $val)->get('news')->result_array();
					foreach ($result as $row)
					{
						$url = $menu['url_path_' . $val] . '/' . $row['url_' . $val];

						$this->db->set('hash_' . $val, md5($url));
						$this->db->where('item_id', $row['news_id']);
						$this->db->where('module', 'news');
						$this->db->update('site_links');
					}
				}
			}

			// Оновлення посилань компоненту захворювань
			if ($module == 'disease')
			{
				$languages = array_keys($this->config->item('languages'));

				foreach ($languages as $language) $this->db->select('menu.url_path_' . $language);
				$this->db->where('id', $menu_id);
				$menu = $this->db->get('menu')->row_array();

				foreach ($languages as $val)
				{
					$result = $this->db->select('disease_id, url_' . $val)->get('disease')->result_array();
					foreach ($result as $row)
					{
						$url = $menu['url_path_' . $val] . '/' . $row['url_' . $val];

						$this->db->set('hash_' . $val, md5($url));
						$this->db->where('item_id', $row['disease_id']);
						$this->db->where('module', 'disease');
						$this->db->update('site_links');
					}
				}
			}

			// Оновлення посилань компоненту звернень пацієнтів
			if ($module == 'patient')
			{
				$languages = array_keys($this->config->item('languages'));

				foreach ($languages as $language) $this->db->select('menu.url_path_' . $language);
				$this->db->where('id', $menu_id);
				$menu = $this->db->get('menu')->row_array();

				foreach ($languages as $val)
				{
					$result = $this->db->select('patient_id, url_' . $val)->get('patient')->result_array();
					foreach ($result as $row)
					{
						$url = $menu['url_path_' . $val] . '/' . $row['url_' . $val];

						$this->db->set('hash_' . $val, md5($url));
						$this->db->where('item_id', $row['patient_id']);
						$this->db->where('module', 'patient');
						$this->db->update('site_links');
					}
				}
			}

			return $component_id;
		}

		/**
		 * Приховування/відображення компоненту
		 *
		 * @param int $component_id
		 * @param int $status
		 */
		public function visibility($component_id, $status)
		{
			$this->db->set('hidden', $status);
			$this->db->where('component_id', $component_id);
			$this->db->update('components');
		}

		/**
		 * Збереження порядку сортування компонентів
		 *
		 * @param array $components
		 */
		public function update_position($components)
		{
			foreach ($components as $position => $component_id)
			{
				$this->db->set('position', intval($position));
				$this->db->where('component_id', intval($component_id));
				$this->db->update('components');
			}
		}

		/**
		 * Зміщення порядку сортування компонентів
		 *
		 * @param int $menu_id
		 */
		private function shift_components($menu_id)
		{
			$this->db->set('`position`', '`position` + 1', FALSE);
			$this->db->where('menu_id', $menu_id);
			$this->db->update('components');
		}
	}