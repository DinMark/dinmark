<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Components_model extends CI_Model {

		/**
		 * Отримання компонентів сторінки
		 *
		 * @param int $menu_id
		 * @return null
		 */
		public function get_components($menu_id = 0)
		{
			if (!$this->init_model->is_admin()) {
			    $this->db->where('hidden', 0);
            }

			$this->db->select('component_id, hidden, module, method, config');
			$this->db->where('menu_id', $menu_id);
			$this->db->order_by('position');

			$result = $this->db->get('components');

			return ($result->num_rows() > 0) ? $result->result_array() : NULL;
		}
	}