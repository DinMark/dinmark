<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * @property Admin_slider_model $admin_slider_model
	 */

	class Admin_slider extends MX_Controller
	{
		protected $big_sizes = array(870, 415);
		protected $thumb_sizes = array(300, 143);

		protected $bg_big_sizes = array(870, 415);
		protected $bg_thumb_sizes = array(300, 143);

		/**
		 * Вивід списку слайдів
		 */
		public function index()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Управління слайдером');

			$this->template_lib->set_admin_menu_active('config');
			$this->template_lib->set_admin_menu_active('slider', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->load->model('admin_slider_model');

			$tpl_data = array(
				'menu_id' => $menu_id,
				'slides' => $this->admin_slider_model->get_slides(),
				'last_slide' => $this->session->userdata('last_slide'),
				'thumb' => $this->thumb_sizes,
			);
			$this->template_lib->set_content($this->load->view('admin/slider_tpl', $tpl_data, TRUE));
		}

		/**
		 * Додавання слайду
		 *
		 * @return string
		 */
		public function add()
		{
			$this->init_model->is_admin('json');

			$this->load->model('admin_slider_model');

			$response = array(
				'success' => TRUE,
				'slide_id' => $this->admin_slider_model->add(intval($this->input->post('add_top')))
			);

			return json_encode($response);
		}

		/**
		 * Редагування слайду
		 */
		public function edit()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Управління слайдером - редагування слайду');

			$this->template_lib->set_admin_menu_active('config');
			$this->template_lib->set_admin_menu_active('slider', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$slide_id = intval($this->input->get('slide_id'));

			if ($menu_id > 0 AND $slide_id > 0)
			{
				$this->init_model->set_menu_id($menu_id, TRUE);

				$this->session->set_userdata('last_slide', $slide_id);
				$this->load->model('admin_slider_model');

				$tpl_data = array(
					'menu_id' => $menu_id,
					'slide_id' => $slide_id,
					'languages' => $this->config->item('languages'),
					'slide' => $this->admin_slider_model->get($slide_id),
					'thumb' => $this->thumb_sizes,
					'big' => $this->big_sizes,
					'bg_thumb' => $this->bg_thumb_sizes,
					'bg_big' => $this->bg_big_sizes,
				);

				$this->template_lib->set_content($this->load->view('admin/edit_tpl', $tpl_data, TRUE));
			}
		}

		/**
		 * Збереження інформації про слайд
		 *
		 * @return string
		 */
		public function save()
		{
			$this->init_model->is_admin('json');

			$this->load->model('admin_slider_model');
			$this->load->helper('form');

			$title = $this->input->post('title');
			$sign = $this->input->post('sign');
			$sign_2 = $this->input->post('sign_2');
			$url = $this->input->post('url');
			//$hidden = $this->input->post('hidden');

			$set = array();

			foreach ($title as $key => $val)
			{
				$set['title_' . $key] = form_prep($val);
				$set['sign_' . $key] = form_prep($sign[$key]);
				$set['sign_2_' . $key] = form_prep($sign_2[$key]);
				$set['url_' . $key] = $url[$key];
				//$set['hidden_' . $key] = isset($hidden[$key]) ? 1 : 0;
			}

			$where = array('slide_id' => intval($this->input->post('slide_id')));
			$this->admin_slider_model->update($set, $where);

			return json_encode(array('success' => TRUE));
		}

		/**
		 * Завантаження слайду
		 *
		 * @return string
		 */
		public function upload()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$slide_id = intval($this->input->post('slide_id'));

			if ($slide_id > 0)
			{
				$dir = ROOT_PATH . 'upload/slider/';
				if (!file_exists($dir)) mkdir($dir);

				$dir = ROOT_PATH . 'upload/slider/' . $slide_id . '/';
				if (!file_exists($dir)) mkdir($dir);

				$this->load->helper('translit');

				$upload_config = array(
					'upload_path' => $dir,
					'overwrite' => FALSE,
					'file_name' => translit_filename($_FILES['slide_image_' . LANG]['name']),
					'allowed_types' => 'gif|jpg|jpeg|png'
				);

				$this->load->library('upload', $upload_config);

				if ($this->upload->do_upload('slide_image_' . LANG))
				{
					$file_name = $this->upload->data('file_name');
					$size = getimagesize($dir . $file_name);

					copy($dir . $file_name, $dir . 's_' . $file_name);

					$this->load->library('Image_lib');

					$this->image_lib->resize_crop($dir . $file_name, $dir . $file_name, $this->big_sizes[0], $this->big_sizes[1], FALSE);
					$this->image_lib->resize_crop($dir . $file_name, $dir . 't_' . $file_name, $this->thumb_sizes[0], $this->thumb_sizes[1], FALSE);

					$this->load->model('admin_slider_model');

					$old_file_name = $this->admin_slider_model->get_image($slide_id, LANG);

					if ($old_file_name != '' AND $old_file_name != $file_name)
					{
						if (file_exists($dir . $old_file_name)) unlink($dir . $old_file_name);
						if (file_exists($dir . 's_' . $old_file_name)) unlink($dir . 's_' . $old_file_name);
						if (file_exists($dir . 't_' . $old_file_name)) unlink($dir . 't_' . $old_file_name);
					}

					$this->admin_slider_model->update_image($slide_id, $file_name, LANG);

					$response['success'] = TRUE;
					$response['width'] = $size[0];
					$response['height'] = $size[1];
					$response['file_name'] = $file_name . '?t=' . time() . rand(10000, 1000000);
				}
			}

			$this->config->set_item('is_ajax_request', TRUE);
			return json_encode($response);
		}

		/**
		 * Обрізка слайду
		 *
		 * @return string
		 */
		public function crop()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$slide_id = intval($this->input->post('slide_id'));
			$coords = $this->input->post('coords');
			$coords = array_map('floatval', $coords);

			if (is_numeric($coords['x']) AND is_numeric($coords['y']) AND $coords['w'] >= 0 AND $coords['h'] >= 0 AND $slide_id > 0)
			{
				$this->load->model('admin_slider_model');
				$file_name = $this->admin_slider_model->get_image($slide_id, LANG);

				if ($file_name !== NULL AND $file_name != '')
				{
					$dir = ROOT_PATH . 'upload/slider/' . $slide_id . '/';

					if (file_exists($dir . 's_' . $file_name))
					{
						$sizes = getimagesize($dir . 's_' . $file_name);
						$w_index = $sizes[0] / 520;
						$h_index = $sizes[1] / round((520 * $sizes[1]) / $sizes[0]);

						$x = $coords['x'] * $w_index;
						$y = $coords['y'] * $h_index;
						$x2 = $coords['x2'] * $w_index;
						$y2 = $coords['y2'] * $h_index;

						$this->load->library('Image_lib');
						$this->image_lib->crop($dir . 's_' . $file_name, $dir . $file_name, $x2 - $x, $y2 - $y, $x, $y, $this->big_sizes[0], $this->big_sizes[1]);
						$this->image_lib->resize_crop($dir . $file_name, $dir . 't_' . $file_name, $this->thumb_sizes[0], $this->thumb_sizes[1], FALSE);

						$response['image'] = '/upload/slider/' . $slide_id . '/t_' . $file_name . '?t' . time() . rand(10000, 1000000);
					}

					$response['success'] = TRUE;
				}
			}

			return json_encode($response);
		}

		/**
		 * Видалення зображення
		 *
		 * @return string
		 */
		public function delete_photo()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$slide_id = intval($this->input->post('slide_id'));

			if ($slide_id > 0)
			{
				$this->load->model('admin_slider_model');
				$file_name = $this->admin_slider_model->get_image($slide_id, LANG);

				if ($file_name !== NULL AND $file_name != '')
				{
					$dir = ROOT_PATH . 'upload/slider/' . $slide_id . '/';

					if ($file_name != '')
					{
						if (file_exists($dir . $file_name)) unlink($dir . $file_name);
						if (file_exists($dir . 's_' . $file_name)) unlink($dir . 's_' . $file_name);
						if (file_exists($dir . 't_' . $file_name)) unlink($dir . 't_' . $file_name);

						$this->admin_slider_model->update_image($slide_id, '', LANG);
					}
				}

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Завантаження фону
		 *
		 * @return string
		 */
		public function upload_bg()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$slide_id = intval($this->input->post('slide_id'));

			if ($slide_id > 0)
			{
				$dir = ROOT_PATH . 'upload/slider/';
				if (!file_exists($dir)) mkdir($dir);

				$dir = ROOT_PATH . 'upload/slider/' . $slide_id . '/';
				if (!file_exists($dir)) mkdir($dir);

				$this->load->helper('translit');

				$upload_config = array(
					'upload_path' => $dir,
					'overwrite' => FALSE,
					'file_name' => translit_filename($_FILES['slide_bg_' . LANG]['name']),
					'allowed_types' => 'gif|jpg|jpeg|png'
				);

				$this->load->library('upload', $upload_config);

				if ($this->upload->do_upload('slide_bg_' . LANG))
				{
					$file_name = $this->upload->data('file_name');
					$size = getimagesize($dir . $file_name);

					copy($dir . $file_name, $dir . 's_' . $file_name);

					$this->load->library('Image_lib');

					$this->image_lib->resize_crop($dir . $file_name, $dir . $file_name, $this->bg_big_sizes[0], $this->bg_big_sizes[1]);
					$this->image_lib->resize_crop($dir . $file_name, $dir . 't_' . $file_name, $this->bg_thumb_sizes[0], $this->bg_thumb_sizes[1]);

					$this->load->model('admin_slider_model');

					$old_file_name = $this->admin_slider_model->get_bg($slide_id, LANG);

					if ($old_file_name != '' AND $old_file_name != $file_name)
					{
						if (file_exists($dir . $old_file_name)) unlink($dir . $old_file_name);
						if (file_exists($dir . 's_' . $old_file_name)) unlink($dir . 's_' . $old_file_name);
						if (file_exists($dir . 't_' . $old_file_name)) unlink($dir . 't_' . $old_file_name);
					}

					$this->admin_slider_model->update_bg($slide_id, $file_name, LANG);

					$response['success'] = TRUE;
					$response['width'] = $size[0];
					$response['height'] = $size[1];
					$response['file_name'] = $file_name . '?t=' . time() . rand(10000, 1000000);
				}
			}

			$this->config->set_item('is_ajax_request', TRUE);
			return json_encode($response);
		}

		/**
		 * Обрізка фону
		 *
		 * @return string
		 */
		public function crop_bg()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$slide_id = intval($this->input->post('slide_id'));
			$coords = $this->input->post('coords');
			$coords = array_map('floatval', $coords);

			if (is_numeric($coords['x']) AND is_numeric($coords['y']) AND $coords['w'] >= 0 AND $coords['h'] >= 0 AND $slide_id > 0)
			{
				$this->load->model('admin_slider_model');
				$file_name = $this->admin_slider_model->get_bg($slide_id, LANG);

				if ($file_name !== NULL AND $file_name != '')
				{
					$dir = ROOT_PATH . 'upload/slider/' . $slide_id . '/';

					if (file_exists($dir . 's_' . $file_name))
					{
						$sizes = getimagesize($dir . 's_' . $file_name);
						$w_index = $sizes[0] / 520;
						$h_index = $sizes[1] / round((520 * $sizes[1]) / $sizes[0]);

						$x = $coords['x'] * $w_index;
						$y = $coords['y'] * $h_index;
						$x2 = $coords['x2'] * $w_index;
						$y2 = $coords['y2'] * $h_index;

						$this->load->library('Image_lib');
						$this->image_lib->crop($dir . 's_' . $file_name, $dir . $file_name, $x2 - $x, $y2 - $y, $x, $y, $this->bg_big_sizes[0], $this->bg_big_sizes[1]);
						$this->image_lib->resize_crop($dir . $file_name, $dir . 't_' . $file_name, $this->bg_thumb_sizes[0], $this->bg_thumb_sizes[1]);

						$response['image'] = '/upload/slider/' . $slide_id . '/t_' . $file_name . '?t' . time() . rand(10000, 1000000);
					}

					$response['success'] = TRUE;
				}
			}

			return json_encode($response);
		}

		/**
		 * Видалення фону
		 *
		 * @return string
		 */
		public function delete_bg()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$slide_id = intval($this->input->post('slide_id'));

			if ($slide_id > 0)
			{
				$this->load->model('admin_slider_model');
				$file_name = $this->admin_slider_model->get_bg($slide_id, LANG);

				if ($file_name !== NULL AND $file_name != '')
				{
					$dir = ROOT_PATH . 'upload/slider/' . $slide_id . '/';

					if ($file_name != '')
					{
						if (file_exists($dir . $file_name)) unlink($dir . $file_name);
						if (file_exists($dir . 's_' . $file_name)) unlink($dir . 's_' . $file_name);
						if (file_exists($dir . 't_' . $file_name)) unlink($dir . 't_' . $file_name);

						$this->admin_slider_model->update_bg($slide_id, '', LANG);
					}
				}

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Приховування слайду
		 *
		 * @return string
		 */
		public function hidden()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$slide_id = intval($this->input->post('slide_id'));
			$hidden = intval($this->input->post('hidden'));

			if ($slide_id > 0 AND is_numeric($hidden))
			{
				$this->load->model('admin_slider_model');

				$set = array('hidden' => $hidden);
				$where = array('slide_id' => $slide_id);
				$this->admin_slider_model->update($set, $where);

				$this->session->set_userdata('last_slide', $slide_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Збереження порядку сортування
		 */
		public function slides_position()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$slides = $this->input->post('slides');
			$slide_id = intval($this->input->post('slide_id'));

			if (is_array($slides))
			{
				$this->load->model('admin_slider_model');

				foreach ($slides as $key => $val)
				{
					if (is_numeric($key) AND is_numeric($val))
					{
						$set = array('position' => intval($key));
						$where = array('slide_id' => intval($val));
						$this->admin_slider_model->update($set, $where);
					}
				}

				$this->session->set_userdata('last_slide', $slide_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Видалення слайду
		 *
		 * @return string
		 */
		public function delete()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$slide_id = intval($this->input->post('slide_id'));

			if ($slide_id > 0)
			{
				$this->load->helper('file');

				$dir = ROOT_PATH . 'upload/slider/' . $slide_id . '/';
				if (file_exists($dir)) delete_files($dir, TRUE, FALSE, 1);

				$this->load->model('admin_slider_model');
				$this->admin_slider_model->delete($slide_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}
	}