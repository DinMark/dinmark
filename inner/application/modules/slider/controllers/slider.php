<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * @property Slider_model $slider_model
	 */

	class Slider extends MX_Controller
	{
		/**
		 * Отримання списку слайдів
		 */
		public function set_slider()
		{
			$this->load->model('slider_model');

			$slides = $this->slider_model->get_slides();
			$total = count($slides);

			if ($total > 0)
			{
				$slider = array(
					'styles' => '',
					'styles_bg' => '',
					'slides' => '',
					'slides_bg' => '',
					'pagination' => '',
					'total' => 0,
				);

				foreach ($slides as $key => $row)
				{
					if ($row['file_name'] != '' OR $row['def_file_name'] != '')
					{
						$row['url'] = '<a href="' . $row['url'] . '"></a>';

						if ($key == 0)
						{
							$slider['styles'] .= '.pic_' . $key . ' a{background: url("upload/slider/' . $row['slide_id'] . '/' . ($row['file_name'] != '' ? $row['file_name'] : $row['def_file_name']) . '") no-repeat 50% 50% / cover}';
							if ($total > 1) $slider['pagination'] .= '<a href="#" class="active"></a>';
							$slider['slides'] .= '<div class="main-slider_slide pic_' . $key . ' active">' . $row['url'] . '</div>';
						}
						else
						{
							$slider['styles'] .= '.pic_' . $key . ' a{background: url("upload/slider/' . $row['slide_id'] . '/' . ($row['file_name'] != '' ? $row['file_name'] : $row['def_file_name']) . '") no-repeat 50% 50% / cover;}';
							if ($total > 1) $slider['pagination'] .= '<a href="#"></a>';
							$slider['slides'] .= '<div class="main-slider_slide pic_' . $key . '">' . $row['url'] . '</div>';
						}

						if ($row['bg_file_name'] != '' OR $row['def_bg_file_name'] != '')
						{
							if ($key == 0)
							{
								$slider['styles_bg'] .= '.pic_bg_' . $key . ' a{background: url("upload/slider/' . $row['slide_id'] . '/' . ($row['bg_file_name'] != '' ? $row['bg_file_name'] : $row['def_bg_file_name']) . '") no-repeat 50% 50% / cover;}';
								$slider['slides_bg'] .= '<div class="slider_bg pic_bg_' . $key . ' active">' . $row['url'] . '</div>';
							}
							else
							{
								$slider['styles_bg'] .= '.pic_bg_' . $key . ' a{background: url("upload/slider/' . $row['slide_id'] . '/' . ($row['bg_file_name'] != '' ? $row['bg_file_name'] : $row['def_bg_file_name']) . '") no-repeat 50% 50% / cover; display: none}';
								$slider['slides_bg'] .= '<div class="slider_bg pic_bg_' . $key . '">' . $row['url'] . '</div>';
							}
						}

						$slider['total']++;
					}
				}

				if ($slider['total'] > 0) {
					$this->template_lib->set_js('slick.min.js');
					$this->template_lib->set_css('slick.css');
					$this->template_lib->set_template_var('slider', $slider);
				}
			}
		}
	}