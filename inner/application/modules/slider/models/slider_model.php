<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Slider_model extends CI_Model
	{
		public function get_slides()
		{
			$this->db->select('title_' . LANG . ' as title, title_' . DEF_LANG . ' as def_title');
			$this->db->select('sign_' . LANG . ' as sign, sign_' . DEF_LANG . ' as def_sign');
			$this->db->select('sign_2_' . LANG . ' as sign_2, sign_2_' . DEF_LANG . ' as def_sign_2');
			$this->db->select('slide_id, url_' . LANG . ' as url, file_name_' . LANG . ' as file_name, file_name_' . DEF_LANG . ' as def_file_name, bg_file_name_' . LANG . ' as bg_file_name, bg_file_name_' . DEF_LANG . ' as def_bg_file_name');
			$this->db->where('hidden', 0);
			$this->db->where('hidden_' . LANG, 0);
			$this->db->order_by('position');

			return $this->db->get('slider')->result_array();
		}
	}