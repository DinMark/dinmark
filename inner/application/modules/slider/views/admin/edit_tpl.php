<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	$this->template_lib->set_css('js/admin/fineuploader/fineuploader-3.5.0.css', TRUE);
	$this->template_lib->set_js('admin/fineuploader/jquery.fineuploader-3.5.0.min.js');

	$this->template_lib->set_css('js/admin/jcrop/css/jquery.Jcrop.min.css', TRUE);
	$this->template_lib->set_js('admin/jcrop/js/jquery.Jcrop.min.js');

	$this->template_lib->set_js('admin/jquery.form.js');

	$sizes = array();
	$bg_sizes = array();
?>
<div class="fm admin_component">
	<div class="component_loader"></div>

	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="gallery"></div>
		</div>
		<div class="fm component_edit_links">
			<a href="#" class="fm save"><b></b>Зберегти</a>
			<a href="#" class="fm apply"><b></b>Застосувати</a>
			<a href="<?=$this->uri->full_url('admin/slider/index?menu_id=' . $menu_id);?>" class="fm cancel"><b></b>До списку слайдів</a>
		</div>
		<div class="fmr component_lang" <?php if (count($languages) == 1): ?> style="display:none"<?php endif; ?>>
			<?php foreach ($languages as $key => $val): ?>
				<a href="#" class="flags <?=$key;?><?=(($key == LANG) ? ' active' : '');?>" data-language="<?=$key;?>">
					<img src="<?=base_url('img/flags_' . $key . '.png');?>" alt="">
				</a>
			<?php endforeach; ?>
		</div>
	</div>

	<form id="slide_form" action="<?=$this->uri->full_url('admin/slider/save');?>" method="post">
		<input type="hidden" name="slide_id" value="<?=$slide_id;?>">
		<?php foreach ($languages as $key => $val): ?>
		<?php
			$sizes[$key] = array(0, 0);
			if ($slide['file_name_' . $key] != '') $sizes[$key] = getimagesize(ROOT_PATH . 'upload/slider/' . $slide_id . '/s_' . $slide['file_name_' . $key]);

			$bg_sizes[$key] = array(0, 0);
			if ($slide['bg_file_name_' . $key] != '') $bg_sizes[$key] = getimagesize(ROOT_PATH . 'upload/slider/' . $slide_id . '/s_' . $slide['bg_file_name_' . $key]);
		?>
		<div class="lang_tab" id="box_<?=$key;?>"<?php if (LANG != $key) echo ' style="display:none"'; ?>>
			<div class="evry_title">
				<label class="block_label">Назва:</label>
				<input type="text" name="title[<?=$key;?>]" value="<?=$slide['title_' . $key];?>">
			</div>
			<div class="evry_title">
				<label class="block_label">Надпис 1:</label>
				<input type="text" name="sign[<?=$key;?>]" value="<?=$slide['sign_' . $key];?>">
			</div>
			<div class="evry_title">
				<label class="block_label">Надпис 2:</label>
				<input type="text" name="sign_2[<?=$key;?>]" value="<?=$slide['sign_2_' . $key];?>">
			</div>
			<div class="evry_title">
				<label class="block_label">Посилання:</label>
				<input type="text" name="url[<?=$key;?>]" value="<?=$slide['url_' . $key];?>">
			</div>
			<div class="evry_title">
				<label class="block_label">Зображення:</label>
				<div class="fm" id="photo_uploader_box_<?=$key;?>"<?php if ($slide['file_name_' . $key] != '') echo ' style="display: none"'; ?>><div id="photo_uploader_<?=$key;?>"></div></div>
				<div class="fm for_photo_cut" id="photo_box_<?=$key;?>" style="<?php if ($slide['file_name_' . $key] == '') echo ' display: none'; ?>">
					<div class="fm photo_cut" id="big_photo_<?=$key;?>">
						<div class="fm photo_holder" style="width:<?=$thumb[0];?>px; height:<?=$thumb[1];?>px;">
							<div style="width:<?=$thumb[0];?>px; height:<?=$thumb[1];?>px;">
								<?php if ($slide['file_name_' . $key] != ''): ?><img src="upload/slider/<?=$slide_id;?>/t_<?=$slide['file_name_' . $key];?>" alt=""><?php endif; ?>
							</div>
						</div>
						<div class="links">
							<a href="#" id="crop_photo_<?=$key;?>" class="fm fpc_edit" data-width="<?=$sizes[$key][0];?>" data-height="<?=$sizes[$key][1];?>"><b></b>Редагувати</a>
							<a href="#" id="delete_photo_<?=$key;?>" class="fm fpc_delete"><b></b>Видалити</a>
						</div>
					</div>
				</div>
				<div class="fm crop_wrapper" id="crop_box_<?=$key;?>"></div>
			</div>
			<div class="evry_title">
				<label class="block_label">Фон:</label>
				<div class="fm" id="bg_uploader_box_<?=$key;?>"<?php if ($slide['bg_file_name_' . $key] != '') echo ' style="display: none"'; ?>><div id="bg_uploader_<?=$key;?>"></div></div>
				<div class="fm for_photo_cut" id="bg_box_<?=$key;?>" style="<?php if ($slide['bg_file_name_' . $key] == '') echo ' display: none'; ?>">
					<div class="fm photo_cut" id="big_bg_<?=$key;?>">
						<div class="fm photo_holder" style="width:<?=$bg_thumb[0];?>px; height:<?=$bg_thumb[1];?>px;">
							<div style="width:<?=$bg_thumb[0];?>px; height:<?=$bg_thumb[1];?>px;">
								<?php if ($slide['bg_file_name_' . $key] != ''): ?><img src="upload/slider/<?=$slide_id;?>/t_<?=$slide['bg_file_name_' . $key];?>" alt=""><?php endif; ?>
							</div>
						</div>
						<div class="links">
							<a href="#" id="crop_bg_<?=$key;?>" class="fm fpc_edit" data-width="<?=$bg_sizes[$key][0];?>" data-height="<?=$bg_sizes[$key][1];?>"><b></b>Редагувати</a>
							<a href="#" id="delete_bg_<?=$key;?>" class="fm fpc_delete"><b></b>Видалити</a>
						</div>
					</div>
				</div>
				<div class="fm crop_wrapper" id="crop_bg_box_<?=$key;?>"></div>
			</div>
		</div>
		<?php endforeach; ?>
		<div class="fm for_sucsess">
			<div class="fmr save_links">
				<a href="#" class="fm save_adm"><b></b>Зберегти</a>
				<a href="#" class="fm apply_adm"><b></b>Застосувати</a>
				<a href="<?=$this->uri->full_url('admin/slider/index?menu_id=' . $menu_id);?>" class="fm cansel_adm"><b></b>До списку слайдів</a>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	var source = {
			ua: '<?=($slide['file_name_ua'] != '') ? ('/upload/slider/' . $slide_id . '/s_' . $slide['file_name_ua']) : '';?>',
			ru: '<?=($slide['file_name_ru'] != '') ? ('/upload/slider/' . $slide_id . '/s_' . $slide['file_name_ru']) : '';?>',
			en: '<?=($slide['file_name_en'] != '') ? ('/upload/slider/' . $slide_id . '/s_' . $slide['file_name_en']) : '';?>'
		},
		api = {
			ua: null,
			ru: null,
			en: null
		},

		bg_source = {
			ua: '<?=($slide['bg_file_name_ua'] != '') ? ('/upload/slider/' . $slide_id . '/s_' . $slide['bg_file_name_ua']) : '';?>',
			ru: '<?=($slide['bg_file_name_ru'] != '') ? ('/upload/slider/' . $slide_id . '/s_' . $slide['bg_file_name_ru']) : '';?>',
			en: '<?=($slide['bg_file_name_en'] != '') ? ('/upload/slider/' . $slide_id . '/s_' . $slide['bg_file_name_en']) : '';?>'
		},
		bg_api = {
			ua: null,
			ru: null,
			en: null
		};

	$(document).ready(function () {
		$('.component_lang').find('a').each(function () {
			var language = $(this).data('language');

			$(this).on('click', function (e) {
				e.preventDefault();

				var language = $(this).data('language');

				$(this).addClass('active').siblings().removeClass('active');
				$('.lang_tab').hide();
				$('#box_' + language).show();
			});

			/* Slide Image */

			$('#photo_uploader_' + language)
				.fineUploader({
					request: {
						endpoint: (language !== DEF_LANG ? '/' + language : '') + '/admin/slider/upload/',
						inputName: 'slide_image_' + language,
						params: {
							slide_id : <?=$slide_id;?>
						}
					},
					multiple: false,
					text: {
						uploadButton: 'Виберіть або перетягніть файл зображення',
						dragZone: '',
						dropProcessing: ''
					},
					validation: {
						allowedExtensions: ['jpeg', 'jpg', 'png', 'gif'],
						sizeLimit: <?php echo intval(ini_get('upload_max_filesize')) * 1048576; ?>
					},
					messages: {
						typeError: "Дозволено завантажувати: {extensions}.",
						sizeError: "Розмір файлу не повинен перевищувати {sizeLimit}.",
						tooManyproductsError: "Дозволено завантажувати файлів: {productLimit}."
					}
				})
				.on('complete', function (event, id, fileName, response) {
					if (response.success) {
						$('.qq-upload-success').remove();
						source[language] = '/upload/slider/<?=$slide_id;?>/s_' + response.file_name;

						$('#photo_uploader_box_' + language).hide();
						$('#big_photo_' + language).html('<div class="fm photo_holder" style="width:<?=$thumb[0];?>px; height:<?=$thumb[1];?>px;"><div style="width:<?=$thumb[0];?>px; height:<?=$thumb[1];?>px;"><img src="/upload/slider/<?=$slide_id;?>/t_' + response.file_name + '" alt=""></div></div><div class="links"><a href="#" id="crop_photo_' + language + '" class="fm fpc_edit" data-width="' + response.width + '" data-height="' + response.height + '"><b></b>Редагувати</a><a href="#" id="delete_photo_' + language + '" class="fm fpc_delete"><b></b>Видалити</a></div>');
						$('#photo_box_' + language).show();
					}
				});

			$('#photo_box_' + language)
				.on('click', '#delete_photo_' + language, function (e) {
					e.preventDefault();

					component_loader_show($('.component_loader'), '');

					$.post(
                        (language !== DEF_LANG ? '/' + language : '') + '/admin/slider/delete_photo/',
						{
							slide_id: <?=$slide_id;?>
						},
						function (response) {
							if (response.success) {
								component_loader_hide($('.component_loader'), '');
								$('#photo_box_' + language).hide('').find('#big_photo_' + language).html('');
								$('#photo_uploader_box_' + language).show();
							}
						},
						'json'
					);
				})
				.on('click', '#crop_photo_' + language, function (e) {
					e.preventDefault();

					$('.for_sucsess').hide();

					if ($.type(api[language]) === 'object') {
						api[language].destroy();
						api[language] = null;
						$('#crop_thumb_box_' + language).html('');
					}

					var $link = $(this),
						width = $link.data('width') > 520 ? 520 : $link.data('width'),
						height = width * $link.data('height') / $link.data('width'),
						resizer = '<div class="fm crop_footer"><div id="crop_preview_' + language + '" style="width:520px;height:148px;overflow:hidden;margin-top:5px"><img src="' + source[language] + '" alt=""></div></div><u><br></u><div class="fm crop_header"><a href="#" class="save_crop"></a><a href="#" class="cansel_crop"></a></div><div class="fm crop_area"><div class="fm ca_panel"><a href="#" class="fmr ca_cencel"><b></b>Скасувати</a><a href="#" class="fmr ca_save"><b></b>Зберегти</a></div><img width="' + width + '" height="' + height + '" src="' + source[language] + '"></div>';

					$('#photo_box_' + language).hide();
					$('#crop_box_' + language)
						.html(resizer)
						.find('img').eq(1).Jcrop({
							keySupport: false,
							aspectRatio: <?=$big[0]/$big[1];?>,
							setSelect: [0, 0, 520, 148],
							realSizes: [$link.data('width'), $link.data('height')],
							onChange: function (coords) {
								crop_preview('#crop_preview_' + language, coords, 520, 148, width, height);
							}
						},
						function () {
							api[language] = this;

							$('#crop_box_' + language)
								.find('.ca_cencel').off('click').on('click', function (e) {
									e.preventDefault();

									api[language].destroy();
									$('#crop_box_' + language).html('');
									$('#photo_box_' + language).show();
									$('.for_sucsess').show();
								})
								.end()
								.find('.ca_save').on('click', function (e) {
									e.preventDefault();

									$.post(
                                        (language !== DEF_LANG ? '/' + language : '') + '/admin/slider/crop/',
										{
											slide_id: <?=$slide['slide_id'];?>,
											coords: api[language].tellScaled()
										},
										function (response) {
											if (response.error === 0) {
												api[language].destroy();
												$('#crop_box_' + language).html('');
												$('#photo_box_' + language).show().find('img').eq(0).attr('src', response.image);
												$('.for_sucsess').show();
											}
										},
										'json'
									);
								});
						});
				});

			/* Slide bg */
			$('#bg_uploader_' + language)
				.fineUploader({
					request: {
						endpoint: (language !== DEF_LANG ? '/' + language : '') + '/admin/slider/upload_bg/',
						inputName: 'slide_bg_' + language,
						params: {
							slide_id : <?=$slide_id;?>
						}
					},
					multiple: false,
					text: {
						uploadButton: 'Виберіть або перетягніть файл зображення',
						dragZone: '',
						dropProcessing: ''
					},
					validation: {
						allowedExtensions: ['jpeg', 'jpg', 'png', 'gif'],
						sizeLimit: <?php echo intval(ini_get('upload_max_filesize')) * 1048576; ?>
					},
					messages: {
						typeError: "Дозволено завантажувати: {extensions}.",
						sizeError: "Розмір файлу не повинен перевищувати {sizeLimit}.",
						tooManyproductsError: "Дозволено завантажувати файлів: {productLimit}."
					}
				})
				.on('complete', function (event, id, fileName, response) {
					if (response.success) {
						$('.qq-upload-success').remove();
						bg_source[language] = '/upload/slider/<?=$slide_id;?>/s_' + response.file_name;

						$('#bg_uploader_box_' + language).hide();
						$('#big_bg_' + language).html('<div class="fm photo_holder" style="width:<?=$bg_thumb[0];?>px; height:<?=$bg_thumb[1];?>px;"><div style="width:<?=$bg_thumb[0];?>px; height:<?=$bg_thumb[1];?>px;"><img src="/upload/slider/<?=$slide_id;?>/t_' + response.file_name + '" alt=""></div></div><div class="links"><a href="#" id="crop_bg_' + language + '" class="fm fpc_edit" data-width="' + response.width + '" data-height="' + response.height + '"><b></b>Редагувати</a><a href="#" id="delete_bg_' + language + '" class="fm fpc_delete"><b></b>Видалити</a></div>');
						$('#bg_box_' + language).show();
					}
				});

			$('#bg_box_' + language)
				.on('click', '#delete_bg_' + language, function (e) {
					e.preventDefault();

					component_loader_show($('.component_loader'), '');

					$.post(
                        (language !== DEF_LANG ? '/' + language : '') + '/admin/slider/delete_bg/',
						{
							slide_id: <?=$slide_id;?>
						},
						function (response) {
							if (response.success) {
								component_loader_hide($('.component_loader'), '');
								$('#bg_box_' + language).hide('').find('#big_bg_' + language).html('');
								$('#bg_uploader_box_' + language).show();
							}
						},
						'json'
					);
				})
				.on('click', '#crop_bg_' + language, function (e) {
					e.preventDefault();

					$('.for_sucsess').hide();

					if ($.type(bg_api[language]) === 'object') {
						bg_api[language].destroy();
						bg_api[language] = null;
						$('#crop_bg_box_' + language).html('');
					}

					var $link = $(this),
						width = $link.data('width') > 520 ? 520 : $link.data('width'),
						height = width * $link.data('height') / $link.data('width'),
						resizer = '<div class="fm crop_footer"><div id="crop_preview_' + language + '" style="width:520px;height:107px;overflow:hidden;margin-top:5px"><img src="' + bg_source[language] + '" alt=""></div></div><u><br></u><div class="fm crop_header"><a href="#" class="save_crop"></a><a href="#" class="cansel_crop"></a></div><div class="fm crop_area"><div class="fm ca_panel"><a href="#" class="fmr ca_cencel"><b></b>Скасувати</a><a href="#" class="fmr ca_save"><b></b>Зберегти</a></div><img width="' + width + '" height="' + height + '" src="' + bg_source[language] + '"></div>';

					$('#bg_box_' + language).hide();
					$('#crop_bg_box_' + language)
						.html(resizer)
						.find('img').eq(1).Jcrop({
							keySupport: false,
							aspectRatio: <?=$big[0]/$big[1];?>,
							setSelect: [0, 0, 520, 107],
							realSizes: [$link.data('width'), $link.data('height')],
							onChange: function (coords) {
								crop_preview('#crop_preview_' + language, coords, 520, 107, width, height);
							}
						},
						function () {
							bg_api[language] = this;

							$('#crop_bg_box_' + language)
								.find('.ca_cencel').off('click').on('click', function (e) {
									e.preventDefault();

									bg_api[language].destroy();
									$('#crop_bg_box_' + language).html('');
									$('#bg_box_' + language).show();
									$('.for_sucsess').show();
								})
								.end()
								.find('.ca_save').on('click', function (e) {
									e.preventDefault();

									$.post(
                                        (language !== DEF_LANG ? '/' + language : '') + '/admin/slider/crop_bg/',
										{
											slide_id: <?=$slide['slide_id'];?>,
											coords: bg_api[language].tellScaled()
										},
										function (response) {
											if (response.success) {
												bg_api[language].destroy();
												$('#crop_bg_box_' + language).html('');
												$('#bg_box_' + language).show().find('img').eq(0).attr('src', response.image);
												$('.for_sucsess').show();
											}
										},
										'json'
									);
								});
						});
				});
		});

		/**
		 * Відправка форми
		 */
		$('.save_adm').add('.component_edit_links .save').on('click', function (e) {
			e.preventDefault();

			$('#slide_form').ajaxSubmit({
				beforeSubmit:function () {
					component_loader_show($('.component_loader'), '');
				},
				success:function (response) {
					if (response.success) {
						component_loader_hide($('.component_loader'), '');
						window.location.href = '<?=$this->uri->full_url('admin/slider/index?menu_id=' . $menu_id);?>';
					}
				},
				dataType: 'json'
			});
		});

		/**
		 * Відправка форми
		 */
		$('a.apply_adm').add('.component_edit_links .apply').on('click', function (e) {
			e.preventDefault();

			$('#slide_form').ajaxSubmit({
				beforeSubmit:function () {
					component_loader_show($('.component_loader'), '');
				},
				success:function (response) {
					if (response.success) {
						component_loader_hide($('.component_loader'), '');
					}
				},
				dataType: 'json'
			});
		});
	});
</script>