<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_import_users_model extends CI_Model
	{
		protected $parser;
		private $current_tag = '';

		private $user_block = FALSE;
		private $carts_block = FALSE;
		private $cart_block = FALSE;

		private $user_data = array();
		private $cart_data = array();

		private $user_order_index = 1;

		// Методи парсера ==============================================================================================

		/**
		 * Запуск обробки файлу
		 *
		 * @param string $file_name
		 */
		public function run($file_name)
		{
			$this->parser = xml_parser_create();
			xml_parser_set_option($this->parser, XML_OPTION_TARGET_ENCODING, 'UTF-8');
			xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);

			xml_set_object($this->parser, $this);
			xml_set_element_handler($this->parser, "tag_open", "tag_close");
			xml_set_character_data_handler($this->parser, "cdata");

			$result = $this->db->select_max('order_index')->get('import_users')->row_array();

			if (isset($result['order_index']))
			{
				$this->user_order_index = $result['order_index'] + 1;
			}

			$fp = fopen($file_name, 'r');

			while ($data = fread($fp, 4096))
			{
				xml_parse($this->parser, $data, feof($fp));
			}
		}

		/**
		 * Обробка відкриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 * @param array $element_attrs
		 */
		private function tag_open($parser, $tag, $element_attrs)
		{
			if ($tag == 'Контрагент')
			{
				$this->user_block = TRUE;
			}

			if ($this->user_block)
			{
				if ($tag == 'ДисконтныеКарты')
				{
					$this->carts_block = TRUE;
				}

				if ($tag == 'Карта')
				{
					$this->cart_block = TRUE;
				}
			}

			$this->current_tag = $tag;
		}

		/**
		 * Обробка закриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 */
		private function tag_close($parser, $tag)
		{
			if ($this->user_block)
			{
				if ($tag == 'Контрагент')
				{
					$this->user_block = FALSE;
					$this->insert_user();
				}

				if ($tag == 'ДисконтныеКарты')
				{
					$this->carts_block = FALSE;
				}

				if ($tag == 'Карта')
				{
					$this->cart_block = FALSE;

					if (count($this->cart_data) > 0)
					{
						$this->user_data['carts'][] = $this->cart_data;
						$this->cart_data = array();
					}
				}
			}

			$this->current_tag = '';
		}

		/**
		 * Обробка значення тегу
		 *
		 * @param object $parser
		 * @param mixed $data
		 */
		private function cdata($parser, $data)
		{
			if ($this->user_block)
			{
				if ($this->cart_block)
				{
					if ($this->current_tag == 'Ид')
					{
						if (!isset($this->cart_data['id']))
						{
							$this->cart_data['id'] = $data;
						}
						else
						{
							$this->cart_data['id'] .= $data;
						}
					}

					if ($this->current_tag == 'КодКарты')
					{
						if (!isset($this->cart_data['code']))
						{
							$this->cart_data['code'] = $data;
						}
						else
						{
							$this->cart_data['code'] .= $data;
						}
					}

					if ($this->current_tag == 'ИдВида')
					{
						if (!isset($this->cart_data['type_id']))
						{
							$this->cart_data['type_id'] = $data;
						}
						else
						{
							$this->cart_data['type_id'] .= $data;
						}
					}

					if ($this->current_tag == 'ЗначениеНакопительнойСкидки_Процент')
					{
						if (!isset($this->cart_data['type_name']))
						{
							$this->cart_data['type_name'] = $data;
						}
						else
						{
							$this->cart_data['type_name'] .= $data;
						}
					}

					if ($this->current_tag == 'Оборот')
					{
						if (!isset($this->cart_data['cash']))
						{
							$this->cart_data['cash'] = $data;
						}
						else
						{
							$this->cart_data['cash'] .= $data;
						}
					}
				}
				else
				{
					if ($this->current_tag == 'Ид')
					{
						if (!isset($this->user_data['id']))
						{
							$this->user_data['id'] = $data;
						}
						else
						{
							$this->user_data['id'] .= $data;
						}
					}

					if ($this->current_tag == 'Наименование')
					{
						if (!isset($this->user_data['name']))
						{
							$this->user_data['name'] = $data;
						}
						else
						{
							$this->user_data['name'] .= $data;
						}
					}
					
					if ($this->current_tag == 'НомерТелефона')
					{
						if (!isset($this->user_data['phone']))
						{
							$this->user_data['phone'] = $data;
						}
						else
						{
							$this->user_data['phone'] .= $data;
						}
					}
				}
			}
		}

		/**
		 * Додавання користувача в чергу
		 */
		private function insert_user()
		{
			$set = array(
				'order_index' => $this->user_order_index,
				'data' => serialize($this->user_data)
			);
			$this->db->insert('import_users', $set);

			$this->user_order_index++;

			$this->user_data = array();
		}

		/**
		 * Деструктор класу
		 */
		public function __destruct()
		{
			xml_parser_free($this->parser);
		}
	}