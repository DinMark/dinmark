<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_import_categories_model extends CI_Model
	{
		private $_parser;

		private $categories_block = false;
		private $category_block = false;

		private $xml = '';

		private $menu_cache = array();

		// Методи парсера ==============================================================================================

		/**
		 * Запуск обробки файлу
		 *
		 * @param string $file_name
		 */
		public function run($file_name)
		{
			$this->_parser = xml_parser_create();

			xml_parser_set_option($this->_parser, XML_OPTION_TARGET_ENCODING, 'UTF-8');
			xml_parser_set_option($this->_parser, XML_OPTION_CASE_FOLDING, 0);
			xml_parser_set_option($this->_parser, XML_OPTION_SKIP_WHITE, 1);

			xml_set_object($this->_parser, $this);
			xml_set_element_handler($this->_parser, 'tag_open', 'tag_close');
			xml_set_character_data_handler($this->_parser, 'cdata');

			$fp = fopen($file_name, 'r');
            error_log ($file_name, 1, "s@zk.dp.ua");
			while ($data = fread($fp, 4096))
			{
				xml_parse($this->_parser, $data, feof($fp));
			}
		}

		/**
		 * Обробка відкриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 * @param array $element_attrs
		 */
		private function tag_open($parser, $tag, $element_attrs)
		{
			if ($tag === 'Классификатор') {
				$this->categories_block = true;
			}

			if ($tag === 'Группы') {
				$this->category_block = true;
			}

			if ($this->category_block) {
				$this->xml .= '<' . $tag . '>';
			}
		}

		/**
		 * Обробка закриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 */
		private function tag_close($parser, $tag)
		{
			if ($tag === 'Классификатор') {
				$this->categories_block = false;
				$this->category_block = false;
				$this->save_categories();
			}

			if ($this->category_block) {
				$this->xml .= '</' . $tag . '>';
			}
		}

		/**
		 * Обробка значення тегу
		 *
		 * @param object $parser
		 * @param mixed $data
		 */
		private function cdata($parser, $data)
		{
			if ($this->category_block) {
				$this->xml .= $data;
			}
		}

		/**
		 * Збереження
		 */
		private function save_categories()
		{
			$xml = simplexml_load_string($this->xml);

			if (is_object($xml)) {
				foreach ($xml as $level) {
					$this->save_level($level, 0);
				}
			}
		}

		/**
		 * Обробка рівня
		 *
		 * @param object $level
		 * @param int $parent_id
		 */
		private function save_level($level, $parent_id)
		{
			if (isset($level->{'Товар'})) {
				if ((string)$level->{'Товар'} === 'false') {
					$parent_id = $this->save_category(
						(string)$level->{'Ид'},
						(string)$level->{'Наименование'},
						(string)$level->{'Наименование_ru'},
						$parent_id
					);
				} else {
					$this->save_product(
						(string)$level->{'Ид'},
						(string)$level->{'Наименование'},
						(string)$level->{'Наименование_ru'},
						$parent_id
					);
				}
			}

			if (isset($level->{'Группы'}->{'Группа'})) {
				foreach ($level->{'Группы'}->{'Группа'} as $group) {
					$this->save_level($group, $parent_id);
				}
			}
		}

		/**
		 * Збереження категорії
		 *
		 * @param string $_1s_id
		 * @param string $name
		 * @param string $name_ru
		 * @param int $parent_id
		 * @return int
		 */
		private function save_category($_1s_id, $name, $name_ru, $parent_id) {
			$level = 0;
			$url_path_id = '';

			if ($parent_id > 0) {
				if (array_key_exists($parent_id, $this->menu_cache)) {
					$level = $this->menu_cache[$parent_id]['level'];
					$url_path_id = $this->menu_cache[$parent_id]['url_path_id'];
				} else {
					$r = $this->db
						->select('level, url_path_id')
						->where('id', $parent_id)
						->get('menu')
						->row_array();

					if ($r !== null) {
						$level = $r['level'] + 1;
						$url_path_id = $r['url_path_id'];

						$this->menu_cache[$parent_id] = array(
							'level' => $r['level'],
							'url_path_id' => $r['url_path_id'],
						);
					}
				}
			}

			if ($parent_id > 0) {
				$url_path_id .= $url_path_id !== '' ? $parent_id . '.' : '.' . $parent_id . '.';
			}

			$r = $this->db
				->select('id, url_ua, url_ru')
				->where('1s_id', form_prep($_1s_id))
				->get('menu')
				->row_array();

			if ($r === null) {
				$rp = $this->db
					->select_max('position', 'max_position')
					->where('parent_id', $parent_id)
					->get('menu')
					->row_array();

				$this->db->insert(
					'menu',
					array(
						'parent_id' => $parent_id,
						'level' => $level,
						'menu_index' => 1,
						'position' => $rp['max_position'] + 1,
						'1s_id' => form_prep($_1s_id),
						'url_path_id' => $url_path_id,
						'name_ua' => form_prep($name),
						'name_ru' => form_prep($name_ru),
						'update' => time(),
					)
				);

				$menu_id = $this->db->insert_id();

				$url = translit($name);
				$url_ru = translit($name_ru);

				$c = $this->db
					->where('hash_ua', md5($url))
					->count_all_results('site_links');

				if ($c > 0) {
					$url .= '-' . $menu_id;
				}

				$c = $this->db
					->where('hash_ru', md5($url_ru))
					->count_all_results('site_links');

				if ($c > 0) {
					$url_ru .= '-' . $menu_id;
				}

				$this->db
					->set('url_ua', $url)
					->set('url_path_ua', $url)
					->set('url_ru', $url_ru)
					->set('url_path_ru', $url_ru)
					->where('id', $menu_id)
					->update('menu');

				$this->db->insert(
					'site_links',
					array(
						'item_id' => $menu_id,
						'menu_id' => $menu_id,
						'module' => 'components',
						'method' => 'get_components',
						'hash_ua' => md5($url),
						'hash_ru' => md5($url_ru),
					)
				);

				$this->db->insert(
					'components',
					array(
						'menu_id' => $menu_id,
						'position' => 0,
						'module' => 'catalog',
						'method' => 'index',
					)
				);
			} else {
				$set = array(
					'parent_id' => $parent_id,
					'level' => $level,
					'menu_index' => 1,
					'url_path_id' => $url_path_id,
                    'name_ua' => form_prep($name),
                    'name_ru' => form_prep($name_ru),
					'update' => time(),
				);

				if ($r['url_ua'] === '') {
					$url = translit($name);

					$c = $this->db
						->where('item_id != ', $r['id'])
						->where('menu_id != ', $r['id'])
						->where('hash_ua', md5($url))
						->count_all_results('site_links');

					if ($c > 0) {
						$url .= '-' . $r['id'];
					}

					$set['url_ua'] = $url;
					$set['url_path_ua'] = $url;

					$this->db->update(
						'site_links',
						array(
							'hash_ua' => md5($url),
						),
						array(
							'item_id' => $r['id'],
							'module' => 'components',
						)
					);
				}

				if ($r['url_ru'] === '') {
					$url = translit($name);

					$c = $this->db
                        ->where('item_id != ', $r['id'])
                        ->where('menu_id != ', $r['id'])
						->where('hash_ru', md5($url))
						->count_all_results('site_links');

					if ($c > 0) {
						$url .= '-' . $r['id'];
					}

					$set['url_ru'] = $url;
					$set['url_path_ru'] = $url;

					$this->db->update(
						'site_links',
						array(
							'hash_ru' => md5($url),
						),
						array(
							'item_id' => $r['id'],
							'module' => 'components',
						)
					);
				}

				$this->db->update(
					'menu',
					$set,
					array(
						'id' => $r['id'],
					)
				);

				$menu_id = $r['id'];
			}

			return $menu_id;
		}

		/**
		 * Збереження товару
		 *
		 * @param string $_1s_id
		 * @param string $name
		 * @param string $name_ru
		 * @param int $menu_id
		 * @return int
		 */
		private function save_product($_1s_id, $name, $name_ru, $menu_id)
		{
			$r = $this->db
				->select('product_id, menu_id, component_id, url_ua, url_ru')
				->where('1s_id', form_prep($_1s_id))
				->get('catalog')
				->row_array();

			$url = translit($name);
			$url_ru = translit($name_ru);

			if ($r === null) {
                $rc = $this->db
                    ->select('component_id')
                    ->where('menu_id', $menu_id)
                    ->get('components')
                    ->row_array();

                $component_id = $rc['component_id'];

				$rp = $this->db
					->select_max('position', 'max_position')
					->where('component_id', $component_id)
					->get('catalog')
					->row_array();
if ($component_id===NULL) {
   // echo form_prep($name).':::::'.$name_ru.':::menu_id='.$menu_id;
 //   $component_id=0;
}

				$this->db->insert(
					'catalog',
					array(
						'menu_id' => $menu_id,
						'component_id' => $component_id,
						'position' => $rp['max_position'] + 1,
						'1s_id' => form_prep($_1s_id),
						'title_ua' => form_prep($name),
						'title_ru' => form_prep($name_ru),
						'url_ua' => $url,
						'url_ru' => $url_ru,
						'update' => time(),
					)
				);

				$product_id = (int)$this->db->insert_id();
			} else {
				$product_id = (int)$r['product_id'];
			}

			if (!is_array($r)) {
				$this->db->insert(
					'catalog_product_bind_components',
					array(
						'product_id' => $product_id,
						'component_id' => $component_id,
						'menu_id' => $menu_id,
					)
				);

				$this->db->insert(
					'site_links',
					array(
						'item_id' => $product_id,
						'component_id' => $component_id,
						'menu_id' => $menu_id,
						'module' => 'catalog',
						'method' => 'details',
						'hash_ua' => md5($url),
						'hash_ru' => md5($url_ru),
					)
				);

				$this->db->insert(
					'seo_tags',
					array(
						'item_id' => $product_id,
						'component_id' => $component_id,
						'menu_id' => $menu_id,
						'module' => 'catalog',
					)
				);
			} else {
				if ($r['url_ua'] !== $url) {
					$this->db->update(
						'catalog',
						array(
                            'title_ua' => form_prep($name),
							'url_ua' => $url,
						),
						array(
							'product_id' => $product_id,
						)
					);

					$this->db->update(
						'site_links',
						array(
							'hash_ua' => md5($url),
						),
						array(
							'item_id' => $product_id,
							'module' => 'catalog',
							'method' => 'details',
						)
					);
				}

                if ($r['url_ru'] !== $url_ru) {
                    $this->db->update(
                        'catalog',
                        array(
                            'title_ru' => form_prep($name_ru),
                            'url_ru' => $url_ru,
                        ),
                        array(
                            'product_id' => $product_id,
                        )
                    );

                    $this->db->update(
                        'site_links',
                        array(
                            'hash_ru' => md5($url_ru),
                        ),
                        array(
                            'item_id' => $product_id,
                            'module' => 'catalog',
                            'method' => 'details',
                        )
                    );
                }
			}
		}

		public function __destruct()
		{
			xml_parser_free($this->_parser);
		}
	}