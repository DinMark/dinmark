<?php defined('ROOT_PATH') OR exit('No direct script access allowed');
	
	//header('Content-Type: text/html; charset=utf-8');
	
	class Admin_import_filters_model extends CI_Model
	{
		protected $parser;
		private $current_tag = '';

		// Опції товарів
		private $filters_block = FALSE;
		private $filter_block = FALSE;
		private $filter_variants_block = FALSE;
		private $filter_variant_block = FALSE;

		private $filter_data = array();
		private $filter_variant_data = array();

		private $filters_order_index = 1;

		// Методи парсера ==============================================================================================

		/**
		 * Запуск обробки файлу
		 *
		 * @param string $file_name
		 */
		public function run($file_name)
		{
			$this->parser = xml_parser_create();
			xml_parser_set_option($this->parser, XML_OPTION_TARGET_ENCODING, 'UTF-8');
			xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);

			xml_set_object($this->parser, $this);
			xml_set_element_handler($this->parser, "tag_open", "tag_close");
			xml_set_character_data_handler($this->parser, "cdata");

			$result = $this->db->select_max('order_index')->get('import_filters')->row_array();

			if (isset($result['order_index']))
			{
				$this->filters_order_index = $result['order_index'] + 1;
			}

			$fp = fopen($file_name, 'r');

			while ($data = fread($fp, 4096))
			{
				xml_parse($this->parser, $data, feof($fp));
			}
		}

		/**
		 * Обробка відкриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 * @param array $element_attrs
		 */
		private function tag_open($parser, $tag, $element_attrs)
		{
			// Обробка опцій
			if ($tag == 'Свойства')
			{
				$this->filters_block = TRUE;
			}

			if ($this->filters_block)
			{
				if ($tag == 'Свойство')
				{
					$this->filter_block = TRUE;
				}

				if ($tag == 'ВариантыЗначений')
				{
					$this->filter_variants_block = TRUE;
				}

				if ($tag == 'Справочник')
				{
					$this->filter_variant_block = TRUE;
				}
			}

			$this->current_tag = $tag;
		}

		/**
		 * Обробка закриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 */
		private function tag_close($parser, $tag)
		{
			if ($this->filters_block)
			{
				if ($tag == 'Свойства')
				{
					$this->filters_block = FALSE;
				}

				if ($tag == 'Свойство')
				{
					$this->filter_block = FALSE;
					$this->insert_filter();
				}

				if ($tag == 'ВариантыЗначений')
				{
					$this->filter_variants_block = FALSE;
				}

				if ($tag == 'Справочник')
				{
					$this->filter_variant_block = FALSE;
					$this->append_filter_variant();
				}
			}

			$this->current_tag = '';
		}

		/**
		 * Обробка значення тегу
		 *
		 * @param object $parser
		 * @param mixed $data
		 */
		private function cdata($parser, $data)
		{
			if ($this->filters_block AND $this->filter_block)
			{
				if ($this->current_tag == 'Ид')
				{
					if (!isset($this->filter_data['id']))
					{
						$this->filter_data['id'] = $data;
					}
					else
					{
						$this->filter_data['id'] .= $data;
					}
				}

				if ($this->current_tag == 'Наименование')
				{
					if (!isset($this->filter_data['name']))
					{
						$this->filter_data['name'] = $data;
					}
					else
					{
						$this->filter_data['name'] .= $data;
					}
				}

				if ($this->current_tag == 'ИдЗначения')
				{
					if (!isset($this->filter_variant_data['id']))
					{
						$this->filter_variant_data['id'] = $data;
					}
					else
					{
						$this->filter_variant_data['id'] .= $data;
					}
				}

				if ($this->current_tag == 'Значение')
				{
					if (!isset($this->filter_variant_data['name']))
					{
						$this->filter_variant_data['name'] = $data;
					}
					else
					{
						$this->filter_variant_data['name'] .= $data;
					}
				}
			}
		}

		/**
		 * Додавання опції в чергу
		 */
		private function insert_filter()
		{
			$set = array(
				'order_index' => $this->filters_order_index,
				'data' => serialize($this->filter_data),
			);
			$this->db->insert('import_filters', $set);

			$this->filters_order_index++;

			$this->filter_data = array();
		}

		/**
		 * Додавання варіанту опції до масиву
		 */
		private function append_filter_variant()
		{
			$this->filter_data['variants'][] = $this->filter_variant_data;
			$this->filter_variant_data = array();
		}

		public function __destruct()
		{
			xml_parser_free($this->parser);
		}
	}