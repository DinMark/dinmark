<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_import_clients_model extends CI_Model
	{
		private $_parser;

		private $clients_block = false;
		private $client_block = false;

		private $xml = '';

		// Методи парсера ==============================================================================================

		/**
		 * Запуск обробки файлу
		 *
		 * @param string $file_name
		 */
		public function run($file_name)
		{
			$this->_parser = xml_parser_create();
			xml_parser_set_option($this->_parser, XML_OPTION_TARGET_ENCODING, 'UTF-8');
			xml_parser_set_option($this->_parser, XML_OPTION_CASE_FOLDING, 0);
			xml_parser_set_option($this->_parser, XML_OPTION_SKIP_WHITE, 1);

			xml_set_object($this->_parser, $this);
			xml_set_element_handler($this->_parser, 'tag_open', 'tag_close');
			xml_set_character_data_handler($this->_parser, 'cdata');

			$fp = fopen($file_name, 'r');

			while ($data = fread($fp, 4096))
			{
				xml_parse($this->_parser, $data, feof($fp));
			}
		}

		/**
		 * Обробка відкриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 * @param array $element_attrs
		 */
		private function tag_open($parser, $tag, $element_attrs)
		{
			if ($tag === 'Контрагенты') {
				$this->clients_block = true;
			}

			if ($tag === 'Контрагент') {
				$this->client_block = true;
			}

			if ($this->client_block) {
				$this->xml .= '<' . $tag . '>';
			}
		}

		/**
		 * Обробка закриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 */
		private function tag_close($parser, $tag)
		{
			if ($this->client_block) {
				$this->xml .= '</' . $tag . '>';
			}

			if ($tag === 'Контрагенты') {
				$this->clients_block = false;
			}

			if ($tag === 'Контрагент') {
				$this->client_block = false;
				$this->save_client();
			}
		}

		/**
		 * Обробка значення тегу
		 *
		 * @param object $parser
		 * @param mixed $data
		 */
		private function cdata($parser, $data)
		{
			if ($this->client_block) {
				$this->xml .= xml_convert($data, true);
			}
		}

		/**
		 * Збереження
		 */
		private function save_client()
		{
			$xml = simplexml_load_string($this->xml);
			$this->xml = '';

			if (isset($xml->id)) {
                $price_type=((string)$xml->price);




				$c = (int)$this->db
					->where('id', (int)$xml->id)
					->count_all_results('clients');

				if ($c === 0) {
					$this->db
						->set('id', (int)$xml->id)
						->set('price_type', $price_type)
						->set('name', $this->db->escape_str((string)$xml->Наименование))
						->set('email', $this->db->escape_str((string)$xml->email))
						->insert('clients');
				} else {
					$this->db
						->set('price_type', $price_type)
						->set('name', $this->db->escape_str((string)$xml->Наименование))
						->set('email', $this->db->escape_str((string)$xml->email))
						->where('id', (int)$xml->id)
						->update('clients');
				}
                $c = (int)$this->db
                    ->where('email', $this->db->escape_str((string)$xml->email))
                    ->count_all_results('users');
                if ($c === 0) {
                  //  $this->db
                      //  ->set('id', (int)$xml->id)
                       // ->set('price_type', $price_type)
                        //->set('name', $this->db->escape_str((string)$xml->Наименование))
                        //->set('email', $this->db->escape_str((string)$xml->email))
                        //->insert('users');
                } else {
                    $st=(string)$xml->id;
                    $st1=str_replace(',','',$st);
                    $this->db
                        ->set('price_type', $price_type)
                        ->set('code', (int)$st1)
                       // ->set('email', $this->db->escape_str((string)$xml->email))
                        ->where('email', $this->db->escape_str((string)$xml->email))
                        ->update('users');
                }
			}
		}

		public function __destruct()
		{
			xml_parser_free($this->_parser);
		}
	}