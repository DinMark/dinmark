	<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_import_model
	 *
	 * @property Admin_catalog_model $admin_catalog_model
	 */
	class Admin_import_model extends CI_Model
	{
		/**
		 * @var array
		 */
		private $products = array();

		/**
		 * @var array
		 */
		private $filters = array();

		/**
		 * @var array
		 */
		private $languages = array();

		/**
		 * @var array
		 */
		private $product_data = array();

		/**
		 * @var array
		 */
		private $image_source = array(1000, 1000);

		/**
		 * @var array
		 */
		private $image_big = array(644, 395);

		/**
		 * @var array
		 */
		private $image_thumb = array(248, 160);

		/**
		 * Отримання кількості елементів черги
		 *
		 * @return array
		 */
		public function get_queue()
		{
			$queue = array(
				'products' => $this->db
					->where('type', 'product')
					->count_all_results('import_tmp'),

				'offers' => $this->db
					->where('type', 'offer')
					->count_all_results('import_tmp'),
			);

			return $queue;
		}

		/**
		 * Імпорт товарів
		 * @param array $languages
		 */
		public function import_products($languages)
		{
			$this->languages = $languages;

			$queue = $this->db
				->where('type', 'product')
				->order_by('record_id', 'asc')
				->limit(1000)
				->get('import_tmp')
				->result_array();

			foreach ($queue as $item)
			{
				$this->product_data = simplexml_load_string($item['data']);

				$locations = $this->get_locations($this->product_data->{'Группы'});

				if ($locations) {
					$filters = $this->check_filters((array)$this->product_data->{'Фільтри'});
					$this->check_product_filters($locations, $filters);
					$this->check_variant($locations, $filters);
					$this->check_photo($locations);
				}

				$this->db->delete('import_tmp', array('record_id' => $item['record_id']));
			}
		}

		/**
		 * Пошук id фільтрів
		 *
		 * @param array $price_filters
		 * @return array
		 */
		private function check_filters($price_filters)
		{
			$filters = array();

			if (isset($price_filters['Фільтр'])) {
				foreach ($price_filters['Фільтр'] as $v) {
				    $filter = (string)$v->ua->{'Назва'};
				    $value = (string)$v->ua->{'Значення'};

                    $filter_ru = (string)$v->ru->{'Назва'};
                    $value_ru = (string)$v->ru->{'Значення'};

					if (!isset($this->filters[$filter][$value])) {
						$this->filters[$filter][$value] = $this->insert_filter($filter, $value, $filter_ru, $value_ru);
					}

					if (isset($this->filters[$filter][$value])) {
						$filters[] = $this->filters[$filter][$value];
					}
				}
			}

			return $filters;
		}

		/**
		 * Додавання нового фільтру
		 *
		 * @param string $filter
		 * @param string $value
         * @param string $filter_ru
         * @param string $value_ru
		 * @return int
		 */
		private function insert_filter($filter, $value, $filter_ru, $value_ru)
		{
			$r = $this->db
				->select('filter_id')
				->where('1s_hash', md5($filter))
				->get('catalog_filters')
				->row_array();

			if (is_array($r)) {
				$filter_id = $r['filter_id'];

                $this->db->update(
                    'catalog_filters',
                    array(
                        'name_ua' => form_prep($filter),
                        'name_ru' => form_prep($filter_ru),
                    ),
                    array(
                        'filter_id' => $filter_id,
                    )
                );
			} else {
				$rp = $this->db
					->select_max('position', 'max_position')
					->where('parent_id', 0)
					->get('catalog_filters')
					->row_array();

				$this->db->insert(
					'catalog_filters',
					array(
						'parent_id' => 0,
						'1s_hash' => md5($filter),
						'position' => $rp['max_position'] + 1,
						'name_ua' => form_prep($filter),
						'name_ru' => form_prep($filter_ru),
					)
				);

				$filter_id = $this->db->insert_id();
			}

			$r = $this->db
				->select('filter_id')
				->where('parent_id', $filter_id)
				->where('1s_hash', md5($value))
				->get('catalog_filters')
				->row_array();

			if (is_array($r)) {
				$value_id = $r['filter_id'];

                $this->db->update(
                    'catalog_filters',
                    array(
                        'name_ua' => form_prep($value),
                        'name_ru' => form_prep($value_ru),
                    ),
                    array(
                        'filter_id' => $value_id,
                    )
                );
			} else {
				$rp = $this->db
					->select_max('position', 'max_position')
					->where('parent_id', $filter_id)
					->get('catalog_filters')
					->row_array();

				$this->db->insert(
					'catalog_filters',
					array(
						'parent_id' => $filter_id,
						'1s_hash' => md5($value),
						'position' => $rp['max_position'] + 1,
						'name_ua' => form_prep($value),
						'name_ru' => form_prep($value_ru),
					)
				);

				$value_id = $this->db->insert_id();
			}

			return array($filter_id, $value_id);
		}

		/**
		 * Отримання товарів
		 *
		 * @param array $groups
		 * @return array
		 */
		public function get_locations($groups)
		{
			$_1s_id = '';

			foreach ($groups as $v) {
				$_1s_id = (string)$v->{'Ид'};

				if (!isset($this->products[$_1s_id])) {
					$r = $this->db
						->select('product_id, component_id, menu_id')
						->where('1s_id', form_prep($_1s_id))
						->get('catalog')
						->row_array();

					if ($r !== null) {
						$this->products[$_1s_id] = $r;
					}
				}
			}

			return isset($this->products[$_1s_id]) ? $this->products[$_1s_id] : false;
		}

		private function check_photo($product)
		{
			$photos = (array)$this->product_data->{'Картинка'};

			if (count($photos) > 0) {
				$dir = ROOT_PATH . 'upload/catalog/';
				if (!file_exists($dir)) {
					mkdir($dir);
				}

				$dir .= $this->init_model->dir_by_id($product['product_id']) . '/';
				if (!file_exists($dir)) {
					mkdir($dir);
				}

				$dir .= $product['product_id'] . '/';
				if (!file_exists($dir)) {
					mkdir($dir);
				}

				foreach ($photos as $image) {
					$image = (string)$image;

					if ($image !== '') {
						$image = trim($image);
						$file_name = pathinfo($image, PATHINFO_BASENAME);

						if (file_exists(ROOT_PATH . 'upload/import/catalog/' . $image))
						{
							$exists = file_exists($dir . $file_name);

							//$this->db->where('product_id', $product['product_id']);
							//$this->db->where('image', $file_name);
							//$c = $this->db->count_all_results('catalog_images');

							if (/*$c === 0 and */copy(ROOT_PATH . 'upload/import/catalog/' . $image, $dir . $file_name))
							{
								$sizes = getimagesize($dir . $file_name);

								$width = ($sizes[0] < $this->image_source[0]) ? $sizes[0] : $this->image_source[0];
								$height = ($width * $sizes[1]) / $sizes[0];

								$this->image_lib->resize($dir . $file_name, $dir . 's_' . $file_name, $width, $height);
								$this->image_lib->resize($dir . $file_name, $dir . $file_name, $this->image_big[0], $this->image_big[1]);
								$this->image_lib->resize($dir . $file_name, $dir . 't_' . $file_name, $this->image_thumb[0], $this->image_thumb[1]);

								if (!$exists) {
									$this->db->insert(
										'catalog_images',
										array(
											'product_id' => $product['product_id'],
											'component_id' => $product['component_id'],
											'menu_id' => $product['menu_id'],
											'image' => $file_name,
										)
									);
								}
							}

							unlink(ROOT_PATH . 'upload/import/catalog/' . $image);
						}
					}
				}
			}
		}

		/**
		 *
		 *
		 * @param array $locations
		 * @param array $filters
		 * @return array
		 */
		private function check_product_filters($locations, $filters)
		{
			foreach ($filters as $v) {
				$c = $this->db
					->where('product_id', $locations['product_id'])
					->where('filter_id', $v[0])
					->where('value_id', $v[1])
					->count_all_results('catalog_product_bind_filters');

				if ($c === 0) {
					$this->db->insert(
						'catalog_product_bind_filters',
						array(
							'product_id' => $locations['product_id'],
							'filter_id' => $v[0],
							'value_id' => $v[1],
						)
					);
				}

				$c = $this->db
					->where('component_id', $locations['component_id'])
					->where('filter_id', $v[1])
					->count_all_results('catalog_bind_filters');

				if ($c === 0) {
					$this->db->insert(
						'catalog_bind_filters',
						array(
							'component_id' => $locations['component_id'],
							'filter_id' => $v[1],
						)
					);
				}
			}
		}

		/**
		 * @param array $locations
		 * @param array $filters
		 */
		private function check_variant($locations, $filters)
		{
			$r = $this->db
				->select('variant_id, url_ua, url_ru')
				->where('1s_id', form_prep((string)$this->product_data->{'Ид'}))
				->get('catalog_variants')
				->row_array();

			if ($r !== null) {
				$variant_id = $r['variant_id'];

				$url = translit($this->product_data->{'Наименование'});
				$url = $r['url_ua'] === '' ? $url : $r['url_ua'];

				$url_ru = translit($this->product_data->{'Наименование_ru'});
				$url_ru = $r['url_ru'] === '' ? $url_ru : $r['url_ru'];
				
				$this->db->update(
					'catalog_variants',
					array(
						'articul' => form_prep($this->product_data->{'Артикул'}),
						'title_ua' => form_prep($this->product_data->{'Наименование'}),
						'title_ru' => form_prep($this->product_data->{'Наименование_ru'}),
						'url_ua' => $url,
						'url_ru' => $url_ru,
						'weight' => (float)$this->product_data->{'ВагаУпаковки'},
						'box' => (float)$this->product_data->{'КількістьВУпаковці'},
						'limit_1' => (int)$this->product_data->{'Поріг1'},
						'limit_2' => (int)$this->product_data->{'Поріг2'},
					),
					array(
						'variant_id' => $variant_id,
					)
				);

				$this->db->update(
					'site_links',
					array(
						'hash_ua' => md5($url),
						'hash_ru' => md5($url_ru),
					),
					array(
						'item_id' => $variant_id,
						'module' => 'catalog',
						'method' => 'variant',
					)
				);
			} else {
				$url = translit($this->product_data->{'Наименование'});
				$url_ru = translit($this->product_data->{'Наименование_ru'});

				$r = $this->db
					->select_max('position', 'max_position')
					->where('product_id', $locations['product_id'])
					->get('catalog_variants')
					->row_array();

				$position = $r['max_position'];

				$this->db->insert(
					'catalog_variants',
					array(
						'product_id' => $locations['product_id'],
						'menu_id' => $locations['menu_id'],
						'component_id' => $locations['component_id'],
						'position' => $position + 1,
						'1s_id' => (string)$this->product_data->{'Ид'},
						'title_ua' => form_prep($this->product_data->{'Наименование'}),
						'title_ru' => form_prep($this->product_data->{'Наименование_ru'}),
						'url_ua' => $url,
						'url_ru' => $url_ru,
						'weight' => (float)$this->product_data->{'ВагаУпаковки'},
						'limit_1' => (int)$this->product_data->{'Поріг1'},
						'limit_2' => (int)$this->product_data->{'Поріг2'},
					)
				);

				$variant_id = $this->db->insert_id();

				$this->db->insert(
					'site_links',
					array(
						'item_id' => $variant_id,
						'component_id' => $locations['component_id'],
						'menu_id' => $locations['menu_id'],
						'module' => 'catalog',
						'method' => 'variant',
						'hash_ua' => md5($url),
						'hash_ru' => md5($url_ru),
					)
				);
			}

			foreach ($filters as $v) {
				$c = $this->db
					->where('variant_id', $variant_id)
					->where('filter_id', $v[0])
					->where('value_id', $v[1])
					->count_all_results('catalog_variant_bind_filters');

				if ($c === 0) {
					$this->db->insert(
						'catalog_variant_bind_filters',
						array(
							'variant_id' => $variant_id,
							'filter_id' => $v[0],
							'value_id' => $v[1],
						)
					);
				}
			}
		}

		##############################################################################################################

		/**
		 * Імпорт товарів
		 */
		public function import_offers()
		{
			$queue = $this->db
				->where('type', 'offer')
				->order_by('record_id', 'asc')
				->limit(500)
				->get('import_tmp')
				->result_array();

			foreach ($queue as $item)
			{
				$offer = json_decode($item['data'], true);

				if (isset($offer['Цены'])) {

				    $this->db
						->set('price', (float)str_replace(',', '.', preg_replace('/[^0-9.]/', '', $offer['Цены'][0]['ЦенаЗаЕдиницу'])))
						->set('currency', form_prep($offer['Цены'][0]['Валюта']))
						->where('1s_id', form_prep($offer['Ид']))
						->update('catalog_variants');
					//echo($offer['Ид']);

                    }

				$this->db->delete('import_tmp', array('record_id' => $item['record_id']));

			}
		}
	}