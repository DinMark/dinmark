<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	set_time_limit(0);
	//ini_set('memory_limit', '512M');

	class Admin_import_products_model extends CI_Model
	{
		protected $parser;

		// Товари
		private $products_block = FALSE;
		private $product_block = FALSE;

		private $xml = '';

		protected $record_id = 0;

		/**
		 * Конструктор класу
		 */
		public function __construct()
		{
			parent::__construct();

			$this->parser = xml_parser_create();
			xml_parser_set_option($this->parser, XML_OPTION_TARGET_ENCODING, 'UTF-8');
			xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);

			xml_set_object($this->parser, $this);
			xml_set_element_handler($this->parser, 'tag_open', 'tag_close');
			xml_set_character_data_handler($this->parser, 'cdata');

			$r = $this->db
				->select_max('record_id', 'max')
				->get('import_tmp')
				->row_array();

			$this->record_id = $r['max'] + 1;
		}

		// Методи парсера ==============================================================================================

		/**
		 * Запуск обробки файлу
		 *
		 * @param string $file_name
		 */
		public function run($file_name)
		{
			$fp = fopen($file_name, 'r');

			while ($data = fread($fp, 4096))
			{
				xml_parse($this->parser, $data, feof($fp));
			}
		}

		/**
		 * Обробка відкриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 * @param array $element_attrs
		 */
		private function tag_open($parser, $tag, $element_attrs)
		{
			if ($tag === 'Товары')
			{
				$this->products_block = TRUE;
			}

			if ($this->products_block and $tag === 'Товар')
			{
				$this->product_block = TRUE;
			}

			if ($this->product_block) {
				$this->xml .= '<' . $tag . '>';
			}
		}

		/**
		 * Обробка закриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 */
		private function tag_close($parser, $tag)
		{
			if ($this->product_block) {
				$this->xml .= '</' . $tag . '>';
			}

			if ($this->products_block and $tag === 'Товар')
			{
				$this->product_block = FALSE;
				$this->insert_product();
			}
		}

		/**
		 * Обробка значення тегу
		 *
		 * @param object $parser
		 * @param mixed $data
		 */
		private function cdata($parser, $data)
		{
			if ($this->product_block) {
				$this->xml .= $data;
			}
		}

		// Методи товарів ==============================================================================================

		/**
		 * Додавання товару до масиву
		 */
		private function insert_product()
		{
		   if ($this->xml !== '') {
				$this->db
					->insert(
						'import_tmp',
						array(
							'record_id' => $this->record_id,
							'type' => 'product',
							'data' => $this->xml,
						)
					);

				$this->record_id++;
			}

			$this->xml = '';
		}

		/**
		 * Деструктор класу
		 */
		public function __destruct()
		{
			xml_parser_free($this->parser);
		}
	}