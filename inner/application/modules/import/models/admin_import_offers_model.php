<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	set_time_limit(0);
	ini_set('memory_limit', '1024M');

	/**
	 * Class Admin_import_offers_model
	 */
	class Admin_import_offers_model extends CI_Model
	{
		protected $parser;
		private $current_tag = '';

		private $offers_block = FALSE;
		private $offer_block = FALSE;
		private $offer_price_block = FALSE;
		private $offer_data = array();
		private $offer_price = array();

		private $prices_block = FALSE;
		private $price_block = FALSE;
		private $price_data = array();

		private $skip = true;

		protected $record_id = 0;
		protected $insert_batch = array();

		// Методи парсера ==============================================================================================

		/**
		 * Запуск обробки файлу
		 *
		 * @param string $file_name
		 */
		public function run($file_name)
		{
			$r = $this->db
				->select_max('record_id', 'max')
				->get('import_tmp')
				->row_array();

			$this->record_id = $r['max'] + 1;

			$this->parser = xml_parser_create();
			xml_parser_set_option($this->parser, XML_OPTION_TARGET_ENCODING, 'UTF-8');
			xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);

			xml_set_object($this->parser, $this);
			xml_set_element_handler($this->parser, 'tag_open', 'tag_close');
			xml_set_character_data_handler($this->parser, 'cdata');

			$fp = fopen($file_name, 'r');

			while ($data = fread($fp, 8096))
			{
				xml_parse($this->parser, $data, feof($fp));
			}
		}

		/**
		 * Обробка відкриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 * @param array $element_attrs
		 */
		private function tag_open($parser, $tag, $element_attrs)
		{
			if ($tag === 'ТипыЦен') {
				$this->prices_block = true;
			}

			if ($this->prices_block) {
				if ($tag === 'ТипЦены') {
					$this->price_block = true;
				}

				if ($this->price_block and ($tag === 'Ид' or $tag === 'Наименование')) {
					$this->skip = false;
				}

				if ($tag === 'Налог') {
					$this->price_block = false;
				}
			}

			if ($tag === 'Предложения') {
				$this->offers_block = true;
			}

			if ($this->offers_block) {
				if ($tag === 'Предложение') {
					$this->offer_block = true;
				}

				if ($tag === 'Ид') {
					$this->skip = false;
				}

				if ($tag === 'Цена') {
					$this->offer_price_block = true;
					$this->skip = false;
				}
			}

			$this->current_tag = $tag;
		}

		/**
		 * Обробка закриваючого тегу
		 *
		 * @param object $parser
		 * @param string $tag
		 */
		private function tag_close($parser, $tag)
		{
			if ($tag === 'ТипыЦен') {
				$this->prices_block = false;
			}

			if ($this->prices_block) {
				if ($tag === 'ТипЦены') {
					$this->insert_price();
					$this->price_block = false;
				}

				if ($tag === 'Ид' || $tag === 'Наименование') {
					$this->skip = true;
				}
			}

			if ($tag === 'Предложения') {
				$this->offers_block = false;
			}

			if ($this->offers_block) {
				if ($tag === 'Предложение') {
					$this->offer_block = false;
					$this->insert_offer();
				}

				if ($tag === 'Ид') {
					$this->skip = true;
				}

				if ($tag === 'Цена') {
					$this->offer_price_block = false;
					$this->skip = true;

					$this->add_offer_price();
				}
			}

			$this->current_tag = '';
		}

		/**
		 * Обробка значення тегу
		 *
		 * @param object $parser
		 * @param mixed $data
		 */
		private function cdata($parser, $data)
		{
			if (!$this->skip and $this->current_tag !== '') {
				$data = trim($data);

				if ($this->price_block) {
					if (isset($this->price_data[$this->current_tag])) {
						$this->price_data[$this->current_tag] .= $data;
					} else {
						$this->price_data[$this->current_tag] = $data;
					}
				}

				if ($this->offer_block and $this->current_tag === 'Ид') {
					if (isset($this->offer_data['Ид'])) {
						$this->offer_data['Ид'] .= $data;
					} else {
						$this->offer_data['Ид'] = $data;
					}
				}

				if ($this->offer_price_block and $data !== '') {
					if (isset($this->offer_price[$this->current_tag])) {
						$this->offer_price[$this->current_tag] .= $data;
					} else {
						$this->offer_price[$this->current_tag] = $data;
					}
				}
			}
		}

		/**
		 * Збереження типу ціни
		 */
		private function insert_price()
		{
			$c = (int)$this->db
				->where('1s_id', form_prep($this->price_data['Ид']))
				->count_all_results('catalog_prices');

			if ($c === 0) {
				$this->db
					->set('1s_id', form_prep($this->price_data['Ид']))
					->set('sign', form_prep($this->price_data['Наименование']))
					->insert('catalog_prices');
			} else {
				$this->db
					->set('sign', form_prep($this->price_data['Наименование']))
					->where('1s_id', form_prep($this->price_data['Ид']))
					->update('catalog_prices');
			}

			$this->price_data = array();
		}

		/**
		 * Додавання залишку в чергу
		 */
		private function insert_offer()
		{
			if (isset($this->offer_data['Цены'][0]['ИдТипаЦены']) and $this->offer_data['Цены'][0]['ИдТипаЦены'] === '6c975ed0-86e0-11e5-9418-00155d46430e') {
				$this->insert_batch[] = array(
					'record_id' => $this->record_id,
					'type' => 'offer',
					'data' => json_encode($this->offer_data),
				);

				$this->record_id++;
			}

			$this->offer_data = array();

			if (count($this->insert_batch) > 1) {
				$this->db
					->insert_batch(
						'import_tmp',
						$this->insert_batch
					);

				$this->insert_batch = array();
			}
		}

		/**
		 *
		 */
		private function add_offer_price()
		{
			$this->offer_data['Цены'][] = $this->offer_price;
			$this->offer_price = array();
		}

		/**
		 * Деструктор класу
		 */
		public function __destruct()
		{
			xml_parser_free($this->parser);
		}
	}