<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_import_sales_model extends CI_Model
	{
		public function get_orders()
		{
			$xml = '';

			$orders = $this->db/*->where('export_1s', 1)*/->where('export_1s_status', 0)->get('orders')->result_array();

			if (count($orders) > 0)
			{
				foreach ($orders as $order)
				{
					$xml .= "\t" . '<Документ>' . "\n";

					$xml .= "\t\t" . '<Номер>' . $order['order_id'] . '</Номер>' . "\n";
					$xml .= "\t\t" . '<Дата>' . date('Y-m-d', $order['order_date']) . '</Дата>' . "\n";
					$xml .= "\t\t" . '<ХозОперация>Заказ товара</ХозОперация>' . "\n";
                    $xml .= "\t\t" . '<АдресДоставки >' . $order['delivery'] . '</АдресДоставки>' . "\n";
                    $xml .= "\t\t" . '<payment >' . $order['payment'] . '</payment>' . "\n";

					$xml .= "\t\t" . '<Роль>Продавец</Роль>' . "\n";
					$xml .= "\t\t" . '<Валюта>грн</Валюта>' . "\n";
					$xml .= "\t\t" . '<Курс>1</Курс>' . "\n";
					$xml .= "\t\t" . '<Сумма>' . $order['cost'] . '</Сумма>' . "\n";
                    $xml .= "\t\t" . '<АдресДоставки>' . $order['delivery'] . '</АдресДоставки>' . "\n";
					$xml .= "\t\t" . '<Контрагенты>' . "\n";
					$xml .= "\t\t\t" . '<Контрагент>' . "\n";

					$this->db->select('name, email, phone, price_type');
					$this->db->where('user_id', $order['user_id']);

					$user = $this->db->get('users')->row_array();

//					if ($user['code'] != '' AND $user['check'] == 1)
//					{
//						$this->db->select('user_id');
//						$this->db->where('code', $user['code']);
//						$cart = $this->db->get('users_1s')->row_array();
//					}

					$phone = isset($user['phone']) ? $user['phone'] : str_replace(array('+', '(', ')', '-', ' '), '', $order['phone']);
					$email = isset($user['email']) ? $user['email'] : $order['email'];

					$xml .= "\t\t\t\t" . '<Ид>' . $phone . '</Ид>' . "\n";
					$xml .= "\t\t\t\t" . '<НомерСайта1>' . $order['user_id'] . '</НомерСайта1>' . "\n";
                    $xml .= "\t\t\t\t" . '<ТипЦен>' .  (isset($user['price_type'])? $user['price_type']:'p0'). '</ТипЦен>' . "\n";
					$xml .= "\t\t\t\t" . '<Наименование>' . (isset($user['name']) ? $user['name'] : $order['name']) . '</Наименование>' . "\n";
					$xml .= "\t\t\t\t" . '<ПолноеНаименование>' . (isset($user['name']) ? $user['name'] : $order['name']) . '</ПолноеНаименование>' . "\n";



					$xml .= "\t\t\t\t" . '<Роль>Покупатель</Роль>' . "\n";
					//if (isset($cart['user_id'])) $xml .= "\t\t\t\t" . '<КодКарты>' . $user['code'] . '</КодКарты>' . "\n";
					$xml .= "\t\t\t\t" . '<Контакты>' . "\n";
					$xml .= "\t\t\t\t\t" . '<Контакт>' . "\n";
					$xml .= "\t\t\t\t\t\t" . '<Тип>ТелефонРабочий</Тип>' . "\n";
					$xml .= "\t\t\t\t\t\t" . '<Значение>'. $phone . '</Значение>' . "\n";
					$xml .= "\t\t\t\t\t" . '</Контакт>' . "\n";
					$xml .= "\t\t\t\t\t" . '<Контакт>' . "\n";
					$xml .= "\t\t\t\t\t\t" . '<Тип>Почта</Тип>' . "\n";
					$xml .= "\t\t\t\t\t\t" . '<Значение>'. $email . '</Значение>' . "\n";
					$xml .= "\t\t\t\t\t" . '</Контакт>' . "\n";
                    $xml .= "\t\t\t\t\t" . '<Контакт>' . "\n";
                    $xml .= "\t\t\t\t\t\t" . '<Тип>АдресДоставки</Тип>' . "\n";
                    $xml .= "\t\t\t\t\t\t" . '<Значение>' . $order['delivery'] . '</Значение>' . "\n";
                    $xml .= "\t\t\t\t\t" . '</Контакт>' . "\n";
					$xml .= "\t\t\t\t" . '</Контакты>' . "\n";

					$xml .= "\t\t\t" . '</Контрагент>' . "\n";
					$xml .= "\t\t" . '</Контрагенты>' . "\n";

					$xml .= "\t\t" . '<Время>' . date('H:i:s', $order['send_date']) . '</Время>' . "\n";
					$xml .= "\t\t" . '<Комментарий>№ ' . $order['order_id'] . ' ' . str_replace('http://', '', $this->input->server('HTTP_HOST')) . '</Комментарий>' . "\n";

					$xml .= "\t\t" . '<Налоги>' . "\n";
					$xml .= "\t\t\t" . '<Налог>' . "\n";
					$xml .= "\t\t\t\t" . '<Наименование>НДС</Наименование>' . "\n";
					$xml .= "\t\t\t\t" . '<УчтеноВСумме>true</УчтеноВСумме>' . "\n";
					$xml .= "\t\t\t\t" . '<Сумма/>' . "\n";
					$xml .= "\t\t\t" . '</Налог>' . "\n";
					$xml .= "\t\t" . '</Налоги>' . "\n";

					$xml .= "\t\t" . '<Товары>' . "\n";

					$this->db->select('orders_products.*');
					$this->db->where('orders_products.order_id', $order['order_id']);

					$this->db->select('catalog.1s_id as product_s_id');
					$this->db->join('catalog', 'catalog.product_id = orders_products.product_id');

					$products = $this->db->get('orders_products')->result_array();

					foreach ($products as $product)
					{
						$xml .= "\t\t\t" . '<Товар>' . "\n";

						$xml .= "\t\t\t\t" . '<Ид>' . $product['product_s_id'] . '</Ид>' . "\n";
						$xml .= "\t\t\t\t" . '<Наименование>' . $product['title'] . '</Наименование>' . "\n";
						$xml .= "\t\t\t\t" . '<БазоваяЕдиница Код="796" НаименованиеПолное="Штука" МеждународноеСокращение="PCE">шт</БазоваяЕдиница>' . "\n";

						$xml .= "\t\t\t\t" . '<СтавкиНалогов>' . "\n";
						$xml .= "\t\t\t\t\t" . '<СтавкаНалога>' . "\n";
						$xml .= "\t\t\t\t\t\t" . '<Наименование>НДС</Наименование>' . "\n";
						$xml .= "\t\t\t\t\t\t" . '<Ставка>Без налога</Ставка>' . "\n";
						$xml .= "\t\t\t\t\t" . '</СтавкаНалога>' . "\n";
						$xml .= "\t\t\t\t" . '</СтавкиНалогов>' . "\n";

//						$product['data_1s'] = $product['data_1s'] != '' ? unserialize($product['data_1s']) : array();
//
//						if (isset($product['data_1s'][0]))
//						{
//							$xml .= "\t\t\t\t" . '<ЗначенияРеквизитов>' . "\n";
//
//							foreach ($product['data_1s'][0] as $data)
//							{
//								$xml .= "\t\t\t\t\t" . '<ЗначениеРеквизита>' . "\n";
//								$xml .= "\t\t\t\t\t\t" . '<Наименование>' . $data['name'] . '</Наименование>' . "\n";
//								$xml .= "\t\t\t\t\t\t" . '<Значение>' . $data['value'] . '</Значение>' . "\n";
//								$xml .= "\t\t\t\t\t" . '</ЗначениеРеквизита>' . "\n";
//							}
//
//							$xml .= "\t\t\t\t" . '</ЗначенияРеквизитов>' . "\n";
//						}

						$xml .= "\t\t\t\t" . '<ЦенаЗаЕдиницу>' . $product['price'] . '</ЦенаЗаЕдиницу>' . "\n";
						$xml .= "\t\t\t\t" . '<Количество>' . $product['total'] . '</Количество>' . "\n";
						$xml .= "\t\t\t\t" . '<Сумма>' . ($product['total'] * $product['price']) . '</Сумма>' . "\n";
						$xml .= "\t\t\t\t" . '<Единица>шт</Единица>' . "\n";
						$xml .= "\t\t\t\t" . '<Коэффициент>1</Коэффициент>' . "\n";

						$xml .= "\t\t\t\t" . '<Налоги>' . "\n";
						$xml .= "\t\t\t\t\t" . '<Налог>' . "\n";
						$xml .= "\t\t\t\t\t\t" . '<Наименование>НДС</Наименование>' . "\n";
						$xml .= "\t\t\t\t\t\t" . '<УчтеноВСумме>true</УчтеноВСумме>' . "\n";
						$xml .= "\t\t\t\t\t\t" . '<Сумма/>' . "\n";
						$xml .= "\t\t\t\t\t\t" . '<Ставка>Без налога</Ставка>' . "\n";
						$xml .= "\t\t\t\t\t" . '</Налог>' . "\n";
						$xml .= "\t\t\t\t" . '</Налоги>' . "\n";

						$xml .= "\t\t\t" . '</Товар>' . "\n";
					}

					$xml .= "\t\t" . '</Товары>' . "\n";
					$xml .= "\t" . '</Документ>' . "\n";
				}
			}
			else
			{
				//$xml .= "\t" . '<Документ></Документ>' . "\n";
			}

			return $xml;
		}

        public function get_klients()
        {
            $xml = '';

            $orders = $this->db/*->where('export_1s', 1)*/->where('status', 0)->get('users')->result_array();

            if (count($orders) > 0)
            {
                foreach ($orders as $user)
                {
                    $xml .= "\t\t" . '<Контрагенты>' . "\n";
                    $xml .= "\t\t\t" . '<Контрагент>' . "\n";



                    $phone = isset($user['phone']) ? $user['phone'] : str_replace(array('+', '(', ')', '-', ' '), '', $user['phone']);
                    $email = isset($user['email']) ? $user['email'] : $user['email'];

                    $xml .= "\t\t\t\t" . '<Ид>' . $phone . '</Ид>' . "\n";
                    //$xml .= "\t\t\t\t" . '<Номер>' . $order['user_id'] . '</Номер>' . "\n";
                    $xml .= "\t\t\t\t" . '<Наименование>' . (isset($user['name']) ? $user['name'] : $user['name']) . '</Наименование>' . "\n";
                    $xml .= "\t\t\t\t" . '<ПолноеНаименование>' . (isset($user['name']) ? $user['name'] : $user['name']) . '</ПолноеНаименование>' . "\n";



                    $xml .= "\t\t\t\t" . '<Роль>Покупатель</Роль>' . "\n";

                    //if (isset($cart['user_id'])) $xml .= "\t\t\t\t" . '<КодКарты>' . $user['code'] . '</КодКарты>' . "\n";
                    $xml .= "\t\t\t\t" . '<Контакты>' . "\n";
                    $xml .= "\t\t\t\t\t" . '<Контакт>' . "\n";
                    $xml .= "\t\t\t\t\t\t" . '<Тип>ТелефонРабочий</Тип>' . "\n";
                    $xml .= "\t\t\t\t\t\t" . '<Значение>'. $phone . '</Значение>' . "\n";
                    $xml .= "\t\t\t\t\t" . '</Контакт>' . "\n";
                    $xml .= "\t\t\t\t\t" . '<Контакт>' . "\n";
                    $xml .= "\t\t\t\t\t\t" . '<Тип>Почта</Тип>' . "\n";
                    $xml .= "\t\t\t\t\t\t" . '<Значение>'. $email . '</Значение>' . "\n";
                    $xml .= "\t\t\t\t\t" . '</Контакт>' . "\n";
                    $xml .= "\t\t\t\t" . '</Контакты>' . "\n";
                    $xml .= "\t\t\t\t" . '<Данные>' . "\n";
                    $xml .= "\t\t\t\t\t" . '<user_id>'.$user['user_id'].'</user_id>' . "\n";
                    $xml .= "\t\t\t\t\t" . '<status>'.$user['status'].'</status>' . "\n";
                    $xml .= "\t\t\t\t\t" . '<code>'.$user['code'].'</code>' . "\n";
                    $xml .= "\t\t\t\t\t" . '<price_type>'.$user['price_type'].'</price_type>' . "\n";
                    $xml .= "\t\t\t\t" . '</Данные>' . "\n";
                    $xml .= "\t\t\t" . '</Контрагент>' . "\n";
                    $xml .= "\t\t" . '</Контрагенты>' . "\n";


                }
            }
            else
            {
                //$xml .= "\t" . '<Документ></Документ>' . "\n";
            }

            return $xml;
        }
	}