<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	ini_set('max_execution_time', 1800);
	ini_set('max_input_time', 1800);

	/**
	 * Class Admin_import
	 *
	 * @property Admin_import_model $admin_import_model
	 * @property Admin_import_categories_model $admin_import_categories_model
	 * @property Admin_import_filters_model $admin_import_filters_model
	 * @property Admin_import_products_model $admin_import_products_model
	 * @property Admin_import_offers_model $admin_import_offers_model
	 * @property Admin_import_users_model $admin_import_users_model
	 */
	class Admin_import extends MX_Controller
	{
		protected $timer = 0;

		/**
		 * Завантаження прайсу з описами
		 *
		 * @return string
		 */
		public function import_upload()
		{
			$response = array(
				'success' => FALSE,
			);

			if ($this->init_model->is_admin() AND $this->init_model->check_access('catalog_index'))
			{
				$upload_config = array(
					'upload_path' => ROOT_PATH . 'upload/import/catalog/',
					'overwrite' => TRUE,
					'file_name' => 'import.xml',
					'allowed_types' => 'xml'
				);

				$this->load->library('upload', $upload_config);

				if ($this->upload->do_upload('price_file'))
				{
					$response['success'] = TRUE;
				}
			}

			return json_encode($response);
		}

		/**
		 * Додавання фільтрів в чергу
		 *
		 * @return string
		 */
		public function import_categories()
		{
			$response = array('success' => FALSE);

			if ($this->init_model->is_admin())
			{
				$file_name = ROOT_PATH . 'upload/import/catalog/import.xml';

				if (file_exists($file_name))
				{
					$this->load->model('admin_import_categories_model');
					$this->load->helpers(array('form', 'translit'));

					$this->admin_import_categories_model->run($file_name);

					$response['success'] = TRUE;
				}
			}

			return json_encode($response);
		}

		/**
		 * Додавання товарів в чергу
		 *
		 * @return string
		 */
		public function import_products()
		{
			$response = array('success' => FALSE);

			if ($this->init_model->is_admin())
			{
				$file_name = ROOT_PATH . 'upload/import/catalog/import.xml';

				if (file_exists($file_name))
				{
					$this->load->model('admin_import_products_model');
					$this->load->helpers(array('form', 'translit'));

					$this->admin_import_products_model->run($file_name);
					exit;
					//unlink(ROOT_PATH . 'upload/import/catalog/import.xml');

					$response['success'] = TRUE;
				}
			}

			return json_encode($response);
		}

		/**
		 * Завантаження прайсу з залишками
		 *
		 * @return string
		 */
		public function offers_upload()
		{
			$response = array(
				'success' => FALSE,
			);

			if ($this->init_model->is_admin() AND $this->init_model->check_access('catalog_index'))
			{
				$upload_config = array(
					'upload_path' => ROOT_PATH . 'upload/import/catalog/',
					'overwrite' => TRUE,
					'file_name' => 'offers.xml',
					'allowed_types' => 'xml'
				);

				$this->load->library('upload', $upload_config);

				if ($this->upload->do_upload('price_file'))
				{
					$response['success'] = TRUE;
				}
			}

			return json_encode($response);
		}

		/**
		 * Додавання товарів в чергу
		 *
		 * @return string
		 */
		public function import_offers()
		{
			$response = array('success' => FALSE);

			if ($this->init_model->is_admin())
			{
				$file_name = ROOT_PATH . 'upload/import/catalog/offers.xml';

				if (file_exists($file_name))
				{
					$this->load->model('admin_import_offers_model');
					$this->admin_import_offers_model->run($file_name);

					//unlink(ROOT_PATH . 'upload/import/catalog/offers.xml');

					$response['success'] = TRUE;
				}
			}

			return json_encode($response);
		}

		/**
		 * Обробка черги імпорту
		 */
		public function queue()
		{
			$time = time();
			$in_work = false;

			$this->load->model('admin_import_model');

			$queue = $this->admin_import_model->get_queue();

			if ($queue['products'] > 0)
			{
				$in_work = true;

				$this->load->library('Image_lib');
				$this->load->helpers(array('form', 'translit'));

				$languages = array_keys($this->config->item('languages'));

				$this->admin_import_model->import_products($languages);
			}
			else
			{
				if ($queue['offers'] > 0)
				{
					$in_work = true;
					$this->admin_import_model->import_offers();
				}
			}

			$this->timer += time() - $time;

			if ($this->timer < 40 and $in_work) {
				$this->queue();
			}

			exit;
		}
	}