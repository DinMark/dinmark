<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);

	ini_set('max_execution_time', 1800);
	ini_set('max_input_time', 1800);
	ini_set('memory_limit', '1024M');

	/**
	 * Class Import
	 *
	 * @property Admin_import_model $admin_import_model
	 * @property Admin_import_filters_model $admin_import_filters_model
	 * @property Admin_import_products_model $admin_import_products_model
	 * @property Admin_import_offers_model $admin_import_offers_model
	 * @property Admin_import_users_model $admin_import_users_model
	 */
	class Import extends MX_Controller
	{protected $timer = 0;
		public function index()
		{
			$get = $this->input->get();
			//$cookie = $this->input->cookie(session_name());

			$data = "===================\n\n" . print_r($get, TRUE) . "\n\n===================";

			$this->load->helper('file');
			write_file(ROOT_PATH . 'upload/log/1s.txt', $data, 'a+');

			// ======================

			$type = $this->input->get('type');
			$mode = $this->input->get('mode');

			if (($type == 'catalog' OR $type == 'sale' OR $type == 'clients') AND $mode == 'checkauth')
			{
				echo "success\n";
				echo session_name() . "\n";
				echo session_id();
			}

			if ($type == 'catalog' AND $mode == 'init')
			{
				if ($handle = opendir(ROOT_PATH . 'upload/import/catalog/'))
				{
					while (false !== ($entry = readdir($handle))) {
						if ($entry != "." && $entry != "..") {
							if (is_file(ROOT_PATH . 'upload/import/catalog/' . $entry))
							{
								unlink(ROOT_PATH . 'upload/import/catalog/' . $entry);
							}
						}
					}
					closedir($handle);
				}

				echo "zip=yes\n";
				//echo "file_limit=" . ($this->_return_bytes(ini_get('upload_max_filesize')) - 200000000);
				echo "file_limit=" . 10000000;
			}

			if ($type == 'catalog' AND $mode == 'file')
			{
				$file_name = $this->input->get('filename');

				$f = fopen(ROOT_PATH . 'upload/import/catalog/' . $file_name, 'ab');
				fwrite($f, file_get_contents('php://input'));
				fclose($f);

				$zip = new ZipArchive;
				$opn = $zip->open(ROOT_PATH . 'upload/import/catalog/' . $file_name);


				if ($opn)
				{
					$zip->extractTo(ROOT_PATH . 'upload/import/catalog/');
					$zip->close();

					//unlink(ROOT_PATH . 'upload/import/catalog/' . $file_name);
				}

				echo 'success';

				exit;
			}

			if ($type == 'catalog' AND $mode == 'import')
			{
				$file_name = $this->input->get('filename');

				if ($file_name == 'import.xml')
				{
					$file_name = ROOT_PATH . 'upload/import/catalog/import.xml';

					if (file_exists($file_name))
					{
						$this->load->helpers(array('form', 'translit'));

						$this->load->model('import/admin_import_categories_model');
						$this->admin_import_categories_model->run($file_name);

						$this->load->model('import/admin_import_products_model');
						$this->admin_import_products_model->run($file_name);

						//unlink(ROOT_PATH . 'upload/import/catalog/import.xml');
					}
				}

				if ($file_name == 'offers.xml')
				{
					$file_name = ROOT_PATH . 'upload/import/catalog/offers.xml';

					if (file_exists($file_name))
					{
						$this->load->model('import/admin_import_offers_model');
						$this->admin_import_offers_model->run($file_name);
                        $this->queue();

						//unlink(ROOT_PATH . 'upload/import/catalog/offers.xml');
					}
				}

				if ($file_name == 'clients.xml')
				{
					$file_name = ROOT_PATH . 'upload/import/catalog/clients.xml';

					if (file_exists($file_name))
					{
						$this->load->model('import/admin_import_clients_model');
						$this->load->helper('xml');
						$this->admin_import_clients_model->run($file_name);

						//unlink(ROOT_PATH . 'upload/import/catalog/leo_clients.xml');
					}
				}

				echo 'success';
				exit;
			}

			if ($type == 'sale' AND $mode == 'init')
			{
				if ($handle = opendir(ROOT_PATH . 'upload/import/sales/'))
				{
					while (false !== ($entry = readdir($handle))) {
						if ($entry != "." && $entry != "..") {
							if (is_file(ROOT_PATH . 'upload/import/sales/' . $entry))
							{
								unlink(ROOT_PATH . 'upload/import/sales/' . $entry);
							}
						}
					}
					closedir($handle);
				}

				echo "zip=yes\n";
				echo "file_limit=" . $this->_return_bytes(ini_get('upload_max_filesize'));
			}

			if ($type == 'sale' AND $mode == 'query')
			{
				$xml = '<?xml version="1.0" encoding="Windows-1251"?>' . "\n";
				$xml .= '<КоммерческаяИнформация ВерсияСхемы="2.05" ДатаФормирования="' . date('Y-m-d') . 'T' . date('H:i:s') . '">' . "\n";

				$this->load->model('import/admin_import_sales_model');
				$xml .= $this->admin_import_sales_model->get_orders();

				$xml .= '</КоммерческаяИнформация>';

				$this->output->set_header('Content-Type: text/xml; charset=Windows-1251');
				$this->config->set_item('is_ajax_request', TRUE);

				return mb_convert_encoding($xml, 'Windows-1251', 'UTF-8');
			}

            if ($type == 'sale' AND $mode == 'queryKag')
            {
                $xml = '<?xml version="1.0" encoding="Windows-1251"?>' . "\n";
                $xml .= '<КоммерческаяИнформация ВерсияСхемы="2.05" ДатаФормирования="' . date('Y-m-d') . 'T' . date('H:i:s') . '">' . "\n";

                $this->load->model('import/admin_import_sales_model');
                $xml .= $this->admin_import_sales_model-> get_klients();

                $xml .= '</КоммерческаяИнформация>';

                $this->output->set_header('Content-Type: text/xml; charset=Windows-1251');
                $this->config->set_item('is_ajax_request', TRUE);

                return mb_convert_encoding($xml, 'Windows-1251', 'UTF-8');
            }

            if ($type == 'sale' AND $mode == 'success')
			{
				$this->db->set('export_1s_status', 1);
				$this->db->where('export_1s_status', 0);
				$this->db->update('orders');

				echo 'success';
			}
            if ($type == 'sale' AND $mode == 'client')
            {
                //$file_name = $this->input->get('filename');
              //  if ($file_name) == 'clients.xml')
              //  {
                    $file_name = ROOT_PATH . 'upload/import/sales/clients.xml';

                    if (file_exists($file_name))
                    {
                        $this->load->model('import/admin_import_clients_model');
                        $this->load->helper('xml');
                        $this->admin_import_clients_model->run($file_name);

                        //unlink(ROOT_PATH . 'upload/import/catalog/leo_clients.xml');
                    }
               // }

            }
			if ($type == 'sale' AND $mode == 'file')
			{
                if (file_exists('upload/import/sales/')) {
                    foreach (glob('upload/import/sales/*') as $file) {
                        unlink($file);
                    }
                }
				$file_name = $this->input->get('filename');

				$f = fopen(ROOT_PATH . 'upload/import/sales/' . $file_name, 'ab');
				fwrite($f, file_get_contents('php://input'));
				fclose($f);

				$zip = new ZipArchive;

				if ($zip->open(ROOT_PATH . 'upload/import/sales/' . $file_name))
				{
					$zip->extractTo(ROOT_PATH . 'upload/import/sales/');
					$zip->close();
				}

				echo 'success';
			}

			if ($type == 'clients' AND $mode == 'init')
			{
				if ($handle = opendir(ROOT_PATH . 'upload/import/users/'))
				{
					while (false !== ($entry = readdir($handle))) {
						if ($entry != "." && $entry != "..") {
							if (is_file(ROOT_PATH . 'upload/import/users/' . $entry))
							{
								unlink(ROOT_PATH . 'upload/import/users/' . $entry);
							}
						}
					}
					closedir($handle);
				}

				echo "zip=yes\n";
				echo "file_limit=" . $this->_return_bytes(ini_get('upload_max_filesize'));
			}


			if ($type == 'clients' AND $mode == 'file')
			{
				$file_name = $this->input->get('filename');

				if ($file_name === NULL)
				{
					$file_name = $this->input->get('file_name');
				}

				$f = fopen(ROOT_PATH . 'upload/import/users/' . $file_name, 'ab');
				fwrite($f, file_get_contents('php://input'));
				fclose($f);

				$zip = new ZipArchive;

				if ($zip->open(ROOT_PATH . 'upload/import/users/' . $file_name))
				{
					$zip->extractTo(ROOT_PATH . 'upload/import/users/');
					$zip->close();
				}

				echo 'success';
			}

			if ($type == 'clients' AND $mode == 'import')
			{
				$file_name = $this->input->get('filename');

				if ($file_name === NULL)
				{
					$file_name = $this->input->get('file_name');
				}

				if ($file_name == 'contractor.xml')
				{
					$file_name = ROOT_PATH . 'upload/import/users/contractor.xml';

					if (file_exists($file_name))
					{
						$this->load->model('import/admin_import_users_model');
						$this->admin_import_users_model->run($file_name);
					}
				}

				echo 'success';
			}

			exit;
		}
        /**
         * Обробка черги імпорту
         */
        public function queue()
        {
            $time = time();
            //echo $time;
            $in_work = false;

            $this->load->model('admin_import_model');

            $queue = $this->admin_import_model->get_queue();

            if ($queue['products'] > 0)
            {
                $in_work = true;
                //echo $queue['products'];
                $this->load->library('Image_lib');
                $this->load->helpers(array('form', 'translit'));

                $languages = array_keys($this->config->item('languages'));

                $this->admin_import_model->import_products($languages);
            }
            else
            {
                if ($queue['offers'] > 0)
                {
                    //echo $queue['offers'];
                    $in_work = true;
                    $this->admin_import_model->import_offers();
                }
            }

            $this->timer += time() - $time;

            if ($in_work) {
                $this->queue();
            }
            echo 'success';
            //echo 'success';
            exit;
        }

		private function _return_bytes($val)
		{
			$val = trim($val);
			$last = strtolower($val[strlen($val)-1]);
            $val=substr ($val,0,strlen($val)-1);
			switch ($last) {
				case 'g':
					$val *= 1024;
				case 'm':
					$val *= 1024;
				case 'k':
					$val *= 1024;
			}

			return $val;
		}
	}