<?php
	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_js('admin/ckeditor/ckeditor.js');
	$this->template_lib->set_js('admin/jquery.form.js');
?>
<div class="admin_component">
	<div class="component_loader"></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component delivery">
			<div class="article"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Редагування інформації про доставку</div></div>
			<a href="#" class="fm save"><b></b>Зберегти</a>
		</div>
		<?php if (count($languages) > 1): ?>
		<div class="fmr component_lang">
			<?php foreach ($languages as $key => $val): ?>
				<a href="#" class="flags <?=$key;?><?=(($key == LANG) ? ' active' : '');?>" data-language="<?=$key;?>"><img src="img/flags_<?=$key;?>.png"></a>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
	<div class="fm admin_view_article">
		<form id="component_article_form" action="<?=$this->uri->full_url('admin/catalog_delivery/update');?>" method="post">
			<?php foreach ($languages as $key => $val): ?>
			<div id="article_tab_<?=$key;?>" class="article_tab"<?=(($key != LANG) ? ' style="display: none"' : ''); ?>>
                <div class="evry_title">
					<label for="delivery_<?=$key;?>" class="block_label">Опис доставки:</label>
					<div class="no_float"><textarea class="component_article" id="delivery_<?=$key;?>" name="delivery[<?=$key;?>]" style="height: 400px"><?=stripslashes($delivery['delivery']['value_' . $key]);?></textarea></div>
				</div>
                <div class="evry_title">
                    <label for="payment_<?=$key;?>" class="block_label">Опис оплати:</label>
                    <div class="no_float"><textarea class="component_article" id="payment_<?=$key;?>" name="payment[<?=$key;?>]" style="height: 200px"><?=stripslashes($delivery['payment']['value_' . $key]);?></textarea></div>
                </div>
			</div>
			<?php endforeach; ?>
			<div class="fm for_sucsess">
				<div class="fmr save_links">
					<a href="#" class="fm save_adm"><b></b>Зберегти</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">

	$(function () {
		$('.component_article').ckeditor({height: 300});

		$('.component_lang').find('a').on('click', function (e) {
			e.preventDefault();

			$(this).closest('div').find('.active').removeClass('active');
			$(this).addClass('active');

			$('.article_tab').hide();
			$('#article_tab_' + $(this).data('language')).show();
		});

		$('.component_edit_links .save, .for_sucsess .save_adm').on('click', function (e) {
			e.preventDefault();

			component_loader_show($('.component_loader'), '');
			$('.component_article').ckeditor({action: 'update'});
			$('#component_article_form').ajaxSubmit({
				success:function (response) {
					if (response.success) component_loader_hide($('.component_loader'), '');
				},
				dataType: 'json'
			});
		});

	});
</script>