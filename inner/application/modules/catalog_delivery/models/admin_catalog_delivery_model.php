<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_catalog_delivery_model extends CI_Model
	{
		/**
		 * Отримання інформації про доставку
		 *
		 * @return array
		 */
		public function get_delivery()
		{
			$delivery = array();

			$result = $this->db->get('catalog_delivery')->result_array();

			foreach ($result as $v)
			{
				$delivery[$v['key']] = $v;
			}

			return $delivery;
		}

		/**
		 * Збереження інформації про доставку
		 *
		 * @param string $key
		 * @param string $value
		 * @param string $language
		 */
		public function update($key, $value, $language)
		{
			$value = str_replace(array("\n", "\r", "\t"), '', $value);
			$value = $this->db->escape_str($value);

			$this->db->update('catalog_delivery', array('value_' . $language => $value), array('key' => $key));
		}
	}