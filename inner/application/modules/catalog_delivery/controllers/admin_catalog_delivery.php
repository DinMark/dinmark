<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_catalog_delivery
	 *
	 * @property Admin_catalog_delivery_model $admin_catalog_delivery_model
	 */
	class Admin_catalog_delivery extends MX_Controller {

		/**
		 * Редагування інформації про доставку
		 */
		public function index()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Редагування інформації про доставку');

			$this->template_lib->set_admin_menu_active('catalog_config');
			$this->template_lib->set_admin_menu_active('catalog_delivery', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->template_lib->set_bread_crumbs('', 'Редагування інформації про доставку');

			$this->load->model('admin_catalog_delivery_model');

			$template_data = array(
				'menu_id' => $menu_id,
				'delivery' => $this->admin_catalog_delivery_model->get_delivery(),
				'languages' => $this->config->item('languages'),
			);
			$this->template_lib->set_content($this->load->view('admin/catalog_delivery_tpl', $template_data, TRUE));
		}

		/**
		 * Збереження інформації про доставку
		 */
		public function update()
		{
			$this->init_model->is_admin('json');
			$response = array('success' => FALSE);

			$this->load->model('admin_catalog_delivery_model');

			$delivery = $this->input->post('delivery');
			$payment = $this->input->post('payment');

			foreach ($delivery as $k => $v)
			{
				$this->admin_catalog_delivery_model->update('delivery', $v, $k);
				$this->admin_catalog_delivery_model->update('payment', $payment[$k], $k);
			}

			$response['success'] = TRUE;
			return json_encode($response);
		}
	}