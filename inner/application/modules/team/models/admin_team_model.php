<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_team_model extends CI_Model
	{
		/**
		 * Загальна кількість
		 *
		 * @param int $component_id
		 * @return string
		 */
		public function count_team($component_id)
		{
			return $this->db->where('component_id', $component_id)->count_all_results('team');
		}

		/**
		 * Отримання всіх
		 *
		 * @param int $component_id
		 * @return array
		 */
		public function get_team($component_id)
		{
			$this->db->select('team.worker_id, team.component_id, team.hidden, team.name_' . LANG . ' as name, team.post_' . LANG . ' as post, team.image');
			$this->db->where('team.component_id', $component_id);
			$this->db->order_by('team.position', 'asc');

			return $this->db->get('team')->result_array();
		}

		/**
		 * Додавання
		 *
		 * @param array $set
		 * @return int
		 */
		public function insert($set)
		{
			$this->db->insert('team', $set);
			return $this->db->insert_id();
		}

		/**
		 * Отримання
		 *
		 * @param $worker_id
		 */
		public function get($worker_id)
		{
			return $this->db->get_where('team', array('worker_id' => $worker_id))->row_array();
		}

		/**
		 * Оновлення
		 *
		 * @param array $set
		 * @param array $where
		 */
		public function update($set, $where)
		{
			$this->db->update('team', $set, $where);
		}

		/**
		 * Отримання останньої позиції в компоненті
		 *
		 * @param int $component_id
		 * @return int
		 */
		public function get_position($component_id)
		{
			$this->db->set('`position`', '`position` + 1', FALSE);
			$this->db->where('component_id', $component_id);
			$this->db->update('team');

			return 0;
		}

		/**
		 * Видалення
		 *
		 * @param int $worker_id
		 */
		public function delete($worker_id)
		{
			$dir = ROOT_PATH . 'upload/team/' . $this->init_model->dir_by_id($worker_id) . '/' . $worker_id . '/';
			if (file_exists($dir)) delete_files($dir, TRUE, FALSE, 1);

			$this->db->delete('team', array('worker_id' => $worker_id));
		}

		/**
		 * Видалення компоненту
		 *
		 * @param int $component_id
		 */
		public function delete_component($component_id)
		{
			$result = $this->db->select('worker_id')->where('component_id', $component_id)->get('team')->result_array();

			foreach ($result as $v)
			{
				$dir = ROOT_PATH . 'upload/team/' . $this->init_model->dir_by_id($v['worker_id']) . '/' . $v['worker_id'] . '/';
				delete_files($dir, TRUE, FALSE, 1);
			}

			$this->db->delete('team', array('component_id' => $component_id));
			$this->db->delete('components', array('component_id' => $component_id));
		}

		### Зображення ###

		/**
		 * Отримання зображеня
		 *
		 * @param int $worker_id
		 * @param $photo_index
		 * @return array
		 */
		public function get_image($worker_id, $photo_index)
		{
			return $this->db->select('image' . $photo_index)->where('worker_id', $worker_id)->get('team')->row('image' . $photo_index);
		}

		/**
		 * Видалення зображення
		 *
		 * @param int $worker_id
		 * @return int
		 */
		public function delete_image($worker_id, $photo_index)
		{
			$im = $this->get_image($worker_id, $photo_index);

			if (is_string($im) AND $im != '')
			{
				$dir = ROOT_PATH . 'upload/team/' . $this->init_model->dir_by_id($worker_id) . '/' . $worker_id . '/';
				$file_name = $im;

				if (file_exists($dir . $file_name)) unlink($dir . $file_name);
				if (file_exists($dir . 's_' . $file_name)) unlink($dir . 's_' . $file_name);

				$this->update(array('image' . $photo_index => ''), array('worker_id' => $worker_id));
			}
		}
	}