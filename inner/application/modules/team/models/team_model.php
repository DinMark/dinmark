<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Team_model extends CI_Model
	{
		/**
		 * Отримання відгуків
		 *
		 * @param int $component_id
		 * @return array
		 */
		public function get_team($component_id)
		{
			$this->db->select('worker_id, component_id, name_' . LANG . ' as name, post_' . LANG . ' as post, image, image2');
			$this->db->where('component_id', $component_id);
			$this->db->where('hidden', 0);
			$this->db->order_by('position', 'asc');

			return $this->db->get('team')->result_array();
		}
	}