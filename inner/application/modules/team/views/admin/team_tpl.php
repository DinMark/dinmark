<?php
	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_js('admin/ui/jquery-ui-1.10.3.custom.min.js');
?>
<div class="admin_component" id="admin_component_<?=$component_id;?>" data-component-id="<?=$component_id;?>" data-menu-id="<?=$menu_id;?>" data-module="team" data-css-class="catalog" data-visibility-url="<?=$this->uri->full_url('admin/components/toggle_visibility');?>" data-delete-url="<?=$this->uri->full_url('admin/team/delete_component');?>">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="<?=(($hidden == 0) ? 'catalog' : 'hidden');?>"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Наша команда</div></div>
			<a class="fm add add_worker" href="#"><b></b>Додати працівника</a>
			<a href="#" class="fm show_hide"><b></b><?=(($hidden == 0) ? 'Приховати' : 'Показати');?></a>
		</div>
		<div class="fmr component_del">
			<a href="#" class="fm delete_component"><b></b></a>
		</div>
		<div class="fmr component_pos">
			<a href="#" class="fm up_component"><b></b></a>
			<a href="#" class="fm down_component"><b></b></a>
		</div>
	</div>
	<div class="fm admin_menu">
		<?php if (isset($team)): ?>
		<ul id="team_list">
			<?php if (count($team) > 0): ?><?php foreach ($team as $v): ?>
			<li data-worker-id="<?=$v['worker_id'];?>">
				<div class="holder<?php if ($v['hidden'] == 1): ?> hidden<?php endif; ?>">
					<div class="cell last_edit <?php if (isset($last_worker) AND ($v['worker_id'] == $last_worker)) echo ' active'; ?>"></div>
					<div class="cell w_30 number"></div>
					<div class="cell w_20 icon"><a href="#" class="hide-show<?php if ($v['hidden'] == 0): ?> active<?php endif; ?>"></a></div>
					<div class="cell no_padding" style="width:108px"><?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/team/' . $this->init_model->dir_by_id($v['worker_id']) . '/' . $v['worker_id'] . '/' . $v['image'] . '?t=' . time() . rand(10000, 1000000));?>" alt=""><?php endif; ?></div>
					<div class="cell w_20 icon"><a href="<?=$this->uri->full_url('admin/team/edit?menu_id=' . $menu_id . '&worker_id=' . $v['worker_id']);?>" class="edit"></a></div>
					<div class="cell auto"><?=(($v['name'] != '') ? $v['name'] : 'Новий запис');?></div>
					<div class="cell w_20 icon sorter"><a href="#" class="single_arrows"></a></div>
					<div class="cell w_20 icon"><a href="#" class="delete"></a></div>
				</div>
			</li>
			<?php endforeach; ?><?php endif; ?>
		</ul>
		<div class="fm admin_massage <?php if (count($team) > 0): ?> adm_hidden<?php endif; ?>">Список пустий.</div>
		<?php endif; ?>
	</div>
</div>

<script type="text/template" id="worker_template">
	<li data-worker-id="%worker_id%">
		<div class="holder">
			<div class="cell last_edit active"></div>
			<div class="cell w_30 number"></div>
			<div class="cell w_20 icon"><a href="#" class="hide-show active"></a></div>
			<div class="cell no_padding" style="width:108px"></div>
			<div class="cell w_20 icon"><a href="<?=$this->uri->full_url('admin/team/edit?menu_id=' . $menu_id . '&worker_id=%worker_id%');?>" class="edit"></a></div>
			<div class="cell auto">Новий запис</div>
			<div class="cell w_20 icon sorter"><a href="#" class="single_arrows"></a></div>
			<div class="cell w_20 icon"><a href="#" class="delete"></a></div>
		</div>
	</li>
</script>

<script type="text/javascript">

	$(document).ready(function () {

		var $component = $('#admin_component_<?=$component_id;?>'),
			$team = $('#team_list');

		function rebuild() {
			$team.find('li').map(function (i) {
				$(this).find('.number').text(i + 1);

				if (i % 2 == 0) {
					$(this).addClass('grey');
				} else {
					$(this).removeClass('grey');
				}
			});
		}

		rebuild();

		$component
			.component()
			.on('click', '.add_worker', function (e) {
				e.preventDefault();

				component_loader_show($component.find('.component_loader'), '');

				$.post(
					'<?=$this->uri->full_url('admin/team/insert');?>',
					{
						component_id: <?=$component_id;?>,
						menu_id: <?=$menu_id;?>
					},
					function (response) {
						if (response.success) {
							$component.find('.admin_massage').addClass('adm_hidden');

							var row = $('#worker_template').html();
							$team.prepend(row.split('%worker_id%').join(response.worker_id));

							rebuild();
							component_loader_hide($component.find('.component_loader'), '');
						}
					},
					'json'
				);
			});

		if ($.type(jQuery().sortable) === 'undefined') {
			$.getScript(
				'<?=base_url('js/ui/jquery-ui-1.10.3.custom.min.js');?>',
				function (data, textStatus, jqxhr) {
					$component.sortable({
						forcePlaceholderSize: true,
						opacity: .6,
						listType: 'ul',
						handle: '.sorter a',
						items: 'li',
						toleranceElement: '> div',
						update: function () {
							rebuild();
							var position = [];

							$team.find('li').each(function () {
								position[$(this).index()] = $(this).data('worker-id');
							});

							if (position.length > 0) {

								component_loader_show($component.find('.component_loader'), '');

								$.post(
									'<?=$this->uri->full_url('admin/team/save_position');?>',
									{
										position: position
									},
									function (response) {
										if (response.success) component_loader_hide($component.find('.component_loader'), '');
									},
									'json'
								);
							}
						},
						placeholder: "ui-state-highlight",
						axis: 'y',
						helper: 'clone'
					});
				}
			);
		} else {
			$component.sortable({
				forcePlaceholderSize: true,
				opacity: .6,
				listType: 'ul',
				handle: '.sorter a',
				items: 'li',
				toleranceElement: '> div',
				update: function () {
					rebuild();
					var position = [];

					$team.find('li').each(function () {
						position[$(this).index()] = $(this).data('worker-id');
					});

					if (position.length > 0) {

						component_loader_show($component.find('.component_loader'), '');

						$.post(
							'<?=$this->uri->full_url('admin/team/save_position');?>',
							{
								position: position
							},
							function (response) {
								if (response.success) component_loader_hide($component.find('.component_loader'), '');
							},
							'json'
						);
					}
				},
				placeholder: "ui-state-highlight",
				axis: 'y',
				helper: 'clone'
			});
		}

		$team
			.on('click', '.hide-show', function (e) {
				e.preventDefault();

				var $link = $(this),
					status = 1,
					worker_id = $link.closest('li').data('worker-id');

				if ($link.hasClass('active')) {
					$link.removeClass('active');
				} else {
					$link.addClass('active');
					status = 0;
				}

				component_loader_show($component.find('.component_loader'), '');

				$.post(
					'<?=$this->uri->full_url('admin/team/visible');?>',
					{
						worker_id: worker_id,
						status: status
					},
					function (response) {
						if (response.success) component_loader_hide($component.find('.component_loader'), '');
					},
					'json'
				);
			})
			.on('click', '.delete', function (e) {
				e.preventDefault();

				var $link = $(this),
					worker_id = $link.closest('li').data('worker-id');

				confirmation('Видалити відгук?', function () {

					component_loader_show($component.find('.component_loader'), '');

					$.post(
						'<?=$this->uri->full_url('admin/team/delete');?>',
						{
							worker_id: worker_id
						},
						function (response) {
							if (response.success) {
								$link.closest('li').slideUp(function () {
									$(this).remove();
									if ($team.find('li').length == 0) $component.find('.admin_massage').removeClass('adm_hidden');
								});

								rebuild();
								component_loader_hide($component.find('.component_loader'), '');
							}
						},
						'json'
					);
				});
			})
			.on('mouseover', '.holder', function () {
				$(this).addClass('active');
			})
			.on('mouseout', '.holder', function () {
				$(this).removeClass('active');
			});
	});
</script>