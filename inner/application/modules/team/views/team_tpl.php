<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if (isset($team) AND count($team) > 0): ?>
	<div class="container">
		<div class="row our-team">
			<?php foreach ($team as $v): ?>
				<div class="our-team__item col-sm-6 col-md-4">
					<figure>
						<?php if ($v['image'] !== ''): ?><img src="<?=base_url('upload/team/' . $this->init_model->dir_by_id($v['worker_id']) . '/' . $v['worker_id'] . '/' . $v['image']);?>" alt="" class="img-original"><?php endif; ?>
		<?php if ($v['image2'] !== ''): ?><img src="<?=base_url('upload/team/' . $this->init_model->dir_by_id($v['worker_id']) . '/' . $v['worker_id'] . '/' . $v['image2']);?>" alt="" class="img-hover"><?php endif; ?>
						<figcaption><?=$v['name'];?></figcaption>
					</figure>
					<?=$v['post'];?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>