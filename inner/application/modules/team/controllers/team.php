<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Team
	 *
	 * @property Admin_team_model $admin_team_model
	 * @property team_model $team_model
	 */
	class team extends MX_Controller
	{
		/**
		 * Вивід списка відгуків
		 *
		 * @param int $menu_id
		 * @param int $component_id
		 * @param int $hidden
		 * @param array $config
		 */
		public function index($menu_id, $component_id, $hidden, $config = array())
		{
			$template_data = array(
				'component_id' => $component_id,
				'menu_id' => $menu_id,
				'hidden' => $hidden,
			);

			if ($this->init_model->is_admin())
			{
				$this->load->model('admin_team_model');

				$template_data['team'] = $this->admin_team_model->get_team($component_id);
				$template_data['last_worker'] = $this->session->userdata('last_team');

				$this->load->view('admin/team_tpl', $template_data);
			}
			else
			{
				$this->load->model('team_model');
				$this->load->view('team_tpl', array('team' => $this->team_model->get_team($component_id)));
			}
		}
	}