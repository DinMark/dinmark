<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_team
	 *
	 * @property Admin_team_model $admin_team_model
	 */
	class Admin_team extends MX_Controller
	{
		/**
		 * Додавання
		 *
		 * @return int|string
		 */
		public function insert()
		{
			$this->init_model->is_admin('json');
			$response = array('success' => FALSE);

			$component_id = intval($this->input->post('component_id'));
			$menu_id = intval($this->input->post('menu_id'));

			if ($component_id > 0 AND $menu_id > 0)
			{
				$this->load->model('admin_team_model');

				$set = array(
					'component_id' => $component_id,
					'menu_id' => $menu_id,
					'position' => $this->admin_team_model->get_position($component_id),
				);
				$worker_id = $this->admin_team_model->insert($set);

				$this->session->set_userdata('last_worker', $worker_id);

				$response['success'] = TRUE;
				$response['worker_id'] = $worker_id;
			}

			return json_encode($response);
		}

		/**
		 * Відображення/приховування
		 *
		 * @return string
		 */
		public function visible()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$worker_id = intval($this->input->post('worker_id'));
			$status = intval($this->input->post('status'));

			if ($worker_id > 0 AND in_array($status, array(0, 1)))
			{
				$this->load->model('admin_team_model');

				$set = array('hidden' => $status);
				$where = array('worker_id' => $worker_id);
				$this->admin_team_model->update($set, $where);

				$this->session->set_userdata('last_worker', $worker_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Редагування
		 */
		public function edit()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Редагування працівника');

			$this->template_lib->set_admin_menu_active('components');
			$this->template_lib->set_admin_menu_active('', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$worker_id = intval($this->input->get('worker_id'));

			if ($menu_id > 0 AND $worker_id > 0)
			{
				$this->init_model->set_menu_id($worker_id, TRUE);
				$this->session->set_userdata('last_worker', $worker_id);

				$this->load->model('admin_team_model');
				$this->load->helper('form');

				#TODO: 404 при помилковому запиті

				$tpl_data = array(
					'worker' => $this->admin_team_model->get($worker_id),
					'worker_id' => $worker_id,
					'menu_id' => $menu_id,
					'languages' => $this->config->item('languages'),
				);

				$content = $this->load->view('admin/edit_tpl', $tpl_data, TRUE);
				$this->template_lib->set_content($content);
			}
		}

		/**
		 * Збереження
		 */
		public function save()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$worker_id = intval($this->input->post('worker_id'));

			if ($worker_id > 0)
			{
				$this->load->model('admin_team_model');
				$this->load->helper('translit');

				$name = $this->input->post('name');
				$post = $this->input->post('post');

				$set = array();

				foreach ($name as $key => $val)
				{
					$set['name_' . $key] = $val;
					$set['post_' . $key] = $post[$key];
				}

				$where = array('worker_id' => $worker_id);
				$this->admin_team_model->update($set, $where);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Збереження порядку сортування
		 */
		public function save_position()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$position = $this->input->post('position');

			if (is_array($position))
			{
				$this->load->model('admin_team_model');

				foreach ($position as $key => $val)
				{
					$set = array('position' => intval($key));
					$where = array('worker_id' => intval($val));
					$this->admin_team_model->update($set, $where);
				}

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Видалення
		 *
		 * @return string
		 */
		public function delete()
		{
			$this->init_model->is_admin();

			$response = array('success' => FALSE);

			$worker_id = intval($this->input->post('worker_id'));

			if ($worker_id > 0)
			{
				$this->load->model('admin_team_model');
				$this->load->helper('file');

				$this->admin_team_model->delete($worker_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту
		 *
		 * @return string
		 */
		public function delete_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$component_id = intval($this->input->post('component_id'));

			if ($component_id > 0)
			{
				$this->load->model('admin_team_model');
				$this->load->helper('file');

				$this->admin_team_model->delete_component($component_id);

				$response['error'] = 0;
			}

			return json_encode($response);
		}

		### ЗОБРАЖЕННЯ ###

		/**
		 * Завантаження зображення статті
		 */
		public function upload_image()
		{
			$this->init_model->is_admin('json');

			$response = array(
				'success' => FALSE,
				'file_name' => ''
			);

			$worker_id = intval($this->input->post('worker_id'));
			$menu_id = intval($this->input->post('menu_id'));
			$photo_index = strval($this->input->post('photo_index'));

			if ($menu_id > 0 AND $worker_id > 0)
			{
				$dir = ROOT_PATH . 'upload/team/';
				if (!file_exists($dir)) mkdir($dir);

				$dir .= $this->init_model->dir_by_id($worker_id) . '/';
				if (!file_exists($dir)) mkdir($dir);

				$dir .= $worker_id . '/';
				if (!file_exists($dir)) mkdir($dir);

				$this->load->helper('translit');
				$file_name = translit_filename($_FILES['worker_image']['name']);

				$upload_config = array(
					'upload_path' => $dir,
					'overwrite' => FALSE,
					'file_name' => 's_' . $file_name,
					'allowed_types' => 'gif|jpg|png|jpeg'
				);

				$this->load->library('upload', $upload_config);

				if ($this->upload->do_upload('worker_image'))
				{
					$upload_file_name = $this->upload->data('file_name');

					$sizes = getimagesize($dir . $upload_file_name);

					$width = $sizes[0] > 1000 ? 1000 : $sizes[0];
					$height = 1000 * $sizes[1] / $sizes[0];

					$this->load->library('Image_lib');
					$this->image_lib->resize($dir . $upload_file_name, $dir . $upload_file_name, $width, $height);
					$this->image_lib->resize($dir . $upload_file_name, $dir . $file_name, 242, 264);

					$this->load->model('admin_team_model');

					$set = array('image' . $photo_index => $file_name);
					$this->admin_team_model->update($set, array('worker_id' => $worker_id));

					$response['success'] = TRUE;
					$response['width'] = $width;
					$response['height'] = $height;
					$response['file_name'] = $file_name;
				}
			}

			$this->config->set_item('is_ajax_request', TRUE);
			return json_encode($response);
		}

		/**
		 * Обрізка зображення
		 */
		public function crop_image()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$worker_id = intval($this->input->post('worker_id'));
			$width = intval($this->input->post('width'));
			$photo_index = strval($this->input->post('photo_index'));

			$coords = $this->input->post('coords');
			$coords = array_map('floatval', $coords);

			if ($coords['x'] >= 0 AND $coords['y'] >= 0 AND $coords['w'] > 0 AND $coords['h'] > 0 AND $worker_id > 0 AND $width > 0)
			{
				$this->load->model('admin_team_model');
				$image = $this->admin_team_model->get_worker_image($worker_id, $photo_index);
				//$image = $this->db->select('image')->where('worker_id', $worker_id)->get('team')->row('image');

				if (is_string($image) AND $image != '')
				{
					$dir = ROOT_PATH . 'upload/team/' . $this->init_model->dir_by_id($worker_id) . '/' . $worker_id . '/';

					if (file_exists($dir . 's_' . $image))
					{
						$sizes = getimagesize($dir . 's_' . $image);

						$w_index = $sizes[0] / $width;
						$h_index = $sizes[1] / round(($width * $sizes[1]) / $sizes[0]);

						$x = $coords['x'] * $w_index;
						$y = $coords['y'] * $h_index;
						$x2 = $coords['x2'] * $w_index;
						$y2 = $coords['y2'] * $h_index;

						$this->load->library('Image_lib');
						$this->image_lib->crop($dir . 's_' . $image, $dir . $image, $x2 - $x, $y2 - $y, $x, $y, 242, 264);

						$response['success'] = TRUE;
						$response['image'] = '/upload/team/' . $this->init_model->dir_by_id($worker_id) . '/' . $worker_id . '/' . $image . '?t=' . time() . rand(10000, 1000000);
					}
				}
			}

			return json_encode($response);
		}


		/**
		 * Видалення зображення
		 */
		public function delete_image()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$worker_id = intval($this->input->post('worker_id'));
			$photo_index = strval($this->input->post('photo_index'));

			if ($worker_id > 0)
			{
				$this->load->model('admin_team_model');
				$this->admin_team_model->delete_image($worker_id, $photo_index);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}
	}