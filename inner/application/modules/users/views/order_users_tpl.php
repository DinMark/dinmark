<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<div class="admin_component" id="admin_component_users">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="users"></div>
		</div>
	</div>
	<div class="fm admin_menu">
		<ul>
			<li class="th">
				<div class="holder">
					<div class="cell w_20">#</div>
					<div class="cell auto" style="width: 180px">Ім’я</div>
					<div class="cell auto">E-mail, телефон</div>
					<div class="cell w_100">Замовлення</div>
					<div class="cell w_20 icon"></div>
				</div>
			</li>
		</ul>
		<ul class="users_list">
			<?php if (count($users) > 0): ?><?php foreach ($users as $user): ?>
			<li data-email="<?=$user['email'];?>">
				<div class="holder">
					<div class="cell w_20 number"></div>
					<div class="cell auto" style="width: 180px"><?=$user['name'];?></div>
					<div class="cell auto">
						<?=$user['email'];?>
						<?php if ($user['phone'] != ''): ?><br><?=$user['phone'];?><?php endif; ?>
					</div>
					<div class="cell w_100"><a class="text_link" href="<?=$this->uri->full_url('admin/users/orders?menu_id=' . $menu_id . '&email=' . $user['email']);?>"><?=$user['orders'];?></a></div>
					<div class="cell w_20 icon"><a href="#" class="delete"></a></div>
				</div>
			</li>
			<?php endforeach; ?><?php endif; ?>
		</ul>
	</div>
	<?php if (isset($paginator)) echo $paginator; ?>
</div>
<script type="text/javascript">
	$(function () {
		var $component = $('#admin_component_users'),
			$loader = $component.find('.component_loader');

		function row_decor() {
			$component.find('.users_list')
				.find('li').removeClass('grey').each(function (i) { $(this).find('.number').text(i + 1); })
				.end()
				.find('li:odd').addClass('grey');
		}

		row_decor();

		$component
			.on('click', '.delete', function (e) {
				e.preventDefault();

				var $link = $(this);

				confirmation('Видалити користувача?', function () {
					$loader.show();
					$.post(
						'<?php echo $this->uri->full_url('admin/users/delete_order_user'); ?>',
						{
							email: $link.closest('li').data('email')
						},
						function (response) {
							if (response.success) {
								$link.closest('li').slideUp(function () {
									$(this).remove();
									row_decor();
									$loader.hide();
								});
							}
						},
						'json'
					);
				});
			})
			.on('mouseover', '.users_list .holder', function () {
				$(this).addClass('active');
			})
			.on('mouseout', '.users_list .holder', function () {
				$(this).removeClass('active');
			});
	});
</script>