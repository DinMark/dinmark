<?php defined('ROOT_PATH') or exit('No direct script access allowed'); ?>
<div class="fm admin_component">
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="subscribers"></div>
		</div>
		<div class="fm component_edit_links"></div>
	</div>
	<div class="fm admin_menu">
		<?php if (count($subscribers) > 0): ?>
		<ul id="subscribers_list">
			<?php foreach ($subscribers as $subscriber): ?>
			<li data-id="<?=$subscriber['subscriber_id'];?>">
				<div class="holder">
					<div class="cell w_40 number"><?=$subscriber['subscriber_id'];?></div>
					<div class="cell auto" style="width:200px; color: #000; vertical-align: top">
						<?=$subscriber['email'];?>
					</div>
					<div class="cell w_20 icon"><a href="#" class="delete"></a></div>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
		<?=$paginator;?>
		<?php else: ?>
			<div class="fm admin_massage">На даний час немає підписників</div>
		<?php endif; ?>
	</div>
</div>
<script type="text/javascript">
	function row_decor() {
		$('#subscribers_list')
			.find('li').removeClass('grey')
			.end()
			.find('li:even').addClass('grey');
	}

	$(function () {
		row_decor();

		$('#subscribers_list')
			.on('click', '.delete', function (e) {
				e.preventDefault();

				var $link = $(this);

				confirmation('Видалити підписника?', function () {
					$.post(
						'<?php echo $this->uri->full_url('admin/users/delete_subscriber'); ?>',
						{
							subscriber_id: $link.closest('li').data('id')
						},
						function (response) {
							if (response.success) {
								$link.closest('li').slideUp(function () {
									$(this).remove();
									row_decor();
								});
							}
						},
						'json'
					);
				});
			})
			.on('mouseover', '.holder', function () {
				$(this).addClass('active');
			})
			.on('mouseout', '.holder', function () {
				$(this).removeClass('active');
			});
	});
</script>