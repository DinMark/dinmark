<?php
	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_js('admin/ui/jquery-ui-1.10.3.custom.min.js');
	$this->template_lib->set_js('admin/ui/jquery.ui.datepicker-uk.js');

	$this->template_lib->set_css('js/admin/ui/jquery-ui-1.10.3.custom.min.css', TRUE);
?>
<div class="fm admin_component">
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="orders"></div>
		</div>
		<div class="fm component_edit_links"></div>
	</div>
	<div class="fm admin_menu">
		<table style="margin:0 0 15px 15px;">
			<tr>
				<td style="padding-right: 20px">Дата замовлення:</td>
				<td>
					<input type="text" id="date_with" value="<?php if ($date_with > 0) echo date('d.m.Y', $date_with); ?>" style="border: 1px solid #ccc; width: 150px; padding: 3px;">
					&dash;
					<input type="text" id="date_to" value="<?php if ($date_to > 0) echo date('d.m.Y', $date_to); ?>" style="border: 1px solid #ccc; width: 150px; padding: 3px;">
				</td>
				<td style="padding-left: 20px"><a href="#" id="date_search" style="color:#a32d2d;">Пошук</a></td>
			</tr>
		</table>
		<?php if (isset($user)): ?>
			<div class="fm admin_massage" style="width: 100%">
				Перегляд замовлень користувача: <b><?=$user['name'];?></b> (<?=$user['email'];?><?php if ($user['phone'] != ''): ?>, <?=$user['phone']?><?php endif; ?>)
			</div>
		<?php endif; ?>
		<?php if (count($orders) > 0): ?>
		<ul id="orders_list">
			<?php foreach ($orders as $order): ?>
			<li data-id="<?php echo $order['order_id']; ?>">
				<div class="holder">
					<div class="cell last_edit"></div>
					<div class="cell w_40 number"><?=$order['order_id'];?></div>
					<div class="cell auto" style="width:200px; color: #000; vertical-align: top">
						<div class="title">
							<strong>Дата:</strong> <?php echo date('d.m.Y H:i', $order['order_date']); ?><br>
							<strong>Ім’я:</strong> <?php echo $order['name']; ?><br>
							<strong>Телефон:</strong> <?php echo $order['phone']; ?><br>
							<strong>E-mail:</strong> <?php echo $order['email']; ?><br><br>
							<strong>Доставка:</strong> <?=$order['delivery'];?><br>
							<strong>Оплата:</strong> <?php
								if ($order['payment'] == 1) echo 'оплати при отриманні';
								if ($order['payment'] == 2) echo 'банкіський переказ';
								if ($order['payment'] == 3) echo 'безготівковий переказ для ЮО';
								if ($order['payment'] == 4) echo 'Приват 24';
							?><br><br>
							<?php if ($order['additional'] != ''): ?>
								<strong>Додатково:</strong> <?php echo $order['additional']; ?><br><br>
							<?php endif; ?>
						</div>
					</div>
					<div class="cell auto" style=" color: #000; vertical-align: top">
						<?php foreach ($order['products'] as $key => $row): ?>
						<p style="margin-top: 0; margin-bottom: 5px">
							<?php echo $key + 1; ?>. <a class="order_links text_link" href="<?php echo $this->uri->full_url($row['product_id']); ?>"><?php echo $row['title']; ?></a>
							<?php if ($row['articul'] != ''): ?><br>&nbsp;&nbsp;&nbsp;&nbsp;Артикул: <?php echo $row['articul']; ?><?php endif; ?>
							<br>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['price']; ?> грн. - <?php echo $row['total']; ?> шт. (<?php echo $row['total']; ?> шт. х <?php echo $row['price']; ?> грн = <?php echo $row['cost']; ?> грн)
						</p>
						<?php endforeach; ?>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;Вього: <?php echo $order['total']; ?> шт. <?php echo $order['cost']; ?> грн</p>
					</div>
					<div class="cell w_20 icon"><a href="#" class="delete"></a></div>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
		<?=$paginator;?>
		<div class="fm admin_massage" style="width: 100%">
			За вказаний період (<strong><?=date('d.m.Y', $stat['order_date_min']);?> - <?=date('d.m.Y', $stat['order_date_max']);?></strong>):<br>
			- зроблено <strong><?=$total . ' ' . declension($total, array('замовлення', 'замовлення', 'замовлень'));?></strong><br>
			- куплено <strong><?=$stat['total_sum'] . ' ' . declension($stat['total_sum'], array('товар', 'товари', 'товарів'));?></strong> на суму <strong><?=round($stat['cost_sum'], 2);?> грн</strong><br>
			- мінімальна сума замовлення: <strong><?=round($stat['cost_min'], 2);?> грн</strong><br>
			- максимальна сума замовлення: <strong><?=round($stat['cost_max'], 2);?> грн</strong><br>
			- мінімальна кількість товарів в замовленні: <strong><?=$stat['total_min'];?></strong><br>
			- максимальна кількість товарів в замовленні: <strong><?=$stat['total_max'];?></strong><br>
		</div>
		<?php else: ?>
			<div class="fm admin_massage">На даний час немає замовлень</div>
		<?php endif; ?>
	</div>
</div>
<script type="text/javascript">
	function row_decor() {
		$('#orders_list')
			.find('li').removeClass('grey')
			.end()
			.find('li:even').addClass('grey');
	}

	$(function () {
		row_decor();

		var $date_with = $('#date_with'),
			$date_to = $('#date_to');

		$date_with
			.datepicker()
			.datepicker("option", $.datepicker.regional['uk'])
			.datepicker("option", "dateFormat", "dd.mm.yy")<?php if ($date_with !== NULL): ?>.datepicker("setDate", "<?=date('d.m.Y', $date_with);?>")<?php endif; ?>;

		$date_to
			.datepicker()
			.datepicker("option", $.datepicker.regional['uk'])
			.datepicker("option", "dateFormat", "dd.mm.yy")<?php if ($date_to !== NULL): ?>.datepicker("setDate", "<?=date('d.m.Y', $date_to);?>")<?php endif; ?>;

		$('#date_search').on('click', function (e) {
			e.preventDefault();

			window.location.href = '<?=$this->uri->full_url('admin/users/orders?menu_id=' . $menu_id .($email != '' ? '&email=' . $email : '') . ($user_id > 0 ? '&user_id=' . $user_id : ''));?>&date_with=' + $date_with.val() + '&date_to=' + $date_to.val();
		});

		$('#orders_list')
			.on('click', '.delete', function (e) {
				e.preventDefault();

				var $link = $(this);

				confirmation('Видалити замовлення?', function () {
					$.post(
						'<?php echo $this->uri->full_url('admin/users/delete_order'); ?>',
						{
							order_id: $link.closest('li').data('id')
						},
						function (response) {
							if (response.success) {
								$link.closest('li').slideUp(function () {
									$(this).remove();
									row_decor();
								});
							}
						},
						'json'
					);
				});
			})
			.on('mouseover', '.holder', function () {
				$(this).addClass('active');
			})
			.on('mouseout', '.holder', function () {
				$(this).removeClass('active');
			});
	});
</script>