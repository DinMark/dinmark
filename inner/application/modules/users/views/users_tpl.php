<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<div class="admin_component" id="admin_component_users">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="users"></div>
		</div>
	</div>
	<div class="fm admin_menu">
		<ul>
			<li class="th">
				<div class="holder">
					<div class="cell w_20">#</div>
					<div class="cell auto" style="width: 180px">Ім’я</div>
					<div class="cell auto">E-mail, телефон</div>
					<div class="cell w_140" style="color: #ff0000">Унікальний код партнера</div>
					<div class="cell w_140">Тип ціни</div>
					<div class="cell w_100">Замовлення</div>
					<div class="cell w_20 icon"></div>
				</div>
			</li>
		</ul>
		<ul class="users_list">
			<?php
            $prices = $this->db
                ->select('catalog_prices.1s_id, catalog_prices.sign')
                ->order_by('catalog_prices.sign', 'desc')
                ->get('catalog_prices')
                ->result_array();
            if (count($users) > 0): ?><?php foreach ($users as $user): ?>
			<li data-id="<?=$user['user_id'];?>">
				<div class="holder">
					<div class="cell w_20 number"></div>
					<div class="cell auto" style="width: 180px"><?=$user['name'];?></div>
					<div class="cell auto">
						<?=$user['email'];?>
						<?php if ($user['phone'] != ''): ?><br><?=$user['phone'];?><?php endif; ?>
					</div>
					<div class="cell w_140">
						<span class="menu_item">
							<input type="text" name="price_code" value="<?=$user['code'];?>">
						</span>
					</div>
					<div class="cell w_140">
						<select name="price_type">
                            <?php foreach ($prices as $price): ?>
                                <option value="<?=$price['1s_id'];?>"<?php if ($user['price_type'] === $price['1s_id']): ?> selected="selected"<?php endif; ?>><?=$price['sign'];?></option>
                            <?php endforeach; ?>

						</select>
					</div>
					<div class="cell w_100">
						<a class="text_link" href="<?=$this->uri->full_url('admin/users/orders?menu_id=' . $menu_id . '&user_id=' . $user['user_id']);?>"><?=$user['orders'];?></a>
					</div>
					<div class="cell w_20 icon"><a href="#" class="delete"></a></div>
				</div>
			</li>
			<?php endforeach; ?><?php endif; ?>
		</ul>
	</div>
	<?php if (isset($paginator)) echo $paginator; ?>
</div>
<script type="text/javascript">
	$(function () {
		var $component = $('#admin_component_users'),
			$loader = $component.find('.component_loader');

		function row_decor() {
			$component.find('.users_list')
				.find('li').removeClass('grey').each(function (i) { $(this).find('.number').text(i + 1); })
				.end()
				.find('li:odd').addClass('grey');
		}

		row_decor();

		var code_val = '';

		$component
			.on('focus', '[name="price_code"]', function () {
				code_val = $(this).val();
			})
			.on('blur keyup', '[name="price_code"]', function (e) {
				if ($(this).val() !== code_val) {
					if ((e.hasOwnProperty('type') && e.type === 'focusout') || (e.hasOwnProperty('keyCode') && e.keyCode === 13)) {
						$loader.show();

						var $input = $(this);

						$.post(
							'<?php echo $this->uri->full_url('admin/users/update_user_code'); ?>',
							{
								user_id: $input.closest('li').data('id'),
								code: $input.val()
							},
							function (response) {
								if (response.success) {
									code_val = $input.val();

									$input.closest('li').find('select').val(response.price_type)
									$loader.hide();
								}
							},
							'json'
						);
					}
				}
			})
			.on('change', 'select', function () {
				$loader.show();

				var $select = $(this);

				$.post(
					'<?php echo $this->uri->full_url('admin/users/update_user_price'); ?>',
					{
						user_id: $select.closest('li').data('id'),
						price_type: $select.val()
					},
					function (response) {
						if (response.success) {
							$loader.hide();
						}
					},
					'json'
				);
			})
			.on('click', '.delete', function (e) {
				e.preventDefault();

				var $link = $(this);

				confirmation('Видалити користувача?', function () {
					$loader.show();
					$.post(
						'<?php echo $this->uri->full_url('admin/users/delete_user'); ?>',
						{
							user_id: $link.closest('li').data('id')
						},
						function (response) {
							if (response.success) {
								$link.closest('li').slideUp(function () {
									$(this).remove();
									row_decor();
									$loader.hide();
								});
							}
						},
						'json'
					);
				});
			})
			.on('mouseover', '.users_list .holder', function () {
				$(this).addClass('active');
			})
			.on('mouseout', '.users_list .holder', function () {
				$(this).removeClass('active');
			});
	});
</script>