<?php
	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_js('admin/ui/jquery-ui-1.10.3.custom.min.js');
	$this->template_lib->set_js('admin/ui/jquery.ui.datepicker-uk.js');

	$this->template_lib->set_css('js/admin/ui/jquery-ui-1.10.3.custom.min.css', TRUE);
?>
<div class="fm admin_component">
    <div class="component_loader"></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="comments"></div>
		</div>
		<div class="fm component_edit_links"></div>
	</div>
	<div class="fm admin_menu">
		<?php if (isset($user)): ?>
			<div class="fm admin_massage" style="width: 100%">
				Перегляд відгуків користувача: <b><?=$user['name'];?></b> (<?=$user['email'];?><?php if ($user['phone'] != ''): ?>, <?=$user['phone']?><?php endif; ?>)
			</div>
		<?php endif; ?>
		<?php if (count($comments) > 0): ?>
		<ul id="comments_list">
			<?php foreach ($comments as $comment): ?>
			<li data-id="<?=$comment['comment_id'];?>">
				<div class="holder">
					<div class="cell last_edit<?=($comment['new'] == 1 ? ' active' : '');?>"></div>
					<div class="cell w_40 number"><?=$comment['comment_id'];?></div>
					<div class="cell w_20 icon"><a href="#" class="hide-show<?=($comment['hidden'] == 0 ? ' active' : '');?>"></a></div>
					<div class="cell auto" style="width:200px; color: #000; vertical-align: top">
						<div class="title">
							<strong>Дата:</strong> <?=date('d.m.Y H:i', $comment['date']);?><br>
							<strong>Ім’я:</strong> <?=$comment['name'];?><br>
							<strong>E-mail:</strong> <?=$comment['email'];?><br><br>
						</div>
					</div>
					<div class="cell auto" style=" color: #000; vertical-align: top">
						<a href="<?=$comment['product_link'];?>" target="_blank" class="text_link"><?=$comment['title'];?></a>
                        <br><br><?=$comment['comment'];?>
					</div>
                    <div class="cell w_400">
                        <textarea style="width: 90%"><?=str_replace('<br>', "\n", stripslashes($comment['answer']))?></textarea>
                        <br>
                        <a href="#" class="text_link save_answer">Зберегти відповідь</a>
                    </div>
					<div class="cell w_20 icon"><a href="#" class="delete"></a></div>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
		<?=$paginator;?>
		<?php else: ?>
			<div class="fm admin_massage">На даний час немає відгуків</div>
		<?php endif; ?>
	</div>
</div>
<script type="text/javascript">
	function row_decor() {
		$('#comments_list')
			.find('li').removeClass('grey')
			.end()
			.find('li:even').addClass('grey');
	}

	$(function () {
		row_decor();

		$('#comments_list')
            .on('click', '.save_answer', function (e) {
                e.preventDefault();

                component_loader_show($('.component_loader'), '');

                var $link = $(this);

                $.post(
                    '<?php echo $this->uri->full_url('admin/users/save_comment_answer'); ?>',
                    {
                        comment_id: $link.closest('li').data('id'),
                        answer: $link.closest('.cell').find('textarea').val()
                    },
                    function (response) {
                        if (response.success) {
                            component_loader_hide($('.component_loader'), '');
                        }
                    },
                    'json'
                );
            })
			.on('click', '.hide-show', function (e) {
				e.preventDefault();

				component_loader_show($('.component_loader'), '');

				var $link = $(this);
				$link.toggleClass('active');

				$.post(
					'<?php echo $this->uri->full_url('admin/users/visibility_comment'); ?>',
					{
						comment_id: $link.closest('li').data('id'),
						status: $link.hasClass('active') ? 0 : 1
					},
					function (response) {
						if (response.success) {
							component_loader_hide($('.component_loader'), '');
						}
					},
					'json'
				);
			})
			.on('click', '.delete', function (e) {
				e.preventDefault();

				var $link = $(this);

				confirmation('Видалити відгук?', function () {
					$.post(
						'<?php echo $this->uri->full_url('admin/users/delete_comment'); ?>',
						{
							comment_id: $link.closest('li').data('id')
						},
						function (response) {
							if (response.success) {
								$link.closest('li').slideUp(function () {
									$(this).remove();
									row_decor();
								});
							}
						},
						'json'
					);
				});
			})
			.on('mouseover', '.holder', function () {
				$(this).addClass('active');
			})
			.on('mouseout', '.holder', function () {
				$(this).removeClass('active');
			});
	});
</script>