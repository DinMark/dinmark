<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_users
	 *
	 * @property Admin_users_model $admin_users_model
	 */
	class Admin_users extends MX_Controller
	{
		/**
		 * Список користувачів сайту
		 */
		public function index()
		{
			$this->init_model->is_admin('redirect');

			$menu_id = intval($this->input->get('menu_id'));
			if ($menu_id < 1) show_error('Invalid request');

			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->template_lib->set_admin_menu_active('users');
			$this->template_lib->set_admin_menu_active('index', 'sub_level');

			$this->template_lib->set_title('Користувачі сайту (зареєстровані)');

			$this->load->model('admin_users_model');

			$page = intval($this->input->get('page'));
			if ($page < 1) $page = 1;

			$template_data = array(
				'menu_id' => $menu_id,
				'users' => $this->admin_users_model->get_users($page)
			);

			$base_url = $this->uri->full_url('admin/users/index?menu_id=' . $menu_id);

			$pagination_config = array(
				'cur_page' => $page,
				'padding' => 1,
				'first_url' => $base_url,
				'base_url' => $base_url . '&page=',
				'per_page' => 100,
				'total_rows' => $this->admin_users_model->get_total_users(),
				'full_tag_open' => '<nav class="fm admin_paginator"><table align="center"><tr><td align="center"><div class="fm paginator">',
				'full_tag_close' => '</div></td></tr></table></nav>',
				'first_link' => FALSE,
				'last_link' => FALSE,
				'prev_link' => '&lt;',
				'next_link' => '&gt;'
			);

			$this->load->library('pagination', $pagination_config);
			$template_data['paginator'] = $this->pagination->create_links();

			$this->template_lib->set_content($this->load->view('users_tpl', $template_data, TRUE));
		}

		public function update_user_code()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$user_id = $this->input->post('user_id');
			$code = (string)$this->input->post('code');

			if ($user_id > 0)
			{
				$this->load->model('admin_users_model');

				$price_type = 'p0';

				if ($code !== '') {
					$code_data = $this->admin_users_model->get_code($code);

					if (isset($code_data['price_type'])) {
						$price_type = $code_data['price_type'];
					} else {
						$code = 0;
					}
				}

				$this->admin_users_model->update_user(
					$user_id,
					array(
						'code' => $code,
						'price_type' => $price_type,
					)
				);


				$response['price_type'] = $price_type;
				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		public function update_user_price()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$user_id = $this->input->post('user_id');
			$price_type = (string)$this->input->post('price_type');

			if ($user_id > 0)
			{
				$this->load->model('admin_users_model');

				$this->admin_users_model->update_user(
					$user_id,
					array(
						'price_type' => $price_type,
					)
				);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		public function delete_user()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$user_id = $this->input->post('user_id');

			if ($user_id > 0)
			{
				$this->load->model('admin_users_model');
				$this->admin_users_model->delete_user($user_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Список користувачів сайту
		 */
		public function order_users()
		{
			$this->init_model->is_admin('redirect');

			$menu_id = intval($this->input->get('menu_id'));
			if ($menu_id < 1) show_error('Invalid request');

			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->template_lib->set_admin_menu_active('users');
			$this->template_lib->set_admin_menu_active('order_users', 'sub_level');

			$this->template_lib->set_title('Користувачі сайту (замовлення)');

			$this->load->model('admin_users_model');

			$page = intval($this->input->get('page'));
			if ($page < 1) $page = 1;

			$template_data = array(
				'menu_id' => $menu_id,
				'users' => $this->admin_users_model->get_order_users($page),
			);

			$base_url = $this->uri->full_url('admin/users/order_users?menu_id=' . $menu_id);

			$pagination_config = array(
				'cur_page' => $page,
				'padding' => 1,
				'first_url' => $base_url,
				'base_url' => $base_url . '&page=',
				'per_page' => 100,
				'total_rows' => $this->admin_users_model->get_total_order_users(),
				'full_tag_open' => '<nav class="fm admin_paginator"><table align="center"><tr><td align="center"><div class="fm paginator">',
				'full_tag_close' => '</div></td></tr></table></nav>',
				'first_link' => FALSE,
				'last_link' => FALSE,
				'prev_link' => '&lt;',
				'next_link' => '&gt;'
			);

			$this->load->library('pagination', $pagination_config);
			$template_data['paginator'] = $this->pagination->create_links();

			$this->template_lib->set_content($this->load->view('order_users_tpl', $template_data, TRUE));
		}

		public function delete_order_user()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$email = $this->input->post('email', TRUE);

			if ($email != '')
			{
				$this->load->model('admin_users_model');
				$this->admin_users_model->delete_user($email);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		public function orders()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Замовлення');

			$this->template_lib->set_admin_menu_active('users');
			$this->template_lib->set_admin_menu_active('orders', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$page = intval($this->input->get('page'));
			if ($page < 1) $page = 1;

			$user_id = intval($this->input->get('user_id'));
			$email = $this->input->get('email', TRUE);

			$date_with = $this->input->get('date_with', TRUE);
			if ($date_with !== NULL AND $date_with !== '')
			{
				$date_with = strtotime($date_with);
			}
			else
			{
				$date_with = NULL;
			}

			$date_to = $this->input->get('date_to', TRUE);
			if ($date_to !== NULL AND $date_to !== '')
			{
				$date_to = strtotime($date_to);
			}
			else
			{
				$date_to = NULL;
			}

			$this->load->model('admin_users_model');
			$this->load->helper('functions');

			$total = $this->admin_users_model->get_orders_total($page, $email, $user_id, $date_with, $date_to);

			$template_data = array(
				'menu_id' => $menu_id,
				'user_id' => $user_id,
				'email' => $email,
				'date_with' => $date_with,
				'date_to' => $date_to,
				'total' => $total,
				'orders' => $this->admin_users_model->get_orders($email, $user_id, $date_with, $date_to),
				'stat' => $this->admin_users_model->get_orders_stat($email, $user_id, $date_with, $date_to),
			);

			if ($user_id > 0)
			{
				$template_data['user'] = $this->admin_users_model->get_user($user_id);
			}

			if ($email != '')
			{
				$template_data['user'] = $this->admin_users_model->get_order_user($email);
			}

			$query = array();
			if ($user_id > 0) $query[] = 'user_id=' . $user_id;
			if ($email != '') $query[] = 'email=' . $email;
			if ($date_with !== NULL) $query[] = 'date_with=' . $date_with;
			if ($date_to !== NULL) $query[] = 'date_to=' . $date_to;

			$base_url = $this->uri->full_url('admin/users/orders');

			$pagination_config = array(
				'cur_page' => $page,
				'padding' => 1,
				'first_url' => $base_url,
				'base_url' => $base_url . (count($query) > 0 ? ('?' . implode('&', $query) . 'page=') : '&page=') . '?page=',
				'per_page' => 100,
				'total_rows' => $total,
				'full_tag_open' => '<nav class="fm admin_paginator"><table align="center"><tr><td align="center"><div class="fm paginator">',
				'full_tag_close' => '</div></td></tr></table></nav>',
				'first_link' => FALSE,
				'last_link' => FALSE,
				'prev_link' => '&lt;',
				'next_link' => '&gt;'
			);

			$this->load->library('pagination', $pagination_config);
			$template_data['paginator'] = $this->pagination->create_links();

			$this->template_lib->set_content($this->load->view('orders_tpl', $template_data, TRUE));
		}

		public function delete_order()
		{
			$this->init_model->is_admin('json');

			$order_id = intval($this->input->post('order_id'));

			if ($order_id > 0)
			{
				$this->load->model('admin_users_model');
				$this->admin_users_model->delete_order($order_id);
			}

			return json_encode(array('success' => TRUE));
		}


		##############################################################################################################################################

		/**
		 * Підписники
		 */
		public function subscribers()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Підпиисники');

			$this->template_lib->set_admin_menu_active('users');
			$this->template_lib->set_admin_menu_active('subscribers', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, true);

			$page = intval($this->input->get('page'));
			if ($page < 1) {
				$page = 1;
			}

			$email = (string)$this->input->get('email', true);

			$this->load->model('admin_users_model');
			$this->load->helper('functions');

			$total = $this->admin_users_model->get_subscribers_total($email);

			$template_data = array(
				'menu_id' => $menu_id,
				'email' => $email,
				'total' => $total,
				'subscribers' => $this->admin_users_model->get_subscribers($page, $email),
			);

			$base_url = $this->uri->full_url('admin/users/subscribers');


			$pagination_config = array(
				'cur_page' => $page,
				'padding' => 1,
				'first_url' => $base_url,
				'base_url' => $base_url . '?' . ($email !== '' ? 'email' . $email . '&' : '') . 'page=',
				'per_page' => 100,
				'total_rows' => $total,
				'full_tag_open' => '<nav class="admin_paginator"><div class="paginator">',
				'full_tag_close' => '</div></nav>',
				'first_link' => false,
				'last_link' => false,
				'prev_link' => '&lt;',
				'next_link' => '&gt;'
			);

			$this->load->library('pagination', $pagination_config);
			$template_data['paginator'] = $this->pagination->create_links();

			$this->template_lib->set_content($this->load->view('subscribers_tpl', $template_data, true));
		}

		/**
		 * Видалення підписника
		 *
		 * @return string
		 */
		public function delete_subscriber()
		{
			$this->init_model->is_admin('json');

			$subscriber_id = (int)$this->input->post('subscriber_id');

			if ($subscriber_id > 0) {
				$this->load->model('admin_users_model');

				$this->admin_users_model->delete_subscriber($subscriber_id);
			}

			return json_encode(array('success' => true));
		}

		###########################################################################################

		/**
		 * Відгуки
		 */
		public function comments()
		{
			error_reporting(E_ALL);
			ini_set('display_errors', true);

			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Відгуки');

			$this->template_lib->set_admin_menu_active('users');
			$this->template_lib->set_admin_menu_active('comments', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$page = intval($this->input->get('page'));
			if ($page < 1) $page = 1;

			$user_id = intval($this->input->get('user_id'));
			$email = $this->input->get('email', TRUE);

			$this->load->model('admin_users_model');
			$this->load->helper('functions');

			$total = $this->admin_users_model->get_comments_total($email, $user_id);
			$comments = $this->admin_users_model->get_comments($page, $email, $user_id);

			$template_data = array(
				'menu_id' => $menu_id,
				'user_id' => $user_id,
				'email' => $email,
				'total' => $total,
				'comments' => $comments,
			);

			foreach ($comments as $comment_key => $comment_val) {
				$menu_id = $this->admin_users_model->get_product_link($comment_val['product_id']);
				$template_data['comments'][$comment_key]['product_link'] = $this->uri->full_url('admin/catalog/edit?menu_id='.$menu_id.'&product_id='.$comment_val['product_id']);
			}

			if ($user_id > 0)
			{
				$template_data['user'] = $this->admin_users_model->get_user($user_id);
			}

			if ($email != '')
			{
				$template_data['user'] = $this->admin_users_model->get_order_user($email);
			}

			$query = array();
			if ($user_id > 0) $query[] = 'user_id=' . $user_id;
			if ($email != '') $query[] = 'email=' . $email;

			$base_url = $this->uri->full_url('admin/users/comments');

			$pagination_config = array(
				'cur_page' => $page,
				'padding' => 1,
				'first_url' => $base_url,
				'base_url' => $base_url . (count($query) > 0 ? ('?' . implode('&', $query) . 'page=') : '&page=') . '?page=',
				'per_page' => 100,
				'total_rows' => $total,
				'full_tag_open' => '<nav class="fm admin_paginator"><table align="center"><tr><td align="center"><div class="fm paginator">',
				'full_tag_close' => '</div></td></tr></table></nav>',
				'first_link' => FALSE,
				'last_link' => FALSE,
				'prev_link' => '&lt;',
				'next_link' => '&gt;'
			);

			$this->load->library('pagination', $pagination_config);
			$template_data['paginator'] = $this->pagination->create_links();

			$this->template_lib->set_content($this->load->view('comments_tpl', $template_data, TRUE));
		}

		/**
		 * Приховування/відображення відгуку
		 *
		 * @return string
		 */
		public function visibility_comment()
		{
			$this->init_model->is_admin('json');

			$comment_id = intval($this->input->post('comment_id'));
			$status = intval($this->input->post('status'));

			if ($comment_id > 0) {
				$this->load->model('admin_users_model');
				$this->admin_users_model->update_comment($comment_id, array('hidden' => $status));
			}

			return json_encode(array('success' => TRUE));
		}

		/**
		 * Збереження відповіді на відгук
		 *
		 * @return string
		 */
		public function save_comment_answer()
		{
			$this->init_model->is_admin('json');

			$comment_id = intval($this->input->post('comment_id'));
			$answer = strval($this->input->post('answer'));

			if ($comment_id > 0) {
				$this->load->model('admin_users_model');
				$this->admin_users_model->update_comment(
					$comment_id,
					array(
						'answer' => $this->db->escape_str(
							str_replace(
								array("\n", "\r", "\t"),
								array('<br>', '', ''),
								$answer
							)
						)
					)
				);
			}

			return json_encode(array('success' => TRUE));
		}

		/**
		 * Видалення відгуку
		 *
		 * @return string
		 */
		public function delete_comment()
		{
			$this->init_model->is_admin('json');

			$comment_id = intval($this->input->post('comment_id'));

			if ($comment_id > 0)
			{
				$this->load->model('admin_users_model');
				$this->admin_users_model->delete_comment($comment_id);
			}

			return json_encode(array('success' => TRUE));
		}
	}