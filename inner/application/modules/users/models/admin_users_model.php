<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_users_model
	 */
	class Admin_users_model extends CI_Model
	{
		/**
		 * Отримання загальної кількості користувачів
		 */
		public function get_total_users()
		{
			return $this->db->count_all_results('users');
		}

		/**
		 * Отримання користувачів сайту
		 *
		 * @return array
		 */
		public function get_users($page)
		{
			$this->db->limit(100, ($page > 0 ? $page - 1 : $page) * 100);

			$result = $this->db
				->select('users.user_id, users.name, users.email, users.phone, users.price_type, users.code')
                ->order_by('users.user_id', 'desc')
				->get('users')
				->result_array();

			if (count($result) > 0)
			{
				foreach ($result as &$row)
				{
					$row['orders'] = $this->db->where('user_id', $row['user_id'])->count_all_results('orders');
				}
			}

			return $result;
		}

		/**
		 * Отримання даних користувача
		 *
		 * @param int $user_id
		 * @return array
		 */
		public function get_user($user_id)
		{
			return $this->db->select('name, email, phone, price_type')->where('user_id', $user_id)->get('users')->row_array();
		}

		/**
		 * Отримання коду на знижку
		 *
		 * @param string $code
		 * @return array|null
		 */
		public function get_code($code) {
			return $this->db
				->where('id', $code)
				->get('clients')
				->row_array();
		}

		/**
		 * Оновлення інформації про користувача
		 *
		 * @param int $user_id
		 * @param array $set
		 */
		public function update_user($user_id, $set)
		{
			$this->db
				->set($set)
				->where('user_id', $user_id)
				->update('users');
		}

		/**
		 * Видалення користувача
		 *
		 * @param string $user_id
		 */
		public function delete_user($user_id)
		{
			$this->db->delete('users', array('user_id' => $user_id));
			$this->db->delete('orders', array('user_id' => $user_id));
		}

		/**
		 * Отримання загальної кількості користувачів по замовленням
		 */
		public function get_total_order_users()
		{
			return $this->db->count_all_results('users');
		}

		/**
		 * Отримання списку користувачів сайту
		 *
		 * @return array
		 */
		public function get_order_users($page)
		{
			$this->db->distinct('orders.email');
			$this->db->select('orders.name, orders.phone, orders.email');
			$this->db->join('users', 'users.email = orders.email', 'left');
			$this->db->where('orders.user_id', 0);
			$this->db->where('users.user_id is null');

			$this->db->limit(100, ($page > 0 ? $page - 1 : $page) * 100);

			$result = $this->db->get('orders')->result_array();

			if (count($result) > 0)
			{
				foreach ($result as &$row)
				{
					$row['orders'] = $this->db->where('email', $row['email'])->count_all_results('orders');
				}
			}

			return $result;
		}

		/**
		 * Отримання даних користувача по замовленню
		 *
		 * @param string $email
		 * @return array
		 */
		public function get_order_user($email)
		{
			return $this->db->select('name, email, phone')->where('email', $email)->limit(1)->get('orders')->row_array();
		}

		/**
		 * Видалення користувача
		 *
		 * @param string $email
		 */
		public function delete_order_user($email)
		{
			$this->db->delete('orders', array('email' => $email));
		}

		/**
		 * Отримання кількості замовлень
		 *
		 * @param int $page
		 * @param string $email
		 * @param int $user_id
		 * @param int $date_with
		 * @param int $date_to
		 *
		 * @return array
		 */
		public function get_orders_total($page, $email, $user_id, $date_with, $date_to)
		{
			if ($user_id > 0)
			{
				$this->db->where('user_id', $user_id);
			}

			if ($email != '')
			{
				$this->db->where('email', $email);
			}

			if ($date_with !== NULL AND $date_to !== NULL)
			{
				$this->db->where('order_date between ' . $date_with . ' and ' . $date_to, NULL, FALSE);
			}

			if ($date_with !== NULL AND $date_to === NULL)
			{
				$this->db->where('order_date >=', $date_with);
			}

			if ($date_with === NULL AND $date_to !== NULL)
			{
				$this->db->where('order_date <=', $date_with);
			}

			$this->db->limit(100, ($page > 0 ? $page - 1 : $page) * 100);

			return $this->db->count_all_results('orders');
		}

		/**
		 * Отримання замовлень
		 *
		 * @param string $email
		 * @param int $user_id
		 * @param int $date_with
		 * @param int $date_to
		 *
		 * @return array
		 */
		public function get_orders($email, $user_id, $date_with, $date_to)
		{
		    $this->db
                ->set('status', 1)
                ->update('orders');

			if ($user_id > 0)
			{
				$this->db->where('user_id', $user_id);
			}

			if ($email != '')
			{
				$this->db->where('email', $email);
			}

			if ($date_with !== NULL AND $date_to !== NULL)
			{
				$this->db->where('order_date between ' . $date_with . ' and ' . $date_to, NULL, FALSE);
			}

			if ($date_with !== NULL AND $date_to === NULL)
			{
				$this->db->where('order_date >=', $date_with);
			}

			if ($date_with === NULL AND $date_to !== NULL)
			{
				$this->db->where('order_date <=', $date_with);
			}

			$result = $this->db->order_by('order_date', 'desc')->get('orders')->result_array();

			foreach ($result as $k => $v)
			{
				$result[$k]['products'] = $this->db->get_where('orders_products', array('order_id' => $v['order_id']))->result_array();
			}

			return $result;
		}

		/**
		 * Отримання статистики замовлень
		 *
		 * @param string $email
		 * @param int $user_id
		 * @param int $date_with
		 * @param int $date_to
		 * @return array
		 */
		public function get_orders_stat($email, $user_id, $date_with, $date_to)
		{
			$this->db->select_sum('total', 'total_sum')->select_sum('cost', 'cost_sum');
			$this->db->select_min('total', 'total_min')->select_max('total', 'total_max');
			$this->db->select_min('cost', 'cost_min')->select_max('cost', 'cost_max');
			$this->db->select_min('order_date', 'order_date_min')->select_max('order_date', 'order_date_max');

			if ($user_id > 0)
			{
				$this->db->where('user_id', $user_id);
			}

			if ($email != '')
			{
				$this->db->where('email', $email);
			}

			if ($date_with !== NULL AND $date_to !== NULL)
			{
				$this->db->where('order_date between ' . $date_with . ' and ' . $date_to, NULL, FALSE);
			}

			if ($date_with !== NULL AND $date_to === NULL)
			{
				$this->db->where('order_date >=', $date_with);
			}

			if ($date_with === NULL AND $date_to !== NULL)
			{
				$this->db->where('order_date <=', $date_with);
			}

			return $this->db->get('orders')->row_array();
		}

		/**
		 * Видалення замовлення
		 *
		 * @param int $order_id
		 */
		public function delete_order($order_id)
		{
			$this->db->delete('orders', array('order_id' => $order_id));
		}


		##############################################################################################################################################

		/**
		 * Отримання кількості підписників
		 *
		 * @param string $email
		 *
		 * @return int
		 */
		public
		function get_subscribers_total($email)
		{
			if ($email !== '') {
				$this->db->where('email', $email);
			}

			return (int)$this->db->count_all_results('subscribers');
		}

		/**
		 * Отримання підписників
		 *
		 * @param int $page
		 * @param string $email
		 *
		 * @return array
		 */
		public
		function get_subscribers($page, $email)
		{
			if ($email !== '') {
				$this->db->where('email', $email);
			}

			$this->db->limit(100, ($page > 0 ? $page - 1 : $page) * 100);

			return $this->db->order_by('subscriber_id', 'desc')->get('subscribers')->result_array();
		}

		/**
		 * Видалення підписника
		 *
		 * @param int $subscriber_id
		 */
		public function delete_subscriber($subscriber_id)
		{
			$this->db->delete('subscribers', array('subscriber_id' => $subscriber_id));
		}

		##########################################################################################

		/**
		 * Отримання кількості відгуків
		 *
		 * @param string $email
		 * @param int $user_id
		 *
		 * @return array
		 */
		public
		function get_comments_total($email, $user_id)
		{
			if ($user_id > 0) $this->db->where('user_id', $user_id);
			if ($email != '') $this->db->where('email', $email);

			return $this->db->count_all_results('catalog_comments');
		}

		/**
		 * Отримання відгуків
		 *
		 * @param int $page
		 * @param string $email
		 * @param int $user_id
		 *
		 * @return array
		 */
		public
		function get_comments($page, $email, $user_id)
		{
			$this->db->select('catalog_comments. *');

			if ($user_id > 0) $this->db->where('catalog_comments.user_id', $user_id);
			if ($email != '') $this->db->where('catalog_comments.email', $email);

			$this->db->join('catalog', 'catalog.product_id = catalog_comments.product_id', 'left');
			$this->db->select('catalog.title_' . LANG . ' as title');

			$this->db->limit(100, ($page > 0 ? $page - 1 : $page) * 100);

			$comments = $this->db->order_by('catalog_comments.date', 'desc')->get('catalog_comments')->result_array();

			$_comments = array();
			foreach ($comments as $v) $_comments[] = $v['comment_id'];

			if (count($_comments) > 0)
			{
				$this->db->set('new', 0);
				$this->db->where_in('comment_id', $_comments);
				$this->db->update('catalog_comments');
			}

			return $comments;
		}

		/**
		 * Приховування/відображення відгуку
		 *
		 * @param int $comment_id
		 * @param array $set
		 */
		public
		function update_comment($comment_id, $set)
		{
			$this->db->update(
				'catalog_comments',
				$set,
				array('comment_id' => $comment_id)
			);
		}

		/**
		 * Видалення відгуку
		 *
		 * @param int $comment_id
		 */
		public
		function delete_comment($comment_id)
		{
			$this->db->delete('catalog_comments', array('comment_id' => $comment_id));
		}

		public function get_product_link($product_id)
		{
			return $this->db->get_where('catalog', array('product_id' => $product_id))->row('menu_id');
		}
	}