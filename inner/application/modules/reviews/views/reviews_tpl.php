<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if (isset($reviews) AND count($reviews) > 0): ?>
<div class="fm review">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="sect-title"><span><? if(LANG=='ua')echo'відгуки:';if(LANG=='ru')echo'отзывы:';if(LANG=='en')echo'reviews:';?></span></div>
			</div>
			<?php foreach ($reviews as $k => $v): ?>
			<div class="col-md-4">
				<div class="fm one_rew">
					<div class="fm or_photo"><?php if ($v['image'] != ''): ?><img src="/upload/reviews/<?=$this->init_model->dir_by_id($v['review_id']);?>/<?=$v['review_id'];?>/<?=$v['image'];?>" alt="" ><?php endif; ?></div>
					<div class="fm or_whu"><?=$v['title'];?></div>
					<div class="fm or_txt"><?=$v['text'];?></div>
				</div>
			</div>
			<?php if ($k % 3 == 2 OR $k == count($reviews) - 1): ?></div><?php endif; ?>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?php endif; ?>