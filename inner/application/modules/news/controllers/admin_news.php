<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class News
	 *
	 * @property Admin_news_model $admin_news_model
	 */
	class Admin_news extends MX_Controller
	{
		/**
		 * Додавання новини
		 *
		 * @return int|string
		 */
		public function insert()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE, 'news_id' => 0);

			$component_id = intval($this->input->post('component_id'));
			$menu_id = intval($this->input->post('menu_id'));

			if ($component_id > 0 AND $menu_id > 0)
			{
				$this->load->model('admin_news_model');

				$db_set = array(
					'component_id' => $component_id,
					'menu_id' => $menu_id,
					'date' => time(),
					'position' => $this->admin_news_model->get_position($component_id),
					'update' => time(),
				);

				$response['success'] = TRUE;
				$response['news_id'] = $this->admin_news_model->insert($db_set);

				$this->session->set_userdata('last_news', $response['news_id']);
			}

			return json_encode($response);
		}

		/**
		 * Відображення/приховування новини
		 *
		 * @return string
		 */
		public function visible()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$news_id = intval($this->input->post('news_id'));
			$status = intval($this->input->post('status'));

			if ($news_id > 0 AND in_array($status, array(0, 1)))
			{
				$this->load->model('admin_news_model');

				$set = array('hidden' => $status);
				$where = array('news_id' => $news_id);

				$this->admin_news_model->update($set, $where);

				$this->session->set_userdata('last_news', $news_id);
				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Редагування новини
		 */
		public function edit()
		{
			$this->init_model->is_admin('redirect');

			$menu_id = intval($this->input->get('menu_id'));
			$news_id = intval($this->input->get('news_id'));

			if ($menu_id > 0 AND $news_id > 0)
			{
				$this->init_model->set_menu_id($menu_id, TRUE);
				$this->session->set_userdata('last_news', $news_id);

				$this->load->model('admin_news_model');

				$tpl_data = array(
					'news' => $this->admin_news_model->get($news_id),
					'news_images' => $this->admin_news_model->get_news_images($news_id),
					'news_id' => $news_id,
					'menu_id' => $menu_id,
					'languages' => $this->config->item('languages'),
				);

				$content = $this->load->view('admin/edit_tpl', $tpl_data, TRUE);
				$this->template_lib->set_content($content);

				$this->template_lib->set_title('Редагування новини');

				$this->template_lib->set_admin_menu_active('components');
				$this->template_lib->set_admin_menu_active('', 'sub_level');
			}
		}

		/**
		 * Збереження новини
		 */
		public function save()
		{
			$this->init_model->is_admin('json');

			$this->load->model('admin_news_model');
			$this->load->helpers(array('form', 'translit'));

			$news_id = intval($this->input->post('news_id'));

			$title = $this->input->post('title');
			$anons = $this->input->post('anons');
			$text = $this->input->post('text');

			$date = $this->input->post('date');
			$time = $this->input->post('time');

			if ($date != '')
			{
				$date = ($time != '') ? strtotime($date . ' ' . $time) : strtotime($date);
			}
			else
			{
				$date = time();
			}

			$set = array('date' => $date);

			foreach ($title as $key => $val)
			{
				$set['title_' . $key] = form_prep($val);
				$set['url_' . $key] = translit($val);
				$set['anons_' . $key] = form_prep($anons[$key]);

				$_text = str_replace(array("\r", "\n", "\t"), '', $text[$key]);
				$set['text_' . $key] = $this->db->escape_str($_text);
			}

			$where = array('news_id' => $news_id);
			$this->admin_news_model->update($set, $where);

			$this->session->set_userdata('last_news', $news_id);

			return json_encode(array('success' => TRUE));
		}

		/**
		 * Видалення новини
		 *
		 * @return string
		 */
		public function delete()
		{
			$this->init_model->is_admin();

			$response = array('success' => FALSE);

			$news_id = intval($this->input->post('news_id'));

			if ($news_id > 0)
			{
				$this->load->model('admin_news_model');
				$this->load->helper('file');

				$this->admin_news_model->delete($news_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Збереження порядку сортування новин
		 */
		public function save_position()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$news_id = $this->input->post('news_id');
			$position = $this->input->post('position');

			if (is_array($position))
			{
				$this->load->model('admin_news_model');

				foreach ($position as $key => $val)
				{
					$set = array('position' => $key);
					$where = array('news_id' => $val);
					$this->admin_news_model->update($set, $where);
				}

				$response['success'] = TRUE;
				$this->session->set_userdata('last_news', $news_id);
			}

			return json_encode($response);
		}

		### ЗОБРАЖЕННЯ ###

		/**
		 * Завантаження зображення новини
		 */
		public function upload_image()
		{
			$this->init_model->is_admin('json');

			$response = array(
				'success' => FALSE,
				'file_name' => ''
			);

			$component_id = intval($this->input->post('component_id'));
			$news_id = intval($this->input->post('news_id'));
			$menu_id = intval($this->input->post('menu_id'));

			if ($component_id > 0 AND $news_id > 0 AND $menu_id > 0)
			{
				$dir = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/upload/news/';
				if (!file_exists($dir)) mkdir($dir);

				$dir = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/upload/news/' . $this->init_model->dir_by_id($news_id) . '/';
				if (!file_exists($dir)) mkdir($dir);

				$dir .= $news_id . '/';
				if (!file_exists($dir)) mkdir($dir);

				$this->load->helper('translit');
				$file_name = translit_filename($_FILES['news_image']['name']);

				$upload_config = array(
					'upload_path' => $dir,
					'overwrite' => TRUE,
					'file_name' => $file_name,
					'allowed_types' => 'gif|jpg|png|jpeg'
				);

				$this->load->library('upload', $upload_config);

				if ($this->upload->do_upload('news_image'))
				{
					$this->load->library('Image_lib');

					$sizes = getimagesize($dir . $file_name);
					$height = (1000 * $sizes[1]) / $sizes[0];

					if ($sizes[0] > 1000)
					{
						$this->image_lib->resize($dir . $file_name, $dir . 's_' . $file_name, 1000, $height);

						$_w = 1000;
						$_h = $height;
					}
					else
					{
						copy($dir . $file_name, $dir . 's_' . $file_name);

						$_w = $sizes[0];
						$_h = $sizes[1];
					}

					$this->image_lib->resize($dir . $file_name, $dir . $file_name, 306, 135);

					$this->load->model('admin_news_model');

					$set = array(
						'news_id' => $news_id,
						'component_id' => $component_id,
						'menu_id' => $menu_id,
						'file_name' => $file_name,
						'position' => $this->admin_news_model->get_image_position($news_id),
					);

					$image_id = $this->admin_news_model->insert_image($set);

					$response['success'] = TRUE;
					$response['file_name'] = $file_name;
					$response['image_id'] = $image_id;
					$response['width'] = $_w;
					$response['height'] = $_h;
				}
			}

			$this->config->set_item('is_ajax_request', TRUE);
			return json_encode($response);
		}

		/**
		 * Обрізка зображення
		 */
		public function crop_image()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$image_id = intval($this->input->post('image_id'));
			$width = intval($this->input->post('width'));
			$coords = $this->input->post('coords');

			if ($coords['x'] >= 0 AND $coords['y'] >= 0 AND $coords['w'] > 0 AND $coords['h'] > 0 AND $image_id > 0)
			{
				$image = $this->db->select('news_id, component_id, file_name')->where('image_id', $image_id)->get('news_images')->row_array();

				if (count($image) > 0)
				{
					$dir = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/upload/news/' . $this->init_model->dir_by_id($image['news_id']) . '/' . $image['news_id'] . '/';

					if (file_exists($dir . 's_' . $image['file_name']))
					{
						$sizes = getimagesize($dir . 's_' . $image['file_name']);

						$w_index = $sizes[0] / $width;
						$h_index = $sizes[1] / round(($width * $sizes[1]) / $sizes[0]);

						$x = $coords['x'] * $w_index;
						$y = $coords['y'] * $h_index;
						$x2 = $coords['x2'] * $w_index;
						$y2 = $coords['y2'] * $h_index;

						$this->load->library('Image_lib');
						$this->image_lib->crop($dir . 's_' . $image['file_name'], $dir . $image['file_name'], $x2 - $x, $y2 - $y, $x, $y, 306, 135);

						$response['success'] = TRUE;
						$response['image'] = '/upload/news/' . $this->init_model->dir_by_id($image['news_id']) . '/' . $image['news_id'] . '/' . $image['file_name'] . '?t' . time();
					}
				}
			}

			return json_encode($response);
		}

		/**
		 * Порядок сортування зображень
		 */
		public function images_position()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$items = $this->input->post('items');

			if (is_array($items) AND count($items) > 0)
			{
				$this->load->model('admin_news_model');
				foreach ($items as $key => $val) $this->admin_news_model->update_image(intval($val), array('position' => intval($key)));

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Видалення зображення
		 */
		public function delete_image()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$image_id = intval($this->input->post('image_id'));

			if ($image_id > 0)
			{
				$this->load->model('admin_news_model');
				$this->admin_news_model->delete_image($image_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту
		 *
		 * @return string
		 */
		public function delete_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$component_id = intval($this->input->post('component_id'));

			if ($component_id > 0)
			{
				$this->load->model('admin_news_model');
				$this->load->helper('file');

				$this->admin_news_model->delete_component($component_id);

				$response['error'] = 0;
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту
		 *
		 * @return string
		 */
		public function delete_last_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$component_id = intval($this->input->post('component_id'));

			if ($component_id > 0)
			{
				$this->load->model('admin_news_model');

				$this->admin_news_model->delete_last_component($component_id);

				$response['error'] = 0;
			}

			return json_encode($response);
		}
	}