<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class News
	 *
	 * @property Admin_news_model $admin_news_model
	 * @property News_model $news_model
	 */
	class News extends MX_Controller
	{
		/**
		 * Вивід списка новин
		 *
		 * @param int $menu_id
		 * @param int $component_id
		 * @param int $hidden
		 * @param array $config
		 */
		public function index($menu_id, $component_id, $hidden, $config = array())
		{
			$page = intval($this->input->get('page'));

			$template_data = array(
				'component_id' => $component_id,
				'menu_id' => $menu_id,
				'hidden' => $hidden,
			);

			$base_url = $this->init_model->get_link($menu_id, '{URL}');

			if ($this->init_model->is_admin())
			{
				$this->load->model('admin_news_model');

				$pagination_config = array(
					'cur_page' => $page,
					'padding' => 1,
					'first_url' => $base_url,
					'base_url' => $base_url . '?page=',
					'per_page' => 30,
					'total_rows' => $this->admin_news_model->count_news($component_id),
					'full_tag_open' => '<nav class="fm admin_paginator">',
					'full_tag_close' => '</nav>',
					'first_link' => FALSE,
					'last_link' => FALSE,
					'prev_link' => '&lt;&lt;',
					'next_link' => '&gt;&gt;'
				);
				$this->load->library('pagination', $pagination_config);
				$pagination = $this->pagination->create_links();

				$template_data['news'] = $this->admin_news_model->get_news($component_id, $page);
				$template_data['last'] = $this->session->userdata('last_news');
				$template_data['pagination'] = $pagination;

				$this->load->view('admin/news_tpl', $template_data);
			}
			else
			{
				define('NEWS', TRUE);

				$this->load->model('news_model');

				$pagination_config = array(
					'cur_page' => $page,
					'padding' => 1,
					'first_url' => $base_url,
					'base_url' => $base_url . '?page=',
					'per_page' => 30,
					'total_rows' => $this->news_model->count_news($component_id),
					'full_tag_open' => '<nav class="fm paginator"><table align="center"><tr><td align="center"><div class="fm paginator">',
					'full_tag_close' => '</div></td></tr></table></nav>',
					'first_link' => FALSE,
					'last_link' => FALSE,
					'prev_link' => '&lt;&lt;',
					'next_link' => '&gt;&gt;'
				);
				$this->load->library('pagination', $pagination_config);
				$pagination = $this->pagination->create_links();

				$template_data['news'] = $this->news_model->get_news($component_id, $page);
				$template_data['pagination'] = $pagination;

				$this->load->view('news_tpl', $template_data);
			}
		}

		public function last_news($menu_id, $component_id, $hidden, $config = array())
		{
			$template_data = array(
				'component_id' => $component_id,
				'menu_id' => $menu_id,
				'hidden' => $hidden,
			);

			if ($this->init_model->is_admin())
			{
				$this->load->view('admin/last_news_tpl', $template_data);
			}
			else
			{
				$this->load->model('news_model');

				$template_data['news'] = $this->news_model->get_last_news();

				$this->load->view('last_news_tpl', $template_data);
			}
		}

		public function last_news_block()
		{
			$template_data = array(

			);

			$this->load->model('news_model');
			$template_data['news'] = $this->news_model->get_last_news();
			$this->load->view('last_news_block_tpl', $template_data);
		}

		/**
		 * Вивід детального новини
		 *
		 * @param int|null $news_id
		 */
		public function details($news_id = NULL)
		{
			if (!is_numeric($news_id)) show_404();

			$this->load->model('news_model');

			$news = $this->news_model->get($news_id);
			if (count($news) == 0) show_404();

			$this->init_model->set_menu_id($news['menu_id']);

			$this->template_lib->set_title($news['title']);
			$this->template_lib->set_bread_crumbs('', '');

			$this->load->library('seo_lib');

			$description = $this->seo_lib->generate_description($news['text']);
			$this->template_lib->set_description($description);

			$keywords = $this->seo_lib->generate_keywords($news['title'] . ' ' . $news['text']);
			$this->template_lib->set_keywords($keywords);

			$language_links = $this->news_model->get_language_links($news_id, $this->config->item('database_languages'));
			$this->template_lib->set_template_var('language_links', $language_links);

			$tpl_data = array(
				'news' => $news,
			);

			$content = $this->load->view('details_tpl', $tpl_data, TRUE);
			$this->template_lib->set_content($content);
		}
	}