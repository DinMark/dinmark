<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if (count($news) > 0): ?>
	<div class="news-list fm">
		<div class="news-list_title fm">Статті, новини</div>
		<?php foreach ($news as $row): ?><?php $url = $this->uri->full_url((($row['main'] == 0) ? $row['menu_url'] . '/' : '') . $row['url']); ?>
		<div class="news-item fm">
			<div class="news-item_img fm">
				<a href="<?=$url;?>">
					<?php if ($row['image'] != NULL): ?><img src="/upload/news/<?=$this->init_model->dir_by_id($row['news_id']);?>/<?=$row['news_id'];?>/<?=$row['image'];?>" alt="<?=$row['title'];?>"><?php endif; ?>
				</a>
			</div>
			<div class="news-item_text fm">
				<div class="news-item_title"><a href="<?=$url;?>"><?=$row['title'];?></a></div>
				<div class="news-item_descr"><?=stripslashes($row['anons']);?></div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>