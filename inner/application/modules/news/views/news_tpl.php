<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if (count($news) > 0): ?>
<section class="fm articles">
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="fm ats_title"><? if(LANG=='ua')echo'Cервіс';if(LANG=='ru')echo'Cервис';if(LANG=='en')echo'Service';?>:</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">			
			<div class="fm ats_all">
				<?php foreach ($news as $row): ?>
				<div class="fm ats_one"><?php $url = $this->uri->full_url((($row['main'] == 0) ? $row['menu_url'] . '/' : '') . $row['url']); ?>
					<div class="fm ats_photo"><a href="<?=$url;?>"><?php if ($row['image'] != NULL): ?><img src="/upload/news/<?=$this->init_model->dir_by_id($row['news_id']);?>/<?=$row['news_id'];?>/<?=$row['image'];?>" alt="<?=$row['title'];?>"><?php endif; ?></a></div>
					<div class="ats_name"><div><a href="<?=$url;?>"><?=$row['title'];?></a></div></div>
					<div class="ats_txt"><?=stripslashes($row['anons']);?></div>
					<div class="ats_link"><a href="<?=$url;?>" class="common-btn"><? if(LANG=='ua')echo'Читати далі';if(LANG=='ru')echo'Читать дальше';if(LANG=='en')echo'Read more';?></a></div>
				</div>
				<?php endforeach; ?>
				<?php //if ($pagination != '') echo $pagination; ?>
			</div>
		</div>
	</div>
</div>
</section>
<?php endif; ?>