<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<article itemscope itemtype="http://schema.org/Article">
	<div class="centre">
		<header><h1 itemprop="headline"><?=$news['title'];?></h1></header>
		<div class="fm article" itemprop="articleBody"><?=stripslashes($news['text']);?></div>
	</div>
	<meta itemprop="image" content="<?=$this->config->item('base_url');?>/upload/news/<?=$this->init_model->dir_by_id($news['news_id']);?>/<?=$news['news_id'];?>/<?=$news['image'];?>">
	<div itemprop="publisher" itemscope itemtype="https://schema.org/Organization" class="hidden">
		<meta itemprop="url" content="<?=$this->uri->full_url();?>">
		<meta itemprop="logo" content="<?=base_url('images/logo.png');?>">
		<meta itemprop="name" content="<?=$this->config->item('base_url');?>">
	</div>
	<meta itemprop="author" content="<?=$this->config->item('base_url');?>">
	<meta itemprop="datePublished" content="<?=date('Y-m-d H:i:s', $news['date']);?>">
	<meta itemprop="dateModified" content="<?=date('Y-m-d H:i:s', $news['date']);?>">
	<meta itemprop="mainEntityOfPage" content="<?=$this->uri->full_url($news['url']);?>">
</article>