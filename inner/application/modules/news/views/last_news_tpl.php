<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if (count($news) > 0): ?>
<section class="fm news_list"><div class="fm nl_title"><span>Варто знати</span></div><div class="centre"><div class="fm all_news_list">
	<?php foreach ($news as $row): ?>
	<div class="fm one_news"><?php $url = $this->uri->full_url((($row['main'] == 0) ? $row['menu_url'] . '/' : '') . $row['url']); ?>
		<div class="fm on_title"><a href="<?=$url;?>"><?=$row['title'];?></a></div>
		<div class="fm on_date"><?=date('d.m.Y', $row['date']);?></div>
		<div class="fm on_photo"><a href="<?=$url;?>"><?php if ($row['image'] != NULL): ?><img src="/upload/news/<?=$this->init_model->dir_by_id($row['news_id']);?>/<?=$row['news_id'];?>/<?=$row['image'];?>" alt="<?=$row['title'];?>"><?php endif; ?></a></div>
		<div class="fm on_text"><?=stripslashes($row['anons']);?></div>
		<a href="<?=$url;?>" class="fmr on_more">Читати далі</a>
	</div>
	<?php endforeach; ?>
</div></div></section>
<?php endif; ?>