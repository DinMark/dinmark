<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * @property Search_model $search_model
	 */

	ini_set('display_errors', true);

	class Search extends MX_Controller
	{
		public function autocomplete() {
			$response = array();

			$this->load->model('search_model');
			$this->load->model('catalog/catalog_model');
			$this->load->helper('functions');

			$query = $this->input->post('query', TRUE);
			$query = strip_tags($query);

			$tpl_data = array(
				'query' => $query,
				'count_results' => 0,
			);

			if ($this->search_model->set_query($query))
			{
				$result = $this->search_model->products_preview(10);

				if (count($result) > 0) {
					$t = '';

					if (LANG === 'ua') {
						$t = 'Результати пошуку за товарними групами';
					}

					if (LANG === 'ru') {
						$t = 'Результаты поиска по товарным группам';
					}

					$response[] = array(
						'title' => '<strong>' . $t . '</strong>',
					);

					foreach ($result as $v) {
						$title = $v['title'];
						$title = str_replace($query, '<strong>' . $query . '</strong>', $title);

						$response[] = array(
							'title' => $title,
							'url' => $this->uri->full_url($v['url']),
						);
					}

					$response[] = array(
						'all' => 'products',
						'total' => $this->search_model->count_products(),
					);
				}

				$result = $this->search_model->variants_preview(10);

				if (count($result) > 0) {
					$t = '';

					if (LANG === 'ua') {
						$t = 'Результати пошуку по стандартам';
					}

					if (LANG === 'ru') {
						$t = 'Результаты поиска по стандартам';
					}

					$response[] = array(
						'title' => '<strong>' . $t . '</strong>',
					);

					foreach ($result as $v) {
						$title = ($v['dins'] !== '' ? $v['dins'] . ' - ' : '') . $v['articul'] . ' - ' . $v['title'];
						$title = str_replace($query, '<strong>' . $query . '</strong>', $title);

						$response[] = array(
							'title' => $title,
							'url' => $this->uri->full_url($v['url']),
						);
					}

					$response[] = array(
						'all' => 'variants',
						'total' => $this->search_model->count_variants(),
					);
				}
			}

			return json_encode($response);
		}

		public function index()
		{
			if (LANG == 'ua') $w = 'Пошук';
			if (LANG == 'ru') $w = 'Поиск';
			if (LANG == 'en') $w = 'Search';

			$this->template_lib->set_title($w);
			$this->template_lib->set_bread_crumbs('', $w);

			$this->load->model('search_model');
			$this->load->model('catalog/catalog_model');
			$this->load->helper('functions');

			$mode = $this->input->get('mode', TRUE);

			if (!in_array($mode, array('products', 'variants'))) {
				$mode = 'products';
			}

			$query = $this->input->get('query', TRUE);
			$query = strip_tags($query);

			$tpl_data = array(
				'query' => $query,
				'count_results' => 0,
			);

			if ($this->search_model->set_query($query))
			{
				$tpl_data['count_results'] = $this->search_model->count_results($mode);

				if ($tpl_data['count_results']['articles'] > 0) $tpl_data['articles'] = $this->search_model->articles_preview();

				if ($tpl_data['count_results']['catalog'] > 0) {
					if ($mode === 'products') {
						$tpl_data['catalog'] = $this->search_model->catalog_products();
					}

					if ($mode === 'variants') {
						$tpl_data['catalog'] = $this->search_model->catalog_variants();
					}
				}

				if ($tpl_data['count_results']['news'] > 0) $tpl_data['news'] = $this->search_model->news_preview();

				$this->load->model('cart/cart_model');
				$tpl_data['cart'] = $this->cart_model->get_cart();
			}

			$content = $this->load->view('search_tpl', $tpl_data, TRUE);
			$this->template_lib->set_content($content);
		}

		public function news($page = 1)
		{
			define('HIDE_BANNER', TRUE);

			if (LANG == 'ua') $w = 'Пошук в новинах';
			if (LANG == 'ru') $w = 'Поиск в новостях';
			if (LANG == 'en') $w = 'Search in news';

			$this->template_lib->add_crumb('', $w);
			$this->seo_lib->set_title($w);

			$this->load->model('search_model');

			$query = $this->input->cookie('search_query', TRUE);
			$query = strip_tags($query);

			if (!is_numeric($page)) $page = 1;

			$tpl_data = array(
				'query' => $query,
				'count' => 0
			);

			if ($this->search_model->set_query($query))
			{
				$tpl_data['count'] = $this->search_model->news_count();

				if ($tpl_data['count'] > 0)
				{
					$tpl_data['news'] = $this->search_model->news($page);

					$pagination_config = array(
						'cur_page' => $page,
						'padding' => 1,
						'first_url' => 'search/news',
						'base_url' => 'search/news/page/',
						'per_page' => 10,
						'total_rows' => $tpl_data['count'],

						'full_tag_open' => '<table align="center"><tr><td align="center"><div class="fm paginator">',
						'full_tag_close' => '</div></td></tr></table>',
						'first_link' => FALSE,
						'last_link' => FALSE,
						'prev_link' => '&lt;&lt;',
						'next_link' => '&gt;&gt;'
					);

					$this->load->library('pagination', $pagination_config);
					$tpl_data['pagination'] = $this->pagination->create_links();

					$this->load->helper('translit');
				}
			}

			$content = $this->load->view('news_tpl', $tpl_data, TRUE);
			$this->template_lib->set_content($content);

			$this->template_lib->set_template('site_main_tpl');
			$this->template_lib->display();
		}
	}