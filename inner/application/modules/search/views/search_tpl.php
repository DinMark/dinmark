<?php
	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_css('js/raty/jquery.raty.css', TRUE);
	$this->template_lib->set_js('raty/jquery.raty.js');

	function crop_str($text, $query)
	{
		$text = stripslashes(strip_tags($text));
		$pos = mb_strpos($text, $query);
		$len = mb_strlen($text);

		$start = $pos - 100;
		$end = $pos + 300;

		if ($start < 0) $start = 0;
		if ($end > $len) $end = $len;

		$substring_limited = mb_substr($text, $start, $end);
		$str = mb_substr($substring_limited, mb_strpos($substring_limited, $query) > 0 ? mb_strpos($substring_limited, ' ') : 0, mb_strrpos($substring_limited, ' '));

		if (mb_strpos($text, $query) > 0) $str = '... ' . $str;
		$str .= ' ...';

		$str = str_replace($query, '<span style="background-color:#d0ee29">' . $query . '</span>', $str);

		return $str;
	}
?>

<section class="search-page fm">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="search-head fm">
					<div class="sect-title fm">
						<span><? if(LANG=='ua')echo'Результати пошуку';if(LANG=='ru')echo'Результаты поиска';if(LANG=='en')echo'Search results';?></span>
					</div>
					<div class="fm sp_long">
						<label><? if(LANG=='ua')echo'по запиту';if(LANG=='ru')echo'по запросу';if(LANG=='en')echo'on request';?>:</label>
						<span><?=$query;?></span>
					</div>
					<div class="fm sp_long">
						<label><? if(LANG=='ua')echo'знайдено результатів';if(LANG=='ru')echo'найдено результатов';if(LANG=='en')echo'results found';?>:</label>
						<span><?=$count_results['all'];?></span>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="fm search_list">
					<?php if (isset($catalog[0])): ?>
						<div class="sect-title fm"><span><? if(LANG=='ua')echo'В каталозі товарів';if(LANG=='ru')echo'В каталоге товаров';if(LANG=='en')echo'In the catalog of products';?></span></div>
						<div class="catalog-wrapper fm">
							<div class="row">
								<?php foreach ($catalog as $v): ?>
									<div class="col-md-3">
										<div class="fm one-good"><?php $url = $this->uri->full_url($v['url']); ?>
											<div class="one-good_wrapper fm">
												<?php if ($v['status'] == 1): ?><div class="one-good_status promo"><b></b><span><? if(LANG=='ua')echo'Акція';if(LANG=='ru')echo'Акция';if(LANG=='en')echo'Share';?></span><i></i></div><?php endif; ?>
					                            <?php if ($v['status'] == 3): ?><div class="one-good_status discount"><b></b><span><? if(LANG=='ua')echo'Знижка';if(LANG=='ru')echo'Скидка';if(LANG=='en')echo'Discount';?></span><i></i></div><?php endif; ?>
					                            <?php if ($v['status'] == 4): ?><div class="one-good_status hit"><b></b><span><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?></span><i></i></div><?php endif; ?>
												<div class="fm one-good_img">
													<div class="cell">
														<a href="<?=$url;?>">
															<?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/t_' . $v['image']);?>" alt="<?=$v['title'];?>"><?php endif; ?>
														</a>
													</div>
												</div>
												<div class="fm one-good_head">
													<a href="<?=$url;?>" class="one-good_name"><?=$v['title'];?></a>
													<!--div class="one-good_icons">
														<a href="#" class="one-good_compare">
															<i class="fa fa-balance-scale" aria-hidden="true"></i>
														</a>
														<a href="#" class="one-good_favorite">
															<i class="fa fa-heart-o" aria-hidden="true"></i>
														</a>
													</div-->
												</div>
												<div class="one-good_options fm">
													<?php foreach($v['filters'] as $_v): ?>
														<div class="one-good_options_row">
															<div class="one-good_options_cell"><?=$_v['filter_name'];?>:</div>
															<div class="one-good_options_cell"><?=implode(', ', $_v['childs']);?></div>
														</div>
													<?php endforeach; ?>
												</div>
												<div class="box-l fm">
													<div class="fm one-good_icons">
														<a href="#" class="one-good_compare"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>
														<a href="#" class="one-good_favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
													</div>
													<a href="<?=$url;?>" class="fmr one-good_btn"><?php if (LANG === 'ua'): ?>купити<?php endif; ?><?php if (LANG === 'ru'): ?>купить<?php endif; ?><?php if (LANG === 'en'): ?>buy<?php endif; ?></a>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>
					<?php if (isset($articles) AND is_array($articles)): ?>
						<div class="fm sect-title"><span><? if(LANG=='ua')echo'В статтях';if(LANG=='ru')echo'В статьях';if(LANG=='en')echo'In articles';?></span></div>
						<?php foreach ($articles as $row): ?>
							<div class="fm one_search_article">
								<a href="<?php echo ($row['main'] != 1) ? $this->uri->full_url($row['url']) : $this->uri->full_url(); ?>"><?php echo $row['title']; ?></a>
								<div class="fm art_ser_all"><p><?php echo crop_str($row['text'], $query); ?></p></div>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>

					<?php if (isset($news) AND is_array($news)): ?>
						<div class="fm sect-title"><span><? if(LANG=='ua')echo'В новинах';if(LANG=='ru')echo'В новостях';if(LANG=='en')echo'In news';?></span></div>
						<?php foreach ($articles as $row): ?>
							<div class="fm one_search_article">
								<a href="<?php echo ($row['main'] != 1) ? $this->uri->full_url($row['url']) : $this->uri->full_url(); ?>"><?php echo $row['title']; ?></a>
								<div class="fm art_ser_all"><p><?php echo crop_str($row['text'], $query); ?></p></div>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$(function () {
		$('.raty').raty({
			readOnly: true,
			path: 'js/raty/images',
			score: function() { return $(this).attr('data-score'); },
			number: function() { return $(this).attr('data-number'); }
		});
	});
</script>