<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class Search_model extends CI_Model
	{
		private $count = array();
		private $query = '';

		public function set_query($query = '')
		{
			if (LANG == 'ua') $def = 'Пошук...';
			if (LANG == 'ru') $def = 'Поиск...';
			if (LANG == 'en') $def = 'Search...';

			$this->query = $query;

			return (!empty($this->query) AND ($this->query != $def) AND mb_strlen($this->query) > 0) ? TRUE : FALSE;
		}

		public function count_products()
		{
			$this->db->join('menu', 'menu.id = catalog.menu_id');
			$this->db->where('menu.hidden', 0);

			$this->db->join('components', 'catalog.component_id = components.component_id');
			$this->db->where('components.hidden', 0);

			$this->db->where('catalog.hidden', 0);

			$this->db->like('`'. $this->db->dbprefix . 'catalog`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);

			return $this->db->count_all_results('catalog');
		}

		public function count_variants()
		{
			$this->db->join('menu', 'menu.id = catalog_variants.menu_id');
			$this->db->where('menu.hidden', 0);

			$this->db->join('components', 'catalog_variants.component_id = components.component_id');
			$this->db->where('components.hidden', 0);

			$this->db->where('catalog_variants.hidden', 0);

			$this->db->group_start();
			$this->db->or_like('`'. $this->db->dbprefix . 'catalog_variants`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->or_like('`'. $this->db->dbprefix . 'catalog_variants`.`articul`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->group_end();

			return $this->db->count_all_results('catalog_variants');
		}

		public function count_results($mode)
		{
			$count = array();

			/* Articles */
			$this->db->join('components', 'components.component_id = component_article.component_id');
			$this->db->where('components.hidden', 0);

			$this->db->join('menu', 'menu.id = component_article.menu_id');
			$this->db->where('menu.hidden', 0);

			$this->db->group_start();
			$this->db->like('`'. $this->db->dbprefix . 'component_article`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->or_like('`' . $this->db->dbprefix . 'component_article`.`text_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->group_end();
			$this->db->group_by('component_article.component_id');

			$count['articles'] = $this->db->count_all_results('component_article');

			/* Catalog */
			if ($mode === 'products') {
				$this->db->join('menu', 'menu.id = catalog.menu_id');
				$this->db->where('menu.hidden', 0);

				$this->db->join('components', 'catalog.component_id = components.component_id');
				$this->db->where('components.hidden', 0);

				$this->db->where('catalog.hidden', 0);

				$this->db->like('`'. $this->db->dbprefix . 'catalog`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);

				$count['catalog'] = $this->db->count_all_results('catalog');
			}

			if ($mode === 'variants') {
				$this->db->join('menu', 'menu.id = catalog_variants.menu_id');
				$this->db->where('menu.hidden', 0);

				$this->db->join('components', 'catalog_variants.component_id = components.component_id');
				$this->db->where('components.hidden', 0);

				$this->db->where('catalog_variants.hidden', 0);

				$this->db->group_start();
				$this->db->or_like('`'. $this->db->dbprefix . 'catalog_variants`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
				$this->db->or_like('`'. $this->db->dbprefix . 'catalog_variants`.`articul`', $this->db->escape_like_str($this->query), 'both', FALSE);
				$this->db->group_end();

				$count['catalog'] = $this->db->count_all_results('catalog_variants');
			}

			$this->db->join('components', 'components.component_id = news.component_id');
			$this->db->join('menu', 'menu.id = news.menu_id');
			$this->db->where('components.hidden', 0);
			$this->db->where('menu.hidden', 0);
			$this->db->group_start();
			$this->db->like('`'. $this->db->dbprefix . 'news`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->or_like('`' . $this->db->dbprefix . 'news`.`text_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->group_end();
			$this->db->group_by('news.news_id');
			$count['news'] = $this->db->count_all_results('news');

			$count['all'] = array_sum($count);

			return $count;
		}

		/* Catalog */
		public function products_preview($limit = null)
		{
			$this->db->select('catalog.title_' . LANG . ' as title, catalog.url_' . LANG . ' as url');

			$this->db->join('menu', 'catalog.menu_id = menu.id');
			$this->db->where('menu.hidden', 0);

			$this->db->join('components', 'catalog.component_id = components.component_id');
			$this->db->where('components.hidden', 0);

			$this->db->where('catalog.hidden', 0);

			$this->db->like('`'. $this->db->dbprefix . 'catalog`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);

			if ($limit !== null) {
				$this->db->limit($limit);
			} else {
				$this->db->limit(100);
			}

			return $this->db->get('catalog')->result_array();
		}

		public function variants_preview($limit = null)
		{
			$this->db->select('catalog_variants.variant_id, catalog_variants.articul, catalog_variants.title_' . LANG . ' as title, catalog_variants.url_' . LANG . ' as url');

			$this->db->join('menu', 'menu.id = catalog_variants.menu_id');
			$this->db->where('menu.hidden', 0);

			$this->db->join('components', 'catalog_variants.component_id = components.component_id');
			$this->db->where('components.hidden', 0);

			$this->db->where('catalog_variants.hidden', 0);

			$this->db->like('`'. $this->db->dbprefix . 'catalog_variants`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->or_like('`'. $this->db->dbprefix . 'catalog_variants`.`articul`', $this->db->escape_like_str($this->query), 'both', FALSE);

			if ($limit !== null) {
				$this->db->limit($limit);
			} else {
				$this->db->limit(100);
			}

			$products = $this->db->get('catalog_variants')->result_array();

			foreach ($products as &$v) {
				$filters = $this->catalog_model->get_variant_filters($v['variant_id']);

				if (isset($filters[1]['childs'])) {
					$v['dins'] = implode(', ', $filters[1]['childs']);
				} else {
					$v['dins'] = '';
				}
			}

			return $products;
		}

		public function catalog_products()
		{
			$prefix = $this->db->dbprefix;

			$this->db->select('catalog.product_id, catalog.status, catalog.title_' . LANG . ' as title, catalog.url_' . LANG . ' as url, catalog.sign_' . LANG . ' as sign');
			$this->db->select('(select `' . $prefix . 'catalog_images`.`image` from `' . $prefix . 'catalog_images` where `' . $prefix . 'catalog_images`.`product_id` = `' . $prefix . 'catalog`.`product_id` order by `' . $prefix . 'catalog_images`.`position` limit 1) as image', FALSE);

			$this->db->join('menu', 'menu.id = catalog.menu_id');
			$this->db->where('menu.hidden', 0);

			$this->db->join('components', 'catalog.component_id = components.component_id');
			$this->db->where('components.hidden', 0);

			$this->db->where('catalog.hidden', 0);

			$this->db->like('`'. $this->db->dbprefix . 'catalog`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);

			$this->db->limit(100);

			$products = $this->db->get('catalog')->result_array();

			foreach ($products as &$v) {
				$v['filters'] = $this->catalog_model->get_filters($v['product_id']);
			}

			return $products;
		}

		public function catalog_variants()
		{
			$prefix = $this->db->dbprefix;

			$this->db->select('catalog_variants.variant_id, catalog_variants.product_id, catalog_variants.title_' . LANG . ' as title, catalog_variants.url_' . LANG . ' as url');
			$this->db->select('(select `' . $prefix . 'catalog_images`.`image` from `' . $prefix . 'catalog_images` where `' . $prefix . 'catalog_images`.`product_id` = `' . $prefix . 'catalog_variants`.`product_id` order by `' . $prefix . 'catalog_images`.`position` limit 1) as image', FALSE);

			$this->db->join('menu', 'menu.id = catalog_variants.menu_id');
			$this->db->where('menu.hidden', 0);

			$this->db->join('components', 'catalog_variants.component_id = components.component_id');
			$this->db->where('components.hidden', 0);

			$this->db->where('catalog_variants.hidden', 0);

			$this->db->group_start();
			$this->db->or_like('`'. $this->db->dbprefix . 'catalog_variants`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->or_like('`'. $this->db->dbprefix . 'catalog_variants`.`articul`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->group_end();

			$this->db->limit(100);

			$products = $this->db->get('catalog_variants')->result_array();

			foreach ($products as &$v) {
				$v['filters'] = $this->catalog_model->get_variant_filters($v['variant_id']);
			}

			return $products;
		}

		/* Articles */

		public function articles_preview()
		{
			$this->db->select('component_article.title_' . LANG . ' as title, component_article.text_' . LANG . ' as text, menu.main, menu.url_path_' . LANG . ' as url');
			$this->db->join('components', 'components.component_id = component_article.component_id');
			$this->db->join('menu', 'menu.id = component_article.menu_id');
			$this->db->where('components.hidden', 0);
			$this->db->where('menu.hidden', 0);
			$this->db->group_start();
			$this->db->like('`'. $this->db->dbprefix . 'component_article`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->or_like('`' . $this->db->dbprefix . 'component_article`.`text_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->group_end();
			$this->db->group_by('component_article.component_id');

			$r = $this->db->get('component_article');

			return ($r->num_rows() > 0) ? $r->result_array() : NULL;
		}

		/* News */

		public function news_preview()
		{
			$this->db->select('news.title_' . LANG . ' as title, news.url_' . LANG . ' as url, news.anons_' . LANG . ' as anons, menu.main, menu.url_path_' . LANG . ' as menu_url');
			$this->db->join('components', 'components.component_id = news.component_id');
			$this->db->join('menu', 'menu.id = news.menu_id');
			$this->db->where('components.hidden', 0);
			$this->db->where('menu.hidden', 0);
			$this->db->group_start();
			$this->db->like('`'. $this->db->dbprefix . 'news`.`title_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->or_like('`' . $this->db->dbprefix . 'news`.`text_' . LANG . '`', $this->db->escape_like_str($this->query), 'both', FALSE);
			$this->db->group_end();
			$this->db->group_by('news.news_id');

			$r = $this->db->get('news');

			return ($r->num_rows() > 0) ? $r->result_array() : NULL;
		}
	}
