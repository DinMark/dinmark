<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_catalog_model
	 */
	class Admin_catalog_model extends CI_Model
	{
		public function get_similar($product_id, $reverse = FALSE)
		{
			$this->db->select('catalog.product_id, catalog.title_' . LANG . ' as title');

			if (!$reverse)
			{
				$this->db->where('catalog_similar.product_id', $product_id);
				$this->db->join('catalog', 'catalog.product_id = catalog_similar.similar_id');
			}
			else
			{
				$this->db->where('catalog_similar.similar_id', $product_id);
				$this->db->join('catalog', 'catalog.product_id = catalog_similar.product_id');
			}

			return $this->db->get('catalog_similar')->result_array();
		}

		/**
		 * Додавання супутнього товару
		 *
		 * @param int $product_id
		 * @param int $similar_id
		 */
		public function set_similar($product_id, $similar_id)
		{
			$this->db->where('product_id', $product_id);
			$this->db->where('similar_id', $similar_id);
			$c = $this->db->count_all_results('catalog_similar');

			if ($c == 0)
			{
				$this->db->insert(
					'catalog_similar',
					array(
						'product_id' => $product_id,
						'similar_id' => $similar_id,
					)
				);
			}
		}

		/**
		 * Видалення супутнього товару
		 *
		 * @param int $product_id
		 * @param int $similar_id
		 */
		public function delete_similar($product_id, $similar_id)
		{
			$this->db->delete(
				'catalog_similar',
				array(
					'product_id' => $product_id,
					'similar_id' => $similar_id,
				)
			);
		}

		/**
		 * Кількість товарів для виводу.
		 * Підраховується загальна кількість товарів, з усіх компонентів, для яких
		 * відмічений даний компонент.
		 *
		 * @param int $component_id
		 *
		 * @return string
		 */
		public function count_products($component_id)
		{
			return $this->db->where('component_id', $component_id)->count_all_results('catalog');
		}

		/**
		 * Отримання списку товарів компоненту
		 *
		 * @param int $component_id
		 * @param int $page
		 * @param $per_page
		 * @param null|string $sort
		 *
		 * @param null $query
		 * @param array $ignore
		 * @return array
		 */
		public function get_products($component_id, $page, $per_page, $sort = NULL, $query = NULL, $ignore = array())
		{
			$prefix = $this->db->dbprefix;

			$this->db->select('catalog.product_id, catalog.hidden, catalog.status, catalog.title_' . LANG . ' as title');
			$this->db->select('(select `' . $prefix . 'catalog_images`.`image` from `' . $prefix . 'catalog_images` where `' . $prefix . 'catalog_images`.`product_id` = `' . $prefix . 'catalog`.`product_id` order by `' . $prefix . 'catalog_images`.`position` limit 1) as image', FALSE);

			if ($component_id > 0) {
				$this->db->where('catalog.component_id', $component_id);
			}

			if (is_array($ignore) AND count($ignore) > 0)
			{
				$this->db->where_not_in('catalog.product_id', $ignore);
			}

			if ($query != '')
			{
				//$this->db->group_start();
				$this->db->like('title_' . LANG, $query);
				//$this->db->or_like('code', $query);
				//$this->db->group_end();
			}

			switch ($sort)
			{
				/*
				case 'price_asc':
					$sort = array('catalog.price', 'asc');
					break;
				case 'price_desc':
					$sort = array('catalog.price', 'desc');
					break;
				*/
				case 'title_asc':
					$sort = array('cast(`' . $prefix . 'catalog`.`title_' . LANG . '` as char character set cp1251)', 'asc');
					break;
				case 'title_desc':
					$sort = array('cast(`' . $prefix . 'catalog`.`title_' . LANG . '` as char character set cp1251)', 'desc');
					break;
				default:
					$sort = array('catalog.position', 'asc');
			}
			$this->db->order_by($sort[0], $sort[1]);

			$this->db->group_by('catalog.product_id');
			if ($per_page > 0) $this->db->limit($per_page, ((($page > 0) ? $page - 1 : 1) * $per_page));
			return $this->db->get('catalog')->result_array();
		}

		/**
		 * Додавання нового товару
		 *
		 * @param int $component_id
		 * @param int $menu_id
		 * @param int $add_top
		 * @param bool|int $product_id
		 *
		 * @return mixed
		 */
		public function add($component_id, $menu_id, $add_top, $product_id = FALSE)
		{
			if ($product_id)
			{
				$position = 0;
				$result = $this->db->select('position')->where('product_id', $product_id)->get('catalog')->row_array();

				if (count($result) > 0)
				{
					$position = $result['position'] + 1;
					$this->db->set('`position`', '`position` + 1', FALSE)->where('component_id', $component_id)->where('position >', $result['position'])->update('catalog');
				}
			}
			else
			{
				$position = ($add_top == 1) ? $this->min_position($component_id) : $this->max_position($component_id);
			}

			$db_data = array(
				'component_id' => $component_id,
				'menu_id' => $menu_id,
				'position' => $position,
			);

			$this->db->insert('catalog', $db_data);
			$product_id = $this->db->insert_id();

			$set = array(
				'product_id' => $product_id,
				'component_id' => $component_id,
				'menu_id' => $menu_id,
			);
			$this->db->insert('catalog_product_bind_components', $set);

			$set = array(
				'item_id' => $product_id,
				'component_id' => $component_id,
				'menu_id' => $menu_id,
				'module' => 'catalog',
				'method' => 'details',
			);
			$this->db->insert('site_links', $set);

			$set = array(
				'item_id' => $product_id,
				'component_id' => $component_id,
				'menu_id' => $menu_id,
				'module' => 'catalog',
			);
			$this->db->insert('seo_tags', $set);

			return $product_id;
		}

		/**
		 * Мінімальна позиція товару в компоненті
		 *
		 * @param int $component_id
		 *
		 * @return int
		 */
		private function min_position($component_id)
		{
			$this->db->set('position', '`position` + 1', FALSE)->where('component_id', $component_id)->update('catalog');

			$position = $this->db->select_min('position', 'min')->where('component_id', $component_id)->get('catalog')->row_array();
			return $position['min'] - 1;
		}

		/**
		 * Максимальна поція товару в компоненті
		 *
		 * @param int $component_id
		 *
		 * @return int
		 */
		private function max_position($component_id)
		{
			$position = $this->db->select_max('position', 'max')->where('component_id', $component_id)->get('catalog')->row_array();
			return $position['max'] + 1;
		}

		/**
		 * Отримання інформації про товар
		 *
		 * @param int $product_id
		 *
		 * @return array
		 */
		public function get($product_id)
		{
			return $this->db->get_where('catalog', array('product_id' => $product_id))->row_array();
		}

		/**
		 * Додавання варіанту товару
		 *
		 * @param int $product_id
		 * @return int
		 */
		public function add_variant($product_id)
		{
			$this->db->insert('catalog_variants', array('product_id' => $product_id));
			return (int) $this->db->insert_id();
		}

		/**
		 * Оновлення варіанту товару
		 *
		 * @param int $variant_id
		 * @param array $set
		 * @return int
		 */
		public function update_variant($variant_id, $set)
		{
			$this->db->update('catalog_variants', $set, array('variant_id' => $variant_id));
		}

		/**
		 * Видалення варіанту товару
		 *
		 * @param int $variant_id
		 */
		public function delete_variant($variant_id)
		{
			$this->db->delete('catalog_variants', array('variant_id' => $variant_id));
		}

		/**
		 * Отримання варіантів товару
		 *
		 * @param int $product_id
		 * @return array
		 */
		public function get_variants($product_id)
		{
			return $this->db->order_by('variant_id')->where('product_id', $product_id)->get('catalog_variants')->result_array();
		}

		/**
		 * Вибірка ID компонентів до яких прив’язаний товар
		 *
		 * @param int $product_id
		 * @return array
		 */
		public function get_product_components($product_id)
		{
			$components = array();

			$result = $this->db->select('component_id')->where('product_id', $product_id)->get('catalog_product_bind_components')->result_array();
			foreach ($result as $v) $components[] = $v['component_id'];

			return $components;
		}

		/**
		 * Отримання опцій товару
		 *
		 * @param int $product_id
		 * @return array
		 */
		public function get_product_options($product_id)
		{
			$options = array();

			$this->db->select('option_id, price, price_usd, price_eur');
			$this->db->where('product_id', $product_id);

			$result = $this->db->get('catalog_product_bind_options')->result_array();

			foreach ($result as $row)
			{
				$options[$row['option_id']] = array(
					'price' => $row['price'],
					'price_usd' => $row['price_usd'],
					'price_eur' => $row['price_eur'],
				);
			}

			return $options;
		}

		/**
		 * Отримання фільтрів товару
		 *
		 * @param int $product_id
		 * @return array
		 */
		public function get_product_filters($product_id)
		{
			$filters = array();

			$this->db->select('value_id');
			$this->db->where('product_id', $product_id);

			$result = $this->db->get('catalog_product_bind_filters')->result_array();
			foreach ($result as $row) $filters[] = $row['value_id'];

			return $filters;
		}

		/**
		 * Отримання інформації про доставку
		 *
		 * @param int $product_id
		 * @return array
		 */
		public function get_product_delivery($product_id)
		{
			$c = $this->db->where('product_id', $product_id)->count_all_results('catalog_product_delivery');

			if ($c == 0)
			{
				$set = array('product_id' => $product_id);
				$this->db->insert('catalog_product_delivery', $set);
			}

			return $this->db->where('product_id', $product_id)->get('catalog_product_delivery')->row_array();
		}

		/**
		 * Оновлення інформації про доставку
		 *
		 * @param int $product_id
		 * @param array $set
		 */
		public function update_product_delivery($product_id, $set)
		{
			$this->db->update('catalog_product_delivery', $set, array('product_id' => $product_id));
		}

		/**
		 * Отримання зображень товару
		 *
		 * @param int $product_id
		 *
		 * @return array
		 */
		public function get_images($product_id)
		{
			return $this->db->order_by('position')->get_where('catalog_images', array('product_id' => $product_id))->result_array();
		}

		/**
		 * Зміна компоненту товару
		 *
		 * @param int $component_id
		 * @param array $products
		 */
		public function change_component($component_id, $products)
		{
			$menu_id = $this->db->select('menu_id')->where('component_id', $component_id)->get('components')->row('menu_id');

			if (is_numeric($menu_id))
			{
				foreach ($products as $v)
				{
					$_component_id = $this->db->select('component_id')->where('product_id', $v)->get('catalog')->row('component_id');

					$set = array('component_id' => $component_id, 'menu_id' => $menu_id);
					$where = array('item_id' => $v, 'module' => 'catalog', 'method' => 'details');
					$this->db->update('site_links', $set, $where);

					$set = array('component_id' => $component_id, 'menu_id' => $menu_id);
					$where = array('item_id' => $v, 'module' => 'catalog');
					$this->db->update('seo_tags', $set, $where);

					$set = array('menu_id' => $menu_id, 'component_id' => $component_id);
					$where = array('product_id' => $v);
					$this->update($set, $where, TRUE);

					$is_c = $this->db->where('product_id', $v)->where('component_id', $component_id)->count_all_results('catalog_product_bind_components');

					if ($is_c == 0)
					{
						$set = array('product_id' => $v, 'component_id' => $component_id, 'menu_id' => $menu_id);
						$this->db->insert('catalog_product_bind_components', $set);
					}

					$this->db->where('product_id', $v)->where('component_id', $_component_id)->delete('catalog_product_bind_components');
				}
			}
		}

		/**
		 * Оновлення інформації про товар
		 *
		 * @param array $set
		 * @param array $where
		 * @param bool $update_links
		 */
		public function update($set, $where, $update_links = FALSE)
		{
			$this->db->update('catalog', $set, $where);

			if ($update_links)
			{
				$languages = $this->config->item('languages');

				$this->db->select('menu_id');
				foreach ($languages as $key => $val)
				{
					$this->db->select('url_' . $key);
				}
				$product = $this->db->where($where)->get('catalog')->row_array();

				foreach ($languages as $key => $val)
				{
					$this->db->set('hash_' . $key, md5($product['url_' . $key]));
					$this->db->where('item_id', $where['product_id']);
					$this->db->where('module', 'catalog');
					$this->db->where('method', 'details');
					$this->db->update('site_links');
				}
			}
		}

		/**
		 * Встановлення/зняття акційної ціни для розділу
		 *
		 * @param int $product_id
		 * @param float $sale_percent
		 * @param int $sale_date
		 */
		public function global_sale($product_id, $sale_percent, $sale_date)
		{
			$product = $this->db->select('component_id')->where('product_id', $product_id)->get('catalog')->row_array();

			if (isset($product['component_id']))
			{
				if ($sale_percent == 0)
				{
					$this->db->set('new_price', 0);
					$this->db->set('sale_percent', 0);
					$this->db->set('sale_date', 0);
				}
				else
				{

					$this->db->set('`new_price`', 'round(`price` - (`price` * ' . $sale_percent . ' / 100), 2)', FALSE);
					$this->db->set('sale_percent', $sale_percent);
					$this->db->set('sale_date', $sale_date);
				}

				$this->db->where('component_id', $product['component_id']);
				$this->db->update('catalog');
			}
		}

		/**
		 * Зняття акційної знижки по закінченню часу
		 */
		public function sale_out()
		{
			$this->db->set('new_price', 0);
			$this->db->set('sale_percent', 0);
			$this->db->set('sale_date', 0);
			$this->db->where('sale_date <', time());
			$this->db->update('catalog');
		}

		/**
		 * Зняття статусу по закінченню часу
		 */
		public function status_out()
		{
			$this->db->set('status', 0);
			$this->db->set('status_date', 0);
			$this->db->where('status_date <', time());
			$this->db->update('catalog');
		}

		/**
		 * Прив’язка продукта до розділів
		 *
		 * @param int $product_id
		 * @param array $components
		 */
		public function update_bind_components($product_id, $components)
		{
			$component_id = $this->db->select('component_id')->where('product_id', $product_id)->get('catalog')->row('component_id');
			$product_components = $this->get_product_components($product_id);

			if (count($product_components) > 0)
			{
				$delete_rows = array_diff($product_components, $components);

				if (count($delete_rows) > 0)
				{
					foreach ($delete_rows as $v)
					{
						if ($v != $component_id) $this->db->delete('catalog_product_bind_components', array('component_id' => intval($v), 'product_id' => $product_id));
					}
				}
			}

			if (count($components) > 0)
			{
				foreach ($components as $v)
				{
					$c = $this->db->where('product_id', $product_id)->where('component_id', intval($v))->count_all_results('catalog_product_bind_components');
					$menu_id = $this->db->select('menu_id')->where('component_id', intval($v))->get('components')->row('menu_id');
					if ($c == 0) $this->db->insert('catalog_product_bind_components', array('product_id' => $product_id, 'component_id' => intval($v), 'menu_id' => $menu_id));
				}
			}

			$this->load->dbutil();
			$this->dbutil->optimize_table($this->db->dbprefix . 'catalog_product_bind_components');
		}

		/**
		 * Прив’язка продукта до фільтрів
		 *
		 * @param int $product_id
		 * @param array $filters
		 */
		public function update_bind_filters($product_id, $filters)
		{
			$component_filters = $this->get_product_filters($product_id);

			$_filters = array();
			foreach ($filters as $v) foreach ($v as $_v) $_filters[] = $_v;

			if (count($component_filters) > 0)
			{
				$delete_rows = array_diff($component_filters, $_filters);

				if (count($delete_rows) > 0)
				{
					foreach ($delete_rows as $v)
					{
						$this->db->delete('catalog_product_bind_filters', array('product_id' => $product_id, 'value_id' => intval($v)));
					}
				}
			}

			if (count($filters) > 0)
			{
				foreach ($filters as $k => $v)
				{
					foreach ($v as $_v)
					{
						$c = $this->db->where('value_id', intval($_v))->where('product_id', $product_id)->count_all_results('catalog_product_bind_filters');
						if ($c == 0) $this->db->insert('catalog_product_bind_filters', array('product_id' => $product_id, 'filter_id' => intval($k), 'value_id' => intval($_v)));
					}
				}
			}

			$this->load->dbutil();
			$this->dbutil->optimize_table($this->db->dbprefix . 'catalog_product_bind_filters');
		}

		/**
		 * Прив’язка продукта до опцій
		 *
		 * @param int $product_id
		 * @param array $price
		 * @param array $price_usd
		 * @param array $price_eur
		 * @param array $options
		 */
		public function update_bind_options($product_id, $options, $price = array(), $price_usd = array(), $price_eur = array())
		{
			$component_options = $this->get_product_options($product_id);

			if (count($component_options) > 0)
			{
				$delete_rows = array_diff($component_options, $options);

				if (count($delete_rows) > 0)
				{
					foreach ($delete_rows as $v)
					{
						$this->db->delete('catalog_product_bind_options', array('option_id' => intval($v), 'product_id' => $product_id));
					}
				}
			}

			if (count($options) > 0)
			{
				foreach ($options as $k => $v)
				{
					$c = $this->db->where('option_id', intval($v))->where('product_id', $product_id)->count_all_results('catalog_product_bind_options');
					if ($c == 0)
					{
						$set = array(
							'option_id' => intval($v),
							'product_id' => $product_id,
							'price' => floatval($price[$k]),
							'price_usd' => floatval($price_eur[$k]),
							'price_eur' => floatval($price_eur[$k]),
						);

						$this->db->insert('catalog_product_bind_options', $set);
					}
					else
					{
						$set = array(
							'price' => floatval($price[$k]),
							'price_usd' => floatval($price_eur[$k]),
							'price_eur' => floatval($price_eur[$k]),
						);

						$where = array(
							'option_id' => intval($v),
							'product_id' => $product_id,
						);

						$this->db->update('catalog_product_bind_options', $set, $where);
					}
				}
			}

			$this->load->dbutil();
			$this->dbutil->optimize_table($this->db->dbprefix . 'catalog_product_bind_options');
		}

		/**
		 * Збереження інформації про нове зображення
		 *
		 * @param int $product_id
		 * @param int $component_id
		 * @param int $menu_id
		 * @param int $file_name
		 *
		 * @return array
		 */
		public function insert_image($product_id, $component_id, $menu_id, $file_name)
		{
			$set = array(
				'product_id' => $product_id,
				'component_id' => $component_id,
				'menu_id' => $menu_id,
				'position' => $this->db->select_max('position', 'max')->where('product_id', $product_id)->get('catalog_images')->row('max') + 1,
				'image' => $file_name
			);
			$this->db->insert('catalog_images', $set);

			return array(
				'image_id' => $this->db->insert_id()
			);
		}

		/**
		 * Оновлення інформації про зображення
		 *
		 * @param array $set
		 * @param array $where
		 */
		public function update_image($set, $where)
		{
			$this->db->update('catalog_images', $set, $where);
		}

		/**
		 * Отримання зображення товару
		 *
		 * @param $image_id
		 *
		 * @return array
		 */
		public function get_image($image_id)
		{
			return $this->db->get_where('catalog_images', array('image_id' => $image_id))->row_array();
		}

		/**
		 * @param int $image_id
		 */
		public function delete_image($image_id)
		{
			$this->db->delete('catalog_images', array('image_id' => $image_id));
		}

		/**
		 * Видалення товару
		 *
		 * @param int $product_id
		 */
		public function delete($product_id)
		{
			$dir = ROOT_PATH . 'upload/catalog/' . $this->init_model->dir_by_id($product_id) . '/' . $product_id . '/';
			delete_files($dir, TRUE, FALSE, 1);

			$this->db->delete('catalog', array('product_id' => $product_id));
			$this->db->delete('catalog_variants', array('product_id' => $product_id));
			$this->db->delete('catalog_images', array('product_id' => $product_id));
			$this->db->delete('catalog_product_delivery', array('product_id' => $product_id));

			$this->db->delete('catalog_product_bind_components', array('product_id' => $product_id));

			$this->db->delete('site_links', array('item_id' => $product_id, 'module' => 'catalog'));
			$this->db->delete('seo_tags', array('item_id' => $product_id, 'module' => 'catalog'));
		}

		/**
		 * Видалення компоненту
		 *
		 * @param int $component_id
		 */
		public function delete_component($component_id)
		{
			$dir = ROOT_PATH . 'upload/catalog/' . $component_id . '/';
			delete_files($dir, TRUE, FALSE, 1);

			$result = $this->db->select('product_id')->where('component_id', $component_id)->get('catalog')->result_array();

			if (isset($result[0]))
			{
				foreach ($result as $v)
				{
					$this->db->delete('catalog_variants', array('product_id' => $v['product_id']));
					$this->db->delete('catalog_product_bind_components', array('product_id' => $v['product_id']));
					$this->db->delete('catalog_product_delivery', array('product_id' => $v['product_id']));
				}
			}

			$this->db->delete('catalog', array('component_id' => $component_id));
			$this->db->delete('catalog_images', array('component_id' => $component_id));
			$this->db->delete('catalog_product_bind_components', array('component_id' => $component_id));
			$this->db->delete('components', array('component_id' => $component_id));
			$this->db->delete('seo_tags', array('component_id' => $component_id));
		}

		/**
		 * Видалення компоненту "Наші бренди"
		 *
		 * @param int $component_id
		 */
		public function delete_brands_menu_component($component_id)
		{
			$this->db->delete('components', array('component_id' => $component_id));
		}

		/**
		 * Видалення компоненту "Розділи каталогу"
		 *
		 * @param int $component_id
		 */
		public function delete_catalog_menu_component($component_id)
		{
			$this->db->delete('components', array('component_id' => $component_id));
		}

		/**
		 * Видалення компоненту "Останні переглянуті товари"
		 *
		 * @param int $component_id
		 */
		public function delete_visited_component($component_id)
		{
			$this->db->delete('components', array('component_id' => $component_id));
		}

		/**
		 * Видалення компоненту "ТОП товари"
		 *
		 * @param int $component_id
		 */
		public function delete_top_component($component_id)
		{
			$this->db->delete('components', array('component_id' => $component_id));
		}

		###################################################################################

		public function after_import_status()
		{
			$this->db->set('availability', 2);
			$this->db->where('is_update', 0);
			$this->db->update('catalog');
		}

		public function get_import_lost()
		{
			return $this->db->select('product_id')->where('is_update', 0)->get('catalog')->result_array();
		}

		public function after_import()
		{
			$this->db->set('is_update', 0);
			$this->db->update('catalog');
		}

		###################################################################################

		/**
		 * Отримання розділів меню у яких додано компонент каталогу
		 *
		 * @return array
		 */
		public function get_components_menu()
		{
			$this->db->select('menu.id, menu.parent_id, menu.url_path_id as path_id, menu.name_' . LANG . ' as name, components.component_id');
			$this->db->join('components', 'components.menu_id = menu.id', 'left');
			$this->db->where('components.module', 'catalog');
			$this->db->where('components.method', 'index');
			$this->db->order_by('menu.menu_index');
			$this->db->order_by('menu.position');
			$menu = $this->db->get('menu')->result_array();

			if (count($menu) == 0) return array();

			$_menu = $menu;
			$menu = array();
			foreach ($_menu as $val)
			{
				$menu[$val['id']] = $val;
			}

			foreach ($menu as $val)
			{
				if ($val['component_id'] !== NULL)
				{
					$ids = explode('.', trim($val['path_id'], '.'));

					foreach ($ids as $id)
					{
						if (is_numeric($id)) $menu[$id]['component_id'] = $val['component_id'];
					}
				}
			}

			$_menu = $menu;
			$menu = array();
			foreach ($_menu as $val)
			{
				if (isset($val['parent_id']) and $val['component_id'] !== NULL)
				{
					$menu[$val['parent_id']][] = $val;
				}
			}

			return $menu;
		}
	}