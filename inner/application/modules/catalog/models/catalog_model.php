<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Catalog_model
	 */
	class Catalog_model extends CI_Model
	{
		/**
		 * Перевірка на наявність дочірніх пунктів меню з каталогом.
		 * Дочірнім рахується пункт меню з доданим компонентом каталогу.
		 *
		 * @param int|null $menu_id
		 * @return bool
		 */
		public function is_parent($menu_id = NULL)
		{
			if (!$this->init_model->is_admin()) $this->db->where('menu.hidden', 0);
			$this->db->join('components', 'components.menu_id = menu.id');
			$this->db->where('menu.parent_id', intval($menu_id));
			$this->db->where('components.module', 'catalog');
			$this->db->where('components.method', 'index');
			$this->db->where('components.hidden', 0);
			return (bool)$this->db->count_all_results('menu');
		}

		/**
		 * Отримання підпунктів активного пункту меню
		 *
		 * @param int|array|null $menu_id
		 * @return array
		 */
		public function get_categories($menu_id = NULL)
		{
			$this->db->select('menu.id, menu.name_' . LANG . ' as name, menu.url_path_' . LANG . ' as url, menu.static_url_' . LANG . ' as static_url, menu.image');
			$this->db->join('components', 'components.menu_id = menu.id');
			if (is_array($menu_id))
			{
				$this->db->where_in('menu.parent_id', $menu_id);
			}
			else
			{
				$this->db->where('menu.parent_id', intval($menu_id));
			}
			$this->db->where('menu.hidden', 0);
			$this->db->where('menu.main', 0);
			$this->db->where('components.module', 'catalog');
			$this->db->where('components.hidden', 0);
			$this->db->order_by('menu.level');
			$this->db->order_by('menu.position');
			$this->db->group_by('menu.id');
			return $this->db->get('menu')->result_array();
		}

		/**
		 * Отримання підкомпонентів
		 *
		 * @param int $menu_id
		 * @return array
		 */
		public function get_components($menu_id)
		{
			$components = array();

			$this->db->select('components.component_id');

			$this->db->like('menu.url_path_id', '.' . intval($menu_id) . '.');
			$this->db->where('menu.hidden', 0);
			$this->db->where('menu.main', 0);

			$this->db->join('components', 'components.menu_id = menu.id');
			$this->db->where('components.module', 'catalog');
			$this->db->where('components.method', 'index');
			$this->db->where('components.hidden', 0);

			$result = $this->db->get('menu')->result_array();
			foreach ($result as $v) $components[] = $v['component_id'];

			return $components;
		}

		/**
		 * Кількість товарів для виводу.
		 * Підраховується загальна кількість товарів, з усіх компонентів, для яких
		 * відмічений даний компонент.
		 *
		 * @param int|array $components
		 * @param array $filters
		 *
		 * @return string
		 */
		public function count_products($components, $filters = array())
		{
			$this->db->select('catalog.product_id');

			$this->db->join('catalog_product_bind_components', 'catalog_product_bind_components.product_id = catalog.product_id');
			$this->db->join('menu', 'catalog.menu_id = menu.id');
			$this->db->join('components', 'catalog.component_id = components.component_id');

			$this->db->where('menu.hidden', 0);
			$this->db->where('components.hidden', 0);

			if (is_numeric($components)) {
				$this->db->where('catalog_product_bind_components.component_id', $components);
			} else {
				$this->db->where_in('catalog_product_bind_components.component_id', $components);
			}

			$this->db->where('catalog.hidden', 0);

			if (isset($filters['min_price']) OR isset($filters['max_price']))
			{
				$this->db->join('catalog_variants', 'catalog_variants.product_id=catalog.product_id');
				if (isset($filters['min_price'])) $this->db->where('catalog_variants.price >=', $filters['min_price']);
				if (isset($filters['max_price'])) $this->db->where('catalog_variants.price <=', $filters['max_price']);
			}

			if (isset($filters['filters']))
			{
				foreach ($filters['filters'] as $filter_id => $values)
				{
					$this->db->join('catalog_product_bind_filters as filter_' . $filter_id, 'filter_' . $filter_id . '.product_id=catalog.product_id');
					$this->db->group_start();
					$this->db->where('filter_' . $filter_id . '.filter_id', $filter_id);
					if (count($values) == 1)
					{
						$this->db->where('filter_' . $filter_id . '.value_id', $values[0]);
					}
					else
					{
						$this->db->where_in('filter_' . $filter_id . '.value_id', $values);
					}
					$this->db->group_end();
				}
			}

			$this->db->group_by('catalog.product_id');

			return $this->db->get('catalog')->num_rows();
		}

		/**
		 * Отримання цінових меж компоненту
		 *
		 * @param int|array $components
		 * @param array $filters
		 *
		 * @return array
		 */
		public function get_price_limits($components, $filters = array())
		{
//			$prefix = $this->db->dbprefix;
//
//			$this->db->select_min($prefix . 'catalog_variants.price', 'min')->select_max($prefix . 'catalog_variants.price', 'max');
//
//			$this->db->join('catalog_variants', 'catalog_variants.product_id = catalog.product_id');
//			$this->db->join('menu', 'menu.id = catalog.menu_id');
//			$this->db->join('components', 'catalog.component_id = components.component_id');
//
//			if (isset($filters['filters']))
//			{
//				foreach ($filters['filters'] as $filter_id => $values)
//				{
//					$this->db->join('catalog_product_bind_filters as filter_' . $filter_id, 'filter_' . $filter_id . '.product_id=catalog.product_id');
//					$this->db->group_start();
//					$this->db->where('filter_' . $filter_id . '.filter_id', $filter_id);
//					if (count($values) == 1)
//					{
//						$this->db->where('filter_' . $filter_id . '.value_id', $values[0]);
//					}
//					else
//					{
//						$this->db->where_in('filter_' . $filter_id . '.value_id', $values);
//					}
//					$this->db->group_end();
//				}
//			}
//
//			$this->db->where('`' . $prefix . 'catalog`.`product_id` IN (select `product_id` from `' . $prefix . 'catalog_product_bind_components` where `component_id` = ' . $component_id . ')', NULL, FALSE);
//			$this->db->where('catalog.hidden', 0);
//			$this->db->where('menu.hidden', 0);
//			$this->db->where('components.hidden', 0);
//
//			$result = $this->db->get('catalog')->row_array();
//
//			$result['min'] = floor($result['min']);
//			$result['max'] = ceil($result['max']);
//
//			return $result;

			return array(
				'min' => 0,
				'max' => 0,
			);
		}

		/**
		 * Вибірка товарів
		 * Виводяться всі товари, з усіх компонентів, для яких
		 * відмічений даний компонент.
		 *
		 * @param int|array $components
		 * @param int $page
		 * @param int $per_page
		 * @param string $sort
		 * @param array $filters
		 *
		 * @return array
		 */
		public function get_products($components, $page = 1, $per_page = 15, $sort = '', $filters = array())
		{
			$prefix = $this->db->dbprefix;

			$this->db->select('catalog.product_id, catalog.status, catalog.title_' . LANG . ' as title, catalog.url_' . LANG . ' as url, catalog.sign_' . LANG . ' as sign, menu.url_path_' . LANG . ' as path_url');
			$this->db->select('(select `' . $prefix . 'catalog_images`.`image` from `' . $prefix . 'catalog_images` where `' . $prefix . 'catalog_images`.`product_id` = `' . $prefix . 'catalog`.`product_id` order by `' . $prefix . 'catalog_images`.`position` limit 1) as image', FALSE);
			$this->db->join('menu', 'menu.id = catalog.menu_id');
			$this->db->join('components', 'components.component_id = catalog.component_id');
			$this->db->where('`' . $prefix . 'catalog`.`product_id` IN (select `product_id` from `' . $prefix . 'catalog_product_bind_components` where `component_id` ' . (is_numeric($components) ? '= ' . $components : 'in(' . implode(',', $components) . ')') . ')', NULL, FALSE);
			$this->db->where('catalog.hidden', 0);

			if (isset($filters['min_price']) OR isset($filters['max_price']))
			{
				$this->db->join('catalog_variants', 'catalog_variants.product_id=catalog.product_id');
				if (isset($filters['min_price'])) $this->db->where('catalog_variants.price >=', $filters['min_price']);
				if (isset($filters['max_price'])) $this->db->where('catalog_variants.price <=', $filters['max_price']);
			}

			if (isset($filters['filters']))
			{
				foreach ($filters['filters'] as $filter_id => $values)
				{
					$this->db->join('catalog_product_bind_filters as filter_' . $filter_id, 'filter_' . $filter_id . '.product_id=catalog.product_id');
					$this->db->group_start();
					$this->db->where('filter_' . $filter_id . '.filter_id', $filter_id);
					if (count($values) == 1)
					{
						$this->db->where('filter_' . $filter_id . '.value_id', $values[0]);
					}
					else
					{
						$this->db->where_in('filter_' . $filter_id . '.value_id', $values);
					}
					$this->db->group_end();
				}
			}

			$this->db->where('menu.hidden', 0);
			$this->db->where('components.hidden', 0);

			switch ($sort)
			{
				/*
				case 'price_asc':
					$sort = array('catalog.price', 'asc');
					break;
				case 'price_desc':
					$sort = array('catalog.price', 'desc');
					break;
				*/
				case 'title_asc':
					$sort = array('cast(`' . $prefix . 'catalog`.`title_' . LANG . '` as char character set cp1251)', 'asc');
					break;
				case 'title_desc':
					$sort = array('cast(`' . $prefix . 'catalog`.`title_' . LANG . '` as char character set cp1251)', 'desc');
					break;
				default:
					$sort = array('catalog.position', 'asc');
			}

			$this->db->order_by($sort[0], $sort[1]);
			$this->db->group_by('catalog.product_id');
			$this->db->limit($per_page, (($page > 0 ? $page - 1 : 0) * $per_page));

			$products = $this->db->get('catalog')->result_array();

			foreach ($products as &$v) {
				$v['filters'] = $this->get_filters($v['product_id']);
			}

			return $products;
		}

		/**
		 * Отримання детальної інформації про продукт
		 *
		 * @param int $product_id
		 * @return array
		 */
		public function get($product_id)
		{
			$this->db->select('catalog.component_id, catalog.menu_id, catalog.status, catalog.title_' . LANG . ' as title, catalog.sign_' . LANG . ' as sign, catalog.text_' . LANG . ' as text, catalog.video, catalog.active_views');
			$this->db->join('menu', 'menu.id = catalog.menu_id');
			$this->db->join('components', 'components.component_id = catalog.component_id');
			$this->db->where('menu.hidden', 0);
			$this->db->where('components.hidden', 0);
			$this->db->where('catalog.product_id', $product_id);
			$this->db->where('catalog.hidden', 0);

			$result = $this->db->get('catalog')->row_array();

			if (count($result) > 0)
			{
				$result['images'] = $this->db->select('image')->where('product_id', $product_id)->order_by('position', 'asc')->limit(1)->get('catalog_images')->result_array();

				$catalog_key = $this->session->userdata('catalog_key');

				if ($catalog_key === NULL)
				{
					$catalog_key = uniqid();
					$this->session->set_userdata('catalog_key', $catalog_key);
				}

				if ($result['active_views'] != '')
				{
					$result['active_views'] = unserialize($result['active_views']);

					foreach ($result['active_views'] as $k => $v)
					{
						if ($v < time())
						{
							unset($result['active_views'][$k]);
						}
					}
				}
				else
				{
					$result['active_views'] = array();
				}

				$result['active_views'][$catalog_key] = time() + 60;

				$this->db->set('active_views', serialize($result['active_views']));
				$this->db->where('product_id', $product_id);
				$this->db->update('catalog');
			}

			return $result;
		}

		/**
		 * Отримання детальної інформації про варіант продукту
		 *
		 * @param int $variant_id
		 * @return array
		 */
		public function get_variant($variant_id)
		{
			$this->db->select('cv.product_id, cv.articul, cv.title_' . LANG . ' as title, cv.url_' . LANG . ' as url, cv.weight, cv.total, cv.box, cv.price, cv.currency');
			$this->db->where('cv.variant_id', $variant_id);
			$this->db->where('cv.hidden', 0);

			return $this->db->get('catalog_variants as cv')->row_array();
		}

		/**
		 * Отримання короткої детальної інформації про продукт
		 *
		 * @param int $product_id
		 * @return array
		 */
		public function get_simple($product_id)
		{
			$prefix = $this->db->dbprefix;

			$this->db->select('catalog.menu_id, catalog.component_id, catalog.status, catalog.title_' . LANG . ' as title, catalog.url_' . LANG . ' as url, catalog.sign_' . LANG . ' as sign');
			$this->db->select('(select `' . $prefix . 'catalog_images`.`image` from `' . $prefix . 'catalog_images` where `' . $prefix . 'catalog_images`.`product_id` = `' . $prefix . 'catalog`.`product_id` order by `' . $prefix . 'catalog_images`.`position` asc limit 1) as image', FALSE);
			$this->db->where('catalog.product_id', $product_id);
			$this->db->where('catalog.hidden', 0);

			$this->db->join('menu', 'menu.id = catalog.menu_id');
			$this->db->where('menu.hidden', 0);

			$this->db->join('components', 'components.component_id = catalog.component_id');
			$this->db->where('components.hidden', 0);

			return $this->db->get('catalog')->row_array();
		}

		/**
		 * Отримання варіантів товару
		 *
		 * @param int $product_id
		 * @param array $variants
		 * @return array
		 */
		public function get_variants($product_id, $variants = array())
		{
			if ($product_id > 0) {
				$product_variants = $this->cache->get('variants/product_' . LANG . '_' . $product_id);

				if ($product_variants) {
					return $product_variants;
				}
			}

			if ($product_id > 0) {
				$this->db->where('cv.product_id', $product_id);
			} else {
				if (count($variants) > 0) {
					$this->db->where_in('cv.variant_id', $variants);
				}
			}

			$result = $this->db
				->select('cv.variant_id, cv.product_id, cv.articul, cv.title_' . LANG . ' as title, cv.url_' . LANG . ' as url, cv.weight, cv.box, cv.total, cv.price, cv.currency')
				//->select('cast(replace(cv.articul, "-", "") as SIGNED) as articul')

				//->order_by('cast(replace(cv.articul, "-", " ") as SIGNED)', 'asc', false)
				//->order_by('cv.articul', 'asc')
				->get('catalog_variants as cv')
				->result_array();

			$variants = array();

			foreach ($result as $v) {
				$v['filters'] = $this->get_variant_filters($v['variant_id']);
				$variants[$v['articul']] = $v;
			}

			ksort($variants, SORT_FLAG_CASE);

//			usort($variants, function ($_a, $_b) {
//				$a = explode('-', $_a['articul']);
//				$b = explode('-', $_b['articul']);
//
//				if (count($a) !== count($b)) {
//					return 0;
//				}
//
//				if ($a[0] === $b[0] and $a[1] === $b[1] and $a[2] === $b[2] and $a[3] === $b[3]) {
//					return 0;
//				}
//
//				if ($a[0] > $b[0] and $a[1] > $b[1] and $a[2] > $b[2] and $a[3] > $b[3]) {
//					return 1;
//				}
//
//				return -1;
//			});

			if ($product_id > 0) {
				$this->cache->save('product_' . LANG . '_' . $product_id, $variants, 3600, 'variants/');
			}

			return $variants;
		}

		/**
		 * Отримання інформації про доставку для конкретного товару
		 *
		 * @param $product_id
		 * @return array
		 */
		public function get_product_delivery($product_id)
		{
			$this->db->select('delivery_' . LANG . ' as delivery, payment_' . LANG . ' as payment');
			return $this->db->where('product_id', $product_id)->get('catalog_product_delivery')->row_array();
		}

		/**
		 * Вибірка схожих товарів.
		 *
		 * @param int $component_id ідентифікатор компонента товару до якого потрібно знайти подібні
		 * @param int $product_id ідентифікатор товару до якого потрібно знайти подібні(використувається для виключення з вибірки)
		 *
		 * @return array масив з даними схожих товарів
		 */
		public function get_similar_products($component_id, $product_id)
		{
			$prefix = $this->db->dbprefix;

			$this->db->select('catalog.product_id, catalog.status, catalog.title_' . LANG . ' as title, catalog.url_' . LANG . ' as url, catalog.sign_' . LANG . ' as sign, menu.url_path_' . LANG . ' as path_url');
			$this->db->select('(select `' . $prefix . 'catalog_images`.`image` from `' . $prefix . 'catalog_images` where `' . $prefix . 'catalog_images`.`product_id` = `' . $prefix . 'catalog`.`product_id` order by `' . $prefix . 'catalog_images`.`position` limit 1) as image', FALSE);
			$this->db->join('menu', 'menu.id = catalog.menu_id');
			$this->db->join('components', 'catalog.component_id = components.component_id');
			$this->db->where('catalog.product_id !=', $product_id);
			$this->db->where('catalog.component_id', $component_id);
			$this->db->where('catalog.hidden', 0);
			$this->db->order_by('catalog.position', 'random');
			$this->db->limit(4);

			$products = $this->db->get('catalog')->result_array();

			foreach ($products as &$v) {
				$v['filters'] = $this->get_filters($v['product_id']);
			}

			return $products;
		}

		public function get_seleted_products($product_id)
		{
			$prefix = $this->db->dbprefix;


			$this->db->group_start();
			$this->db->where('catalog_similar.product_id', $product_id);
			$this->db->or_where('catalog_similar.similar_id', $product_id);
			$this->db->group_end();

			$this->db->select('catalog.product_id, catalog.status, catalog.title_' . LANG . ' as title, catalog.url_' . LANG . ' as url, catalog.sign_' . LANG . ' as sign, menu.url_path_' . LANG . ' as path_url');
			$this->db->select('(select `' . $prefix . 'catalog_images`.`image` from `' . $prefix . 'catalog_images` where `' . $prefix . 'catalog_images`.`product_id` = `' . $prefix . 'catalog`.`product_id` order by `' . $prefix . 'catalog_images`.`position` limit 1) as image', FALSE);

			$this->db->join('`' . $this->db->dbprefix('catalog') . '`', '`' . $this->db->dbprefix('catalog') . '`.`product_id` = if(' . $product_id . '=`' . $this->db->dbprefix('catalog_similar') . '`.`similar_id`, `' . $this->db->dbprefix('catalog_similar') . '`.`product_id`, `' . $this->db->dbprefix('catalog_similar') . '`.`similar_id`)', '', false);

			$this->db->where('catalog.hidden', 0);

			$this->db->join('menu', 'menu.id = catalog.menu_id');
			$this->db->join('components', 'catalog.component_id = components.component_id');

			$this->db->order_by('catalog.position', 'random');

			$products = $this->db->get('catalog_similar')->result_array();

			foreach ($products as &$v) {
				$v['filters'] = $this->get_filters($v['product_id']);
			}

			return $products;
		}

		/**
		 * Отримання опції для замовлення товару
		 *
		 * @param $product_id
		 * @return array|null
		 */
		public function get_options($product_id)
		{
			$options = array();

			$this->db->select('catalog_options.option_id, catalog_options.parent_id, catalog_options_parent.name_' . LANG . ' as parent_name, catalog_options.name_' . LANG . ' as name');
			$this->db->select('catalog_product_bind_options.price');

			$this->db->join('catalog_options', 'catalog_options.option_id = catalog_product_bind_options.option_id');
			$this->db->join('catalog_options as catalog_options_parent', 'catalog_options_parent.option_id = catalog_options.parent_id');

			$this->db->where('catalog_product_bind_options.product_id', $product_id);
			$this->db->where('catalog_options.hidden', 0);
			$this->db->where('catalog_options_parent.hidden', 0);

			$this->db->order_by('catalog_options_parent.position', 'asc');
			$this->db->order_by('catalog_options.position', 'asc');

			$result = $this->db->get('catalog_product_bind_options')->result_array();

			foreach ($result as $v)
			{
				$options[$v['parent_id']]['option_name'] = $v['parent_name'];
				$options[$v['parent_id']]['childs'][] = array(
					'option_id' => $v['option_id'],
					'name' => $v['name'],
					'price' => $v['price'],
				);
			}

			return array_shift($options);
		}

		/**
		 * Отримання фільтрів товару
		 *
		 * @param $product_id
		 * @return array
		 */
		public function get_filters($product_id)
		{
			$filters = $this->cache->get('pfilters/filters_' . LANG . '_' . $product_id);

			if ($filters) {
				return $filters;
			}

			$filters = array();

			$this->db->select('catalog_product_bind_filters.filter_id, catalog_product_bind_filters.value_id, catalog_filters.name_' . LANG . ' as value_name, catalog_filters_parent.name_' . LANG . ' as filter_name');

			$this->db->join('catalog_filters', 'catalog_filters.filter_id = catalog_product_bind_filters.value_id');
			$this->db->join('catalog_filters as catalog_filters_parent', 'catalog_filters_parent.filter_id = catalog_product_bind_filters.filter_id');

			$this->db->where('catalog_product_bind_filters.product_id', $product_id);
			$this->db->where('catalog_filters.hidden', 0);
			$this->db->where('catalog_filters_parent.hidden', 0);

			$this->db->order_by('catalog_filters_parent.position', 'asc');
			$this->db->order_by('catalog_filters.position', 'asc');

			$result = $this->db->get('catalog_product_bind_filters')->result_array();

			foreach ($result as $v)
			{
				if (!is_numeric($v['value_name']) or $v['value_name'] > 0) {
					$filters[$v['filter_id']]['filter_name'] = $v['filter_name'];
					$filters[$v['filter_id']]['childs'][$v['value_id']] = mb_strtolower($v['value_name']);

					asort($filters[$v['filter_id']]['childs']);
				}
			}

			$this->cache->save('filters_' . LANG . '_' . $product_id, $filters, 1800, 'pfilters/');

			return $filters;
		}

		/**
		 * Отримання фільтрів варіанту товару
		 *
		 * @param $variant_id
		 * @return array
		 */
		public function get_variant_filters($variant_id)
		{
			$filters = $this->cache->get('vfilters/filters_' . LANG . '_' . $variant_id);

			if ($filters) {
				return $filters;
			}

			$filters = array();

			$this->db->select('catalog_variant_bind_filters.filter_id, catalog_variant_bind_filters.value_id, catalog_filters.name_' . LANG . ' as value_name, catalog_filters_parent.name_' . LANG . ' as filter_name');

			$this->db->join('catalog_filters', 'catalog_filters.filter_id = catalog_variant_bind_filters.value_id');
			$this->db->join('catalog_filters as catalog_filters_parent', 'catalog_filters_parent.filter_id = catalog_variant_bind_filters.filter_id');

			$this->db->where('catalog_variant_bind_filters.variant_id', $variant_id);
			$this->db->where('catalog_filters.hidden', 0);
			$this->db->where('catalog_filters_parent.hidden', 0);

			$this->db->order_by('catalog_filters_parent.position', 'asc');
			$this->db->order_by('catalog_filters.position', 'asc');

			$result = $this->db->get('catalog_variant_bind_filters')->result_array();

			foreach ($result as $v)
			{
				if (!is_numeric($v['value_name']) or $v['value_name'] > 0) {
					$filters[$v['filter_id']]['filter_name'] = $v['filter_name'];
					$filters[$v['filter_id']]['childs'][$v['value_id']] = mb_strtolower($v['value_name']);

					asort($filters[$v['filter_id']]['childs']);
				}
			}

			$this->cache->save('filters_' . LANG . '_' . $variant_id, $filters, 1800, 'vfilters/');
			return $filters;
		}

		/**
		 * Отримання меню брендів
		 *
		 * @return array
		 */
		public function get_brands_menu()
		{
			$this->db->select('id, name_' . LANG . ' as name, url_' . LANG . ' as url, static_url_' . LANG . ' as static_url, image');
			$this->db->where('menu_index', 4)->where('hidden', 0);
			$this->db->order_by('position', 'asc');

			return $this->db->get('menu')->result_array();
		}

		/**
		 * Отримання меню каталогу
		 *
		 * @return array
		 */
		public function get_catalog_menu()
		{
			$this->db->select('menu.id, menu.name_' . LANG . ' as name, menu.url_' . LANG . ' as url, menu.static_url_' . LANG . ' as static_url, menu.image');
			$this->db->join('components', 'components.menu_id = menu.id');
			$this->db->where('menu.parent_id', 0)->where('menu.menu_index', 1)->where('menu.hidden', 0);
			$this->db->where('components.module', 'catalog');
			$this->db->where('components.method', 'index');
			$this->db->order_by('menu.position', 'asc');

			$result = $this->db->get('menu')->result_array();

			if (count($result) > 0)
			{
				foreach ($result as &$v)
				{
					$this->db->join('components', 'components.menu_id = menu.id');
					$this->db->where('menu.parent_id', $v['id'])->where('menu.menu_index', 1)->where('menu.hidden', 0);
					$this->db->where('components.module', 'catalog');
					$this->db->where('components.method', 'index');

					$v['childrens_total'] = $this->db->count_all_results('menu');

					if ($v['childrens_total'] > 0)
					{
						$this->db->select('menu.name_' . LANG . ' as name, menu.url_' . LANG . ' as url, menu.static_url_' . LANG . ' as static_url');
						$this->db->join('components', 'components.menu_id = menu.id');
						$this->db->where('menu.parent_id', $v['id'])->where('menu.menu_index', 1)->where('menu.hidden', 0);
						$this->db->where('components.module', 'catalog');
						$this->db->where('components.method', 'index');
						$this->db->order_by('menu.position', 'asc');
						$this->db->limit(5);

						$v['childrens'] = $this->db->get('menu')->result_array();
					}
				}
			}

			return $result;
		}

		/**
		 * Отримання посилань на різних мовах
		 *
		 * @param int $product_id
		 * @param array $languages
		 *
		 * @return array
		 */
		public function get_language_links($product_id, $languages)
		{
			foreach ($languages as $language)
			{
				$this->db->select('url_' . $language['code']);
			}
			$this->db->where('product_id', $product_id);

			$result = $this->db->get('catalog')->row_array();
			$links = array();

			foreach ($languages as $language)
			{
				$links[$language['code']] = $this->uri->full_url($result['url_' . $language['code']], $language['url']);
			}

			return $links;
		}

		/**
		 * Отримання посилань для варіанту на різних мовах
		 *
		 * @param int $variant_id
		 * @param array $languages
		 *
		 * @return array
		 */
		public function get_variant_language_links($variant_id, $languages)
		{
			foreach ($languages as $language)
			{
				$this->db->select('url_' . $language['code']);
			}
			$this->db->where('variant_id', $variant_id);

			$result = $this->db->get('catalog_variants')->row_array();
			$links = array();

			foreach ($languages as $language)
			{
				$links[$language['code']] = $this->uri->full_url($result['url_' . $language['code']], $language['url']);
			}

			return $links;
		}

		/**
		 * Отримання інформації про доставку
		 *
		 * @return array
		 */
		public function get_delivery()
		{
			$delivery = array();

			$result = $this->db->select('key, value_' . LANG . ' as value')->get('catalog_delivery')->result_array();
			foreach ($result as $v) $delivery[$v['key']] = $v['value'];

			return $delivery;
		}

		/**
		 * Встановлення останнього переглянутого товару
		 *
		 * @param int $product_id
		 */
		public function set_visited($product_id)
		{
			$products = $this->input->cookie($this->config->item('cookie_prefix') . 'visited_products');

			if ($products == NULL)
			{
				$products = array($product_id);
			}
			else
			{
				$products = explode(',', $products);
				$products = array_map('intval', $products);

				if (!in_array($product_id, $products))
				{
					array_unshift($products, $product_id);
					if (count($products) > 4) array_pop($products);
				}
			}

			$cookie_time = time() + 86400;
			if ($products != '') $cookie_time = time() - 200;

			$cookie = array(
				'name' => 'visited_products',
				'value' => implode(',', $products),
				'expire' => $cookie_time,
				'path' => '/',
				'httponly' => TRUE
			);
			$this->input->set_cookie($cookie);
		}

		/**
		 * Отримання переглянутих товарів
		 *
		 * @return array
		 */
		public function get_visited_products()
		{
			$_products = array();
			$products = $this->input->cookie($this->config->item('cookie_prefix') . 'visited_products');

			if ($products !== NULL)
			{
				$products = explode(',', $products);
				$products = array_map('intval', $products);

				$prefix = $this->db->dbprefix;

				$this->db->select('catalog.product_id, catalog.status, catalog.title_' . LANG . ' as title, catalog.url_' . LANG . ' as url, catalog.sign_' . LANG . ' as sign, menu.url_path_' . LANG . ' as path_url');
				$this->db->select('(select `' . $prefix . 'catalog_images`.`image` from `' . $prefix . 'catalog_images` where `' . $prefix . 'catalog_images`.`product_id` = `' . $prefix . 'catalog`.`product_id` order by `' . $prefix . 'catalog_images`.`position` limit 1) as image', FALSE);
				$this->db->join('menu', 'menu.id = catalog.menu_id');
				$this->db->join('components', 'catalog.component_id = components.component_id');
				$this->db->where_in('catalog.product_id', $products);
				$this->db->where('catalog.hidden', 0);
				$this->db->where('menu.hidden', 0);
				$this->db->where('components.hidden', 0);

				$sort = array('catalog.position', 'asc');
				$this->db->order_by($sort[0], $sort[1]);

				$this->db->limit(4);

				$result = $this->db->get('catalog')->result_array();

				if (count($result) > 0)
				{
					$_products = array(); //array_flip($products);

					foreach ($result as $v) {
						$v['filters'] = $this->get_filters($v['product_id']);
						$_products[$v['product_id']] = $v;
					}
				}
			}

			return $_products;
		}

		/**
		 * Отримання топ товарів
		 *
		 * @return array
		 */
		public function get_top_products()
		{
			$products = array();

			$codes = array(
				1 => 'promo',
				2 => 'new',
				3 => 'discount',
				4 => 'hit',
			);

			$prefix = $this->db->dbprefix;

			foreach ($codes as $k => $v)
			{
				$this->db->select('catalog.product_id, catalog.status, catalog.title_' . LANG . ' as title, catalog.url_' . LANG . ' as url, catalog.sign_' . LANG . ' as sign, menu.url_path_' . LANG . ' as path_url');
				$this->db->select('(select `' . $prefix . 'catalog_images`.`image` from `' . $prefix . 'catalog_images` where `' . $prefix . 'catalog_images`.`product_id` = `' . $prefix . 'catalog`.`product_id` order by `' . $prefix . 'catalog_images`.`position` limit 1) as image', FALSE);
				$this->db->select('(select count(*) from `' . $prefix . 'catalog_comments` where `' . $prefix . 'catalog_comments`.`product_id` = `' . $prefix . 'catalog`.`product_id`) as comments', FALSE);
				$this->db->select('(select sum(`' . $prefix . 'catalog_comments`.`stars`) from `' . $prefix . 'catalog_comments` where `' . $prefix . 'catalog_comments`.`product_id` = `' . $prefix . 'catalog`.`product_id`) as stars', FALSE);
				$this->db->join('menu', 'menu.id = catalog.menu_id');
				$this->db->join('components', 'catalog.component_id = components.component_id');
				$this->db->where('catalog.hidden', 0);
				$this->db->where('catalog.status', $k);
				$this->db->where('menu.hidden', 0);
				$this->db->where('components.hidden', 0);
				$this->db->order_by('catalog.position', 'asc');
				$this->db->limit(4);

				$result = $this->db->get('catalog')->result_array();

				foreach ($result as &$_v) {
					$_v['filters'] = $this->get_filters($_v['product_id']);
				}

				if (count($result) > 0) $products[$v] = $result;
			}

			return $products;
		}

		/**
		 * Отримання відгуків до товару
		 *
		 * @param int $product_id
		 * @return array
		 */
		public function get_comments($product_id)
		{
			return $this->db->select('name, stars, comment, answer, date')->where('product_id', $product_id)->where('hidden', 0)->get('catalog_comments')->result_array();
		}
	}