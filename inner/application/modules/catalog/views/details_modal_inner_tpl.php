<div id="cart_error" style="display:none;"><? if(LANG=='ua')echo'Мінімальна к-сть повинна бути не меншою та кратною к-сті в упаковці.';if(LANG=='ru')echo'Минимальное кол-во должно быть не менее и кратной Количеству в упаковке.';if(LANG=='en')echo'Minimum amount should be at least equal to amount of the package.';?></div>
<a href="#" class="cart_close close_modal"></a>
<div class="fm cart_title"><?=$product['title'];?></div>
<div class="fm cart_place">
	<div class="fm cart_row">
		<div class="fm cart_cell cc_1 th"><span>Ø</span></div>
		<div class="fm cart_cell cc_1 th"><span>L</span></div>
		<div class="fm cart_cell cc_2 th"><span><? if(LANG=='ua')echo'Матеріал';if(LANG=='ru')echo'Материал';if(LANG=='en')echo'Materіal';?></span></div>
		<div class="fm cart_cell cc_3 th"><span><? if(LANG=='ua')echo'Кількість<br>шт./уп.';if(LANG=='ru')echo'Количество<br>шт./уп.';if(LANG=='en')echo'Quantity<br>pcs./pc.';?></span></div>
		<div class="fm cart_cell cc_4 th"><span><? if(LANG=='ua')echo'Вага,<br>кг/уп.';if(LANG=='ru')echo'Вес,<br>кг./уп.';if(LANG=='en')echo'Weight,<br>kg./pc.';?></span></div>
		<div class="fm cart_cell cc_5 th">
			<div class="fm cc_5_big"><span><? if(LANG=='ua')echo'Ціна за 100 шт.';if(LANG=='ru')echo'Цена за 100 шт.';if(LANG=='en')echo'Price per 100 pcs.';?></span></div>
			<div class="fm cc_5_sm_2"><span><? if(LANG=='ua')echo'Ціна Роздріб';if(LANG=='ru')echo'Цена Розница';if(LANG=='en')echo'Retail Price';?></span></div>
			<div class="fm cc_5_sm_1"><span><? if(LANG=='ua')echo'Ціна VIP';if(LANG=='ru')echo'Цена VIP';if(LANG=='en')echo'VIP Price';?></span></div>
			<div class="fm cc_5_sm_1"><span><? if(LANG=='ua')echo'Ціна ОПТ';if(LANG=='ru')echo'Цена ОПТ';if(LANG=='en')echo'Whols. Price';?></span></div>
		</div>
		<div class="fm cart_cell cc_6 th"><span><? if(LANG=='ua')echo'К-сть,<br>шт.';if(LANG=='ru')echo'Кол-во,<br>шт.';if(LANG=='en')echo'Quant.,<br>pcs.';?></span></div>
		<div class="fm cart_cell cc_7 th"></div>
	</div>
	<?php foreach ($variants as $v): ?>
	<div class="fm cart_row" data-product-id="<?=$product_id;?>" data-variant-id="<?=$v['variant_id'];?>" data-box="<?=$v['box'];?>">
		<div class="fm cart_cell cc_1"><span><?php if ($v['diameter'] != ''): ?><?=$v['diameter'];?><?php else: ?>-<?php endif; ?></span></div>
		<div class="fm cart_cell cc_1"><span><?php if ($v['length'] != ''): ?><?=$v['length'];?><?php else: ?>-<?php endif; ?></span></div>
		<div class="fm cart_cell cc_2"><div<?php if ($v['material'] == 'A2' OR $v['material'] == '10,9'): ?> class="green_pimp"<?php endif; ?><?php if ($v['material'] == 'A4' OR $v['material'] == '12,9'): ?> class="red_pimp"<?php endif; ?>><?=$v['material'];?></div></div>
		<div class="fm cart_cell cc_3"><span><?=$v['box'];?></span></div>
		<div class="fm cart_cell cc_4"><span><?=$v['weight'];?></span></div>
		<div class="fm cart_cell cc_5" style="width:118px; background:#f8f8f8;"><span><?=$v['price'];?></span></div>
		<div class="fm cart_cell cc_5" style="background:#f8f8f8;"><span><?php if ($v['price_opt'] > 0 AND $v['limit_opt'] > 0): ?><?=round($v['price_opt'], 2);?><br><i>(<? if(LANG=='ua')echo'від';if(LANG=='ru')echo'от';if(LANG=='en')echo'from';?> <?=round($v['limit_opt'], 2);?> <? if(LANG=='ua')echo'шт.';if(LANG=='ru')echo'шт.';if(LANG=='en')echo'pcs.';?>)</i><?php else: ?>-<?php endif; ?></span></div>
		<div class="fm cart_cell cc_5" style="background:#f8f8f8;"><span><?php if ($v['price_vip'] > 0 AND $v['limit_vip'] > 0): ?><?=round($v['price_vip'], 2);?><br><i>(<? if(LANG=='ua')echo'від';if(LANG=='ru')echo'от';if(LANG=='en')echo'from';?> <?=round($v['limit_vip'], 2);?> <? if(LANG=='ua')echo'шт.';if(LANG=='ru')echo'шт.';if(LANG=='en')echo'pcs.';?>)</i><?php else: ?>-<?php endif; ?></span></div>
		<div class="fm cart_cell cc_7"><input type="text" value="<?php if (isset($cart[$product_id][$v['variant_id']])): ?><?=$cart[$product_id][$v['variant_id']];?><?php endif; ?>" class="ct_total cart_total"></div>
        <? if(LANG=='ua')$msg_1 = 'В кошику';if(LANG=='ru')$msg_1 = 'В корзине';if(LANG=='en')$msg_1 = 'In Cart';?>
        <? if(LANG=='ua')$msg_2 = 'Купити';if(LANG=='ru')$msg_2 = 'Купить';if(LANG=='en')$msg_2 = 'Buy';?>
        <? if(LANG=='ua')$msg_3 = 'Уточнити ціну';if(LANG=='ru')$msg_3 = 'Уточнить цену';if(LANG=='en')$msg_3 = 'Check price';?>
		<div class="fm cart_cell cc_8"><a href="#" class="to_cart<?php if (isset($cart[$product_id][$v['variant_id']])): ?> show_cart_form<?php endif; ?>"><b></b><span><?=(isset($cart[$product_id][$v['variant_id']]) ? $msg_1 : ($v['price'] > 0 ? $msg_2 : '<i>'.$msg_3.'</i>'));?></span></a></div>
	</div>
	<?php endforeach; ?>
	<div class="fm cart_row legend">
		<div class="fm one_legend"><div class="green_pimp">А2</div> - <? if(LANG=='ua')echo'харчова нержавійка';if(LANG=='ru')echo'пищевая нержавейка';if(LANG=='en')echo'food-SS';?></div>
		<div class="fm one_legend"><div class="red_pimp">А4</div> - <? if(LANG=='ua')echo'кислотостійка нержавійка';if(LANG=='ru')echo'кислотостойкая нержавейка';if(LANG=='en')echo'acid-SS';?></div>
	</div>
</div>
<div class="fm pop_details">
	<div class="fm pd_photo">
		<div>
			<?php if ($product['image'] != NULL): ?>
			<img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($product_id) . '/' . $product_id . '/t_' . $product['image']);?>" alt="">
			<?php endif; ?>
		</div>
	</div>
	<div class="fm pd_text">
		<?=stripslashes($product['sign']);?>
	</div>
</div>