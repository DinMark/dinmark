<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<?php if (is_array($catalog_menu) AND count($catalog_menu) > 0): ?>
    <div class="row hidden">
        <?php foreach ($catalog_menu as $key => $row): ?>
			<div class="col-md-4">
				<div class="category-item fm">
					<?php $link = $this->uri->full_url($row['static_url'] != '' ? $row['static_url'] : $row['url']); ?>
					<div class="category-item_img"><?php if ($row['image'] != ''): ?>
							<div class="cell">
							<a href="<?=$link;?>"><img src="<?=base_url('upload/menu/' . $row['id'] . '/' . $row['image']);?>" alt=""></a>
							</div><?php endif; ?>
					</div>
					<div class="category-item_name">
						<a href="<?=$link;?>"><?=$row['name'];?></a>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>