<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if (count($filters) > 0): ?>
	<ul id="filters_list">
		<?php foreach ($filters as $k => $v): ?>
			<li>
				<div class="holder">
					<div class="cell auto"><span class="menu_item"><?=$v['name'];?></span></div>
				</div>
				<?php if (isset($v['childs'])): ?>
					<ul>
						<?php foreach ($v['childs'] as $_v): ?>
						<li>
							<div class="holder">
								<div class="cell w_20">
									<div class="controls">
										<label for="filter_<?=$_v['filter_id'];?>" class="check_label">
											<i></i>
											<input type="checkbox" id="filter_<?=$_v['filter_id'];?>" name="filters[<?=$k;?>][]" value="<?=$_v['filter_id'];?>"<?=(in_array($_v['filter_id'], $product_filters) ? ' checked="checked"' : '');?>>
										</label>
									</div>
								</div>
								<div class="cell auto"><span class="menu_item"><?=$_v['name'];?></span></div>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
