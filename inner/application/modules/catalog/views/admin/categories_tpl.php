<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<div class="admin_component" id="admin_component_<?=$component_id;?>" data-component-id="<?=$component_id;?>" data-module="catalog" data-css-class="catalog" data-visibility-url="<?=$this->uri->full_url('admin/components/toggle_visibility');?>" data-delete-url="<?=$this->uri->full_url('admin/catalog/delete_component');?>">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="<?=(($hidden == 0) ? 'catalog' : 'hidden');?>"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Категорії товарів</div></div>
			<a href="#" class="fm show_hide"><b></b><?=(($hidden == 0) ? 'Приховати' : 'Показати');?></a>
		</div>
		<div class="fmr component_del">
			<a href="#" class="fm delete_component"><b></b></a>
		</div>
		<div class="fmr component_pos">
			<a href="#" class="fm up_component"><b></b></a>
			<a href="#" class="fm down_component"><b></b></a>
		</div>
	</div>
	<div class="fm admin_menu">
		<ul>
			<?php foreach ($categories as $key => $category): ?>
			<li<?php if ($key % 2 == 0) echo ' class="grey"'; ?>>
				<div class="holder">
					<?php $url = $category['static_url'] != '' ? $category['static_url'] : $this->uri->full_url($category['url']); ?>
					<div class="cell no_padding w_180">
						<?php if ($category['image'] != ''): ?><a href="<?php echo $url; ?>"><img src="<?=base_url('upload/menu/' . $category['id'] . '/' . $category['image']);?>" alt="" style="max-width: 180px"></a><?php endif; ?>
					</div>
					<div class="cell icon w_20"><a href="<?=$this->uri->full_url('admin/menu/edit?menu_index=1&menu_id=' . $menu_id . '&item_id=' . $category['id'] . '&catalog=1');?>" class="picture active"></a></div>
					<div class="cell auto">
						<span class="menu_item"><a href="<?=$url;?>"><?=stripslashes($category['name']);?></a></span>
					</div>
				</div>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
<script type="text/javascript">
	$(function () {
		$('#admin_component_<?=$component_id;?>').component({
			onDelete: function () {
				$('.com_catalog').show();
			}
		});

		$('.admin_menu')
			.on('mouseover', '.holder', function () {
				$(this).addClass('active');
			})
			.on('mouseout', '.holder', function () {
				$(this).removeClass('active');
			});
	});
</script>