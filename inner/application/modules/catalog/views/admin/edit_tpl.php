<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	$this->template_lib->set_css('js/admin/fineuploader/fineuploader-3.5.0.css', TRUE);
	$this->template_lib->set_css('js/admin/jcrop/css/jquery.Jcrop.min.css', TRUE);
	$this->template_lib->set_css('js/admin/ui/jquery-ui-1.10.3.custom.min.css', TRUE);

	$this->template_lib->set_js('admin/fineuploader/jquery.fineuploader-3.5.0.min.js');
	$this->template_lib->set_js('admin/jcrop/js/jquery.Jcrop.min.js');
	$this->template_lib->set_js('admin/ckeditor/ckeditor.js');
	$this->template_lib->set_js('admin/jquery.form.js');
	$this->template_lib->set_js('admin/checkboxes.js');
	$this->template_lib->set_js('admin/ui/jquery-ui-1.10.3.custom.min.js');
	$this->template_lib->set_js('admin/ui/jquery.ui.datepicker-uk.js');
?>
<div class="admin_component">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="catalog"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Детальне товару</div></div>
			<a href="#" class="fm save save_product"><b></b>Зберегти</a>
			<a href="#" class="fm apply apply_product"><b></b>Застосувати</a>
			<a href="<?php echo $this->init_model->get_link($menu_id, '{URL}'); ?>?page=<?=$page;?>" class="fm cancel"><b></b>До списку товарів</a>
		</div>
		<?php if (count($languages) > 1): ?>
			<div class="fmr component_lang">
				<?php foreach ($languages as $key => $val): ?>
					<a href="#" class="flags <?=$key;?><?=(($key == LANG) ? ' active' : '');?>" data-language="<?=$key;?>"><img src="img/flags_<?=$key;?>.png"></a>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	<form id="product_form" action="<?=$this->uri->full_url('admin/catalog/save');?>" method="post" autocomplete="off">
		<input type="hidden" name="component_id" value="<?=$product['component_id'];?>">
		<input type="hidden" name="menu_id" value="<?=$product['menu_id'];?>">
		<input type="hidden" name="product_id" value="<?=$product_id;?>">
		<?php foreach ($languages as $key => $val): ?>
		<div class="lang_tab lang_tab_<?=$key;?>"<?=((LANG != $key) ? ' style="display:none"' : '');?>>
			<div class="evry_title">
				<label for="title_<?=$key;?>" class="block_label">Назва:</label>
				<input type="text" id="title_<?=$key;?>" name="title[<?=$key;?>]" value="<?=$product['title_' . $key];?>">
			</div>
			<div class="evry_title">
				<label for="sign_<?=$key;?>" class="block_label">Короткий опис:</label>
				<div class="no_float"><textarea id="sign_<?=$key;?>" class="product_text" name="sign[<?=$key;?>]"><?=stripslashes($product['sign_' . $key]);?></textarea></div>
			</div>
			<div class="evry_title">
				<label for="text_<?=$key;?>" class="block_label">Опис:</label>
				<div class="no_float"><textarea id="text_<?=$key;?>" class="product_text" name="text[<?=$key;?>]"><?=stripslashes($product['text_' . $key]);?></textarea></div>
			</div>
		</div>
		<?php endforeach; ?>
		<div class="evry_title">
			<label for="video" class="block_label">Відео:</label>
			<div class="no_float"><textarea id="video" name="video"><?=stripslashes($product['video']);?></textarea></div>
		</div>
		<div class="evry_title">
			<label class="block_label">Статус:</label>
			<div class="no_float controls">
				<label class="radio_label" style="padding:0 0 7px 6px;"><i><input type="radio" name="status" value="0"<?php if ($product['status'] == 0) echo ' checked="checked"'; ?>></i> без статусу</label>
				<label class="radio_label" style="padding:0 0 7px 6px;"><i><input type="radio" name="status" value="1"<?php if ($product['status'] == 1) echo ' checked="checked"'; ?>></i> акція</label>
				<label class="radio_label" style="padding:0 0 7px 6px;"><i><input type="radio" name="status" value="2"<?php if ($product['status'] == 2) echo ' checked="checked"'; ?>></i> новинка</label>
				<label class="radio_label" style="padding:0 0 7px 6px;"><i><input type="radio" name="status" value="3"<?php if ($product['status'] == 3) echo ' checked="checked"'; ?>></i> знижка</label>
				<label class="radio_label" style="padding:0 0 7px 6px;"><i><input type="radio" name="status" value="4"<?php if ($product['status'] == 4) echo ' checked="checked"'; ?>></i> хіт продаж</label>
				<div class="fm clear_both_div">діє до <input type="text" id="status_date" name="status_date" value="<?=($product['status_date'] > 0 ? $product['status_date'] : '');?>" class="short-er"></div>
			</div>
		</div>
		<div class="evry_title">
			<label class="block_label">Виводити в розділах:</label>
			<div class="no_float">
				<div id="component_selector" class="fm admin_menu controls" style="max-height: 300px; overflow: auto" data-type="checkbox"><?php if (count($components_menu) > 0) build_components_menu($components_menu, 0, $product_components, $product['component_id']); ?></div>
			</div>
		</div>
		<?php if ($filters_menu != ''): ?>
		<div class="evry_title">
			<label class="block_label">Фільтри пошуку:</label>
			<div class="no_float">
				<div id="filters_selector" class="fm admin_menu controls" style="max-height: 300px; overflow: auto" data-type="checkbox">
					<?=$filters_menu;?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="evry_title padding">
			<div class="no_float bold">Зображення</div>
		</div>
		<div class="evry_title padding">
			<div class="fm" id="gallery_image_uploader_box"><div id="gallery_image_uploader"></div></div>
		</div>
		<div class="evry_title" id="gallery_images_box">
			<label class="block_label">&nbsp;</label>
			<div class="no_float">
				<div class="fm images_list">
					<ul id="images_list" style="width:100%;">
						<?php if (count($images) > 0): ?>
							<?php foreach ($images as $image): ?>
								<li data-image-id="<?=$image['image_id'];?>" style="float: left; width: <?=$thumb_w;?>px; height: <?=$thumb_h;?>px;">
									<div class="fm for_photo_cut">
										<div class="fm photo_cut" style="width: <?=$thumb_w;?>px; height: <?=$thumb_h;?>px;">
											<?php $sizes = getimagesize(ROOT_PATH . 'upload/catalog/' . $dir_code . '/' . $image['product_id'] . '/s_' . $image['image']); ?>
											<div style="width: <?=$thumb_w;?>px; height: <?=$thumb_h;?>px;"><img src="/upload/catalog/<?=$dir_code;?>/<?=$image['product_id'];?>/t_<?=$image['image'] . '?t=' . time() . rand(10000, 1000000);?>" alt=""></div>
											<div class="links">
												<a href="#" class="fm fpc_edit" data-image-id="<?=$image['image_id'];?>" data-src="/upload/catalog/<?=$dir_code;?>/<?=$image['product_id'];?>/s_<?=$image['image'];?>" data-width="<?=$sizes[0];?>" data-height="<?=$sizes[1];?>"><b></b>Редагувати</a>
												<?php if ($this->config->item('watermark') != ''): ?><a href="#" class="fm fpc_watermark" data-image-id="<?=$image['image_id'];?>" data-src="/upload/catalog/<?=$dir_code;?>/<?=$image['product_id'];?>/s_<?=$image['image'];?>" data-width="<?=$sizes[0];?>" data-height="<?=$sizes[1];?>"><b></b>Водяний знак</a><?php endif; ?>
												<a href="#" class="fm fpc_delete"><b></b>Видалити</a>
											</div>
										</div>
									</div>
								</li>
							<?php endforeach; ?>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="evry_title padding">
			<div class="no_float bold">Доставка</div>
		</div>
		<?php foreach ($languages as $key => $val): ?>
		<div class="lang_tab lang_tab_<?=$key;?>"<?=((LANG != $key) ? ' style="display:none"' : '');?>>
			<div class="evry_title">
				<label for="delivery_text_<?=$key;?>" class="block_label">Опис доставки:</label>
				<div class="no_float"><textarea id="delivery_text_<?=$key;?>" class="product_text" name="delivery[<?=$key;?>]"><?=stripslashes($product_delivery['delivery_' . $key]);?></textarea></div>
			</div>
            <div class="evry_title">
                <label for="payment_text_<?=$key;?>" class="block_label">Опис оплати:</label>
                <div class="no_float"><textarea id="payment_text_<?=$key;?>" class="product_text" name="payment[<?=$key;?>]"><?=stripslashes($product_delivery['payment_' . $key]);?></textarea></div>
            </div>
		</div>
		<?php endforeach; ?>
        <div class="evry_title padding">
            <div class="no_float bold">Супутні товари</div>
        </div>
        <div class="evry_title padding">
            <div class="fm sim_title_2"><div>Пошук товарів</div><div>Вибрані товари</div></div>
            <div class="fm sim_cols">
                <div class="fm sim_one_col">
                    <div class="evry_title">
                        <input type="text" id="search_similar" placeholder="Введіть назву або артикул для пошуку">
                    </div>
                    <div class="admin_menu">
                        <ul id="search_similar_list" class="fm sim_result"></ul>
                    </div>
                </div>
                <div class="fm sim_one_col">
                    <div class="admin_menu">
                        <ul id="similar_list" class="fm sim_result">
					        <?php foreach ($similar_products as $v): ?>
                                <li>
                                    <div class="holder">
                                        <div class="cell auto"><?=$v['title'];?></div>
                                        <div class="cell w_140"><a class="text_link remove_similar" data-product-id="<?=$v['product_id'];?>">скасувати</a></div>
                                    </div>
                                </li>
					        <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="evry_title padding">
            <div class="no_float bold">Товар супутній до</div>
        </div>
        <div class="evry_title padding">
            <div class="fm sim_title_2"><div>Пошук товарів</div><div>Вибрані товари</div></div>
            <div class="fm sim_cols">
                <div class="fm sim_one_col">
                    <div class="evry_title">
                        <input type="text" id="search_similar_to" placeholder="Введіть назву або артикул для пошуку">
                    </div>
                    <div class="admin_menu">
                        <ul id="search_similar_to_list"></ul>
                    </div>
                </div>
                <div class="fm sim_one_col">
                    <div class="admin_menu">
                        <ul id="similar_to_list">
					        <?php foreach ($similar_to_products as $v): ?>
                                <li>
                                    <div class="holder">
                                        <div class="cell auto"><?=$v['title'];?></div>
                                        <div class="cell w_140"><a class="text_link remove_similar_to" data-product-id="<?=$v['product_id'];?>">скасувати</a></div>
                                    </div>
                                </li>
					        <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
		<div class="fm for_sucsess">
			<div class="fmr save_links">
				<a href="#" class="fm save_adm save_product"><b></b>Зберегти</a>
				<a href="#" class="fm apply_adm apply_product"><b></b>Застосувати</a>
				<a href="<?=$this->init_model->get_link($menu_id, '{URL}');?>?page=<?=$page;?>" class="fm cansel_adm"><b></b>До списку товарів</a>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	function row_decor() {
		$('.admin_menu')
			.on('mouseenter', '.holder', function () {
				$(this).addClass('active');
			})
			.on('mouseleave', '.holder', function () {
				$(this).removeClass('active');
			})
			.find('li').removeClass('grey')
			.end()
			.find('li:even').addClass('grey');
	}

	function make_sortable() {
		$("#images_list").sortable({
			//axis: 'y',
			//handle: '.sortable_column a',
			scroll: true,
			crollSpeed: 2000,
			forcePlaceholderSize: true,
			placeholder: "ui-state-highlight",
			start: function(e, ui ){
				ui.placeholder.css({
					float: 'left',
					width: <?=$thumb_w;?>,
					height: <?=$thumb_h;?>
				});
			},
			update: function () {
				var images = [];

				$('#images_list').find('li').each(function (i) {
					images[i] = $(this).data('image-id');
				});

				$.post(
					'<?=$this->uri->full_url('admin/catalog/gallery_images_position');?>',
					{
						images: images
					},
					function (response) {
						if (response.success) row_decor();
					},
					'json'
				);
			}
		});
	}

	$(function () {

		$('.component_lang').on('click', 'a', function (e) {
			e.preventDefault();
			$(this).addClass('active').siblings().removeClass('active');
			$('.lang_tab').hide();
			$('.lang_tab_' + $(this).data('language')).show();
		});


		$('#status_date')
			.datepicker()
			.datepicker("option", $.datepicker.regional['uk'])
			.datepicker("option", "dateFormat", "dd.mm.yy")<?php if ($product['status_date'] > 0): ?>.datepicker("setDate", "<?=date('d.m.Y', $product['status_date']);?>")<?php endif; ?>;

		$('.product_text').ckeditor();

		/* ГАЛЕРЕЯ ЗОБРАЖЕНЬ */

		var thumb_w = <?=$thumb_w;?>,
			thumb_h = <?=$thumb_h;?>;

		row_decor();
		make_sortable();

		$('#gallery_image_uploader')
			.fineUploader({
				request: {
					endpoint: '<?=$this->uri->full_url('admin/catalog/upload_gallery_image');?>',
					inputName: 'gallery_image',
					params: {
						component_id : <?=$product['component_id'];?>,
						menu_id : <?=$product['menu_id'];?>,
						product_id : <?=$product_id;?>
					}
				},
				multiple: true,
				text: {
					uploadButton: 'Виберіть або перетягніть файл зображення',
					dragZone: '',
					dropProcessing: ''
				},
				validation: {
					allowedExtensions: ['jpeg', 'jpg', 'png', 'gif'],
					sizeLimit: <?=(intval(ini_get('upload_max_filesize')) * 1048576);?>,
					productLimit: 1
				},
				messages: {
					typeError: "Дозволено завантажувати: {extensions}.",
					sizeError: "Розмір файлу не повинен перевищувати {sizeLimit}.",
					tooManyproductsError: "Дозволено завантажувати файлів: {productLimit}."
				}
			})
			.on('complete', function (event, id, fileName, response) {
				if (response.success) {
					$('.qq-upload-success').remove();

					var row = '<li id="image_' + response.image_id + '" data-image-id="' + response.image_id + '" style="float:left; width: ' + thumb_w + 'px; max-height: ' + thumb_h + 'px;">\
								<div class="fm for_photo_cut">\
									<div class="fm photo_cut" style="width: ' + thumb_w + 'px; max-height: ' + thumb_h + 'px;">\
										<div style="width: ' + thumb_w + 'px; max-height: ' + thumb_h + 'px;"><img src="/upload/catalog/<?=$dir_code;?>/<?=$product_id;?>/t_' + response.file_name + '" alt="" style="max-width: ' + thumb_w + 'px; max-height: ' + thumb_h + 'px;"></div>\
										<div class="links">\
											<a href="#" class="fm fpc_edit" data-image-id="' + response.image_id + '" data-src="/upload/catalog/<?=$dir_code;?>/<?=$product_id;?>/s_' + response.file_name + '" data-width="' + response.width + '" data-height="' + response.height + '"><b></b>Редагувати</a>\
											<?php if ($this->config->item('watermark') != ''): ?><a href="#" class="fm fpc_watermark" data-image-id="' + response.image_id + '" data-src="/upload/catalog/<?=$dir_code;?>/<?=$product_id;?>/s_' + response.file_name + '" data-width="' + response.width + '" data-height="' + response.height + '" data-position="0"><b></b>Водяний знак</a><?php endif; ?>\
											<a href="#" class="fpc_delete">Видалити</a>\
										</div>\
									</div>\
								</div>\
							</li>';

					$('#images_list')
						.append(row)
						.find('.lang_tab').hide()
						.end()
						.find('.lang_tab_' + $('.sub_admin_bottom').find('a.active').data('language')).show();

					row_decor();
					make_sortable();
				}
			});

		$('#images_list')
			.on('click', '.fpc_delete', function (e) {
				e.preventDefault();
				var $link = $(this);
				confirmation('Видалити зображення?', function () {
					$.post(
						'<?=$this->uri->full_url('admin/catalog/delete_gallery_image');?>',
						{
							image_id: $link.closest('li').data('image-id')
						},
						function (response) {
							if (response.success) $link.closest('li').slideUp().remove();
						},
						'json'
					);
				});
			})
			.on('click', '.fpc_edit', function (event) {
				event.preventDefault();

				var $link = $(this),
					width = $link.data('width') > 700 ? 700 : $link.data('width'),
					height = width * $link.data('height') / $link.data('width'),
					crop_modal = '<div id="crop_overlay" class="confirm_overlay" style="display: block; opacity: 0.5; height:' + $(document).height() + 'px"></div><div id="crop_modal" class="crop_modal"><div class="fm crop_area"><div class="fm ca_panel"><a id="crop_cancel" href="#" class="fmr ca_cencel"><b></b>Скасувати</a><a id="crop_save" href="#" class="fmr ca_save"><b></b>Зберегти</a><span class="controls"><label class="check_label active"><i><input type="checkbox" name="proportion" checked="checked" value="1"></i>Пропорційно</label></span></div><div id="crop_preview" class="fm crop_review"><div style="overflow: hidden" class="crop_prew_border"><img src="' + $link.data('src') + '" alt=""></div></div><div id="crop_source" class="fm crop_source"><img width="' + width + '" height="' + height + '" src="' + $link.data('src') + '"></div></div></div>';

				$('body').append(crop_modal);
				$('#crop_modal').css('top', $(document).scrollTop() + 50);

				$('#crop_source').find('img').Jcrop({
					keySupport: false,
					aspectRatio: <?=$thumb_w/$thumb_h;?>,
					setSelect: [0, 0, <?=$thumb_w;?>, <?=$thumb_h;?>],
					realSizes: [$link.data('width'), $link.data('height')],
					onChange: function (coords) {
						crop_preview($('#crop_preview').find('div'), coords, <?=$thumb_w;?>, <?=$thumb_h;?>, width, height);
					}
				}, function () {
					var api = this;

					$('[name="proportion"]').off('change').on('change', function () {
						if ($(this).prop('checked')) {
							$(this).closest('label').addClass('active');
							api.setOptions({aspectRatio: <?=$thumb_w/$thumb_h;?>});
						} else {
							$(this).closest('label').removeClass('active');
							api.setOptions({aspectRatio: 0});
						}
						api.focus();
					});

					$('#crop_cancel').off('click').on('click', function (e) {
						e.preventDefault();
						api.destroy();
						$('#crop_modal').add('#crop_overlay').remove();
					});

					$('#crop_save').off('click').on('click', function (e) {
						e.preventDefault();
						component_loader_show($('.component_loader'));
						$.post(
							'<?=$this->uri->full_url('admin/catalog/crop_gallery_image');?>',
							{
								image_id: $link.data('image-id'),
								width: width,
								coords: api.tellScaled()
							},
							function (response) {
								if (response.success) {
									api.destroy();
									$link.closest('.for_photo_cut').find('img').attr('src', response.image);
									$('#crop_modal').add('#crop_overlay').remove();
									component_loader_hide($('.component_loader'));
								}
							},
							'json'
						);
					});
				});
			})
			.on('click', '.fpc_watermark', function (e) {
				e.preventDefault();

				var $link = $(this),
					width = $link.data('width') > 500 ? 500 : $link.data('width'),
					height = width * $link.data('height') / $link.data('width'),
					watermark_modal = '<div id="watermark_overlay" class="confirm_overlay" style="display: block; opacity: 0.5; height:' + $(document).height() + 'px"></div><div class="fm watermark_area"><div id="watermark_modal" class="watermark_modal" style="width: ' + width + 'px; margin: 0 0 0 -' + Math.round(width/2) + 'px"><div class="fm ca_panel"><a id="cancel_watermark" href="#" class="fmr ca_cencel"><b></b>Скасувати</a><a id="save_watermark" href="#" class="fmr ca_save"><b></b>Зберегти</a></div><div id="watermark_tiles" class="watermark_tiles"><img width="' + width + '" height="' + height + '" src="' + $link.data('src') + '"><div><a href="#" data-value="7"></a><a href="#" data-value="8"></a><a href="#" data-value="9"></a><a href="#" data-value="4"></a><a href="#" data-value="5"></a><a href="#" data-value="6"></a><a href="#" data-value="1"></a><a href="#" data-value="2"></a><a href="#" data-value="3"></a></div></div></div><div style="width: 100%; height: 1px; clear: both"></div></div>';

				$('body').append(watermark_modal);

				if ($link.data('position') > 0) $('#watermark_tiles').find('[data-value="' + $link.data('position') + '"]').addClass('active');

				var a_w = Math.round(width / 3),
					a_h = Math.round(height / 3);

				$('#watermark_tiles').find('a').each(function (i, val) {
					$(this).css({
						width: (a_w - (i % 3 == 2 ? 2 : 1)) + 'px',
						height: (a_h - 1) + 'px',
						left: (i % 3) * a_w + 'px'
					});

					if (i < 3) $(this).css('top', 0);
					if (i >= 3 && i <= 5) $(this).css('top', a_h + 'px');
					if (i > 5) $(this).css('top', (a_h * 2) + 'px');
				});

				$('#watermark_tiles').find('a').off('click').on('click', function (e) {
					e.preventDefault();
					$(this).addClass('active').siblings().removeClass('active');
				});

				$('#cancel_watermark').off('click').on('click', function (e) {
					e.preventDefault();
					$('#watermark_modal').add('#watermark_overlay').remove();
				});

				$('#save_watermark').off('click').on('click', function (e) {
					e.preventDefault();

					component_loader_show($('.component_loader'), '');
					var position = $('#watermark_tiles').find('a.active').data('value');

					$.post(
						'<?=$this->uri->full_url('admin/catalog/watermark_gallery_image');?>',
						{
							image_id: $link.data('image-id'),
							position: position
						},
						function (response) {
							if (response.success) {
								$link.data('position', position);
								$('#watermark_modal').add('#watermark_overlay').remove();
								component_loader_hide($('.component_loader'), '');
							}
						},
						'json'
					);
				});
			});

		/**
		 * Відправка форми
		 */
		$('.apply_product').on('click', function (e) {
			e.preventDefault();

			component_loader_show($('.component_loader'), '');
			$('.product_text').ckeditor({action: 'update'});
			$('.product_a_text').ckeditor({action: 'update'});

			$('#product_form').ajaxSubmit({
				success: function (response) {
					if (response.success) component_loader_hide($('.component_loader'), '');
				},
				dataType: 'json'
			});
		});

		$('.save_product').on('click', function (e) {
			e.preventDefault();

			component_loader_show($('.component_loader'), '');
			$('.product_text').ckeditor({action: 'update'});
			$('.product_a_text').ckeditor({action: 'update'});

			$('#product_form').ajaxSubmit({
				success: function (response) {
					if (response.success) window.location.href = '<?=$this->init_model->get_link($product['menu_id'], '{URL}');?>?page=<?=$page;?>';
				},
				dataType: 'json'
			});
		});

        $(function () {
            var similar_search,
                products = '',
                $search_similar_list = $('#search_similar_list'),
                $similar_list = $('#similar_list');

            $('#search_similar').on('keyup paste', function () {
                clearTimeout(similar_search);

                var $this = $(this);

                if ($(this).val() != '') {
                    similar_search = setTimeout(function () {
                        var ignore = [<?=$product_id;?>];

                        $('#similar_list').find('a').map(function () {
                            ignore.push($(this).data('product-id'));
                        });

                        $.post(
                            '<?=$this->uri->full_url('admin/catalog/search');?>',
                            {
                                menu_id: <?=$menu_id;?>,
                                ignore: ignore,
                                query: $this.val()
                            },
                            function (response) {
                                if (response.success) {
                                    if (response.products.length > 0) {
                                        products = '';
                                        $.each(response.products, function (i, val) {
                                            products += '<li><div class="holder"><div class="cell auto">' + val.title + '</div><div class="cell w_140"><a class="text_link set_similar" data-product-id="' + val.product_id + '">вибрати</a></div></div></li>';
                                        });
                                        $search_similar_list.html(products);
                                        row_decor();
                                    } else {
                                        $search_similar_list.html('<li>Нічого не знайдено</li>');
                                    }
                                }
                            },
                            'json'
                        );
                    }, 500);
                }
            });

            $search_similar_list
                .on('click', '.set_similar', function (e) {
                    e.preventDefault();

                    var $link = $(this);

                    $.post(
                        '<?=$this->uri->full_url('admin/catalog/set_similar');?>',
                        {
                            menu_id: <?=$product['menu_id'];?>,
                            product_id: <?=$product_id;?>,
                            similar_id: $link.data('product-id')
                        },
                        function (response) {
                            if (response.success) {
                                var $row = $link.closest('li').clone();
                                $link.closest('li').remove();

                                $row.find('.set_similar').addClass('remove_similar').text('скасувати').removeClass('set_similar');
                                $similar_list.append($row);

                                row_decor();
                            }
                        },
                        'json'
                    );
                });

            $similar_list
                .on('click', '.remove_similar', function (e) {
                    e.preventDefault();

                    var $link = $(this);

                    $.post(
                        '<?=$this->uri->full_url('admin/catalog/delete_similar');?>',
                        {
                            menu_id: <?=$product['menu_id'];?>,
                            product_id: <?=$product_id;?>,
                            similar_id: $link.data('product-id')
                        },
                        function (response) {
                            if (response.success) {
                                $link.closest('li').remove();
                                row_decor();
                            }
                        },
                        'json'
                    );
                });
        });

        $(function () {
            var similar_to_search,
                products = '',
                $search_similar_to_list = $('#search_similar_to_list'),
                $similar_to_list = $('#similar_to_list');

            $('#search_similar_to').on('keyup paste', function () {
                clearTimeout(similar_to_search);

                var $this = $(this);

                if ($(this).val() != '') {
                    similar_to_search = setTimeout(function () {
                        var ignore = [<?=$product_id;?>];

                        $('#similar_list').find('a').map(function () {
                            ignore.push($(this).data('product-id'));
                        });

                        $.post(
                            '<?=$this->uri->full_url('admin/catalog/search');?>',
                            {
                                menu_id: <?=$menu_id;?>,
                                ignore: ignore,
                                query: $this.val()
                            },
                            function (response) {
                                if (response.success) {
                                    if (response.products.length > 0) {
                                        products = '';
                                        $.each(response.products, function (i, val) {
                                            products += '<li><div class="holder"><div class="cell auto">' + val.title + '</div><div class="cell w_140"><a class="text_link set_similar_to" data-product-id="' + val.product_id + '">вибрати</a></div></div></li>';
                                        });
                                        $search_similar_to_list.html(products);
                                        row_decor();
                                    } else {
                                        $search_similar_to_list.html('<li>Нічого не знайдено</li>');
                                    }
                                }
                            },
                            'json'
                        );
                    }, 500);
                }
            });

            $search_similar_to_list
                .on('click', '.set_similar_to', function (e) {
                    e.preventDefault();

                    var $link = $(this);

                    $.post(
                        '<?=$this->uri->full_url('admin/catalog/set_similar');?>',
                        {
                            menu_id: <?=$product['menu_id'];?>,
                            product_id: $link.data('product-id'),
                            similar_id: <?=$product_id;?>
                        },
                        function (response) {
                            if (response.success) {
                                var $row = $link.closest('li').clone();
                                $link.closest('li').remove();

                                $row.find('.set_similar_to').addClass('remove_similar_to').text('скасувати').removeClass('set_similar_to');
                                $similar_to_list.append($row);

                                row_decor();
                            }
                        },
                        'json'
                    );
                });

            $similar_to_list
                .on('click', '.remove_similar_to', function (e) {
                    e.preventDefault();

                    var $link = $(this);

                    $.post(
                        '<?=$this->uri->full_url('admin/catalog/delete_similar');?>',
                        {
                            menu_id: <?=$product['menu_id'];?>,
                            product_id: $link.data('product-id'),
                            similar_id: <?=$product_id;?>
                        },
                        function (response) {
                            if (response.success) {
                                $link.closest('li').remove();
                                row_decor();
                            }
                        },
                        'json'
                    );
                });
        });
	});
</script>