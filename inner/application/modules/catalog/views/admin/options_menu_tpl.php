<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if (count($options) > 0): ?>
	<ul id="options_list">
		<?php foreach ($options as $v): ?>
			<li>
				<div class="holder">
					<div class="cell auto"><span class="menu_item"><?=$v['name'];?></span></div>
				</div>
				<?php if (isset($v['childs'])): ?>
					<ul>
						<?php foreach ($v['childs'] as $_v): ?>
							<li>
								<div class="holder">
									<div class="cell w_20">
										<div class="controls">
											<label for="option_<?=$_v['option_id'];?>" class="check_label">
												<i></i>
												<input type="checkbox" id="option_<?=$_v['option_id'];?>" name="options[]" value="<?=$_v['option_id'];?>"<?=(isset($product_options[$_v['option_id']]) ? ' checked="checked"' : '');?>>
											</label>
										</div>
									</div>
									<div class="cell auto"><span class="menu_item"><?=$_v['name'];?></span></div>
									<div class="cell w_180"><input type="text" name="option_price[]" value="<?=((isset($product_options[$_v['option_id']]['price']) AND $product_options[$_v['option_id']]['price'] > 0) ? $product_options[$_v['option_id']]['price'] : '');?>" class="option_price"> грн</div>
									<div class="cell w_70"><input type="text" name="option_price_usd[]" value="<?=((isset($product_options[$_v['option_id']]['price_usd']) AND $product_options[$_v['option_id']]['price_usd'] > 0) ? $product_options[$_v['option_id']]['price_usd'] : '');?>" class="option_price_usd"> &dollar;</div>
									<div class="cell w_70"><input type="text" name="option_price_eur[]" value="<?=((isset($product_options[$_v['option_id']]['price_eur']) AND $product_options[$_v['option_id']]['price_eur'] > 0) ? $product_options[$_v['option_id']]['price_eur'] : '');?>" class="option_price_eur"> &euro;</div>
								</div>
							</li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
