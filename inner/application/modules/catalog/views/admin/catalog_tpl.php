<?php
	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_css('js/admin/fineuploader/fineuploader-3.5.0.css', TRUE);

	$this->template_lib->set_js('admin/fineuploader/jquery.fineuploader-3.5.0.min.js');
	$this->template_lib->set_js('admin/ui/jquery-ui-1.10.3.custom.min.js');
?>
<div class="admin_component" id="admin_component_<?=$component_id;?>" data-component-id="<?=$component_id;?>" data-module="catalog" data-css-class="catalog" data-visibility-url="<?=$this->uri->full_url('admin/components/toggle_visibility');?>" data-delete-url="<?=$this->uri->full_url('admin/catalog/delete_component');?>">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="<?=(($hidden == 0) ? 'catalog' : 'hidden');?>"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Каталог</div></div>
			<?php if (!$is_parent): ?>
				<a class="fm filters" href="<?=$this->uri->full_url('admin/catalog_filters/component?menu_id=' . $menu_id . '&component_id=' . $component_id);?>"><b></b>Фільтри</a>
				<!--a class="fm options" href="<?=$this->uri->full_url('admin/catalog_options/component?menu_id=' . $menu_id . '&component_id=' . $component_id);?>"><b></b>Опції</a-->
				<a class="fm add add_product" href="#"><b></b>Додати товар</a>
				<a class="fm add_bottom" href="#"><b class="active"></b>Додавати внизу</a>
			<?php endif; ?>
			<a href="#" class="fm show_hide"><b></b><?=(($hidden == 0) ? 'Приховати' : 'Показати');?></a>
		</div>
		<div class="fmr component_del">
			<a href="#" class="fm delete_component"><b></b></a>
		</div>
		<div class="fmr component_pos">
			<a href="#" class="fm up_component"><b></b></a>
			<a href="#" class="fm down_component"><b></b></a>
		</div>
	</div>
	<div class="fm per_page">
		<a id="catalog_export" href="#">Експорт</a>
		<a id="catalog_import" href="#">Імпорт</a>
	</div>
	<div class="fmr per_page">
		<span>Відображати</span>
		<a href="#" class="per_page <?php if ($per_page == 20) echo ' active'; ?>" data-value="20">20</a>
		<a href="#" class="per_page <?php if ($per_page == 50) echo ' active'; ?>" data-value="50">50</a>
		<a href="#" class="per_page <?php if ($per_page == 100) echo ' active'; ?>" data-value="100">100</a>
		<a href="#" class="per_page <?php if ($per_page == 0) echo ' active'; ?>" data-value="0">Всі</a>
		<span>на сторінці</span>
	</div>
	<div id="component_selector" style="display: none;">
		<div class="fm admin_menu controls" style="max-height: 300px; overflow: auto; border: 1px solid #ccc; border-bottom: none;" data-type="radio"><?php if (count($components) > 0) build_components_menu_list($components, 0, array(), $component_id); ?></div>
		<div class="fm for_sucsess" style="margin-bottom: 30px">
			<div class="fmr save_links">
				<a id="move_products" href="#" class="fm save_adm"><b></b>Перенести вибрані товари</a>
			</div>
		</div>
	</div>
	<div class="fm admin_menu">
		<ul>
			<li class="th">
				<div class="holder">
					<a href="#" class="delete_selected no_cell" style="display:none;">Видалити вибрані</a>
					<div class="cell last_edit"></div>
					<div class="cell w_20"><div class="fm controls"><label class="check_label"><i><input name="product_all" type="checkbox" value="1"></i></label></div></div>
					<div class="cell w_30"></div>
					<div class="cell w_20"></div>
					<div class="cell no_padding" style="width:246px;"></div>
					<div class="cell w_20"></div>
					<div class="cell auto"><a href="#" class="sort_cell<?php if ($sort == 'title_asc') echo ' down'; if ($sort == 'title_desc') echo ' up'; ?>" data-value="<?php if ($sort == 'title_asc') echo 'title_desc'; if ($sort == 'title_desc') echo 'title_asc'; if ($sort == NULL OR $sort == 'price_asc' OR $sort == 'price_desc') echo 'title_asc'; ?>">Назва</a><?php if ($sort == 'title_asc' OR $sort == 'title_desc'): ?><a href="#" class="default_sorting"></a><?php endif; ?></div>
					<?php if ($sort == NULL): ?>
						<div class="cell w_20"></div>
						<div class="cell w_20"></div>
					<?php endif; ?>
					<div class="cell w_20"></div>
				</div>
			</li>
		</ul>
		<ul class="products_list">
			<?php if (count($products) > 0): ?><?php foreach ($products as $product): ?>
			<li data-id="<?=$product['product_id'];?>">
				<div class="holder<?php if ($product['hidden'] == 1): ?> hidden<?php endif; ?>">
					<div class="cell last_edit <?php if (isset($last_product) AND ($product['product_id'] == $last_product)) echo ' active'; ?>"></div>
					<div class="cell w_20 icon"><div class="fm controls"><label class="check_label"><i><input type="checkbox" name="product[]" value="<?=$product['product_id'];?>"></i></label></div></div>
					<div class="cell w_30 number"></div>
					<div class="cell w_20 icon"><a href="#" class="hide-show<?php if ($product['hidden'] == 0): ?> active<?php endif; ?>"></a></div>
					<div class="cell no_padding" style="width:246px;">
						<?php if ($product['status'] == 1): ?><span class="ribbon promo"><b>Акція</b></span><?php endif; ?>
						<?php if ($product['status'] == 2): ?><span class="ribbon new"><b>Новинка</b></span><?php endif; ?>
						<?php if ($product['status'] == 3): ?><span class="ribbon discount"><b>Знижка</b></span><?php endif; ?>
						<?php if ($product['status'] == 4): ?><span class="ribbon hit"><b>Хіт продаж</b></span><?php endif; ?>
						<?php if ($product['image'] != ''): ?><img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($product['product_id']) . '/' . $product['product_id'] . '/t_' . $product['image'] . '?t=' . time() . rand(10000, 1000000));?>" alt=""><?php endif; ?>
					</div>
					<div class="cell w_20 icon"><a href="<?=$this->uri->full_url('admin/catalog/edit?menu_id=' . $menu_id . '&product_id=' . $product['product_id'] . '&page=' . $page);?>" class="edit"></a></div>
					<div class="cell auto"><?=($product['title'] != '' ? $product['title'] : 'Новий товар');?></div>
					<?php if ($sort == NULL): ?>
						<div class="cell w_20 icon"><a href="#" class="duble_click"></a></div>
						<div class="cell w_20 icon sorter"><a href="#" class="single_arrows"></a></div>
					<?php endif; ?>
					<div class="cell w_20 icon"><a href="#" class="delete"></a></div>
				</div>
			</li>
			<?php endforeach; ?><?php endif; ?>
		</ul>
	</div>
	<?php if (isset($paginator)) echo $paginator; ?>
</div>
<div class="confirm_overlay" id="export_overlay" style="opacity: 0.5; display: none;"></div>
<div class="confirm_modal" id="export_modal" style="display: none;">
	<div class="fm cm_panel">
		<div class="fm cm_icon_place">
			<div id="modal_icon" class="fm cm_export"></div>
		</div>
	</div>
	<div class="fm cm_for_text">
		<div class="controls">
			<label class="radio_label"><i><input type="radio" name="export_type" value="1"></i>Експортувати каталог повністю</label>
			<label class="radio_label"><i><input type="radio" name="export_type" value="2" checked="checked"></i>Експортувати поточний розділ</label>
		</div>
	</div>
	<div class="fm cm_bottom_panel">
		<div class="fmr delete_or_no">
			<a id="export_yes" href="#" class="fm export_yes"><b></b>Експортувати</a>
			<a id="export_no" href="#" class="fm export_no"><b></b>Скасувати</a>
		</div>
	</div>
</div>
<div class="confirm_overlay" id="import_overlay" style="opacity: 0.5; display: none;"></div>
<div class="confirm_modal" id="import_modal" style="display: none;">
	<div class="fm cm_panel">
		<div class="fm cm_icon_place">
			<div id="modal_icon" class="fm cm_import"></div>
		</div>
	</div>
	<div class="fm cm_for_text">
		<!--
		<div class="controls">
			<label class="radio_label"><i><input type="radio" name="import_type" value="1"></i>Імпортувати каталог повністю</label>
			<label class="radio_label"><i><input type="radio" name="import_type" value="2" checked="checked"></i>Імпортувати поточний розділ</label>
		</div>
		-->
		<div class="controls">
			Відсутні в прайсі товари:
			<!--<label class="radio_label"><i><input type="radio" name="after_import" value="1"></i>змінити статус на "Немає в наявності"</label>-->
			<label class="radio_label"><i><input type="radio" name="after_import" value="2"></i>видалити з каталогу</label>
			<label class="radio_label"><i><input type="radio" name="after_import" value="0" checked="checked"></i> невиконувати ніяких дій</label>
		</div>
		<div class="import_box"></div>
		<div class="import_info"></div>
	</div>
	<div class="fm cm_bottom_panel">
		<div class="fmr delete_or_no">
			<a id="import_no" href="#" class="fm import_no"><b></b>Скасувати</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {

		$('#catalog_import').on('click', function (e) {
			e.preventDefault();
			$('#import_overlay').add('#import_modal').show();
		});

		$('#import_no').on('click', function (e) {
			e.preventDefault();
			$('#import_overlay').add('#import_modal').hide();
		});

		$('.import_box')
			.fineUploader({
				request: {
					endpoint: '<?=$this->uri->full_url('admin/catalog/import');?>',
					inputName: 'price_file',
					params: {
						//import_type: $('[name="import_type"]:checked').val(),
						after_import: $('[name="after_import"]:checked').val(),
						menu_id: <?=$menu_id;?>,
						component_id: <?=$component_id?>
					}
				},
				multiple: true,
				text: {
					uploadButton: 'Виберіть або перетягніть файл прайсу',
					dragZone: '',
					dropProcessing: ''
				},
				validation: {
					allowedExtensions: ['xls'],
					sizeLimit: <?=(intval(ini_get('upload_max_filesize')) * 1048576);?>,
					productLimit: 1
				},
				messages: {
					typeError: "Дозволено завантажувати: {extensions}.",
					sizeError: "Розмір файлу не повинен перевищувати {sizeLimit}.",
					tooManyproductsError: "Дозволено завантажувати файлів: {productLimit}."
				}
			})
			.on('complete', function (event, id, fileName, response) {
				if (response.success) {
					$('.qq-upload-success').remove();
					$('.import_info').text('Товари успішно імпортовано');
					window.location.reload();
				}
			});

		$('#catalog_export').on('click', function (e) {
			e.preventDefault();
			$('#export_overlay').add('#export_modal').show();
		});

		$('#export_yes').on('click', function (e) {
			e.preventDefault();
			window.location.href = '<?=$this->uri->full_url('admin/catalog/export');?>?menu_id=<?=$menu_id;?>&export_type=' + $('[name="export_type"]:checked').val();
		});

		$('#export_no').on('click', function (e) {
			e.preventDefault();
			$('#export_overlay').add('#export_modal').hide();
		});

		var $component = $('#admin_component_<?=$component_id;?>'),
			$loader = $component.find('.component_loader'),
			product_add_top = 0,
			before = <?=$before?>,
			after = <?=$after?>,
			page = <?=$page;?>,
			last_page = <?=$last_page?>;

		if (before > 0) {
			var rows = '';
			for (var i = 0; i < before; i++) rows += '<li></li>';
			$component.find('.products_list').prepend(rows);
		}

		if (after > 0) {
			var rows = '';
			for (var i = 0; i < after; i++) rows += '<li></li>';
			$component.find('.products_list').append(rows);
		}

		function sortable() {
			if (jQuery().sortable) {
				set_sortable();
			} else {
				$.getScript(
					'<?=base_url('js/admin/ui/jquery-ui-1.10.3.custom.min.js');?>',
					'set_sortable'
				);
			}
		}

		function set_sortable() {
			$component.find(".products_list")
				.sortable({
					axis: 'y',
					handle: '.sorter a',
					scroll: true,
					crollSpeed: 2000,
					forcePlaceholderSize: true,
					placeholder: "ui-state-highlight",
					update: function () {
						var products = [];

						$component.find('.products_list').find('li').each(function (i) {
							if ($(this).data('id') != undefined) {
								products.push([i, $(this).data('id')]);
								$(this).find('.number').text(i + 1);
							}
						});

						component_loader_show($loader, '');

						$.post(
							'<?=$this->uri->full_url('admin/catalog/update_position');?>',
							{
								products: products
							},
							function (response) {
								if (response.success) {
									row_decor();
									component_loader_hide($loader, '');
								}
							},
							'json'
						);
					}
				});
		}

		function row_decor() {
			$component.find('.products_list')
				.find('li').removeClass('grey').each(function (i) { $(this).find('.number').text(i + 1); })
				.end()
				.find('li:odd').addClass('grey');

			$component.find('#component_selector')
				.find('li').removeClass('grey').each(function (i) { $(this).find('.number').text(i + 1); })
				.end()
				.find('li:odd').addClass('grey');

			sortable();
		}

		$component.component({
			onDelete: function () {
				$('.com_catalog').show();
			}
		});

		row_decor();

		$component
			.find('.controls').style_input()
			.end()
			.on('click', 'a.per_page', function (e) {
				e.preventDefault();

				if (!$(this).hasClass('active')) {
					setcookie('a_catalog_per_page', $(this).data('value'), 86400, '/');
					window.location.href = '<?=$base_url;?>';
				}
			})
			.on('click', '.sort_cell', function (e) {
				e.preventDefault();

				setcookie('a_catalog_sort', $(this).data('value'), 86400, '/');
				window.location.reload();
			})
			.on('click', '.default_sorting', function (e) {
				e.preventDefault();

				setcookie('a_catalog_sort', null, -20, '/');
				window.location.reload();
			})
			.on('click', '.add_product', function (e) {
				e.preventDefault();

				var $link = $(this);
				component_loader_show($loader, '');

				$.post(
					'<?=$this->uri->full_url('admin/catalog/add');?>',
					{
						component_id: <?=$component_id;?>,
						menu_id: <?=$menu_id;?>,
						add_top: product_add_top
					},
					function (response) {
						if (response.success) {
							if (product_add_top === 1 && page != 1) {
								window.location.href = '<?=$base_url;?>';
							} else if (product_add_top === 0 && page != last_page) {
								window.location.href = '<?=$base_url;?>?page=<?=$last_page;?>';
							} else {
								var product = '<li data-id="' + response.product_id + '"><div class="holder"><div class="cell last_edit"></div><div class="cell w_20"><div class="fm controls"><label class="check_label"><i><input type="checkbox" name="product[]" value="' + response.product_id + '"></i></label></div></div><div class="cell w_30 number"></div><div class="cell w_20 icon"><a href="#" class="hide-show active"></a></div><div class="cell no_padding" style="width: 246px"></div><div class="cell w_20 icon"><a class="edit" href="<?php echo $this->uri->full_url('admin/catalog/edit?menu_id=' . $menu_id . '&page=' . $page . '&product_id='); ?>' + response.product_id + '"></a></div><div class="cell auto">Новий товар</div><div class="cell w_80"><b>0</b> грн</div><div class="cell w_20 icon"><a href="#" class="duble_click"></a></div><?php if ($sort == NULL): ?><div class="cell w_20 icon sorter"><a href="#" class="single_arrows"></a></div><div class="cell w_20 icon"><a href="#" class="delete"></a></div><?php endif; ?></div></li>';
								if (product_add_top === 1) $component.find('.products_list').prepend(product); else $component.find('.products_list').append(product);

								row_decor();
								component_loader_hide($loader, '');
							}
						}
					},
					'json'
				);
			})
			.on('change', '.admin_menu :checkbox', function () {

				if ($(this).closest('li').hasClass('th')) {
					if ($(this).prop('checked')) {
						$(this).closest('.admin_menu').find('label').addClass('active').find(':checkbox').prop('checked', true);
					} else {
						$(this).closest('.admin_menu').find('label').removeClass('active').find(':checkbox').prop('checked', false);
					}
				}

				var ck_all = 0;

				$component.find('.admin_menu').find('li').find(':checkbox').map(function () {
					if (!$(this).closest('li').hasClass('th')) if ($(this).prop('checked')) ck_all++;
				});

				if (ck_all > 0) {
					$('#component_selector').show();

					$(this).closest('.admin_menu').find('.delete_selected').show();
					$(this).closest('.admin_menu').find('label').eq(0).addClass('active').find(':checkbox').prop('checked', true);
				} else {
					$('#component_selector').hide();

					$(this).closest('.admin_menu').find('.delete_selected').hide();
					$(this).closest('.admin_menu').find('label').eq(0).removeClass('active').find(':checkbox').prop('checked', false);
				}
			})
			.on('click', '.add_bottom', function (e) {
				e.preventDefault();

				var $b = $(this).find('b');

				if (!$b.hasClass('active')) {
					$b.addClass('active');
					product_add_top = 0;
				} else {
					$b.removeClass('active');
					product_add_top = 1;
				}
			})
			.on('click', '.hide-show', function (e) {
				e.preventDefault();

				var $link = $(this),
					$holder = $link.closest('.holder'),
					status = 0;

				if ($holder.hasClass('hidden')) {
					$holder.removeClass('hidden');
					$link.addClass('active');
				} else {
					$holder.addClass('hidden');
					$link.removeClass('active');
					status = 1;
				}

				component_loader_show($loader, '');

				$.post(
					'<?php echo $this->uri->full_url('admin/catalog/visibility'); ?>',
					{
						product_id: $holder.closest('li').data('id'),
						status: status
					},
					function (response) {
						if (response.success) component_loader_hide($loader, '');
					},
					'json'
				);
			})
			.on('click', '.duble_click', function (e) {
				e.preventDefault();

				var $link = $(this);

				component_loader_show($loader, '');

				$.post(
					'<?=$this->uri->full_url('admin/catalog/copy');?>',
					{
						product_id: $link.closest('li').data('id')
					},
					function (response) {
						if (response.success) {
							var product = '<li data-id="' + response.product_id + '"><div class="holder"><div class="cell last_edit"></div><div class="cell w_20"><div class="fm controls"><label class="check_label"><i><input type="checkbox" name="product[]" value="' + response.product_id + '"></i></label></div></div><div class="cell w_30 number"></div><div class="cell w_20 icon"><a href="#" class="hide-show active"></a></div><div class="cell no_padding" style="width:246px">' + (response.image != '' ? '<img src="' + response.image + '" alt="">' : '') + '</div><div class="cell w_20 icon"><a class="edit" href="<?=$this->uri->full_url('admin/catalog/edit?menu_id=' . $menu_id . '&product_id=');?>' + response.product_id + '"></a></div><div class="cell auto">' + response.title + '</div><!--div class="short good_price"><b>0</b> грн</div--><div class="cell w_20 icon"><a href="#" class="duble_click"></a></div><?php if ($sort == NULL): ?><div class="cell w_20 icon sorter"><a href="#" class="single_arrows"></a></div><div class="cell w_20 icon"><a href="#" class="delete"></a></div><?php endif; ?></div></li>';
							$(product).insertAfter($link.closest('li'));

							row_decor();
							component_loader_hide($loader, '');
						}
					},
					'json'
				);
			})
			.on('click', '.delete', function (e) {
				e.preventDefault();

				var $link = $(this);

				confirmation('Видалити товар?', function () {

					component_loader_show($loader, '');

					$.post(
						'<?=$this->uri->full_url('admin/catalog/delete');?>',
						{
							product_id: $link.closest('li').data('id')
						},
						function (response) {
							if (response.success) {
								$link.closest('li').slideUp(function () {
									component_loader_hide($loader, '');
									$(this).remove();
									row_decor();
								});
							}
						},
						'json'
					);
				});
			})
			.on('click', '.delete_selected', function (e) {
				e.preventDefault();

				confirmation('Видалити вибрані товари?', function () {
					$.when(
						$component.find('.products_list').find(':checked').each(function () {
							if (!$(this).closest('li').hasClass('th')) {
								component_loader_show($loader, '');
								var $link = $(this);
								$.post(
									'<?=$this->uri->full_url('admin/catalog/delete');?>',
									{
										product_id: $link.val()
									},
									function (response) {
										if (response.success) {
											$link.closest('li').slideUp(function () {
												$(this).remove();
												row_decor();
											});
										}
									},
									'json'
								);
							}
						})
					).then(function () {
						$component.find('.delete_selected').hide();
						$component.find('.admin_menu').find('label').eq(0).removeClass('active').find(':checkbox').prop('checked', false);
						component_loader_hide($loader, '');
					});
				});
			})
			.on('mouseover', '.products_list .holder', function () {
				$(this).addClass('active');
			})
			.on('mouseout', '.products_list .holder', function () {
				$(this).removeClass('active');
			})
			.on('mouseover', '#component_selector .holder', function () {
				$(this).addClass('active');
			})
			.on('mouseout', '#component_selector .holder', function () {
				$(this).removeClass('active');
			});


		$('#move_products').on('click', function (e) {
			e.preventDefault();

			var selected_component = $('[name="component"]:checked').val(),
				products_list = [];

			if ($.isNumeric(selected_component)) {
				$component.find('.products_list').find('[type="checkbox"]').each(function () {
					if ($(this).prop('checked')) {
						products_list.push($(this).val());
					}
				});

				if (products_list.length > 0) {

					component_loader_show($loader, '');

					$.post(
						'<?=$this->uri->full_url('admin/catalog/change_component');?>',
						{
							component_id: selected_component,
							products: products_list
						},
						function (response) {
							if (response.success) {

								component_loader_hide($loader, '');

								for (var i = 0; i < products_list.length; i++) {
									$component.find('li[data-id="' + products_list[i] + '"]').slideUp(function () {
										$(this).remove();
									});
								}

								row_decor();

								$component.find('[name="product_all"]').prop('checked', false);
							}
						},
						'json'
					);
				}
			}
		});
	});
</script>