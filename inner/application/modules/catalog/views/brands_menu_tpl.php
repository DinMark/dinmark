<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<?php if (isset($brands_menu) AND count($brands_menu) > 0): ?>
<div class="fm our_brands">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="sect-title"><span><? if(LANG=='ua')echo'Бренди';if(LANG=='ru')echo'Бренды';if(LANG=='en')echo'Brands';?></span></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="ob_box">
			<div class="ob_left"><a href="#" class="hidden"></a></div>
			<div class="ob_container">
				<div class="ob_long">
					<?php foreach ($brands_menu as $v): ?>
					<div class="fm ob_one_logo">
						<div>
							<a href="<?=$this->uri->full_url($v['static_url'] !== '' ? $v['static_url'] : $v['url']);?>">
								<?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/menu/' . $v['id'] . '/' . $v['image']);?>" alt="<?=$v['name'];?>"><?php endif; ?>
							</a>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="ob_right"><a href="#"></a></div>
		</div>
	</div>
</div>
<?php endif; ?>