<?php defined('ROOT_PATH') OR exit('No direct script access allowed');
	$this->template_lib->set_css('js/raty/jquery.raty.css', TRUE);
	$this->template_lib->set_js('raty/jquery.raty.js');
?>
<?php if (isset($products) AND count($products) > 0): ?>
<div class="fm most-interest">
<div class="container">
	<div class="fm most-interest_links">
		<div class="most-interest_title fm">ПОпулярні товари</div>
		<ul>
			<?php if (isset($products['promo'])): ?><li>
				<div class="link-wrapper"><? if(LANG=='ua')echo'Акції';if(LANG=='ru')echo'Акции';if(LANG=='en')echo'Shares';?><i class="fa fa-angle-down" aria-hidden="true"></i></div>
			</li><?php endif; ?>
			<?php if (isset($products['discount'])): ?><li>
				<div class="link-wrapper"><? if(LANG=='ua')echo'Знижки';if(LANG=='ru')echo'Скидки';if(LANG=='en')echo'Discounts';?><i class="fa fa-angle-down" aria-hidden="true"></i></div>
			</li><?php endif; ?>
			<?php if (isset($products['hit'])): ?><li>
				<div class="link-wrapper"><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?><i class="fa fa-angle-down" aria-hidden="true"></i></div>
			</li><?php endif; ?>
		</ul>
	</div>
	<div class="most-interest_wrapper fm">
		<?php if (isset($products['promo'])): ?>
		<div id="catalog_promo" class="most-interest_tab fm">
			<div class="row">
				<?php foreach ($products['promo'] as $v): ?>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="fm one-good"><?php $url = $this->uri->full_url($v['url']); ?>
						<div class="one-good_wrapper fm">
							<?php if ($v['status'] == 1): ?><div class="one-good_status promo"><b></b><span><? if(LANG=='ua')echo'Акція';if(LANG=='ru')echo'Акция';if(LANG=='en')echo'Share';?></span><i></i></div><?php endif; ?>
                            <?php if ($v['status'] == 3): ?><div class="one-good_status discount"><b></b><span><? if(LANG=='ua')echo'Знижка';if(LANG=='ru')echo'Скидка';if(LANG=='en')echo'Discount';?></span><i></i></div><?php endif; ?>
                            <?php if ($v['status'] == 4): ?><div class="one-good_status hit"><b></b><span><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?></span><i></i></div><?php endif; ?>
							<div class="fm one-good_img">
								<div class="cell">
									<a href="<?=$url;?>">
										<?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/t_' . $v['image']);?>" alt="<?=$v['title'];?>"><?php endif; ?>
									</a>
								</div>
							</div>
							<div class="fm one-good_head">
								<a href="<?=$url;?>" class="one-good_name"><?=$v['title'];?></a>
							</div>
							<div class="one-good_options fm">
								<?php foreach($v['filters'] as $_v): ?>
									<div class="one-good_options_row">
										<div class="one-good_options_cell"><?=$_v['filter_name'];?>:</div>
										<div class="one-good_options_cell"><?=implode(', ', $_v['childs']);?></div>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="box-l fm">
								<div class="fm one-good_icons">
									<a href="#" class="one-good_compare"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>
									<a href="#" class="one-good_favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
								</div>
								<a href="<?=$url;?>" class="fmr one-good_btn"><? if(LANG=='ua')echo'купити';if(LANG=='ru')echo'купить';if(LANG=='en')echo'buy';?></a>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
		<?php endif; ?>

		<?php if (isset($products['discount'])): ?>
		<div id="catalog_discount" class="most-interest_tab fm">
			<div class="row">
				<?php foreach ($products['discount'] as $v): ?>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="fm one-good"><?php $url = $this->uri->full_url($v['url']); ?>
						<div class="one-good_wrapper fm">
							<?php if ($v['status'] == 1): ?><div class="one-good_status promo"><b></b><span><? if(LANG=='ua')echo'Акція';if(LANG=='ru')echo'Акция';if(LANG=='en')echo'Share';?></span><i></i></div><?php endif; ?>
                            <?php if ($v['status'] == 3): ?><div class="one-good_status discount"><b></b><span><? if(LANG=='ua')echo'Знижка';if(LANG=='ru')echo'Скидка';if(LANG=='en')echo'Discount';?></span><i></i></div><?php endif; ?>
                            <?php if ($v['status'] == 4): ?><div class="one-good_status hit"><b></b><span><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?></span><i></i></div><?php endif; ?>
							<div class="fm one-good_img">
								<div class="cell">
									<a href="<?=$url;?>">
										<?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/t_' . $v['image']);?>" alt="<?=$v['title'];?>"><?php endif; ?>
									</a>
								</div>
							</div>
							<div class="fm one-good_head">
								<a href="<?=$url;?>" class="one-good_name"><?=$v['title'];?></a>
							</div>
							<div class="one-good_options fm">
								<?php foreach($v['filters'] as $_v): ?>
									<div class="one-good_options_row">
										<div class="one-good_options_cell"><?=$_v['filter_name'];?>:</div>
										<div class="one-good_options_cell"><?=implode(', ', $_v['childs']);?></div>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="box-l fm">
								<a href="<?=$url;?>" class="one-good_btn"><? if(LANG=='ua')echo'купити';if(LANG=='ru')echo'купить';if(LANG=='en')echo'buy';?></a>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
            </div>
		</div>
		<?php endif; ?>

		<?php if (isset($products['hit'])): ?>
		<div id="catalog_hit" class="most-interest_tab fm">
			<div class="row">
				<?php foreach ($products['hit'] as $v): ?>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="fm one-good"><?php $url = $this->uri->full_url($v['url']); ?>
						<div class="one-good_wrapper fm">
							<?php if ($v['status'] == 1): ?><div class="one-good_status promo"><b></b><span><? if(LANG=='ua')echo'Акція';if(LANG=='ru')echo'Акция';if(LANG=='en')echo'Share';?></span><i></i></div><?php endif; ?>
                            <?php if ($v['status'] == 3): ?><div class="one-good_status discount"><b></b><span><? if(LANG=='ua')echo'Знижка';if(LANG=='ru')echo'Скидка';if(LANG=='en')echo'Discount';?></span><i></i></div><?php endif; ?>
                            <?php if ($v['status'] == 4): ?><div class="one-good_status hit"><b></b><span><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?></span><i></i></div><?php endif; ?>
							<div class="fm one-good_img">
								<div class="cell">
									<a href="<?=$url;?>">
										<?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/t_' . $v['image']);?>" alt="<?=$v['title'];?>"><?php endif; ?>
									</a>
								</div>
							</div>
							<div class="fm one-good_head">
								<a href="<?=$url;?>" class="one-good_name"><?=$v['title'];?></a>
							</div>
							<div class="one-good_options fm">
								<?php foreach($v['filters'] as $_v): ?>
									<div class="one-good_options_row">
										<div class="one-good_options_cell"><?=$_v['filter_name'];?>:</div>
										<div class="one-good_options_cell"><?=implode(', ', $_v['childs']);?></div>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="box-l fm">
								<a href="<?=$url;?>" class="one-good_btn"><? if(LANG=='ua')echo'купити';if(LANG=='ru')echo'купить';if(LANG=='en')echo'buy';?></a>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
            </div>
		</div>
		<?php endif; ?>
	</div>
</div>
</div>
<script type="text/javascript">
	function changeMostInterest(){
		if($(window).outerWidth(true) <= 767){
			$('.most-interest').addClass('mobile').removeClass('desktop');

			//popular products accordeon
			for(var i = 0; i < $('.most-interest_tab').length; i++){

				var $li = $('.most-interest_links > ul > li').eq(i),
					$tab = $('.most-interest_tab').eq(i);

				$tab.appendTo($li);
			}

			$('.most-interest_links .link-wrapper').next().hide();
			$('.most-interest_links .link-wrapper').removeClass('active');
			$('.most-interest_links li:first').children('.most-interest_tab').show().prev('.link-wrapper').addClass('active');

		}else if($(window).outerWidth(true) >= 767){

			$('.most-interest').removeClass('mobile').addClass('desktop');
			var $all_li = $('.most-interest').find('.most-interest_links').children('ul').children('li'),
				$tabWrapper = $('.most-interest_wrapper');

			$all_li.each(function(index){
				var $tab = $(this).find('.most-interest_tab');

				$tab.appendTo($tabWrapper);
			});

			$('.most-interest_tab').hide().first().show();
			$('.most-interest_links .link-wrapper').removeClass('active');
			$('.most-interest_links li').eq(0).children('.link-wrapper').addClass('active');
		}
	};

	changeMostInterest()
	$(window).resize(function(){
		changeMostInterest()
	});

	//popular products accordeon
	$('body').on('click', '.most-interest.mobile .link-wrapper', function(){
		if(!$(this).hasClass('active')){
			$('.link-wrapper').removeClass('active').next().slideUp(400);
			$(this).addClass('active').next().slideDown(400);
		} else {
			$(this).removeClass('active').next().slideUp(400);
		}
	});

	//popular products tabs
	$('body').on('click', '.most-interest.desktop .link-wrapper', function(){
		var $currentLi = $(this).parent('li');
		$('.most-interest_links .link-wrapper').removeClass('active').parent('li').eq($currentLi.index()).children('.link-wrapper').addClass('active');
		$('.most-interest_tab').hide().eq($currentLi.index()).fadeIn(400);
		console.log($currentLi.index());
	});
</script>
<?php endif; ?>