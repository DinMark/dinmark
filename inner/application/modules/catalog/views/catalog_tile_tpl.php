<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<?php if (isset($products[0])): ?><?php foreach ($products as $v): ?>
<div class="col-md-4">
	<div class="fm one-good"><?php $url = $this->uri->full_url($v['url']); ?>
		<div class="one-good_wrapper fm">
			<?php if ($v['status'] == 1): ?><div class="one-good_status promo"><b></b><span><? if(LANG=='ua')echo'Акція';if(LANG=='ru')echo'Акция';if(LANG=='en')echo'Share';?></span><i></i></div><?php endif; ?>
            <?php if ($v['status'] == 3): ?><div class="one-good_status discount"><b></b><span><? if(LANG=='ua')echo'Знижка';if(LANG=='ru')echo'Скидка';if(LANG=='en')echo'Discount';?></span><i></i></div><?php endif; ?>
            <?php if ($v['status'] == 4): ?><div class="one-good_status hit"><b></b><span><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?></span><i></i></div><?php endif; ?>
			<div class="fm one-good_img">
				<div class="cell">
					<a href="<?=$url;?>">
						<?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/t_' . $v['image']);?>" alt="<?=$v['title'];?>"><?php endif; ?>
					</a>
				</div>
			</div>
			<div class="fm one-good_head">
				<a href="<?=$url;?>" class="one-good_name"><?=$v['title'];?></a>
			</div>
			<div class="one-good_options fm">
				<?php foreach($v['filters'] as $_v): ?>
					<div class="one-good_options_row">
						<div class="one-good_options_cell"><?=$_v['filter_name'];?>:</div>
						<div class="one-good_options_cell"><?=implode(', ', $_v['childs']);?></div>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="box-l fm">
				<a href="<?=$url;?>" class="fmr one-good_btn"><?php if (LANG === 'ua'): ?>купити<?php endif; ?><?php if (LANG === 'ru'): ?>купить<?php endif; ?><?php if (LANG === 'en'): ?>buy<?php endif; ?></a>
			</div>
		</div>
	</div>
</div>
<?php endforeach; ?><?php endif; ?>