<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<?php if (isset($products[0])): ?><?php foreach ($products as $v): ?>
<div class="fm one_good"><?php $url = $this->uri->full_url($v['url']); ?>
	<div class="fm og_photo">
		<div>
			<a href="<?=$url;?>">
				<?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/t_' . $v['image']);?>" alt="<?=$v['title'];?>"><?php endif; ?>
				<?php if ($v['status'] == 1): ?><span class="ribbon promo"><b></b><i></i><span><? if(LANG=='ua')echo'Акція';if(LANG=='ru')echo'Акция';if(LANG=='en')echo'Share';?></span></span><?php endif; ?>
				<?php if ($v['status'] == 2): ?><span class="ribbon new"><b></b><i></i><span><? if(LANG=='ua')echo'Новинка';if(LANG=='ru')echo'Новинка';if(LANG=='en')echo'New';?></span></span><?php endif; ?>
                <?php if ($v['status'] == 3): ?><span class="ribbon discount"><b></b><i></i><span><? if(LANG=='ua')echo'Знижка';if(LANG=='ru')echo'Скидка';if(LANG=='en')echo'Discount';?></span></span><?php endif; ?>
                <?php if ($v['status'] == 4): ?><span class="ribbon hit"><b></b><i></i><span><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?></span></span><?php endif; ?>
			</a>
		</div>
	</div>
	<div class="fm og_name"><a href="<?=$url;?>"><?=$v['title'];?></a></div>
	<div class="fm og_txt"><?=stripslashes($v['sign']);?></div>
	<div class="fm for_price">
		<a href="<?=$url;?>" class="fm to_detais"><? if(LANG=='ua')echo'Розмірний ряд';if(LANG=='ru')echo'Размерный ряд';if(LANG=='en')echo'Dimensional number';?></a>
	</div>
</div>
<?php endforeach; ?><?php endif; ?>