<?php
	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_css('js/raty/jquery.raty.css', TRUE);
	$this->template_lib->set_js('raty/jquery.raty.js');

	$this->template_lib->set_css('js/slider/slider.css', TRUE);
	$this->template_lib->set_js('slider/slider.js');

	$this->template_lib->set_js('pagination.js');
	$this->template_lib->set_js('components/catalog_list.js?v=1');
?>
<?php if (isset($products) AND is_string($products) AND trim($products) != ''): ?>
<section class="main-catalog fm <?php if (isset($template) AND $template == 'list') echo ' list'; ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div id="catalog_filters" class="filter-place fm">
                    <div class="catalog-header fm">
						<h1 class="catalog-header_name fm"><?php if (LANG == 'ua') echo 'Фільтр'; if (LANG == 'ru') echo 'Фильтр'; if (LANG == 'en') echo 'Filter'; ?></h1>
					</div>
					<?php foreach ($filters as $v): ?>
						<?php if (count($v['childs']) > 1): ?>
                            <div class="filter-box fm" data-filter-id="<?=$v['filter_id'];?>">
								<div class="filter-box_title fm"><span>></span><?=$v['name'];?></div>
								<div class="filter-box_content fm">
									<ul>
										<?php foreach ($v['childs'] as $_v): ?>
										<li><a href="#" data-value-id="<?=$_v['value_id'];?>"><?=$_v['name'];?><span class="cancel-filter">x</span></a></li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
					<!--div class="filter-box fm">
						<div class="filter-box_title fm"><span>></span>Ціна</div>
						<div class="filter-box_content fm">
							<div class="fm fs_place">
								<div id="price_slider"></div>
								<span id="price_with" class="fs_place_with"></span>
								<span id="price_to" class="fs_place_to"></span>
							</div>
						</div>
					</div-->
				</div>
			</div>
			<div class="col-md-9">
				<div id="products_list" class="catalog-wrapper fm">
					<div class="catalog-header fm">
						<h1 class="catalog-header_name fm"><?=$current_name;?></h1>
						<div class="catalog-sort fmr">
							<div class="fm dropdown filter-dropdown sort-dropdown">
								<div class="overflow"><span><?php if (LANG === 'ua'): ?>Сортувати<?php endif; ?><?php if (LANG === 'ru'): ?>Сотировать<?php endif; ?><?php if (LANG === 'en'): ?>Sort<?php endif; ?><i class="fa fa-angle-down" aria-hidden="true"></i></span></div>
								<ul>
									<li><a href="#" data-value="price_asc"><? if(LANG=='ua')echo'Від дешевших до дорожчих';if(LANG=='ru')echo'От дешевых к более дорогим';if(LANG=='en')echo'From cheaper to more expensive';?></a></li>
									<li><a href="#" data-value="price_desc"><? if(LANG=='ua')echo'Від дорогих до дешевих';if(LANG=='ru')echo'От дорогих к дешевым';if(LANG=='en')echo'From expensive to cheap';?></a></li>
									<li><a href="#" data-value="title_asc"><? if(LANG=='ua')echo'Назві (А-Я)';if(LANG=='ru')echo'Имени (А-Я)';if(LANG=='en')echo'Name (A-Z)';?></a></li>
									<li><a href="#" data-value="title_desc"><? if(LANG=='ua')echo'Назві (Я-А)';if(LANG=='ru')echo'Имени (Я-А)';if(LANG=='en')echo'Name (Z-A)';?></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<?=$products;?>
					</div>
				</div>
				<div id="products_pagination"><?php if (isset($pagination)) echo $pagination; ?></div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	min_price = <?=$price_limits['min'];?>;
	max_price = <?=$price_limits['max'];?>;
	component_id = <?=$component_id;?>;
	components = '<?=isset($components) ? implode(',', $components) : '';?>';
	catalog_total_products = <?=$total_products;?>;
</script>
<?php endif; ?>