<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<?php if (isset($catalog_menu) AND count($catalog_menu) > 0): ?>
<section class="fm categories">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sect-title"><span><? if(LANG=='ua')echo'Категорії';if(LANG=='ru')echo'Категории';if(LANG=='en')echo'Categories';?></span></div>
            </div>
            <?php foreach ($catalog_menu as $v): ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="category-item category-item--extra fm"><?php $url = $this->uri->full_url($v['url']); ?>
                    <div class="category-item_inner">
                        <div class="category-item_img">
                            <div class="cell">
                                <a href="<?=$url;?>"><?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/menu/' . $v['id'] . '/' . $v['image']);?>" alt="<?=$v['name'];?>"><?php endif; ?></a>
                            </div>
                        </div>
                        <div class="category-item_name">
                            <a href="<?=$url;?>"><?=$v['name'];?></a>
                        </div>
                        <?php if ($v['childrens_total'] > 0): ?>
                            <div class="category-item_list">
                                <ul>
                                    <?php foreach ($v['childrens'] as $_k => $_v): ?><li><a href="<?=$this->uri->full_url($_v['url']);?>"><?=$_v['name'];?></a></li><?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>