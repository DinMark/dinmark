<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_css('js/colorbox/colorbox.css', TRUE);
	$this->template_lib->set_js('colorbox/colorbox.js');

	$this->template_lib->set_css('js/raty/jquery.raty.css', TRUE);
	$this->template_lib->set_js('raty/jquery.raty.js');

	$this->template_lib->set_js('components/catalog_details.js');

	$_product_delivery = '';

	if (isset($product_delivery['delivery']) and $product_delivery['delivery'] !== '')
	{
		$_product_delivery = stripslashes($product_delivery['delivery']);
	}
    elseif (isset($delivery['delivery']) and $delivery['delivery'] !== '')
	{
		$_product_delivery = stripslashes($delivery['delivery']);
	}

	$_product_payment = '';

	if (isset($product_delivery['payment']) and $product_delivery['payment'] !== '')
	{
		$_product_payment = stripslashes($product_delivery['payment']);
	}
    elseif (isset($delivery['payment']) and $delivery['payment'] !== '')
	{
		$_product_payment = stripslashes($delivery['payment']);
	}
?>
<section class="product-details fm" itemscope itemtype="http://schema.org/Product">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="product-details_head fm">
					<div class="product-details_name fm">
						<h1 itemprop="name">
							<?=$variant['title'];?>
							<span>Артикул: <b><?=$variant['articul'];?></b></span>
						</h1>
					</div>
				</div>
			</div>
			<div id="details_photo" class="col-md-5">
                <?php if ($product['status'] == 1): ?><div class="one-good_status promo"><b></b><span><? if(LANG=='ua')echo'Акція';if(LANG=='ru')echo'Акция';if(LANG=='en')echo'Share';?></span><i></i></div><?php endif; ?>
                <?php if ($product['status'] == 3): ?><div class="one-good_status discount"><b></b><span><? if(LANG=='ua')echo'Знижка';if(LANG=='ru')echo'Скидка';if(LANG=='en')echo'Discount';?></span><i></i></div><?php endif; ?>
                <?php if ($product['status'] == 4): ?><div class="one-good_status hit"><b></b><span><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?></span><i></i></div><?php endif; ?>
				<?php if (isset($product['image']) and (string)$product['image'] != ''): ?>
				<div class="product-details_photos fm">
					<div class="one-photo fm">
						<div class="cell">
							<a href="<?=base_url('upload/catalog/' . $dir_code . '/' . $product_id . '/' . $product['image']);?>">
								<img src="<?=base_url('upload/catalog/' . $dir_code . '/' . $product_id . '/' . $product['image']);?>" alt="<?=$product['title'];?>" itemprop="image">
							</a>
						</div>
					</div>
				</div>
				<?php endif; ?>
                <div class="order-call_title"><?=$call_label;?></div>
			</div>
			<div class="col-md-4">
				<div class="product-center fm">
					<div class="product-center_title">
						<div class="product-center_title"><?php if (LANG == 'ua') echo 'Специфікації:'; if (LANG == 'ru') echo 'Спецификации:'; if (LANG == 'en') echo 'Specifications:'; ?></div>
						<div class="product-center_icons fmr">
							<a href="#" class="product-compare select_compare" data-product-id="<?=$variant_id;?>">
								<i class="fa fa-balance-scale" aria-hidden="true"></i>
							</a>
							<a href="#" class="product-favorite select_favorite" data-product-id="<?=$variant_id;?>">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
							</a>
						</div>
					</div>
					<div class="product-options fm" itemprop="description">
						<?php foreach ($filters as $v): ?>
						<div class="product-options_row">
							<div class="product-options_cell"><?=$v['filter_name'];?></div>
							<div class="product-options_cell"><?=implode(', ', $v['childs']);?></div>
						</div>
						<?php endforeach; ?>
						<div class="prop_txt"><? if(LANG=='ua')echo'В наявності: Гуртом та у роздріб';if(LANG=='ru')echo'В наличии: Оптом и в розницу';if(LANG=='en')echo'In stock: Wholesale and retail';?></div>
                        <a href="#" class="product-buy"><?php if (LANG == 'ua') echo 'Купити'; if (LANG == 'ru') echo 'Купить'; if (LANG == 'en') echo 'Buy'; ?></a>
					</div>
                    <form action="#" class="order-call_form">
                        <input type="tel" class="phone_mask" placeholder="<?php if (LANG == 'ua') echo 'ввести номер'; if (LANG == 'ru') echo 'ввести номер'; if (LANG == 'en') echo 'Enter number'; ?>">
                        <input type="submit" value="<?php if (LANG == 'ua') echo 'Купити в 1 клік'; if (LANG == 'ru') echo 'Купить в 1 клик'; if (LANG == 'en') echo 'Buy in 1 click'; ?>">
                    </form>
				</div>
			</div>
			<div class="col-md-3">
				<div class="product-delivery fm">
					<div class="product-delivery_head">Наши переваги</div>
					<div class="product-delivery_item fm">
						<div class="product-delivery_icon icon_1 fm"></div>
						<div class="product-delivery_text fm">
							<div class="product-delivery_title fm"><? if(LANG=='ua')echo'ШВИДКА ДОСТАВКА';if(LANG=='ru')echo'быстрая ДОСТАВКА';if(LANG=='en')echo'Fasr delivery';?></div>
						</div>
					</div>
					<div class="product-delivery_item fm">
						<div class="product-delivery_icon icon_2 fm"></div>
						<div class="product-delivery_text fm">
							<div class="product-delivery_title fm"><? if(LANG=='ua')echo'Сертифікований товар';if(LANG=='ru')echo'Сертификованый товар';if(LANG=='en')echo'Certified goods';?></div>
						</div>
					</div>
					<div class="product-delivery_item fm">
						<div class="product-delivery_icon icon_3 fm"></div>
						<div class="product-delivery_text fm">
							<div class="product-delivery_title fm"><? if(LANG=='ua')echo'Система знижок';if(LANG=='ru')echo'Система скидок';if(LANG=='en')echo'Discount system';?></div>
						</div>
					</div>
					<div class="product-delivery_item fm">
						<div class="product-delivery_icon icon_4 fm"></div>
						<div class="product-delivery_text fm">
							<div class="product-delivery_title fm"><? if(LANG=='ua')echo'Кваліфікований персонал';if(LANG=='ru')echo'Квалифицированый персонал';if(LANG=='en')echo'Qualified staff';?></div>
						</div>
					</div>
					<div class="product-delivery_item fm">
						<div class="product-delivery_icon icon_5 fm"></div>
						<div class="product-delivery_text fm">
							<div class="product-delivery_title fm"><? if(LANG=='ua')echo'Досвід роботи 7 років';if(LANG=='ru')echo'Опыт работы 7 лет';if(LANG=='en')echo'Work experience 7 years';?></div>
						</div>
					</div>
					<div class="product-delivery_item fm">
						<div class="product-delivery_icon icon_6 fm"></div>
						<div class="product-delivery_text fm">
							<div class="product-delivery_title fm"><? if(LANG=='ua')echo'Тисячі щасливих клієнтів';if(LANG=='ru')echo'Тысячи счастливых клиентов';if(LANG=='en')echo'Thousands of happy customers';?></div>
						</div>
					</div>
					<div class="product-delivery_item fm">
						<div class="product-delivery_icon icon_7 fm"></div>
						<div class="product-delivery_text fm">
							<div class="product-delivery_title fm"><? if(LANG=='ua')echo'100% гарантія повернення коштів';if(LANG=='ru')echo'100% гарантия возврата средств';if(LANG=='en')echo'100% refund guarantee';?></div>
						</div>
					</div>
				</div>
				<div class="fm share42init"></div>
				<script type="text/javascript" src="<?=base_url('/js/share42/share42.js');?>"></script>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="details-tabs fm">
					<div class="details-tabs_links fm">
						<ul>
							<li>
								<div class="link-wrapper active fm">асортимент</div>
							</li>
							<li>
								<div class="link-wrapper fm">технічні характеристики</div>
							</li>
							<li>
								<div class="link-wrapper fm"><? if(LANG=='ua')echo'супутні товари';if(LANG=='ru')echo'сопутствующие товары';if(LANG=='en')echo'Related products';?></div>
							</li>
							<?php if ($_product_delivery !== ''): ?>
                                <li>
                                    <div class="link-wrapper fm"><? if(LANG=='ua')echo'доставка';if(LANG=='ru')echo'доставка';if(LANG=='en')echo'delivery';?></div>
                                </li>
							<?php endif; ?>
							<?php if ($_product_delivery !== ''): ?>
                                <li>
                                    <div class="link-wrapper fm"><? if(LANG=='ua')echo'оплата';if(LANG=='ru')echo'оплата';if(LANG=='en')echo'payment';?></div>
                                </li>
							<?php endif; ?>
                            <li>
                                <div class="link-wrapper link-wrapper--reviews fm"><? if(LANG=='ua')echo'відгуки';if(LANG=='ru')echo'отзывы';if(LANG=='en')echo'Reviews';?><?php if (count($comments) > 0): ?><b><?=count($comments);?></b><?php endif; ?></div>
                            </li>
						</ul>
					</div>

					<div class="details-tabs_content fm">
						<div class="details-tab fm">
							<div class="product-table product-table--invert fm">
								<table class="catalog_table">
									<thead>
									<tr>
										<td><span><? if(LANG=='ua')echo'Артикул';if(LANG=='ru')echo'Артикул';if(LANG=='en')echo'Article';?></span></td>
										<td><span><? if(LANG=='ua')echo'Назва';if(LANG=='ru')echo'Название';if(LANG=='en')echo'Name';?></span></td>
										<td><span><? if(LANG=='ua')echo'Упаковка, шт';if(LANG=='ru')echo'Упаковка, шт';if(LANG=='en')echo'Pack, pcs';?></span></td>
										<?php if ($this->config->item('price_type')): ?>
											<?php if (LANG === 'ua'): ?><td><span>Ціна базова, грн</span></td><?php endif; ?>
											<?php if (LANG === 'ru'): ?><td><span>Цена базовая, гр</span></td><?php endif; ?>
											<?php if (LANG === 'en'): ?><td><span>Basic price, UAH</span></td><?php endif; ?>
										<?php endif; ?>
										<td><span><? if(LANG=='ua')echo'Ціна';if(LANG=='ru')echo'Цена';if(LANG=='en')echo'Price';?></span></td>
										<td><span><? if(LANG=='ua')echo'Ціна';if(LANG=='ru')echo'Цена';if(LANG=='en')echo'Price';?></span><b>-3%</b></td>
										<td><span><? if(LANG=='ua')echo'Ціна';if(LANG=='ru')echo'Цена';if(LANG=='en')echo'Price';?></span><b>-5%</b></td>
										<td><span><? if(LANG=='ua')echo'Кількість';if(LANG=='ru')echo'Количество';if(LANG=='en')echo'Quantity';?></span></td>
										<td></td>
									</tr>
									</thead>
									<tbody>
										<tr data-product-id="<?=$variant['product_id'];?>" data-variant-id="<?=$variant_id;?>" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
											<td class="product-table_cpu"><span><?=$variant['articul'];?></span></td>
											<td class="product-table_name"><?=$variant['title'];?></td>
											<td><span><?=$variant['box'];?></span></td>
											<?php if ($this->config->item('price_type')): ?>
												<td>
													<span class="fwb"><?=$this->init_model->get_price($variant['price'], $variant['currency'], 0, 'p0');?></span>
													<span class="amount">(за 100 шт.)</span>
												</td>
											<?php endif ?>
											<td>
												<?php $price = $this->init_model->get_price($variant['price'], $variant['currency']); ?>
												<span class="fwb" itemprop="price" content="<?=$price;?>"><?=$price;?></span>
												<span class="amount">(за 100 шт.)</span>
												<span itemprop="priceCurrency" content="UAH" class="hidden"></span>
											</td>
											<td>
												<?php if ($this->init_model->check_percents() and $variant['price'] > 0): ?>
													<span class="fwb"><?=$this->init_model->get_price($variant['price'], $variant['currency'], 3);?></span>
													<span class="amount">(від <?=$this->init_model->get_limit_items($variant['box'], 3);?> шт.)</span>
												<?php else: ?>
													<span class="fwb">-</span>
												<?php endif; ?>
											</td>
											<td>
												<?php if ($this->init_model->check_percents() and $variant['price'] > 0): ?>
													<span class="fwb"><?=$this->init_model->get_price($variant['price'], $variant['currency'], 5);?></span>
													<span class="amount">(від <?=$this->init_model->get_limit_items($variant['box'], 5);?> шт.)</span>
												<?php else: ?>
													<span class="fwb">-</span>
												<?php endif; ?>
											</td>
											<td>
												<?php if ($variant['price'] > 0): ?>
                                                    <a href="#" class="product-table_minus">-</a>
                                                    <input type="text" class="cart_total" data-box="<?=$variant['box']?>">
                                                    <a href="#" class="product-table_plus">+</a>
                                                <?php endif; ?>
                                            </td>
											<td>
												<?php if ($variant['price'] > 0): ?>
                                                    <a href="#" class="product-table_buy to_cart">
                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                    </a>
												<?php else: ?>
                                                    <a href="#" class="product-table_buy price_form_open">
                                                        <i class="fa fa-question" aria-hidden="true"></i>
                                                        <span><?php if (LANG == 'ua') {echo 'Уточнити ціну';} if (LANG == 'ru') {echo 'Уточнить цену';} if (LANG == 'en') {echo 'Refine price';} ?></span>
                                                    </a>
												<?php endif; ?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="details-tab fm">
							<div class="product-options fm">
								<?php foreach ($filters as $v): ?>
									<div class="product-options_row">
										<div class="product-options_cell"><?=$v['filter_name'];?></div>
										<div class="product-options_cell"><?=implode(', ', $v['childs']);?></div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
                        <div class="details-tab fm"></div>
						<?php if ($_product_delivery !== ''): ?><div class="details-tab fm"><?=$_product_delivery;?></div><?php endif; ?>
						<?php if ($_product_payment !== ''): ?><div class="details-tab fm"><?=$_product_payment;?></div><?php endif; ?>
                        <div id="product_reviews" class="details-tab fm">
                            <div class="sect-title"><span><? if(LANG=='ua')echo'Відгуки';if(LANG=='ru')echo'Отзывы';if(LANG=='en')echo'Reviews';?></span></div>
                            <div id="comments_list" class="fm dc_all">
								<?php if (isset($comments[0])): ?><?php foreach ($comments as $comment): ?>
                                    <div class="fm dc_one">
                                        <div class="fm dc_title"><span><?=$comment['name'];?></span><div class="raty" data-number="5" data-score="<?=$comment['stars'];?>"></div><span><?=date('d.m.Y', $comment['date']);?></span></div>
                                        <div class="fm dc_txt"><?=$comment['comment'];?></div>
                                    </div>
								<?php endforeach; ?><?php endif; ?>
                            </div>
                            <div class="fm dc_write">
                                <div class="dc_write__field">
                                    <input id="comment_name" type="text" value="" placeholder="<? if(LANG=='ua')echo'Ім’я';if(LANG=='ru')echo'Имя';if(LANG=='en')echo'Name';?>">
                                    <input id="comment_email" type="text" value="" placeholder="E-mail">
                                </div>
                                <div class="dc_write__field">
                                    <textarea id="comment_text" placeholder="<? if(LANG=='ua')echo'Повідомлення';if(LANG=='ru')echo'Сообщение';if(LANG=='en')echo'Message';?>"></textarea>
                                    <a id="comment_send" href="#" class="fm dc_link" data-product-id="<?=$product_id;?>"><? if(LANG=='ua')echo'Додати відгук';if(LANG=='ru')echo'Добавить отзыв';if(LANG=='en')echo'Add a review';?></a>
                                    <div class="raty_active" data-number="5" data-score="0"></div>
                                    <input type="hidden" id="comment_stars" value="0">
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php if (isset($similar_products[0])): ?>
<section class="you_saw">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="sect-title">
					<span><? if(LANG=='ua')echo'Також Вас можуть зацікавити такі товари';if(LANG=='ru')echo'Также Вас могут заинтересовать такие товары';if(LANG=='en')echo'You may also be interested in these items';?>:</span>
				</div>
			</div>
			<?php foreach ($similar_products as $v): ?>
				<div class="col-md-3">
					<div class="fm one-good"><?php $url = $this->uri->full_url($v['url']); ?>
						<div class="one-good_wrapper fm">
							<?php if ($v['status'] == 1): ?><div class="one-good_status promo"><b></b><span><? if(LANG=='ua')echo'Акція';if(LANG=='ru')echo'Акция';if(LANG=='en')echo'Share';?></span><i></i></div><?php endif; ?>
							<?php if ($v['status'] == 3): ?><div class="one-good_status discount"><b></b><span><? if(LANG=='ua')echo'Знижка';if(LANG=='ru')echo'Скидка';if(LANG=='en')echo'Discount';?></span><i></i></div><?php endif; ?>
							<?php if ($v['status'] == 4): ?><div class="one-good_status hit"><b></b><span><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?></span><i></i></div><?php endif; ?>
							<div class="fm one-good_img">
								<div class="cell">
									<a href="<?=$url;?>">
										<?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/t_' . $v['image']);?>" alt="<?=$v['title'];?>"><?php endif; ?>
									</a>
								</div>
							</div>
							<div class="fm one-good_head">
								<a href="<?=$url;?>" class="one-good_name"><?=$v['title'];?></a>
								<!--div class="one-good_icons">
									<a href="#" class="one-good_compare">
										<i class="fa fa-balance-scale" aria-hidden="true"></i>
									</a>
									<a href="#" class="one-good_favorite">
										<i class="fa fa-heart-o" aria-hidden="true"></i>
									</a>
								</div-->
							</div>
							<div class="one-good_options fm">
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Область застосування:</div>
									<div class="one-good_options_cell">для машинобудування</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
							</div>
							<div class="box-l fm">
								<div class="fm one-good_icons">
									<a href="#" class="one-good_compare"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>
									<a href="#" class="one-good_favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
								</div>
								<a href="<?=$url;?>" class="fmr one-good_btn"><?php if (LANG === 'ua'): ?>купити<?php endif; ?><?php if (LANG === 'ru'): ?>купить<?php endif; ?><?php if (LANG === 'en'): ?>buy<?php endif; ?></a>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if (isset($visited_products) AND count($visited_products) > 0): ?>
<section class="you_saw">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="sect-title">
					<span><? if(LANG=='ua')echo'Щойно Ви переглядали';if(LANG=='ru')echo'Только что Вы просматривали';if(LANG=='en')echo'Your reviewed products';?></span>
				</div>
			</div>
			<?php foreach ($visited_products as $v): ?>
				<div class="col-md-3">
					<div class="fm one-good"><?php $url = $this->uri->full_url($v['url']); ?>
						<div class="one-good_wrapper fm">
							<?php if ($v['status'] == 1): ?><div class="one-good_status promo"><b></b><span><? if(LANG=='ua')echo'Акція';if(LANG=='ru')echo'Акция';if(LANG=='en')echo'Share';?></span><i></i></div><?php endif; ?>
							<?php if ($v['status'] == 3): ?><div class="one-good_status discount"><b></b><span><? if(LANG=='ua')echo'Знижка';if(LANG=='ru')echo'Скидка';if(LANG=='en')echo'Discount';?></span><i></i></div><?php endif; ?>
							<?php if ($v['status'] == 4): ?><div class="one-good_status hit"><b></b><span><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?></span><i></i></div><?php endif; ?>
							<div class="fm one-good_img">
								<div class="cell">
									<a href="<?=$url;?>">
										<?php if ($v['image'] != ''): ?><img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/t_' . $v['image']);?>" alt="<?=$v['title'];?>"><?php endif; ?>
									</a>
								</div>
							</div>
							<div class="fm one-good_head">
								<a href="<?=$url;?>" class="one-good_name"><?=$v['title'];?></a>
								<!--div class="one-good_icons">
									<a href="#" class="one-good_compare">
										<i class="fa fa-balance-scale" aria-hidden="true"></i>
									</a>
									<a href="#" class="one-good_favorite">
										<i class="fa fa-heart-o" aria-hidden="true"></i>
									</a>
								</div-->
							</div>
							<div class="one-good_options fm">
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Область застосування:</div>
									<div class="one-good_options_cell">для машинобудування</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
								<div class="one-good_options_row">
									<div class="one-good_options_cell">Стандарт:</div>
									<div class="one-good_options_cell">DIN 933</div>
								</div>
							</div>
							<div class="box-l fm">
								<div class="fm one-good_icons">
									<a href="#" class="one-good_compare"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>
									<a href="#" class="one-good_favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
								</div>
								<a href="<?=$url;?>" class="fmr one-good_btn"><? if(LANG=='ua')echo'купити';if(LANG=='ru')echo'купить';if(LANG=='en')echo'buy';?></a>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<?php endif; ?>

<script type="text/javascript">
	$(function () {
		$('#details_photo').photo_gallery({
			group: 'tabs',
			product_id: <?=$product_id;?>,
			price: 0
		});

		$('.dt_photos').photo_gallery({
			group: 'photos',
			product_id: <?=$product_id;?>,
			price: 0
		});
	});

	$('.product-details_photos').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
	});

	function changeProductTabs(){
		if($(window).outerWidth(true) <= 767){
			$('.details-tabs').addClass('mobile').removeClass('desktop');

			//popular products accordeon
			for(var i = 0; i < $('.details-tab').length; i++){

				var $li = $('.details-tabs_links > ul > li').eq(i),
					$tab = $('.details-tab').eq(i);

				$tab.appendTo($li);
			}

			$('.details-tabs_links .link-wrapper').next().hide();
			$('.details-tabs_links .link-wrapper').removeClass('active');
			$('.details-tabs_links li:first').children('.details-tab').show().prev('.link-wrapper').addClass('active');

		}else if($(window).outerWidth(true) >= 767){

			$('.details-tabs').removeClass('mobile').addClass('desktop');
			var $all_li = $('.details-tabs').find('.details-tabs_links').children('ul').children('li'),
				$tabWrapper = $('.details-tabs_content');

			$all_li.each(function(index){
				var $tab = $(this).find('.details-tab');

				$tab.appendTo($tabWrapper);
			});

			$('.details-tab').hide().first().show();
			$('.details-tabs_links .link-wrapper').removeClass('active');
			$('.details-tabs_links li').eq(0).children('.link-wrapper').addClass('active');
		}
	};

	changeProductTabs()
	$(window).resize(function(){
		changeProductTabs()
	});

	//popular products accordeon
	$('body').on('click', '.details-tabs.mobile .link-wrapper', function(){
		if(!$(this).hasClass('active')){
			$('.link-wrapper').removeClass('active').next().slideUp(400);
			$(this).addClass('active').next().slideDown(400);
		} else {
			$(this).removeClass('active').next().slideUp(400);
		}
	});

	//popular products tabs
	$('body').on('click', '.details-tabs.desktop .link-wrapper', function(){
		var $currentLi = $(this).parent('li');
		$('.details-tabs_links .link-wrapper').removeClass('active').parent('li').eq($currentLi.index()).children('.link-wrapper').addClass('active');
		$('.details-tab').hide().eq($currentLi.index()).fadeIn(400);
		console.log($currentLi.index());
	});
</script>