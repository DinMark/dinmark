<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Catalog
	 *
	 * @property Admin_catalog_model $admin_catalog_model
	 * @property Catalog_model $catalog_model
	 * @property Catalog_filters_model $catalog_filters_model
	 * @property Cart_model $cart_model
	 */
	class Catalog extends MX_Controller
	{
		private $sort = '';
		private $page = 1;
		private $per_page = 33;
		private $admin_per_page = 20;
		private $template = 'tile';

		/**
		 * Вивід списка(клієнтського або адміністратора) товарів
		 *
		 * @param int $menu_id
		 * @param int $component_id
		 * @param int $hidden
		 * @param string $config
		 * @return string
		 */
		public function index($menu_id, $component_id, $hidden, $config = '')
		{
			$this->load->model('catalog_model');
			$this->load->helper('functions');

			$is_parent = $this->catalog_model->is_parent($menu_id);
			$is_main = $this->init_model->is_main();

			$template_data = array(
				'menu_id' => $menu_id,
				'component_id' => $component_id,
				'is_parent' => $is_parent,
				'h1' => $this->template_lib->get_h1(),
				'is_main' => $is_main,
			);

			$this->template_lib->set_h1();

			if ($this->init_model->is_admin())
			{
				$template_data['hidden'] = $hidden;

				if ($is_parent)
				{
					$template_data['categories'] = $this->catalog_model->get_categories($menu_id);
					return $this->load->view('admin/categories_tpl', $template_data, TRUE);
				}
				else
				{
					$this->load->model('admin_catalog_model');
					$this->load->helper('admin_catalog');

					//$this->catalog_options();
					$sort = $this->input->cookie('a_catalog_sort');

					$per_page = $this->input->cookie('a_catalog_per_page');
					if ($per_page !== NULL) $per_page = intval($per_page);
					if (in_array($per_page, array(0, 20, 50, 100), TRUE)) $this->admin_per_page = $per_page;

					$this->page = intval($this->input->get('page'));
					if ($this->page < 1) $this->page = 1;

					$template_data['products'] = $this->admin_catalog_model->get_products($component_id, $this->page, $this->admin_per_page, $sort);
					$template_data['last_product'] = $this->session->userdata('last_product');
					$template_data['sort'] = $sort;
					$template_data['per_page'] = $this->admin_per_page;

					$total_products = $this->admin_catalog_model->count_products($component_id);
					$template_data['last_page'] = ceil($total_products / $this->admin_per_page);
					$template_data['page'] = $this->page;

					$base_url = $this->init_model->get_link($menu_id, '{URL}');
					$template_data['base_url'] = $base_url;

					if ($this->admin_per_page > 0)
					{
						$template_data['before'] = $this->admin_per_page * ($this->page - 1);
						$template_data['after'] = $total_products - (($this->admin_per_page * ($this->page - 1)) + $this->admin_per_page);

						$pagination_config = array(
							'cur_page' => $this->page,
							'padding' => 1,
							'first_url' => $base_url,
							'base_url' => $base_url . '?page=',
							'per_page' => $this->admin_per_page,
							'total_rows' => $total_products,
							'full_tag_open' => '<nav class="fm admin_paginator"><table align="center"><tr><td align="center"><div class="fm paginator">',
							'full_tag_close' => '</div></td></tr></table></nav>',
							'first_link' => FALSE,
							'last_link' => FALSE,
							'prev_link' => '&lt;',
							'next_link' => '&gt;'
						);

						$this->load->library('pagination', $pagination_config);
						$template_data['paginator'] = $this->pagination->create_links();
					}
					else
					{
						$template_data['paginator'] = '';
						$template_data['before'] = 0;
						$template_data['after'] = 0;
					}

					$template_data['components'] = $this->admin_catalog_model->get_components_menu();

					return $this->load->view('admin/catalog_tpl', $template_data, TRUE);
				}
			}
			else
			{
				$template_data = array(
					'menu_id' => $menu_id,
					'component_id' => $component_id
				);

				if ($is_parent)
				{
					$template_data['catalog_menu'] = $this->catalog_model->get_categories($menu_id);
					return $this->load->view('categories_tpl', $template_data, TRUE);
				}
				else
				{
					$this->catalog_options();

					$total_products = $this->catalog_model->count_products($component_id);

					$base_url = $this->init_model->get_link($menu_id, '{URL}');
					$template_data['base_url'] = $base_url;

					if ($total_products > 0)
					{
						$pagination_config = array(
							'cur_page' => $this->page,
							'padding' => 1,
							'first_url' => $base_url,
							'base_url' => $base_url . '?page=',
							'per_page' => $this->per_page,
							'total_rows' => $total_products,
							'full_tag_open' => '<nav class="fm paginator"><table align="center"><tr><td align="center"><div class="fm paginator">',
							'full_tag_close' => '</div></td></tr></table></nav>',
							'first_link' => FALSE,
							'last_link' => FALSE,
							'prev_link' => '',
							'next_link' => ''
						);

						$this->load->library('pagination', $pagination_config);

						$template_data['sort'] = $this->sort;
						$template_data['template'] = $this->template;
						$template_data['per_page'] = $this->per_page;
						$template_data['total_products'] = $total_products;
						$template_data['price_limits'] = $this->catalog_model->get_price_limits($component_id);
						$template_data['pagination'] = $this->pagination->create_links();
						$template_data['current_name'] = $this->init_model->get_link($menu_id, '{NAME}');

						$this->load->model('catalog_filters/catalog_filters_model');
						$template_data['filters'] = $this->catalog_filters_model->get_filters($component_id);

						$_template_data = array(
							'products' => $this->catalog_model->get_products($component_id, $this->page, $this->per_page, $this->sort),
						);
						$template_data['products'] = $this->load->view('catalog_' . $this->template . '_tpl', $_template_data, TRUE);
					}

					return $this->load->view('catalog_tpl', $template_data, TRUE);
				}
			}
		}

		/**
		 * Вивід списка(клієнтського або адміністратора) товарів підкатегорій
		 *
		 * @param int $menu_id
		 * @param int $component_id
		 * @param int $hidden
		 * @param string $config
		 * @return string
		 */
		public function category($menu_id, $component_id, $hidden, $config = '')
		{
			$this->load->model('catalog_model');
			$this->load->helper('functions');

			$is_parent = $this->catalog_model->is_parent($menu_id);
			$is_main = $this->init_model->is_main();

			$template_data = array(
				'menu_id' => $menu_id,
				'component_id' => $component_id,
				'is_parent' => $is_parent,
				'h1' => $this->template_lib->get_h1(),
				'is_main' => $is_main,
			);

			$this->template_lib->set_h1();

			if ($this->init_model->is_admin())
			{
				$this->template_vars['hidden'] = $hidden;

				$this->template_vars['categories'] = $this->catalog_model->get_categories($menu_id);
				return $this->load->view('admin/category_tpl', $this->template_vars, TRUE);
			}
			else
			{
				$components = $this->catalog_model->get_components($menu_id);

				if (count($components) > 0)
				{
					$template_data['components'] = $components;

					$this->catalog_options();

					$total_products = $this->catalog_model->count_products($components);

					$base_url = $this->init_model->get_link($menu_id, '{URL}');
					$template_data['base_url'] = $base_url;

					if ($total_products > 0)
					{
						$pagination_config = array(
							'cur_page' => $this->page,
							'padding' => 1,
							'first_url' => $base_url,
							'base_url' => $base_url . '?page=',
							'per_page' => $this->per_page,
							'total_rows' => $total_products,
							'full_tag_open' => '<nav class="fm paginator"><table align="center"><tr><td align="center"><div class="fm paginator">',
							'full_tag_close' => '</div></td></tr></table></nav>',
							'first_link' => FALSE,
							'last_link' => FALSE,
							'prev_link' => '',
							'next_link' => ''
						);

						$this->load->library('pagination', $pagination_config);

						$template_data['sort'] = $this->sort;
						$template_data['template'] = $this->template;
						$template_data['per_page'] = $this->per_page;
						$template_data['total_products'] = $total_products;
						$template_data['price_limits'] = $this->catalog_model->get_price_limits($components);
						$template_data['pagination'] = $this->pagination->create_links();
						$template_data['current_name'] = $this->init_model->get_link($menu_id, '{NAME}');

						$this->load->model('catalog_filters/catalog_filters_model');
						$template_data['filters'] = $this->catalog_filters_model->get_filters($components);

						$_template_data = array(
							'products' => $this->catalog_model->get_products($components, $this->page, $this->per_page, $this->sort),
						);
						$template_data['products'] = $this->load->view('catalog_' . $this->template . '_tpl', $_template_data, TRUE);
					}

					return $this->load->view('catalog_tpl', $template_data, TRUE);
				}
			}

			return '';
		}

		/**
		 * Пошук товарів по фільтрах
		 *
		 * @return string
		 */
		public function filter()
		{
			$response = array(
				'success' => TRUE,
				'total' => 0,
				'products' => '',
				'filters' => array(),
			);

			$this->catalog_options();

			$component_id = intval($this->input->post('component_id'));
			$components = strval($this->input->post('components'));
			$get_total = intval($this->input->post('get_total'));
			$get_limits = intval($this->input->post('get_limits'));
			$check_filters = intval($this->input->post('check_filters'));
			$this->page = intval($this->input->post('page'));

			// Побудова масиву параметрів фільтрації
			$filters = array();

			$min_price = floatval($this->input->post('min_price'));
			if ($min_price > 0) $filters['min_price'] = $min_price;

			$max_price = floatval($this->input->post('max_price'));
			if ($max_price > 0) $filters['max_price'] = $max_price;

			$_filters = $this->input->post('filters');
			if (!is_array($_filters)) $_filters = array();

			foreach ($_filters as $k => $v)
			{
				if (is_array($v) AND count($v) > 0)
				{
					$k = intval($k);
					foreach ($v as $_v) $filters['filters'][$k][] = intval($_v);
				}
			}

			if ($components !== '') {
				$components = explode(',', $components);

				foreach ($components as $k => &$v) {
					$v = (int)$v;

					if ($v < 1) {
						unset($components[$k]);
					}
				}
			} else {
				$components = array();
			}

			$this->load->model('catalog_model');

			if ($get_limits == 1)
			{
				$response['price_limits'] = $this->catalog_model->get_price_limits(count($components) > 0 ? $components : $component_id, $filters);

				$filters['min_price'] = $response['price_limits']['min'];
				$filters['max_price'] = $response['price_limits']['max'];
			}

			$template_data = array();
			$template_data['products'] = $this->catalog_model->get_products(count($components) > 0 ? $components : $component_id, $this->page, $this->per_page, $this->sort, $filters);


			if (isset($template_data['products'][0]))
			{
				$response['products'] = $this->load->view('catalog_' . $this->template . '_tpl', $template_data, TRUE);
			}

			if ($get_total == 1)
			{
				$response['total'] = $this->catalog_model->count_products(count($components) > 0 ? $components : $component_id, $filters);
			}

			if ($check_filters == 1)
			{
				$response['filters'] = array();

				$component_filters = $this->input->post('component_filters');

				if (is_array($component_filters) AND count($component_filters) > 0)
				{
					//$component_filters = array_map('intval', $component_filters);

					$this->load->model('catalog_filters/catalog_filters_model');
					$response['filters'] = $this->catalog_filters_model->check_filters(count($components) > 0 ? $components : $component_id, $component_filters, $filters);
				}
			}

			return json_encode($response);
		}

		/**
		 * Вивід детальної інформації
		 *
		 * @param int $product_id
		 */
		public function details($product_id = 0)
		{
			$product_id = intval($product_id);
			if ($product_id == 0) show_404();

			$this->load->model('catalog_model');

			$product = $this->catalog_model->get($product_id);
			if (count($product) == 0) show_404();

			if ($this->init_model->is_admin()) redirect($this->uri->full_url('admin/catalog/edit?menu_id=' . $product['menu_id'] . '&product_id=' . $product_id));

			$language_links = $this->catalog_model->get_language_links($product_id, $this->config->item('database_languages'));
			$this->template_lib->set_template_var('language_links', $language_links);

			if (LANG === 'ua') {
				$this->template_lib->set_title('Купити ' . $product['title'] . ' | Dinmark');
				$this->template_lib->set_description('Купити ' . $product['title'] . ' в інтернет магазині Dinmark з доставкою по Україні ☏ +380960110103');
			}

            if (LANG === 'ru') {
                $this->template_lib->set_title('Купить ' . $product['title'] . ' | Dinmark');
                $this->template_lib->set_description('Купить ' . $product['title'] . ' в интернет магазине Dinmark с доставкой по Украине ☏ +380960110103');
            }

			$this->template_lib->set_bread_crumbs('', '');
			//$this->template_lib->set_bread_crumbs('', $product['title']);

			$dir_code = $this->init_model->dir_by_id($product_id);

//			echo '<pre>';
//			print_r($product);
//			echo '</pre>';
//			exit;

			$template_vars = array(
				'base_url' => $this->init_model->get_link($product['menu_id'], '{URL}'),
				'dir_code' => $dir_code,
				'product_id' => $product_id,
				'product' => $product,
				'variants' => $this->catalog_model->get_variants($product_id),
				'options' => $this->catalog_model->get_options($product_id),
				'filters' => $this->catalog_model->get_filters($product_id),
				'comments' => $this->catalog_model->get_comments($product_id),
				'delivery' => $this->catalog_model->get_delivery(),
				'product_delivery' => $this->catalog_model->get_product_delivery($product_id),
				'similar_products' => $this->catalog_model->get_similar_products($product['component_id'], $product_id),
				'selected_products' => $this->catalog_model->get_seleted_products($product_id),
				'visited_products' => $this->catalog_model->get_visited_products($product_id),
			);

			$this->load->model('catalog_help/catalog_help_model');
			$template_vars['call_label'] = $this->catalog_help_model->get_help('call_form');

			$this->catalog_model->set_visited($product_id);

			$this->load->helper('functions');
			$this->template_lib->set_content($this->load->view('details_tpl', $template_vars, TRUE));

			//$this->load->model('cart/cart_model');
			//print_r($this->cart_model->get_cart());
			//exit;

			//$modal = $this->load->view('details_modal_tpl', array(), TRUE);
			//$this->template_lib->set_template_var('catalog_modal', $modal);
		}

		/**
		 * Вивід детальної інформації варіанту
		 *
		 * @param int $product_id
		 */
		public function variant($variant_id = 0)
		{
			$variant_id = intval($variant_id);
			if ($variant_id == 0) show_404();

			$this->load->model('catalog_model');

			$variant = $this->catalog_model->get_variant($variant_id);
			if (count($variant) == 0) {
				show_404();
			}

			$product = $this->catalog_model->get_simple($variant['product_id']);
			if (count($product) == 0) {
				show_404();
			}

			if ($this->init_model->is_admin()) {
				redirect($this->uri->full_url('admin/catalog/edit?menu_id=' . $product['menu_id'] . '&product_id=' . $variant['product_id']));
			}

			$language_links = $this->catalog_model->get_variant_language_links($variant_id, $this->config->item('database_languages'));
			$this->template_lib->set_template_var('language_links', $language_links);


            if (LANG === 'ua') {
                $this->template_lib->set_title('Купити ' . $variant['title'] . ' | Dinmark');
                $this->template_lib->set_description('Купити ' . $product['title'] . ' в інтернет магазині Dinmark з доставкою по Україні ☏ +380960110103');
            }

            if (LANG === 'ru') {
                $this->template_lib->set_title('Купить ' . $variant['title'] . ' | Dinmark');
                $this->template_lib->set_description('Купить ' . $variant['title'] . ' в интернет магазине Dinmark с доставкой по Украине ☏ +380960110103');
            }

			$this->template_lib->set_bread_crumbs($this->uri->full_url($product['url']), $product['title']);
			$this->template_lib->set_bread_crumbs('', $variant['title']);

			$dir_code = $this->init_model->dir_by_id($variant['product_id']);

			$template_vars = array(
				'base_url' => $this->init_model->get_link($product['menu_id'], '{URL}'),
				'dir_code' => $dir_code,
				'product_id' => $variant['product_id'],
				'product' => $product,
				'variant_id' => $variant_id,
				'variant' => $variant,
				'filters' => $this->catalog_model->get_variant_filters($variant_id),
				'comments' => $this->catalog_model->get_comments($variant['product_id']),
				'delivery' => $this->catalog_model->get_delivery(),
				'product_delivery' => $this->catalog_model->get_product_delivery($variant['product_id']),
				'similar_products' => $this->catalog_model->get_similar_products($product['component_id'], $variant['product_id']),
				'visited_products' => $this->catalog_model->get_visited_products($variant['product_id']),
			);

			$this->load->model('catalog_help/catalog_help_model');
			$template_vars['call_label'] = $this->catalog_help_model->get_help('call_form');

			$this->catalog_model->set_visited($variant['product_id']);

			$this->load->helper('functions');
			$this->template_lib->set_content($this->load->view('variant_tpl', $template_vars, TRUE));
		}

		public function variants()
		{
			$response = array(
				'success' => FALSE,
			);

			$product_id = intval($this->input->post('product_id'));

			$variants = $this->input->post('variants');
			$variants = explode(',', $variants);
			$variants = array_map('intval', $variants);

			if ($product_id > 0 AND count($variants) > 0)
			{
				$this->load->model('catalog_model');
				$this->load->model('cart/cart_model');

				$template_data = array(
					'product_id' => $product_id,
					'product' => $this->catalog_model->get_simple($product_id),
					'variants' => $this->catalog_model->get_variants($product_id, $variants),
					'cart' => $this->cart_model->get_cart(),
				);

				$response['variants'] = $this->load->view('details_modal_inner_tpl', $template_data, TRUE);
				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Встановлення параметрів каталогу
		 * (сортування, кількість на сторінці, поточна сторінка)
		 */
		private function catalog_options()
		{
			$page = $this->input->get('page');
			if (is_numeric($page)) $this->page = abs($page);

			//$per_page = intval($this->input->cookie('catalog_per_page'));
			//if (is_numeric($per_page) AND $per_page > 0) $this->per_page = abs($per_page);

			$sort = $this->input->cookie($this->config->item('cookie_prefix') . 'catalog_sort');
			if (in_array($sort, array('', 'price_asc', 'price_desc', 'title_asc', 'title_desc'))) $this->sort = $sort;

			$template = $this->input->cookie($this->config->item('cookie_prefix') . 'catalog_template');
			if (in_array($template, array('tile', 'list'))) $this->template = $template;
		}

		/**
		 * Вивід компоненту "Наші бренди"
		 *
		 * @param int $menu_id
		 * @param int $component_id
		 * @param int $hidden
		 * @param string $config
		 * @return string
		 */
		public function brands_menu($menu_id, $component_id, $hidden, $config = '')
		{
			$this->load->model('catalog_model');

			$template_data = array(
				'menu_id' => $menu_id,
				'component_id' => $component_id,
			);

			if ($this->init_model->is_admin())
			{
				$template_data['hidden'] = $hidden;

				return $this->load->view('admin/brands_menu_tpl', $template_data, TRUE);
			}
			else
			{
				$this->load->model('catalog_model');

				$template_data = array(
					'menu_id' => $menu_id,
					'brands_menu' => $this->catalog_model->get_brands_menu(),
				);

				return $this->load->view('brands_menu_tpl', $template_data, TRUE);
			}
		}

		/**
		 * Вивід компоненту "Розділи каталогу"
		 *
		 * @param int $menu_id
		 * @param int $component_id
		 * @param int $hidden
		 * @param string $config
		 * @return string
		 */
		public function catalog_menu($menu_id, $component_id, $hidden, $config = '')
		{
			$this->load->model('catalog_model');

			$template_data = array(
				'menu_id' => $menu_id,
				'component_id' => $component_id,
			);

			if ($this->init_model->is_admin())
			{
				$template_data['hidden'] = $hidden;

				return $this->load->view('admin/catalog_menu_tpl', $template_data, TRUE);
			}
			else
			{
				$this->load->model('catalog_model');

				$template_data = array(
					'menu_id' => $menu_id,
					'catalog_menu' => $this->catalog_model->get_catalog_menu(),
				);

				return $this->load->view('catalog_menu_tpl', $template_data, TRUE);
			}
		}

		/**
		 * Вивід компоненту "Останні переглянуті товари"
		 *
		 * @param int $menu_id
		 * @param int $component_id
		 * @param int $hidden
		 * @param string $config
		 * @return string
		 */
		public function visited($menu_id, $component_id, $hidden, $config = '')
		{
			$this->load->model('catalog_model');

			$template_data = array(
				'menu_id' => $menu_id,
				'component_id' => $component_id,
			);

			if ($this->init_model->is_admin())
			{
				$template_data['hidden'] = $hidden;

				return $this->load->view('admin/visited_tpl', $template_data, TRUE);
			}
			else
			{
				$this->load->model('catalog_model');
				$this->load->model('cart/cart_model');

				$template_data = array(
					'menu_id' => $menu_id,
					'products' => $this->catalog_model->get_visited_products(),
					'cart' => $this->cart_model->get_cart(),
				);

				return $this->load->view('visited_tpl', $template_data, TRUE);
			}
		}

		/**
		 * Вивід компоненту "Топ товари"
		 *
		 * @param int $menu_id
		 * @param int $component_id
		 * @param int $hidden
		 * @param string $config
		 * @return string
		 */
		public function top($menu_id, $component_id, $hidden, $config = '')
		{
			$this->load->model('catalog_model');

			$template_data = array(
				'menu_id' => $menu_id,
				'component_id' => $component_id,
			);

			if ($this->init_model->is_admin())
			{
				$template_data['hidden'] = $hidden;

				return $this->load->view('admin/top_tpl', $template_data, TRUE);
			}
			else
			{
				$this->load->model('catalog_model');
				$this->load->model('cart/cart_model');

				$template_data = array(
					'menu_id' => $menu_id,
					'products' => $this->catalog_model->get_top_products(),
					'cart' => $this->cart_model->get_cart(),
				);

				return $this->load->view('top_tpl', $template_data, TRUE);
			}
		}

		/**
		 * Додавання відгуку до товару
		 *
		 * @return string
		 */
		public function send_comment()
		{
			$response = array('success' => FALSE);

			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$comment = $this->input->post('comment');
			$stars = abs(intval($this->input->post('stars')));
			$product_id = intval($this->input->post('product_id'));

			if ($product_id > 0 AND $name != '' AND $email != '' AND $comment != '' AND in_array($stars, array(0, 1, 2, 3, 4, 5)))
			{
				$this->load->helper('form');

				$set = array(
					'product_id' => $product_id,
					'name' => form_prep($name),
					'email' => form_prep($email),
					'stars' => $stars,
					'comment' => form_prep($comment),
					'date' => time(),
				);
				$this->db->insert('catalog_comments', $set);

				$response['success'] = TRUE;
				$response['date'] = date('d.m.Y');
				$response['comment_id'] = $this->db->insert_id();
			}

			return json_encode($response);
		}
	}
