<?php defined('ROOT_PATH') or exit('No direct script access allowed');

	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('memory_limit','512M');
	set_time_limit(0);

	/**
	 * Class Admin_catalog
	 *
	 * @property Admin_catalog_model $admin_catalog_model
	 * @property Admin_catalog_options_model $admin_catalog_options_model
	 * @property Admin_catalog_filters_model $admin_catalog_filters_model
	 */
	class Admin_catalog extends MX_Controller
	{
		private $image_source = array(1000, 1000);
		private $image_big = array(470, 303);
		private $image_thumb = array(248, 160);
		private $excel;

		/**
		 * Пошук товарів
		 *
		 * @return string json response
		 */
		public function search()
		{
			$response = array('success' => FALSE);

			$this->load->helper('form');

			$component_id = intval($this->input->post('component_id'));
			$menu_id = intval($this->input->post('menu_id'));
			$query = form_prep($this->input->post('query'));

			$ignore = $this->input->post('ignore');
			$ignore = is_array($ignore) ? array_map('intval', $ignore) : array();

			if ($this->init_model->is_admin() AND $query != '')
			{
				$this->load->model('admin_catalog_model');

				$response = array(
					'success' => TRUE,
					'products' => $this->admin_catalog_model->get_products($component_id, NULL, NULL, NULL, $query, $ignore),
				);
			}

			return json_encode($response);
		}

		/**
		 * Додавання супутного товару
		 *
		 * @return string
		 */
		public function set_similar()
		{
			$response = array('success' => FALSE);

			$this->load->helper('form');

			$menu_id = intval($this->input->post('menu_id'));
			$product_id = intval($this->input->post('product_id'));
			$similar_id = intval($this->input->post('similar_id'));

			if ($this->init_model->is_admin() AND $product_id > 0 AND $similar_id > 0)
			{
				$this->load->model('admin_catalog_model');
				$this->admin_catalog_model->set_similar($product_id, $similar_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Видалення супутного товару
		 *
		 * @return string
		 */
		public function delete_similar()
		{
			$response = array('success' => FALSE);

			$this->load->helper('form');

			$menu_id = intval($this->input->post('menu_id'));
			$product_id = intval($this->input->post('product_id'));
			$similar_id = intval($this->input->post('similar_id'));

			if ($this->init_model->is_admin() AND $product_id > 0 AND $similar_id > 0)
			{
				$this->load->model('admin_catalog_model');
				$this->admin_catalog_model->delete_similar($product_id, $similar_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Додавання товару
		 *
		 * @return string json response
		 */
		public function add()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$this->load->model('admin_catalog_model');

			$component_id = intval($this->input->post('component_id'));
			$menu_id = intval($this->input->post('menu_id'));
			$add_top = intval($this->input->post('add_top'));

			if ($component_id > 0 AND $menu_id > 0 AND is_numeric($add_top))
			{
				$response = array(
					'success' => TRUE,
					'product_id' => $this->admin_catalog_model->add($component_id, $menu_id, $add_top)
				);
			}

			$this->cache->clean();

			return json_encode($response);
		}

		/**
		 * Копіювання товару
		 */
		public function copy()
		{
			$this->init_model->is_admin('redirect');

			$response = array('success' => FALSE, 'image' => '');

			$product_id = intval($this->input->post('product_id'));

			if ($product_id > 0)
			{
				$this->load->model('admin_catalog_model');

				$product = $this->admin_catalog_model->get($product_id);

				unset($product['product_id'], $product['position']);
				$product['title_ua'] = str_replace(' - копія', '', $product['title_ua']) . ' - копія';
				$product['title_ru'] = '';
				$product['title_en'] = '';
				$product['url_ua'] = '';
				$product['url_ru'] = '';
				$product['url_en'] = '';

				$_product_id = $this->admin_catalog_model->add($product['component_id'], $product['menu_id'], 0, $product_id);
				$this->admin_catalog_model->update($product, array('product_id' => $_product_id));

				$images = $this->admin_catalog_model->get_images($product_id);

				if (count($images) > 0)
				{
					$dir = ROOT_PATH . 'upload/catalog/';
					if (!file_exists($dir)) mkdir($dir);

					$dir .= $this->init_model->dir_by_id($_product_id) . '/';
					if (!file_exists($dir)) mkdir($dir);

					$dir .= $_product_id . '/';
					if (!file_exists($dir)) mkdir($dir);

					$source_dir = ROOT_PATH . 'upload/catalog/' . $this->init_model->dir_by_id($product_id) . '/' . $product_id . '/';

					foreach ($images as $k => $image)
					{
						if ($k == 0)
						{
							$response['image'] = '/upload/catalog/' . $this->init_model->dir_by_id($_product_id) . '/' . $_product_id . '/t_' . $image['image'];
						}

						copy($source_dir . $image['image'], $dir . $image['image']);
						copy($source_dir . 't_' . $image['image'], $dir . 't_' . $image['image']);
						copy($source_dir . 's_' . $image['image'], $dir . 's_' . $image['image']);

						unset($image['image_id']);
						$image['product_id'] = $_product_id;

						$this->db->insert('catalog_images', $image);
					}
				}

				$response['product_id'] = $_product_id;
				$response['title'] = $product['title_ua'];
				$response['success'] = TRUE;

				$this->cache->clean();
			}

			return json_encode($response);
		}

		/**
		 * Приховування/відображення товару
		 *
		 * @return string
		 */
		public function visibility()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$product_id = intval($this->input->post('product_id'));
			$status = intval($this->input->post('status'));

			if ($product_id > 0 AND in_array($status, array(0, 1)))
			{
				$this->load->model('admin_catalog_model');

				$set = array('hidden' => $status);
				$where = array('product_id' => $product_id);
				$this->admin_catalog_model->update($set, $where);

				$response['success'] = TRUE;

				$this->cache->clean();
			}

			return json_encode($response);
		}

		/**
		 * Збереження порядку сортування товарів компоненту
		 *
		 * @return string
		 */
		public function update_position()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$products = $this->input->post('products');

			if (is_array($products))
			{
				$this->load->model('admin_catalog_model');

				foreach ($products as $val)
				{
					if (is_numeric($val[0]) AND is_numeric($val[1]))
					{
						$set = array('position' => intval($val[0]));
						$where = array('product_id' => intval($val[1]));
						$this->admin_catalog_model->update($set, $where);
					}
				}

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Редагування товару
		 */
		public function edit()
		{
			$this->init_model->is_admin('redirect');

			$menu_id = intval($this->input->get('menu_id'));
			$product_id = intval($this->input->get('product_id'));
			$page = intval($this->input->get('page'));

			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->template_lib->set_title('Редагування товару');

			$this->template_lib->set_admin_menu_active('components');
			$this->template_lib->set_admin_menu_active('', 'sub_level');

			$this->template_lib->set_bread_crumbs('', 'Редагування товару');

			if ($menu_id > 0 AND $product_id > 0)
			{
				$this->session->set_userdata('last_product', $product_id);

				$this->load->model('admin_catalog_model');
				$this->load->model('catalog_filters/admin_catalog_filters_model');
				$this->load->model('catalog_options/admin_catalog_options_model');
				$this->load->helper('admin_catalog');

				$product = $this->admin_catalog_model->get($product_id);
				$product_components = $this->admin_catalog_model->get_product_components($product_id);

				/*
				$_template_data = array(
					'options' => $this->admin_catalog_options_model->get_options_menu($product_components),
					'product_options' => $this->admin_catalog_model->get_product_options($product_id),
				);
				$options_menu = $this->load->view('admin/options_menu_tpl', $_template_data, TRUE);
				*/

				$_template_data = array(
					'filters' => $this->admin_catalog_filters_model->get_filters_menu($product_components),
					'product_filters' => $this->admin_catalog_model->get_product_filters($product_id),
				);
				$filters_menu = $this->load->view('admin/filters_menu_tpl', $_template_data, TRUE);

				$template_data = array(
					'product_id' => $product_id,
					'product' => $product,
					'product_delivery' => $this->admin_catalog_model->get_product_delivery($product_id),

					'variants' => intval($this->input->get('show_variants')) == 1 ? $this->admin_catalog_model->get_variants($product_id) : FALSE,

					'images' => $this->admin_catalog_model->get_images($product_id),
					'thumb_w' => $this->image_thumb[0],
					'thumb_h' => $this->image_thumb[1],

					//'options_menu' => trim($options_menu),
					'filters_menu' => trim($filters_menu),
					'product_components' => $product_components,

					'components_menu' => $this->admin_catalog_model->get_components_menu(),

					'menu_id' => $menu_id,
					'languages' => $this->config->item('languages'),
					'dir_code' => $this->init_model->dir_by_id($product_id),
					'page' => $page,

					'similar_products' => $this->admin_catalog_model->get_similar($product_id),
					'similar_to_products' => $this->admin_catalog_model->get_similar($product_id, TRUE),
				);

				$this->template_lib->set_content($this->load->view('admin/edit_tpl', $template_data, TRUE));
			}
			else
			{
				show_404();
			}
		}

		/**
		 * Збереження інформації про товар
		 *
		 * @return string
		 */
		public function save()
		{
			$this->init_model->is_admin('json');

			$product_id = intval($this->input->post('product_id'));
			$component_id = intval($this->input->post('component_id'));
			if ($product_id < 1 OR $component_id < 1) exit;

			$this->load->dbutil();
			$this->load->model('admin_catalog_model');
			$this->load->helper('form');
			$this->load->helper('translit');

			/*
			$price = floatval($this->input->post('price'));
			$sale_percent = floatval($this->input->post('sale_percent'));
			$global_sale = intval($this->input->post('global_sale'));

			$new_price = 0;
			$sale_date = 0;

			if ($sale_percent > 0)
			{
				$new_price = $price - round($sale_percent * $price / 100, 2);

				$sale_date = $this->input->post('sale_date');
				if ($sale_date != '')
				{
					$sale_date = strtotime($sale_date);
				}
			}
			*/

			$status = intval($this->input->post('status'));
			$status_date = $this->input->post('status_date');

			if ($status > 0 AND $status_date != '')
			{
				$status_date = strtotime($status_date);
			}
			else
			{
				$status_date = 0;
			}

			$set = array(
				//'price' => $price,
				//'price_usd' => floatval($this->input->post('price_usd')),
				//'price_eur' => floatval($this->input->post('price_eur')),
				//'new_price' => $new_price,
				//'dealer_price' => floatval($this->input->post('dealer_price')),
				//'dealer_percent' => floatval($this->input->post('dealer_percent')),
				//'wholesale_price' => floatval($this->input->post('wholesale_price')),
				//'wholesale_percent' => floatval($this->input->post('wholesale_percent')),
				//'sale_percent' => $sale_percent,
				//'sale_date' => $sale_date,
				'status' => $status,
				'status_date' => $status_date,
				//'availability' => intval($this->input->post('availability')),
				//'demand' => intval($this->input->post('demand')),
				//'code' => $this->input->post('code'),
				'update' => time(),
			);

			$title = $this->input->post('title');
			$sign = $this->input->post('sign');
			$text = $this->input->post('text');

			foreach ($title as $key => $val)
			{
				$set['title_' . $key] = form_prep($val);
				//$set['url_' . $key] = translit($val);

				$_sign = str_replace(array("\r", "\n", "\t"), '', $sign[$key]);
				$set['sign_' . $key] = $this->db->escape_str($_sign);

				$_text = str_replace(array("\r", "\n", "\t"), '', $text[$key]);
				$set['text_' . $key] = $this->db->escape_str($_text);
			}

			$video = $this->input->post('video');
			$video = str_replace(array("\r", "\n", "\t"), '', $video);
			$set['video'] = $this->db->escape_str($video);

			$where = array('product_id' => $product_id);
			//$this->admin_catalog_model->update($set, $where, TRUE);
			$this->admin_catalog_model->update($set, $where);

			$components = $this->input->post('components');
			if (!is_array($components)) $components = array();
			$this->admin_catalog_model->update_bind_components($product_id, $components);

			$filters = $this->input->post('filters');
			if (!is_array($filters)) $filters = array();
			$this->admin_catalog_model->update_bind_filters($product_id, $filters);

			/*
			$options = $this->input->post('options');
			if (!is_array($options)) $options = array();
			$option_price = $this->input->post('option_price');
			if (!is_array($option_price)) $option_price = array();
			$option_price_usd = $this->input->post('option_price_usd');
			if (!is_array($option_price_usd)) $option_price_usd = array();
			$option_price_eur = $this->input->post('option_price_eur');
			if (!is_array($option_price_eur)) $option_price_eur = array();
			$this->admin_catalog_model->update_bind_options($product_id, $options, $option_price, $option_price_usd, $option_price_eur);
			*/

			$set = array();

			$delivery = $this->input->post('delivery');
			$payment = $this->input->post('payment');

			foreach ($delivery as $k => $v)
			{
				$_delivery = str_replace(array("\r", "\n", "\t"), '', $v);
				$set['delivery_' . $k] = $this->db->escape_str($_delivery);

				$_payment = str_replace(array("\r", "\n", "\t"), '', $payment[$k]);
				$set['payment_' . $k] = $this->db->escape_str($_payment);
			}

			$this->admin_catalog_model->update_product_delivery($product_id, $set);

			/*
			if ($global_sale == 1)
			{
				$this->admin_catalog_model->global_sale($product_id, $sale_percent, $sale_date);
			}
			*/

			$this->cache->clean();
			return json_encode(array('success' => TRUE));
		}

		/**
		 * Додавання варіанту товару
		 *
		 * @return string json response
		 */
		public function add_variant()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$this->load->model('admin_catalog_model');

			$product_id = intval($this->input->post('product_id'));

			if ($product_id > 0)
			{
				$response = array(
					'success' => TRUE,
					'variant_id' => $this->admin_catalog_model->add_variant($product_id)
				);
			}

			$this->cache->clean();

			return json_encode($response);
		}

		/**
		 * Збереження варіантів товару
		 *
		 * @return string
		 */
		public function save_variants()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$variants = $this->input->post('variant');

			if (is_array($variants))
			{
				$languages = array_keys($this->config->item('languages'));

				$this->load->model('admin_catalog_model');
				$this->load->helper('form');

				foreach ($variants as $k => $v)
				{
					$set = array(
						'articul' => $v['articul'],
						'diameter' => $v['diameter'],
						'length' => $v['length'],
						'material' => $v['material'],
						'weight' => floatval(str_replace(',', '.', $v['weight'])),
						'total' => $v['total'],
						'box' => $v['box'],
						'price' => floatval(str_replace(',', '.', $v['price'])),
						'price_opt' => floatval(str_replace(',', '.', $v['price_opt'])),
						'limit_opt' => intval($v['limit_opt']),
						'price_vip' => floatval(str_replace(',', '.', $v['price_vip'])),
						'limit_vip' => intval($v['limit_vip']),
					);

					foreach ($languages as $lang)
					{
						$set['title_' . $lang] = form_prep($v['title'][$lang]);
					}

					$this->admin_catalog_model->update_variant($k, $set);
				}

				$response['success'] = TRUE;
			}

			$this->cache->clean();

			return json_encode($response);
		}

		/**
		 * Видалення варіанту товару
		 *
		 * @return string
		 */
		public function delete_variant()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$variant_id = intval($this->input->post('variant_id'));

			if ($variant_id > 0)
			{
				$this->load->model('admin_catalog_model');
				$this->admin_catalog_model->delete_variant($variant_id);

				$response['success'] = TRUE;
			}

			$this->cache->clean();

			return json_encode($response);
		}

		/**
		 * Зміна компоненту товару
		 *
		 * @return int|string
		 */
		public function change_component()
		{
			$this->init_model->is_admin('json');

			$component_id = intval($this->input->post('component_id'));
			$products = $this->input->post('products');
			$products = array_map('intval', $products);

			if ($component_id < 1 OR count($products) == 0) exit;

			$this->load->model('admin_catalog_model');
			$this->admin_catalog_model->change_component($component_id, $products);

			$this->cache->clean();

			return json_encode(array('success' => TRUE));
		}

		/**
		 * Завантаження зображення галереї
		 *
		 * @return string
		 */
		public function upload_gallery_image()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$component_id = intval($this->input->post('component_id'));
			$menu_id = intval($this->input->post('menu_id'));
			$product_id = intval($this->input->post('product_id'));

			if ($component_id > 0 AND $menu_id > 0 AND $product_id > 0)
			{
				$dir = ROOT_PATH . 'upload/catalog/';
				if (!file_exists($dir)) mkdir($dir);

				$dir .= $this->init_model->dir_by_id($product_id) . '/';
				if (!file_exists($dir)) mkdir($dir);

				$dir .= $product_id . '/';
				if (!file_exists($dir)) mkdir($dir);

				$this->load->helper('translit');
				$file_name = translit_filename($_FILES['gallery_image']['name']);

				$upload_config = array(
					'upload_path' => $dir,
					'overwrite' => TRUE,
					'file_name' => $file_name,
					'allowed_types' => 'gif|jpg|jpeg|png'
				);

				$this->load->library('upload', $upload_config);

				if ($this->upload->do_upload('gallery_image'))
				{
					$sizes = getimagesize($dir . $file_name);

					$width = ($sizes[0] < $this->image_source[0]) ? $sizes[0] : $this->image_source[0];
					$height = ($width * $sizes[1]) / $sizes[0];

					$this->load->library('Image_lib');

					$this->image_lib->resize($dir . $file_name, $dir . 's_' . $file_name, $width, $height);
					$this->image_lib->resize($dir . $file_name, $dir . $file_name, $this->image_big[0], $this->image_big[1]);
					$this->image_lib->resize($dir . $file_name, $dir . 't_' . $file_name, $this->image_thumb[0], $this->image_thumb[1]);

					$this->load->model('admin_catalog_model');
					$result = $this->admin_catalog_model->insert_image($product_id, $component_id, $menu_id, $file_name);

					$response['success'] = TRUE;
					$response['image_id'] = $result['image_id'];
					$response['width'] = $width;
					$response['height'] = $height;
					$response['file_name'] = $file_name . '?t=' . time() . rand(10000, 1000000);
				}
			}

			$this->config->set_item('is_ajax_request', TRUE);
			return json_encode($response);
		}

		/**
		 * Обрізка зображення галереї
		 *
		 * @return string
		 */
		public function crop_gallery_image()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$image_id = intval($this->input->post('image_id'));
			$width = floatval($this->input->post('width'));
			$coords = array_map('floatval', $this->input->post('coords'));

			if (is_numeric($coords['x']) AND is_numeric($coords['y']) AND $coords['w'] >= 0 AND $coords['h'] >= 0 AND $image_id > 0 AND $width > 0)
			{
				$this->load->model('admin_catalog_model');

				$result = $this->admin_catalog_model->get_image($image_id);

				if (count($result) > 0)
				{
					$dir_code = $this->init_model->dir_by_id($result['product_id']);
					$dir = ROOT_PATH . 'upload/catalog/' . $dir_code . '/' . $result['product_id'] . '/';

					if (file_exists($dir . 's_' . $result['image']))
					{
						$sizes = getimagesize($dir . 's_' . $result['image']);
						$w_index = $sizes[0] / $width;
						$h_index = $sizes[1] / round(($width * $sizes[1]) / $sizes[0]);

						$x = $coords['x'] * $w_index;
						$y = $coords['y'] * $h_index;
						$x2 = $coords['x2'] * $w_index;
						$y2 = $coords['y2'] * $h_index;

						$this->load->library('Image_lib');

						$this->image_lib->crop($dir . 's_' . $result['image'], $dir . 't_' . $result['image'], $x2 - $x, $y2 - $y, $x, $y);
						$this->image_lib->resize($dir . 't_' . $result['image'], $dir . 't_' . $result['image'], $this->image_thumb[0], $this->image_thumb[1]);

						$response['image'] = '/upload/catalog/' . $dir_code . '/' . $result['product_id'] . '/t_' . $result['image'] . '?t=' . time() . rand(10000, 1000000);
					}

					$response['success'] = TRUE;
				}
			}

			return json_encode($response);
		}

		/**
		 * Накладання водяного знаку
		 *
		 * @return string
		 */
		public function watermark_gallery_image()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$image_id = intval($this->input->post('image_id'));
			$position = intval($this->input->post('position'));

			if ($position > 0 AND $image_id > 0 AND $this->config->item('watermark') != '')
			{
				$this->load->model('admin_catalog_model');

				$result = $this->admin_catalog_model->get_image($image_id);

				if (count($result) > 0)
				{
					$dir_code = $this->init_model->dir_by_id($result['product_id']);
					$dir = ROOT_PATH . 'upload/catalog/' . $dir_code . '/' . $result['product_id'] . '/';

					if (file_exists($dir . 's_' . $result['image']))
					{
						$this->load->library('Image_lib');
						$this->image_lib->watermark($dir . 's_' . $result['image'], $dir . $result['image'], ROOT_PATH . 'upload/watermarks/' . $this->config->item('watermark'), $position, $this->config->item('watermark_padding'), $this->config->item('watermark_opacity'));

						$this->db->set('watermark_position', $position);
						$this->db->where('image_id', $image_id);
						$this->db->update('catalog_images');
					}

					$response['success'] = TRUE;
				}
			}

			return json_encode($response);
		}

		/**
		 * Збереження порядку сортування зображень товару
		 *
		 * @return string
		 */
		public function gallery_images_position()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => TRUE);
			$images = $this->input->post('images');

			if (is_array($images))
			{
				$this->load->model('admin_catalog_model');

				foreach ($images as $key => $val)
				{
					if (is_numeric($key) AND is_numeric($val))
					{
						$set = array('position' => intval($key));
						$where = array('image_id' => intval($val));
						$this->db->update('catalog_images', $set, $where);
					}
				}

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Видалення зображення галереї
		 *
		 * @return string
		 */
		public function delete_gallery_image()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => TRUE);
			$image_id = intval($this->input->post('image_id'));

			if ($image_id > 0)
			{
				$this->load->model('admin_catalog_model');

				$result = $this->admin_catalog_model->get_image($image_id);

				if (count($result) > 0)
				{
					$dir_code = $this->init_model->dir_by_id($result['product_id']);
					$dir = ROOT_PATH . 'upload/catalog/' . $dir_code . '/' . $result['product_id'] . '/';

					if ($result['image'] != '')
					{
						if (file_exists($dir . $result['image'])) unlink($dir . $result['image']);
						if (file_exists($dir . 's_' . $result['image'])) unlink($dir . 's_' . $result['image']);
						if (file_exists($dir . 't_' . $result['image'])) unlink($dir . 't_' . $result['image']);
						if (file_exists($dir . 'b_' . $result['image'])) unlink($dir . 'b_' . $result['image']);

						if (file_exists($dir . 'sepia_' . $result['image'])) unlink($dir . 'sepia_' . $result['image']);
						if (file_exists($dir . 'grayscale_' . $result['image'])) unlink($dir . 'grayscale_' . $result['image']);
						if (file_exists($dir . 'flop_' . $result['image'])) unlink($dir . 'flop_' . $result['image']);

						$this->admin_catalog_model->delete_image($image_id);
					}
				}

				$response['error'] = 0;
			}

			return json_encode($response);
		}

		/**
		 * Видалення товару
		 *
		 * @return string
		 */
		public function delete()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$product_id = intval($this->input->post('product_id'));

			if ($product_id > 0)
			{
				$this->load->model('admin_catalog_model');
				$this->load->helper('file');

				$this->admin_catalog_model->delete($product_id);

				$response['success'] = TRUE;

				$this->cache->clean();
			}

			return json_encode($response);
		}

		/**
		 * Експорт товварів
		 */
		public function export()
		{
			$this->init_model->is_admin('json');

			if ($this->input->get('export_type') == 1)
			{
				require_once rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/inner/application/third_party/excel/PHPExcel.php';
				$this->excel = new PHPExcel();

				$this->excel->getProperties()->setCreator($this->config->item('site_name_' . LANG))->setLastModifiedBy($this->config->item('site_name_' . LANG))->setTitle($this->config->item('site_name_' . LANG) . ". Каталог товарів");
				$this->excel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
				$this->excel->getActiveSheet()->setShowSummaryBelow(FALSE);

				$this->excel->getActiveSheet()->getStyle('A1:E1')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$this->excel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setRGB('E0E6DF');

				$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(70);
				$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
				$this->excel->getActiveSheet()->setCellValue('A1', 'Назва категорії/товару');
				$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(TRUE);
				$this->excel->getActiveSheet()->setCellValue('B1', 'Код');
				$this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(TRUE);
				$this->excel->getActiveSheet()->setCellValue('C1', 'Ціна');
				$this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(TRUE);
				$this->excel->getActiveSheet()->setCellValue('D1', 'Наявність');
				$this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
				$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(TRUE);
				$this->excel->getActiveSheet()->setCellValue('E1', 'ID');
				$this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$last_row = $this->export_categories(0);
				$this->excel->getActiveSheet()->setAutoFilter('A1:E' . $last_row);

				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				$objWriter->save(rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/upload/categories.xls');
			}
			else
			{
				require_once rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/inner/application/third_party/excel/PHPExcel.php';
				$this->excel = new PHPExcel();

				$this->excel->getProperties()->setCreator($this->config->item('site_name_' . LANG))->setLastModifiedBy($this->config->item('site_name_' . LANG))->setTitle($this->config->item('site_name_' . LANG) . ". Каталог товарів");
				$this->excel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
				$this->excel->getActiveSheet()->setShowSummaryBelow(FALSE);

				$this->excel->getActiveSheet()->getStyle('A1:E1')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$this->excel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setRGB('E0E6DF');

				$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(70);
				$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
				$this->excel->getActiveSheet()->setCellValue('A1', 'Назва категорії/товару');
				$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(TRUE);
				$this->excel->getActiveSheet()->setCellValue('B1', 'Код');
				$this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(TRUE);
				$this->excel->getActiveSheet()->setCellValue('C1', 'Ціна');
				$this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(TRUE);
				$this->excel->getActiveSheet()->setCellValue('D1', 'Наявність');
				$this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
				$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(TRUE);
				$this->excel->getActiveSheet()->setCellValue('E1', 'ID');
				$this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$last_row = $this->export_categories($this->input->get('menu_id'), 2);
				$this->excel->getActiveSheet()->setAutoFilter('A1:E' . $last_row);

				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				$objWriter->save(ROOT_PATH . 'upload/categories.xls');
			}

			$this->load->helper('download');
			$data = file_get_contents(ROOT_PATH . 'upload/categories.xls');
			force_download('price.xls', $data);

			exit;
		}

		public function export_categories($menu_id = 0, $row_number = 2)
		{
			if ($menu_id == 0)
			{
				$this->db->select('menu.id, menu.name_' . LANG . ' as name');
				$this->db->join('components', 'components.menu_id = menu.id');
				$this->db->where('components.module', 'catalog');
				$this->db->where('components.method', 'index');
				$this->db->order_by('menu.position');
				$result = $this->db->get('menu')->result_array();
			}
			else
			{
				$this->db->select('id, name_' . LANG . ' as name');
				$this->db->where('id', $menu_id);
				$this->db->order_by('position');
				$result = $this->db->get('menu')->result_array();
			}

			if (count($result) > 0)
			{
				foreach ($result as $row)
				{
					//$categories = $categories != '' ? $categories . ' > ' . $row['name'] : $row['name'];

					$this->db->select('product_id, availability, price, new_price, code, title_' . LANG . ' as title');
					$this->db->where('menu_id', $row['id']);
					$this->db->order_by('position');

					$_result = $this->db->get('catalog')->result_array();

					if (count($_result) > 0)
					{
						$this->excel->getActiveSheet()->setCellValue('A' . $row_number, $row['name']);
						$this->excel->getActiveSheet()->setCellValue('E' . $row_number, $row['id']);
						$this->excel->getActiveSheet()->getStyle('A' . $row_number)->getFont()->setBold(TRUE);
						$row_number++;

						foreach ($_result as $_row)
						{
							$this->excel->getActiveSheet()->setCellValue('A' . $row_number, $_row['title']);
							$this->excel->getActiveSheet()->getStyle('A' . $row_number)->getAlignment()->setWrapText(TRUE);
							$this->excel->getActiveSheet()->setCellValue('B' . $row_number, $_row['code']);
							$this->excel->getActiveSheet()->setCellValue('C' . $row_number, $_row['new_price'] > 0 ? $_row['new_price'] : $_row['price']);
							if ($_row['availability'] == 1) $this->excel->getActiveSheet()->setCellValue('D' . $row_number, '+');
							if ($_row['availability'] == 2) $this->excel->getActiveSheet()->setCellValue('D' . $row_number, '-');
							$this->excel->getActiveSheet()->getStyle('D' . $row_number)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$this->excel->getActiveSheet()->setCellValue('E' . $row_number, $_row['product_id']);
							$this->excel->getActiveSheet()->getRowDimension($row_number)->setOutlineLevel(1);
							$row_number++;
						}
					}
					else
					{
						if ($menu_id > 0) $this->export_categories($row['id'], $row_number);
					}
				}
			}

			return $row_number;
		}

		public function import()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$import_type = intval($this->input->post('import_type'));
			$after_import = intval($this->input->post('after_import'));
			//$menu_id = intval($this->input->post('menu_id'));
			//$component_id = intval($this->input->post('component_id'));

			//ini_set('max_execution_time', 1800);
			//ini_set('max_input_time', 1800);

			//ini_set('max_execution_time', 1800);
			//ini_set('max_input_time', 1800);

			$upload_config = array(
				'upload_path' => ROOT_PATH . 'upload',
				'overwrite' => TRUE,
				'file_name' => 'import_price.xls',
				'allowed_types' => 'xls'
			);

			$this->load->library('upload', $upload_config);

			if ($this->upload->do_upload('price_file'))
			{
				$cache_title = array();
				$cache_codes = array();

				$this->load->helpers(array('translit', 'form'));

				require_once ROOT_PATH . 'inner/application/third_party/excel/PHPExcel/IOFactory.php';

				$file_type = PHPExcel_IOFactory::identify(ROOT_PATH . 'upload/import_price.xls');

				$_reader = PHPExcel_IOFactory::createReader($file_type);
				$_reader->setReadDataOnly(true);

				$reader = $_reader->load(ROOT_PATH . 'upload/import_price.xls');

				for ($i = 2; $i <= $reader->getActiveSheet()->getHighestRow(); $i++)
				{
					$price_code = strval($reader->getActiveSheet()->getCell('A' . $i)->getFormattedValue());
					$price_sub_code = strval($reader->getActiveSheet()->getCell('B' . $i)->getFormattedValue());

					$title = strval($reader->getActiveSheet()->getCell('D' . $i)->getValue());
					$articul = strval($reader->getActiveSheet()->getCell('U' . $i)->getCalculatedValue());

					$variant_title = strval($reader->getActiveSheet()->getCell('T' . $i)->getCalculatedValue());

					$price = floatval($reader->getActiveSheet()->getCell('H' . $i)->getFormattedValue());
					$price_opt = floatval($reader->getActiveSheet()->getCell('I' . $i)->getFormattedValue());
					$limit_opt = intval($reader->getActiveSheet()->getCell('K' . $i)->getFormattedValue());
					$price_vip = floatval($reader->getActiveSheet()->getCell('J' . $i)->getFormattedValue());
					$limit_vip = intval($reader->getActiveSheet()->getCell('L' . $i)->getFormattedValue());

					$box = intval($reader->getActiveSheet()->getCell('M' . $i)->getFormattedValue());
					$weight = floatval($reader->getActiveSheet()->getCell('N' . $i)->getFormattedValue());

					$material = strval($reader->getActiveSheet()->getCell('E' . $i)->getFormattedValue());
					$diameter = strval($reader->getActiveSheet()->getCell('F' . $i)->getFormattedValue());
					$length = strval($reader->getActiveSheet()->getCell('G' . $i)->getFormattedValue());

					//$availability = $reader->getActiveSheet()->getCell('D' . $i)->getValue();

					if ($title != '' AND $articul != '')
					{
						$title = form_prep($title);

						if (isset($cache_codes[$price_code . $price_sub_code]))
						{
							$menu_id = $cache_codes[$price_code . $price_sub_code][0];
							$component_id = $cache_codes[$price_code . $price_sub_code][1];
						}
						else
						{
							$menu = $this->db->select('id')->where('price_code', $price_code)->where('price_sub_code', $price_sub_code)->get('menu')->row_array();

							if (isset($menu['id']))
							{
								$component = $this->db->select('component_id')->where('menu_id', $menu['id'])->where('module', 'catalog')->get('components')->row_array();

								if (isset($component['component_id']))
								{
									$menu_id = $menu['id'];
									$component_id = $component['component_id'];

									$cache_codes[$price_code . $price_sub_code] = array($menu['id'], $component['component_id']);
								}
							}
						}

						$set = array(
							'menu_id' => $menu_id,
							'component_id' => $component_id,
							'title_' . DEF_LANG => $title,
							'update' => time(),
							'is_update' => 1,
						);

						//if ($title != '')
						//{
							if (isset($cache_title[$title]))
							{
								$product_id = $cache_title[$title];

								unset($set['title_' . DEF_LANG]);
								$this->db->update('catalog', $set, array('product_id' => $product_id));

								$this->db->where(array(
									'component_id' => $component_id,
									'menu_id' => $menu_id,
									'product_id' => $product_id,
								));

								$c = $this->db->count_all_results('catalog_product_bind_components');

								if ($c == 0)
								{
									$_set = array(
										'component_id' => $component_id,
										'menu_id' => $menu_id,
									);
									$_where = array(
										'product_id' => $product_id,
									);
									$this->db->update('catalog_product_bind_components', $_set, $_where);

									$_set = array(
										'component_id' => $component_id,
										'menu_id' => $menu_id,
										'hash_' . DEF_LANG => md5($product_id),
									);
									$_where = array(
										'item_id' => $product_id,
										'module' => 'catalog',
										'method' => 'details',
									);
									$this->db->update('site_links', $_set, $_where);

									$_set = array(
										'component_id' => $component_id,
										'menu_id' => $menu_id,
									);
									$_where = array(
										'item_id' => $product_id,
										'module' => 'catalog',
									);
									$this->db->update('seo_tags', $_set, $_where);
								}
							}
							else
							{
								$product = $this->db->select('product_id')->where('title_' . LANG, $title)->get('catalog')->row_array();

								if (isset($product['product_id']))
								{
									$product_id = $product['product_id'];
								}
								else
								{
									if (isset($menu_id) AND isset($component_id))
									{
										$set['menu_id'] = $menu_id;
										$set['component_id'] = $component_id;
										$set['position'] = intval($this->db->select_max('position', 'max')->where('component_id', $component_id)->get('catalog')->row('max'));

										$this->db->insert('catalog', $set);
										$product_id = $this->db->insert_id();

										$this->db->update(
											'catalog',
											array(
												'url_' . DEF_LANG => $product_id,
											),
											array(
												'product_id' => $product_id
											)
										);

										$_set = array(
											'product_id' => $product_id,
											'component_id' => $component_id,
											'menu_id' => $menu_id,
										);
										$this->db->insert('catalog_product_bind_components', $_set);

										$set = array(
											'item_id' => $product_id,
											'component_id' => $component_id,
											'menu_id' => $menu_id,
											'module' => 'catalog',
											'method' => 'details',
											'hash_' . DEF_LANG => md5($product_id),
										);
										$this->db->insert('site_links', $set);

										$set = array(
											'item_id' => $product_id,
											'component_id' => $component_id,
											'menu_id' => $menu_id,
											'module' => 'catalog',
										);
										$this->db->insert('seo_tags', $set);
									}
								}
							}

							if (isset($product_id))
							{
								$cache_title[$title] = $product_id;

								$c = $this->db->where('articul', $articul)->count_all_results('catalog_variants');

								if ($c == 0)
								{
									$set = array(
										'product_id' => $product_id,
										'title_' . LANG => form_prep($variant_title),
										'articul' => $articul,
										'weight' => $weight,
										'box' => $box,
										'material' => $material,
										'diameter' => $diameter,
										'length' => $length,
										'price' => $price,
										'price_opt' => $price_opt,
										'limit_opt' => $limit_opt,
										'price_vip' => $price_vip,
										'limit_vip' => $limit_vip,
									);

									$this->db->insert('catalog_variants', $set);
								}
								else
								{
									$set = array(
										'weight' => $weight,
										'box' => $box,
										'material' => $material,
										'diameter' => $diameter,
										'length' => $length,
										'price' => $price,
										'price_opt' => $price_opt,
										'limit_opt' => $limit_opt,
										'price_vip' => $price_vip,
										'limit_vip' => $limit_vip,
									);

									$this->db->update('catalog_variants', $set, array('articul' => $articul));
								}
							}
						//}
					}
				}

				$this->load->model('admin_catalog_model');

				/*
				if ($after_import == 1)
				{
					$this->admin_catalog_model->after_import_status();
				}
				*/

				if ($after_import == 2)
				{
					$products = $this->admin_catalog_model->get_import_lost();

					if (count($products) > 0)
					{
						$this->load->helper('file');

						foreach ($products as $v)
						{
							$this->admin_catalog_model->delete($v['product_id']);
						}
					}
				}

				$this->admin_catalog_model->after_import();

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту
		 *
		 * @return string
		 */
		public function delete_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$component_id = intval($this->input->post('component_id'));

			if ($component_id > 0)
			{
				$this->load->model('admin_catalog_model');
				$this->load->helper('file');

				$this->admin_catalog_model->delete_component($component_id);

				$response['error'] = 0;

				$this->cache->clean();
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту "Наші бренди"
		 *
		 * @return string
		 */
		public function delete_brands_menu_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$component_id = intval($this->input->post('component_id'));

			if ($component_id > 0)
			{
				$this->load->model('admin_catalog_model');
				$this->admin_catalog_model->delete_brands_menu_component($component_id);

				$response['error'] = 0;
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту "Розділи каталогу"
		 *
		 * @return string
		 */
		public function delete_catalog_menu_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$component_id = intval($this->input->post('component_id'));

			if ($component_id > 0)
			{
				$this->load->model('admin_catalog_model');
				$this->admin_catalog_model->delete_catalog_menu_component($component_id);

				$response['error'] = 0;
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту "Останні переглянуті товари"
		 *
		 * @return string
		 */
		public function delete_visited_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$component_id = intval($this->input->post('component_id'));

			if ($component_id > 0)
			{
				$this->load->model('admin_catalog_model');
				$this->admin_catalog_model->delete_visited_component($component_id);

				$response['error'] = 0;
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту "Топ товари"
		 *
		 * @return string
		 */
		public function delete_top_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$component_id = intval($this->input->post('component_id'));

			if ($component_id > 0)
			{
				$this->load->model('admin_catalog_model');
				$this->admin_catalog_model->delete_top_component($component_id);

				$response['error'] = 0;
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту "Товари підрозділів"
		 *
		 * @return string
		 */
		public function delete_category_component()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$component_id = intval($this->input->post('component_id'));

			if ($component_id > 0)
			{
				$this->load->model('admin_catalog_model');
				$this->admin_catalog_model->delete_top_component($component_id);

				$response['error'] = 0;
			}

			return json_encode($response);
		}
	}