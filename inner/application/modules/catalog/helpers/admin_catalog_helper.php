<?php

	if (!defined('BASEPATH')) exit('No direct script access allowed');

	function build_components_menu($menu, $id = 0, $components, $component_id)
	{
		echo '<ul>';

		foreach ($menu[$id] as $val)
		{
			$has_sub_level = isset($menu[$val['id']]);

			echo '<li><div class="holder">';

			if (!$has_sub_level)
			{
				$checked = in_array($val['component_id'], $components) ? ' checked="checked"' : '';
				$disabled = $val['component_id'] == $component_id ? ' disabled="disabled"' : '';
				echo '<div class="cell w_20"><div class="controls"><label for="product_component_' . $val['component_id'] . '" class="check_label"><i></i><input type="checkbox" id="product_component_' . $val['component_id'] . '" name="components[]" value="' . $val['component_id'] . '"' . $checked . $disabled . '></label></div></div><div class="cell auto">' . ($id > 0 ? '<b></b>' : '') . '<span class="menu_item">' . $val['name'] . '</span>';
			}
			else
			{
				echo '<div class="cell w_20"></div><div class="cell auto">' . ($id > 0 ? '<b></b>' : '') . '<span class="menu_item">' . $val['name'] . '</span></div>';
			}

			echo '</div>';

			if ($has_sub_level) build_components_menu($menu, $val['id'], $components, $component_id);

			echo '</li>';
		}

		echo '</ul>';
	}

	function build_components_menu_list($menu, $id = 0, $components = array(), $component_id = 0)
	{
		echo '<ul>';

		foreach ($menu[$id] as $val)
		{
			$has_sub_level = isset($menu[$val['id']]);

			echo '<li><div class="holder">';

			if (!$has_sub_level)
			{
				$checked = $val['component_id'] == $component_id ? ' checked="checked"' : '';
				//$disabled = $val['component_id'] == $component_id ? ' disabled="disabled"' : '';
				echo '<div class="cell w_20"><label for="product_component_' . $val['component_id'] . '" class="radio_label"><i></i><input type="radio" id="product_component_' . $val['component_id'] . '" name="component" value="' . $val['component_id'] . '"' . $checked . '></label></div><div class="cell auto">' . ($id > 0 ? '<b></b>' : '') . '<span class="menu_item">' . $val['name'] . '</span>';
			}
			else
			{
				echo '<div class="cell w_20"></div><div class="cell auto"><span class="menu_item">' . $val['name'] . '</span>';
			}

			echo '</div></div>';
			if ($has_sub_level) build_components_menu_list($menu, $val['id'], $components, $component_id);
			echo '</li>';
		}

		echo '</ul>';
	}