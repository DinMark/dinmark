<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * @property Admin_config_model $admin_config_model
	 */

	class Admin_config extends MX_Controller
	{
		public function common()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Загальні налаштування сайту');
			$this->template_lib->set_js('admin/checkboxes.js');
			$this->template_lib->set_admin_menu_active('config');
			$this->template_lib->set_admin_menu_active('common', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->load->model('admin_config_model');
			$this->template_lib->set_content($this->load->view('common_tpl', array('config' => $this->admin_config_model->get_common()), TRUE));
		}

		public function save_common()
		{
			$this->init_model->is_admin('json');

			$data = array(
				'percent' => $this->input->post('percent'),
				'site_email' => $this->input->post('site_email'),
				'site_skype' => $this->input->post('site_skype'),
				'site_phones' => $this->input->post('site_phones'),
				'print_icon' => $this->input->post('print_icon'),
				'delete_alert' => $this->input->post('delete_alert'),
				'address_' . LANG => $this->db->escape_str($this->input->post('address')),
			);

			$this->load->model('admin_config_model');
			$this->admin_config_model->save_common($data);

			$this->cache->delete('db_config');

			return json_encode(array('error' => 0));
		}

		public function languages()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Налаштування мов сайту');
			$this->template_lib->set_admin_menu_active('config');
			$this->template_lib->set_admin_menu_active('languages', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->load->model('admin_config_model');
			$this->template_lib->set_content($this->load->view('languages_tpl', array('config' => $this->admin_config_model->get_languages()), TRUE));
		}

		public function save_languages()
		{
			$this->init_model->is_admin('json');

			$data = array(
				'def_lang' => $this->input->post('def_lang'),
				'languages' => serialize($this->input->post('languages'))
			);

			$this->load->model('admin_config_model');
			$this->admin_config_model->save_languages($data);

			$this->cache->delete('db_config');

			return json_encode(array('error' => 0));
		}

		public function access()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Зміна доступа');
			$this->template_lib->set_admin_menu_active('config');
			$this->template_lib->set_admin_menu_active('access', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->load->model('admin_config_model');
			$this->template_lib->set_content($this->load->view('access_tpl', array('config' => $this->admin_config_model->get_admin()), TRUE));
		}

		public function save_admin()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 1);
			$this->load->model('admin_config_model');

			$set = array('login' => $this->input->post('login'));

			if ($this->input->post('new_password') != '')
			{
				$where = array('password' => md5($this->input->post('old_password') . $this->config->item('encryption_key')));

				if ($this->admin_config_model->check_password($where))
				{
					$set['password'] = md5($this->input->post('new_password') . $this->config->item('encryption_key'));
					$where = array('id' => 1);
					$this->admin_config_model->save_admin($set, $where);

					$response['error'] = 0;
				}
			}

			return json_encode($response);
		}

		public function exchange()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Курси валют');
			$this->template_lib->set_admin_menu_active('config');
			$this->template_lib->set_admin_menu_active('exchange', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->template_lib->set_bread_crumbs('', 'Курси валют');

			$this->load->model('admin_config_model');
			$this->template_lib->set_content($this->load->view('exchange_tpl', array('config' => $this->admin_config_model->get_exchange()), TRUE));
		}

		public function save_exchange()
		{
			$this->init_model->is_admin('json');

			$data = array(
				'usd' => floatval($this->input->post('usd')),
				'eur' => floatval($this->input->post('eur')),
				'pln' => floatval($this->input->post('pln')),
				'auto_exchange' => intval($this->input->post('auto_exchange')),
			);

			$this->load->model('admin_config_model');
			$this->admin_config_model->save_exchnage($data);
			//$this->admin_config_model->change_prices($data['usd'], $data['eur']);

			$this->cache->delete('db_config');

			return json_encode(array('error' => 0));
		}

		/**
		 * Оновлення курсів валют через ПриватБанк
		 *
		 * @return string
		 */
		public function update_exchange()
		{
			$response = array('success' => FALSE);

			if ($this->init_model->is_admin())
			{
				$ch = curl_init('https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5');
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
				curl_exec($ch);
				$result = curl_exec($ch);
				curl_close($ch);

				if ($result)
				{
					$xml = simplexml_load_string($result);

					if (isset($xml->row[0]))
					{
						$vars = array();

						foreach ($xml->row as $v)
						{
							$vars[mb_strtolower(strval($v->exchangerate->attributes()->ccy))] = floatval($v->exchangerate->attributes()->sale);
						}

						$this->load->model('admin_config_model');

						$this->admin_config_model->save_exchnage($vars);

						if (!isset($vars['usd'])) $vars['usd'] = 0;
						if (!isset($vars['eur'])) $vars['eur'] = 0;
						//if (!isset($vars['rur'])) $vars['rur'] = 0;

						$this->admin_config_model->change_prices($vars['usd'], $vars['eur'], $vars['rur']);

						$this->cache->delete('db_config');

						$response['success'] = TRUE;
						$response['usd'] = $vars['usd'];
						$response['eur'] = $vars['eur'];
						//$response['rur'] = $vars['rur'];
					}
				}
			}

			return json_encode($response);
		}

		public function limits()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Пороги цін');
			$this->template_lib->set_admin_menu_active('config');
			$this->template_lib->set_admin_menu_active('limits', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->template_lib->set_bread_crumbs('', 'Пороги цін');

			$this->load->model('admin_config_model');
			$this->template_lib->set_content($this->load->view('limits_tpl', array('config' => $this->admin_config_model->get_limits()), TRUE));
		}

		public function save_limits()
		{
			$this->init_model->is_admin('json');

			$data = array(
				'l3' => floatval($this->input->post('l3')),
				'l5' => floatval($this->input->post('l5')),
			);

			$this->load->model('admin_config_model');
			$this->admin_config_model->save_limits($data);

			$this->cache->delete('db_config');

			return json_encode(array('error' => 0));
		}

		public function watermark()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Водяний знак');
			$this->template_lib->set_admin_menu_active('config');
			$this->template_lib->set_admin_menu_active('watermark', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->load->model('admin_config_model');
			$this->template_lib->set_content($this->load->view('watermark_tpl', array('config' => $this->admin_config_model->get_watermark()), TRUE));
		}

		public function save_watermark()
		{
			$this->init_model->is_admin('json');

			$data = array(
				'watermark_padding' => $this->input->post('watermark_padding'),
				'watermark_opacity' => $this->input->post('watermark_opacity')
			);

			$this->load->model('admin_config_model');
			$this->admin_config_model->save_watermark($data);

			$this->cache->delete('db_config');

			return json_encode(array('error' => 0));
		}

		public function upload_watermark()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

				$dir = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/upload/watermarks/';
				if (!file_exists($dir)) mkdir($dir);

				$this->load->helper('translit');
				$file_name = translit_filename($_FILES['watermark_image']['name']);

				$upload_config = array(
					'upload_path' => $dir,
					'overwrite' => FALSE,
					'file_name' => $file_name,
					'allowed_types' => 'gif|jpg|jpeg|png'
				);

				$this->load->library('upload', $upload_config);

				if ($this->upload->do_upload('watermark_image'))
				{
					$file_name = $this->upload->data('file_name');

					$this->load->model('admin_config_model');
					$config = $this->admin_config_model->get_watermark();

					if ($config['watermark'] != '' AND $config['watermark'] != $file_name AND file_exists($dir . $config['watermark'])) unlink($dir . $config['watermark']);

					$set = array('watermark' => $file_name);
					$this->admin_config_model->save_watermark($set);

					$response['success'] = TRUE;
					$response['file_name'] = $file_name . '?t=' . time() . rand(100000, 1000000);
				}

			$this->config->set_item('is_ajax_request', TRUE);
			$this->cache->delete('db_config');

			return json_encode($response);
		}

		public function delete_watermark()
		{
			$this->init_model->is_admin('json');

			$response = array('error' => 0);

			$this->load->model('admin_config_model');

			$config = $this->admin_config_model->get_watermark();

			if ($config['watermark'] != '')
			{
				$file = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/upload/watermarks/' . $config['watermark'];
				if (file_exists($file)) unlink($file);

				$this->admin_config_model->save_watermark(array('watermark' => ''));
			}

			return json_encode($response);
		}

		public function gag()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Заглушка');
			$this->template_lib->set_admin_menu_active('config');
			$this->template_lib->set_admin_menu_active('gag', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->load->model('admin_config_model');
			$this->template_lib->set_content($this->load->view('gag_tpl', array('config' => $this->admin_config_model->get_gag(), 'languages' => $this->config->item('languages')), TRUE));
		}

		public function save_gag()
		{
			$this->init_model->is_admin('json');

			$this->load->model('admin_config_model');
			$this->load->helper('form');

			$is_gag = $this->input->post('is_gag');

			$gag = array(
				'ua' => str_replace(array("\r", "\n", "\t"), '', $this->input->post('ua')),
				'ru' => str_replace(array("\r", "\n", "\t"), '', $this->input->post('ru')),
				'en' => str_replace(array("\r", "\n", "\t"), '', $this->input->post('en')),
			);

			$this->admin_config_model->save_gag($is_gag, $gag);

			$this->cache->delete('db_config');

			return json_encode(array('error' => 0));
		}

		public function sales()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Налаштування знижок');
			$this->template_lib->set_admin_menu_active('config');
			$this->template_lib->set_admin_menu_active('sales', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->load->model('admin_config_model');
			$this->template_lib->set_content($this->load->view('sales_tpl', array('config' => $this->admin_config_model->get_sales(), 'languages' => $this->config->item('languages')), TRUE));
		}

		public function save_sale()
		{
			$this->init_model->is_admin('json');

			$this->load->model('admin_config_model');
			$this->load->helper('form');

			$set = array(
				'dealer_percent' => floatval($this->input->post('dealer_percent')),
				'dealer_limit' => floatval($this->input->post('dealer_limit')),
				'wholesale_percent' => floatval($this->input->post('wholesale_percent')),
				'wholesale_limit' => floatval($this->input->post('wholesale_limit')),
			);

			$dealer_sign = $this->input->post('dealer_sign');
			$wholesale_sign = $this->input->post('wholesale_sign');

			foreach ($dealer_sign as $k => $v)
			{
				$v = str_replace(array("\n", "\r", "\t") ,'', $v);
				$v = $this->db->escape_str($v);

				$set['dealer_sign_' . $k] = $v;

				$wholesale_sign[$k] = str_replace(array("\n", "\r", "\t") ,'', $wholesale_sign[$k]);
				$wholesale_sign[$k] = $this->db->escape_str($wholesale_sign[$k]);

				$set['wholesale_sign_' . $k] = $wholesale_sign[$k];
			}

			$this->load->model('admin_config_model');
			$this->admin_config_model->save_sales($set, intval($this->input->post('rewrite_percents')));

			$this->cache->delete('db_config');
			return json_encode(array('success' => TRUE));
		}
	}
