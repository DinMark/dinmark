<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="fm admin_component">
	<div class="component_loader"></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="common"></div>
		</div>
		<div class="fm component_edit_links">
			<a href="#" class="fm save"><b></b>Зберегти</a>
		</div>
	</div>
	<div class="evry_title">
		<label class="block_label">Основний e-mail:</label>
		<input type="text" name="site_email" value="<?=$config['site_email'];?>" class="short">
	</div>
	<div class="evry_title">
		<label class="block_label">Skype:</label>
		<input type="text" name="site_skype" value="<?=$config['site_skype'];?>" class="short">
	</div>
	<div class="evry_title">
		<label class="block_label">Номери телефонів:</label>
		<input type="text" name="site_phones" value="<?=form_prep(stripslashes($config['site_phones']));?>">
	</div>
	<div class="evry_title">
		<label class="block_label">Адреса:</label>
		<input type="text" name="address" value="<?=form_prep(stripslashes($config['address_' . LANG]));?>">
	</div>
	<div class="evry_title">
		<label class="block_label">Додати іконку друку:</label>
		<div class="fm select"><input type="checkbox" name="print_icon" value="1"<?php if ($config['print_icon'] == 1) echo ' checked="checked"'; ?> /></div>
	</div>
	<div class="evry_title">
		<label class="block_label">Попередження при видаленні:</label>
		<div class="fm select"><input type="checkbox" name="delete_alert" value="1"<?php if ($config['delete_alert'] == 1) echo ' checked="checked"'; ?> /></div>
	</div>
	<div class="fm for_sucsess short">
		<div class="fmr save_links">
			<a href="#" class="fm save_adm"><b></b>Зберегти</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		/**
		 * Заміна чекбоксів
		 */
		$('input[name="print_icon"], input[name="delete_alert"]').iphoneStyle({
			resizeContainer: false,
			resizeHandle: false,
			onChange: function(elem, value) {
				(value === true) ? $(elem).attr('checked', 'checked') : $(elem).removeAttr('checked');
			}
		});

		/**
		 * Збереження змін
		 */
		$('.for_sucsess .save_adm, .component_edit_links .save').on('click', function (e) {
			e.preventDefault();

			component_loader_show($('.component_loader'), '');

			var uri = '<?=$this->uri->full_url('admin/config/save_common');?>',
				request = {
					site_email: $('input[name="site_email"]').val(),
					site_skype: $('input[name="site_skype"]').val(),
					site_phones: $('input[name="site_phones"]').val(),
					address: $('input[name="address"]').val(),
					print_icon: ($('input[name="print_icon"]').attr('checked') === 'checked') ? 1 : 0,
					delete_alert: ($('input[name="delete_alert"]').attr('checked') === 'checked') ? 1 : 0
				};

			$.post(
				uri,
				request,
				function (response) {
					if (response.error === 0) component_loader_hide($('.component_loader'), '');
				},
				'json'
			);
		});
	});
</script>