<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	$this->template_lib->set_js('admin/jquery.form.js');
	$this->template_lib->set_js('admin/ckeditor/ckeditor.js');
?>
<div class="fm admin_component">
	<div class="component_loader"></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="sales"></div>
		</div>
		<div class="fm component_edit_links">
			<a href="#" class="fm save"><b></b>Зберегти</a>
		</div>
		<?php if (count($languages) > 1): ?>
			<div class="fmr component_lang">
				<?php foreach ($languages as $key => $val): ?>
					<a href="#" class="flags <?=$key;?><?=(($key == LANG) ? ' active' : '');?>" data-language="<?=$key;?>"><img src="img/flags_<?=$key;?>.png"></a>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	<form id="sale_form" action="<?=$this->uri->full_url('admin/config/save_sale');?>" method="post">
		<div class="evry_title">
			<label for="deale_percent" class="block_label">Дилерський відсоток:</label>
			<input type="text" id="dealer_percent" name="dealer_percent" value="<?=$config['dealer_percent'];?>" class="short"> %
		</div>
		<div class="evry_title">
			<label for="deale_limit" class="block_label">Дилерський ліміт:</label>
			<input type="text" id="dealer_limit" name="dealer_limit" value="<?=$config['dealer_limit'];?>" class="short"> грн
		</div>
		<?php foreach ($languages as $key => $val): ?>
			<div class="lang_tab lang_tab_<?=$key;?>"<?=((LANG != $key) ? ' style="display:none"' : '');?>>
				<div class="evry_title">
					<label for="dealer_sign_<?=$key;?>" class="block_label">Опис дилерської ціни:</label>
					<div class="no_float"><textarea id="dealer_sign_<?=$key;?>" class="price_text" name="dealer_sign[<?=$key;?>]"><?=stripslashes($config['dealer_sign_' . $key]);?></textarea></div>
				</div>
			</div>
		<?php endforeach; ?>
		<div class="evry_title">
			<label for="wholesale_percent" class="block_label">Оптовий відсоток:</label>
			<input type="text" id="wholesale_percent" name="wholesale_percent" value="<?=$config['wholesale_percent'];?>" class="short"> %
		</div>
		<div class="evry_title">
			<label for="wholesale_limit" class="block_label">Оптовий ліміт:</label>
			<input type="text" id="wholesale_limit" name="wholesale_limit" value="<?=$config['wholesale_limit'];?>" class="short"> грн
		</div>
		<?php foreach ($languages as $key => $val): ?>
			<div class="lang_tab lang_tab_<?=$key;?>"<?=((LANG != $key) ? ' style="display:none"' : '');?>>
				<div class="evry_title">
					<label for="wholesale_sign_<?=$key;?>" class="block_label">Опис оптової ціни:</label>
					<div class="no_float"><textarea id="wholesale_sign_<?=$key;?>" class="price_text" name="wholesale_sign[<?=$key;?>]"><?=stripslashes($config['wholesale_sign_' . $key]);?></textarea></div>
				</div>
			</div>
		<?php endforeach; ?>
		<div class="evry_title">
			<label class="block_label">&nbsp;</label>
			<div class="no_float controls">
				<label class="check_label">
					<i><input type="checkbox" name="rewrite_percents" value="1" checked="checked"></i>
					замінити власні відсотки товару на задані вище
				</label>
			</div>
		</div>
	</form>
	<div class="fm for_sucsess short">
		<div class="fmr save_links">
			<a href="#" class="fm save_adm"><b></b>Зберегти</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$('.component_lang').on('click', 'a', function (e) {
			e.preventDefault();
			$(this).addClass('active').siblings().removeClass('active');
			$('.lang_tab').hide();
			$('.lang_tab_' + $(this).data('language')).show();
		});

		$('.price_text').ckeditor();

		$('.for_sucsess .save_adm, .component_edit_links .save').on('click', function (e) {
			e.preventDefault();

			component_loader_show($('.component_loader'), '');
			$('.price_text').ckeditor({action: 'update'});

			$('#sale_form').ajaxSubmit({
				success: function (response) {
					if (response.success) component_loader_hide($('.component_loader'), '');
				},
				dataType: 'json'
			});
		});
	});
</script>