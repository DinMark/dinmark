<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="admin_component">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="exchange"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Пороги цін</div></div>
			<a href="#" class="fm save"><b></b>Зберегти</a>
		</div>
	</div>
	<div class="evry_title">
		<label for="l3" class="block_label">-3%:</label>
		<input type="text" id="l3" name="l3" value="<?=$config['l3'];?>" class="short-er">
	</div>
	<div class="evry_title">
		<label for="l5" class="block_label">-5%:</label>
		<input type="text" id="l5" name="l5" value="<?=$config['l5'];?>" class="short-er">
	</div>
	<div class="fm for_sucsess">
		<div class="fmr save_links">
			<a href="#" class="fm save_adm"><b></b>Зберегти</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function () {
		/**
		 * Збереження змін
		 */
		$('.save_adm').on('click', function (e) {
			e.preventDefault();

			component_loader_show($('.component_loader'), '');

			var uri = '<?=$this->uri->full_url('admin/config/save_limits');?>',
				request = {
					l3: $('#l3').val(),
					l5: $('#l5').val()
				};

			$.post(
				uri,
				request,
				function (response) {
					component_loader_hide($('.component_loader'), '');
				},
				'json'
			);
		});
	});
</script>