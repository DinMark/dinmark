<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_config_model extends CI_Model
	{
		public function get_common()
		{
			$config = array();
			$where = array(
				'site_email',
				'site_skype',
				'site_phones',
				'address_' . LANG,
				'print_icon',
				'delete_alert',
				'percent',
			);

			$r = $this->db->select('key, val')->where_in('key', $where)->get('config')->result_array();

			foreach ($r as $row)
			{
				$config[$row['key']] = $row['val'];
			}

			return $config;
		}

		public function save_common($data)
		{
			foreach ($data as $key => $val)
			{
				$this->db->set('val', $val)->where('key', $key)->update('config');
			}
		}

		public function get_languages()
		{
			$config = array();

			$where = array(
				'def_lang',
				'languages'
			);

			$r = $this->db->select('key, val')->where_in('key', $where)->get('config')->result_array();

			foreach ($r as $row)
			{
				if ($row['key'] == 'languages') $row['val'] = ($row['val'] != '') ? unserialize($row['val']) : array();
				$config[$row['key']] = $row['val'];
			}

			return $config;
		}

		public function save_languages($data)
		{
			foreach ($data as $key => $val)
			{
				$this->db->set('val', $val)->where('key', $key)->update('config');
			}
		}

		public function get_admin()
		{
			return $this->db->select('login')->where('id', 1)->get('admin')->row_array();
		}

		public function save_admin($set, $where)
		{
			$this->db->update('admin', $set, $where);
		}

		public function check_password($where)
		{
			$r = $this->db->where($where)->count_all_results('admin');
			return ($r > 0) ? TRUE : FALSE;
		}

		public function save_password($set, $where)
		{
			$this->db->update('admin', $set, $where);
		}

		public function get_exchange()
		{
			$config = array();
			$where = array('usd', 'eur', 'pln', 'auto_exchange');

			$r = $this->db->select('key, val')->where_in('key', $where)->get('config')->result_array();

			foreach ($r as $row)
			{
				$config[$row['key']] = $row['val'];
			}

			return $config;
		}

		public function save_exchnage($data)
		{
			foreach ($data as $key => $val)
			{
				$this->db->set('val', $val)->where('key', $key)->update('config');
			}
		}

		public function change_prices($usd, $eur)
		{
			if ($usd > 0 OR $eur > 0)
			{
				$result = $this->db->select('product_id, sale_percent, price_usd, price_eur')->where('price_usd >', 0)->or_where('price_eur >', 0)->get('catalog')->result_array();

				foreach ($result as $row)
				{
					$price = '';

					if ($row['price_usd'] > 0)
					{
						$price = round($row['price_usd'] * $usd);
					}
					else
					{
						if ($row['price_eur'] > 0)
						{
							$price = round($row['price_eur'] * $eur);
						}
					}

					if ($price != '')
					{
						$set = array(
							'price' => $price,
						);

						if ($row['sale_percent'] > 0)
						{
							$set['new_price'] = round($price - ($row['sale_percent'] * $price / 100), 2);
						}

						$this->db->update('catalog', $set, array('product_id' => $row['product_id']));
					}
				}

				$result = $this->db->select('product_id, option_id, price_usd, price_eur')->where('price_usd >', 0)->or_where('price_eur >', 0)->get('catalog_product_bind_options')->result_array();

				foreach ($result as $row)
				{
					$price = '';

					if ($row['price_usd'] > 0)
					{
						$price = round($row['price_usd'] * $usd);
					}
					else
					{
						if ($row['price_eur'] > 0)
						{
							$price = round($row['price_eur'] * $eur);
						}
					}

					if ($price != '')
					{
						$this->db->update('catalog_product_bind_options', array('price' => $price), array('product_id' => $row['product_id'], 'option_id' => $row['option_id']));
					}
				}
			}
		}

		public function get_limits()
		{
			$config = array();
			$where = array('l3', 'l5');

			$r = $this->db->select('key, val')->where_in('key', $where)->get('config')->result_array();

			foreach ($r as $row)
			{
				$config[$row['key']] = $row['val'];
			}

			return $config;
		}

		public function save_limits($data)
		{
			foreach ($data as $key => $val)
			{
				$this->db->set('val', $val)->where('key', $key)->update('config');
			}
		}

		public function get_watermark()
		{
			$config = array();
			$where = array('watermark', 'watermark_padding', 'watermark_opacity');
			$r = $this->db->select('key, val')->where_in('key', $where)->get('config')->result_array();

			foreach ($r as $row) $config[$row['key']] = $row['val'];

			return $config;
		}

		public function save_watermark($data)
		{
			foreach ($data as $key => $val) $this->db->set('val', $val)->where('key', $key)->update('config');
		}

		public function get_gag()
		{
			$config = array();
			$config['is_gag'] = $this->db->select('val')->where('key', 'is_gag')->get('config')->row('val');

			$result = $this->db->select('lang, text')->get('gag')->result_array();
			foreach ($result as $v) $config[$v['lang']] = $v['text'];

			return $config;
		}

		public function save_gag($is_gag, $gag)
		{
			$this->db->set('val', $is_gag);
			$this->db->where('key', 'is_gag');
			$this->db->update('config');

			foreach ($gag as $k => $v)
			{
				$this->db->set('text', $this->db->escape_str($v));
				$this->db->where('lang', $k);
				$this->db->update('gag');
			}
		}

		public function get_sales()
		{
			$config = array();

			$keys = array(
				'dealer_percent',
				'dealer_limit',
				'wholesale_percent',
				'wholesale_limit',
			);

			$languages = $this->config->item('languages');

			foreach ($languages as $k => $v)
			{
				$keys[] = 'dealer_sign_' . $k;
				$keys[] = 'wholesale_sign_' . $k;
			}

			$result = $this->db->where_in('key', $keys)->get_where('config')->result_array();

			foreach ($result as $v)
			{
				$config[$v['key']] = $v['val'];
			}

			return $config;
		}

		public function save_sales($set, $rewrite)
		{
			foreach ($set as $k => $v)
			{
				$this->db->set('val', $v)->where('key', $k)->update('config');
			}

			$dealer_percent = $set['dealer_percent'];
			$wholesale_percent = $set['wholesale_percent'];

			$this->db->select('product_id, price, dealer_percent, dealer_menu_percent, wholesale_percent, wholesale_menu_percent');

			$result = $this->db->get('catalog')->result_array();

			foreach ($result as $v)
			{
				$set = array();

				if (($v['dealer_percent'] == 0 AND $v['dealer_menu_percent'] == 0) OR $rewrite == 1)
				{
					$set['dealer_percent'] = 0;
					$set['dealer_menu_percent'] = 0;
					$set['dealer_price'] = round($v['price'] - ($dealer_percent * $v['price'] / 100), 2);
				}

				if (($v['wholesale_percent'] == 0 AND $v['wholesale_menu_percent'] == 0) OR $rewrite == 1)
				{
					$set['wholesale_percent'] = 0;
					$set['wholesale_menu_percent'] = 0;
					$set['wholesale_price'] = round($v['price'] - ($wholesale_percent * $v['price'] / 100), 2);
				}

				if (count($set) > 0) $this->db->update('catalog', $set, array('product_id' => $v['product_id']));
			}
		}
	}