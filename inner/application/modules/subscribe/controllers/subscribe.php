<?php defined('ROOT_PATH') or exit('No direct script access allowed');

	/**
	 * Компонент підписки
	 *
	 * @property Subscribe_model $subscribe_model
	 */
	class Subscribe extends MX_Controller
	{
		/**
		 * Обробка запиту підписки
		 *
		 * @return string
		 */
		public function send()
		{
			$response = array('success' => false);

			if ($this->input->is_ajax_request()/* and $this->security->csrf_verify()*/) {
				$this->load->library('form_validation');

				$email = $this->input->post('email', true);

				if ($this->form_validation->valid_email($email)) {
					$this->load->model('subscribe/subscribe_model');
					$this->load->helper('string');

					$response['subscribe'] = (int)$this->subscribe_model->insert($email);
				}

				$response['success'] = true;
			}

			return json_encode($response);
		}
	}