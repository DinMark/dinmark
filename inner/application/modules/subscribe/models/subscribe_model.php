<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Subscribe_model extends CI_Model
	{
		/**
		 * Підписка на новини
		 *
		 * @param string $email
		 * @return bool
		 */
		public function insert($email)
		{
			$email = $this->db->escape_str($email);

			$c = (int)$this->db
				->where('lower(`' . $this->db->dbprefix . 'subscribers`.`email`)', mb_strtolower($email, 'UTF-8'))
				->count_all_results('subscribers');

			if ($c === 0) {
				$this->db->insert(
					'subscribers',
					array(
						'email' => $email,
					)
				);

				return true;
			}

			return false;
		}
	}