<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	function build_options($items, $item)
	{
		foreach ($item as $val)
		{
			echo '<li id="option_' . $val['option_id'] . '" data-option-id="' . $val['option_id'] . '">';
			echo '<div class="holder' . (($val['hidden'] == 1) ? ' hidden' : '') . '">';
			echo '<div class="cell last_edit"></div>';
			echo '<div class="cell w_20 icon"><a href="#" class="hide-show ' . (($val['hidden'] == 0) ? ' active' : '') . '"></a></div>';
			echo '<div class="cell w_30 number_cell"><div class="number"></div><div class="add_items"><a href="#" class="up_add"></a><a href="#" class="child_add"></a><a href="#" class="down_add"></a></div></div>';
			echo '<div class="cell w_20 icon"><a href="#" class="edit"></a></div>';
			echo '<div class="cell auto"><span class="menu_item">' . (($val['name'] != '') ? $val['name'] : (($val['def_name'] != '') ? $val['def_name'] : 'Нова опція')) . '</span></div>';
			echo '<div class="cell w_70 icon no_padding"><div class="fm step_left"><a href="#"></a></div><div class="fm step_right"><a href="#"></a></div><div class="fm double_step_right"><a href="#"></a></div></div>';
			echo '<div class="cell w_20 icon"><a href="#" class="single_arrows option_sorter"></a></div>';
			echo '<div class="cell w_20 icon"><a href="#" class="delete"></a></div>';
			echo '</div>';

			if (isset($items[$val['option_id']]))
			{
				echo '<ul>';
				build_options($items, $items[$val['option_id']]);
				echo '</ul>';
			}
			echo '</li>';
		}
	}

	if (isset($options[0])) build_options($options, $options[0]);