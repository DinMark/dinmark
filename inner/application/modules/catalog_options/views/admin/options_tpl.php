<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	$this->template_lib->set_js('admin/ui/jquery-ui-1.10.3.custom.min.js');
	$this->template_lib->set_js('admin/jquery.mjs.nestedSortable.js');

	//$this->template_lib->set_css('js/admin/fineuploader/fineuploader-3.5.0.css', TRUE);
	//$this->template_lib->set_js('admin/fineuploader/jquery.fineuploader-3.5.0.min.js');
?>
<div class="admin_component" id="admin_options_component">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="option"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Опції товарів каталогу</div></div>
			<a class="fm add add_option" href="#"><b></b>Додати опцію</a>
			<a class="fm add_bottom" href="#"><b class="active"></b>Додавати внизу</a>
		</div>
		<?php if (count($languages) > 1): ?>
		<div class="fmr component_lang">
			<?php foreach ($languages as $key => $val): ?>
			<a href="#" class="flags <?=$key;?><?=(($key == LANG) ? ' active' : '');?>" data-language="<?=$key;?>"><img src="img/flags_<?=$key;?>.png"></a>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
	<div class="fm admin_menu">
		<ul id="options_list"><?=$options;?></ul>
	</div>
</div>

<script type="text/javascript">

	var option_min_level = 1,
		option_max_level = 2,
		option_add_bottom = 1,
		move_option_id = <?=$last;?>;

	$(function() {

		$('#option_<?=$last;?>').find('.last_edit').eq(0).addClass('active');

		var $component = $('#admin_options_component'),
			$loader = $component.find('.component_loader'),
			$options = $('#options_list'),
			$languages = $component.find('.component_lang');

		function save_position() {

			component_loader_show($('.component_loader'), '');

			var items = [];

			$('#option_' + move_option_id).closest('ul').find('li').each(function (i, val) {
				items[i] = {
					option_id: $(val).data('option-id'),
					position: i,
					parent_id: $(val).data('parent-id'),
					level: $(val).data('level')
				}
			});

			$.post(
				'<?=$this->uri->full_url('admin/catalog_options/update_position');?>',
				{
					option_id: move_option_id,
					items : items
				},
				function (response) {
					if (response.success) component_loader_hide($('.component_loader'), '');
				},
				'json'
			);
		}

		function init_sortable() {
			$options.nestedSortable({
				forcePlaceholderSize: true,
				opacity: .4,
				tabSize: 5,
				listType : 'ul',
				handle: 'a.option_sorter',
				items: 'li',
				toleranceElement: '> div',
				update : function (event, ui) {
					move_option_id = $(ui.item).data('option-id');

					$('.last_edit.active').removeClass('active');
					$('#option_' + move_option_id).find('.last_edit').eq(0).addClass('active');

					set_options_data();
					save_position();
				},
				placeholder: "ui-state-highlight",
				axis : 'y',
				helper : 'clone',
				protectRoot : false,
				maxLevels : option_max_level
			});
		}

		function set_options_data() {

			$options.find('li').map(function (i) {

				var level = $(this).parents('ul').length, parent_id = 0;
				if (level > 0) parent_id = $(this).parents('li').eq(0).data('option-id');

				$(this)
					.data('level', level - 1).data('parent-id', parent_id)
					.find('.number').eq(0).text(i + 1)
					.end().end()
					.find('.step_left').css('visibility', 'hidden')
					.end()
					.find('.step_right').css('visibility', 'hidden')
					.end()
					.find('.double_step_right').css('visibility', 'hidden');

				if (i % 2 == 0) {
					$(this).addClass('grey');
				} else {
					$(this).removeClass('grey');
				}

				if (level > 1) {
					if ($(this).find('.auto').eq(0).find('b').length === 0) $(this).find('.auto').eq(0).prepend('<b></b>');
					$(this).find('.step_left').eq(0).css('visibility', 'visible');
				} else {
					$(this).find('.auto').eq(0).find('b').remove();
				}

				if ($(this).index() > 0) {
					$(this).find('.double_step_right').eq(0).css('visibility', 'visible');

					if ($(this).find('ul').length > 0 && ((level + 1) != option_max_level)) $(this).find('.step_right').eq(0).css('visibility', 'visible');
				}

				if ($(this).find('ul').length > 0) {
					$(this).find('.option_sorter').eq(0).addClass('double_arrows');
				} else {
					$(this).find('.option_sorter').eq(0).removeClass('double_arrows');
				}

				if (level >= option_max_level) {
					$(this)
						.find('.step_right').eq(0).css('visibility', 'hidden')
						.end().end()
						.find('.child_add').eq(0).addClass('no_active');

					if (level === option_max_level) $(this).find('.double_step_right').eq(0).css('visibility', 'hidden');
					if (level > option_max_level) $(this).find('.auto').eq(0).find('span').addClass('through');
				} else {
					$(this)
						.find('.child_add').eq(0).removeClass('no_active')
						.end()
						.find('.auto').eq(0).find('span').removeClass('through');
				}
			});

			init_sortable();
		}

		set_options_data();

		$component
			.find('.component_lang').on('click', 'a', function (e) {
				e.preventDefault();

				component_loader_show($loader, '');

				var language = $(this).siblings().removeClass('active').end().addClass('active').data('language');

				$.post(
					'<?=$this->uri->full_url('admin/catalog_options/options_load');?>',
					{
						language : language
					},
					function (response) {
						if (response.success) {

							$options.html(response.options_list);

							$('#option_' + move_option_id).find('.last_edit').eq(0).addClass('active');
							set_options_data();
							component_loader_hide($loader, '');
						}
					},
					'json'
				);
			})
			.end()
			.on('click', '.add_bottom', function (e) {
				e.preventDefault();

				$(this).find('b').toggleClass('active');

				if ($(this).find('b').hasClass('active')) {
					option_add_bottom = 1;
				} else {
					option_add_bottom = 0;
				}
			})
			.on('click', '.add_option', function (e) {
				e.preventDefault();

				component_loader_show($loader, '');

				$.post(
					'<?=$this->uri->full_url('admin/catalog_options/insert');?>',
					{
						parent_id: 0
					},
					function (response) {
						if (response.success) {
							move_option_id = response.option_id;
							$('.last_edit').removeClass('active');

							var item = $('#option_template').html();
							item = item.split('%option_id%').join(response.option_id);

							option_add_bottom === 0 ? $options.prepend(item) : $options.append(item);

							$.when(set_options_data()).then(save_position());
						}
					},
					'json'
				);
			});

		$options
			.on('click', '.up_add', function (e) {
				e.preventDefault();

				component_loader_show($loader, '');

				var $link = $(this),
					$li = $link.closest('li'),
					parent_id = $link.parents('li').eq(1).length > 0 ? $link.parents('li').eq(1).data('menu-id') : 0;

				$.post(
					'<?=$this->uri->full_url('admin/catalog_options/insert');?>',
					{
						parent_id: parent_id
					},
					function (response) {
						if (response.success) {
							move_option_id = response.option_id;
							$('.last_edit').removeClass('active');

							var item = $('#option_template').html();
							item = item.split('%option_id%').join(response.option_id);

							$li.before(item);

							$.when(set_options_data()).then(save_position());
						}
					},
					'json'
				);
			})
			.on('click', '.child_add', function (e) {
				e.preventDefault();

				component_loader_show($loader, '');

				var $link = $(this),
					$li = $link.closest('li');

				$.post(
					'<?=$this->uri->full_url('admin/catalog_options/insert');?>',
					{
						option_id: $li.data('option-id')
					},
					function (response) {
						if (response.success) {
							move_option_id = response.option_id;
							$('.last_edit').removeClass('active');

							if ($li.find('ul').length === 0) $li.append('<ul></ul>');

							var item = $('#option_template').html();
							item = item.split('%option_id%').join(response.option_id);

							option_add_bottom === 0 ? $li.find('ul').eq(0).prepend(item) : $li.find('ul').eq(0).append(item);


							$.when(set_options_data()).then(save_position());
						}
					},
					'json'
				);
			})
			.on('click', '.down_add', function (e) {
				e.preventDefault();

				component_loader_show($loader, '');

				var $link = $(this),
					$li = $link.closest('li'),
					parent_id = $link.parents('li').eq(1).length > 0 ? $link.parents('li').eq(1).data('option-id') : 0;

				$.post(
					'<?=$this->uri->full_url('admin/catalog_options/insert');?>',
					{
						parent_id : parent_id
					},
					function (response) {
						if (response.success) {
							move_option_id = response.option_id;
							$('.last_edit').removeClass('active');

							var item = $('#option_template').html();
							item = item.split('%option_id%').join(response.option_id);

							$li.after(item);

							$.when(set_options_data()).then(save_position());
						}
					},
					'json'
				);
			})
			.on('click', '.edit', function (e) {
				e.preventDefault();

				var $holder = $(this).closest('.holder'), item;

				if (!$(this).hasClass('active')) {
					item = $holder.find('.auto').find('span.menu_item').text();
					if (item == 'Нова опція') item = '';

					$holder.find('.auto').find('span.menu_item').html('<input type="text" name="item" value="' + item + '" placeholder="Введіть назву опції товару">');
					$holder.find('.auto').find('span.menu_item').find('input').focus();

					$(this).addClass('active');
				} else {
					$(this).removeClass('active');

					$options.find('.last_edit.active').removeClass('active');
					$holder.find('.last_edit').addClass('active');

					component_loader_show($('.component_loader'), '');

					item = $holder.find('.auto').find('input').eq(0).val();

					$.post(
						'<?=$this->uri->full_url('admin/catalog_options/update');?>',
						{
							option_id : $holder.closest('li').data('option-id'),
							name : item,
							language : $languages.find('.active').length > 0 ? $languages.find('.active').data('language') : '<?=LANG;?>'
						},
						function (response) {
							if (response.success) {
								$holder.find('.auto').find('span.menu_item').text(item);
								component_loader_hide($('.component_loader'), '');
							}
						},
						'json'
					);
				}
			})
			.on('keydown', '.auto input', function (e) {
				if (e.keyCode == 13) {
					var $holder = $(this).closest('.holder'),
						$link = $holder.find('.edit'), item;

					$options.find('.last_edit.active').removeClass('active');
					$holder.find('.last_edit').addClass('active');

					$link.removeClass('active');
					component_loader_show($('.component_loader'), '');

					item = $(this).val();

					$.post(
						'<?=$this->uri->full_url('admin/catalog_options/update');?>',
						{
							option_id : $holder.closest('li').data('option-id'),
							name : item,
							language : $languages.find('.active').length > 0 ? $languages.find('.active').data('language') : '<?=LANG;?>'
						},
						function (response) {
							if (response.success) {
								$holder.find('.auto').find('span.menu_item').text(item);
								component_loader_hide($('.component_loader'), '');
							}
						},
						'json'
					);
				}
			})
			.on('click', '.hide-show', function (e) {
				e.preventDefault();

				var $holder = $(this).closest('.holder'),
					status = 0;

				if (!$holder.hasClass('hidden')) status = 1;

				$holder.toggleClass('hidden');
				$(this).toggleClass('active');

				$options.find('.last_edit.active').removeClass('active');
				$holder.find('.last_edit').addClass('active');

				component_loader_show($('.component_loader'), '');

				$.post(
					'<?=$this->uri->full_url('admin/catalog_options/hidden');?>',
					{
						option_id : $holder.closest('li').data('option-id'),
						status : status
					},
					function (response) {
						if (response.success) component_loader_hide($('.component_loader'), '');
					},
					'json'
				);
			})
			.on('click', '.step_left', function (e) {
				e.preventDefault();

				var $li = $(this).closest('li'),
					$ul = $(this).closest('ul'),
					$li_prev = $(this).parents('li').eq(1),
					$li_clone = $li.clone();

				move_option_id = $li.data('option-id');

				$li.remove();
				$li_prev.after($li_clone);

				if ($ul.find('li').length === 0) $li_prev.find('ul').remove();

				$options.find('.last_edit.active').removeClass('active');
				$li_clone
					.find('.holder').removeClass('active')
					.end()
					.find('.last_edit').eq(0).addClass('active')
					.end().end()
					.find('.add_items').eq(0).hide()
					.end().end()
					.find('.number').eq(0).show();

				set_options_data();
				save_position();
			})
			.on('click', '.step_right', function (e) {
				e.preventDefault();

				var $parent = $(this).closest('li'),
					$parent_clone = $parent.clone(),
					$prev_item = $parent.prev('li');

				move_option_id = $parent.data('option-id');
				$parent.remove();

				$options.find('.last_edit.active').removeClass('active');
				$parent_clone.find('.last_edit').eq(0).addClass('active');

				if ($prev_item.find('ul').length > 0) {
					$prev_item.find('ul').eq(0).append($parent_clone);
				} else {
					$prev_item.append('<ul></ul>');
					$prev_item.find('ul').eq(0).append($parent_clone);
				}

				set_options_data();
				save_position();
			})
			.on('click', '.double_step_right', function (e) {
				e.preventDefault();

				var $childs = $(this).closest('li').find('ul').length > 0 ? $(this).closest('li').find('ul').html() : '';
				$(this).closest('li').find('ul').remove();

				var $parent = $(this).closest('li').prev('li'),
					$clone = $(this).closest('li').clone();

				move_option_id = $(this).closest('li').data('option-id');
				$(this).closest('li').remove();

				$options.find('.last_edit.active').removeClass('active');
				$clone.find('.last_edit').eq(0).addClass('active');

				if ($parent.find('ul').length === 0) $parent.append('<ul></ul>');
				$parent.find('ul').eq(0).append($clone);
				if ($childs != '') $parent.find('ul').eq(0).append($childs);

				set_options_data();
				save_position();
			})
			.on('click', '.delete', function (e) {
				e.preventDefault();

				var $ul = $(this).closest('ul'),
					$li = $(this).closest('li');

				confirmation('Видалити опцію?<br><span>Якщо є дочірні опції, то вони також будуть видалені!</span>', function () {

					component_loader_show($('.component_loader'), '');

					$.post(
						'<?=$this->uri->full_url('admin/catalog_options/delete');?>',
						{
							option_id : $li.data('option-id')
						},
						function (response) {
							if (response.success) {
								$li.remove();

								if ($ul.find('li').length === 0) {
									$ul.remove();
								} else {
									set_options_data();
								}

								component_loader_hide($('.component_loader'), '');
							}
						},
						'json'
					);
				});
			})
			.on('click', '.option_sorter', function (e) {
				e.preventDefault();
			})
			.on('mouseenter', '.holder', function () {
				$(this)
					.addClass('active').find('.number_cell').find('.number').hide()
					.end()
					.find('.add_items').show();
			})
			.on('mouseleave', '.holder', function () {
				$(this)
					.removeClass('active').find('.number_cell').find('.add_items').hide()
					.end()
					.find('.number').show();
			});
	});
</script>

<script type="text/template" id="option_template">
	<li id="option_%option_id%" data-option-id="%option_id%">
		<div class="holder">
			<div class="cell last_edit active"></div>
			<div class="cell w_20 icon">
				<a href="#" class="hide-show active"></a>
			</div>
			<div class="cell w_30 number_cell">
				<div class="number"></div>
				<div class="add_items">
					<a href="#" class="up_add"></a>
					<a href="#" class="child_add"></a>
					<a href="#" class="down_add"></a>
				</div>
			</div>
			<div class="cell w_20 icon">
				<a href="#" class="edit"></a>
			</div>
			<div class="cell auto">
				<span class="menu_item">Нова опція</span>
			</div>
			<div class="cell w_70 icon no_padding">
				<div class="fm step_left">
					<a href="#"></a>
				</div>
				<div class="fm step_right">
					<a href="#"></a>
				</div>
				<div class="fm double_step_right">
					<a href="#"></a>
				</div>
			</div>
			<div class="cell w_20 icon">
				<a href="#" class="single_arrows option_sorter"></a>
			</div>
			<div class="cell w_20 icon">
				<a href="#" class="delete"></a>
			</div>
		</div>
	</li>
</script>