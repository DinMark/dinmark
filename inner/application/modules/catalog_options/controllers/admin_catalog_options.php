<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_catalog_options
	 *
	 * @property Admin_catalog_options_model $admin_catalog_options_model
	 */

	class Admin_catalog_options extends MX_Controller
	{
		/**
		 * Список опцій
		 *
		 * @return void
		 */
		public function index()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Управління опціями товарів каталогу');

			$this->template_lib->set_admin_menu_active('catalog_config');
			$this->template_lib->set_admin_menu_active('catalog_options', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->load->model('admin_catalog_options_model');

			$template_data = array(
				'languages' => $this->config->item('languages'),
				'menu_id' => $menu_id,
				'last' => intval($this->session->userdata('last_option')),
			);

			$_template_data = array('options' => $this->admin_catalog_options_model->get_options(LANG));
			$template_data['options'] = $this->load->view('admin/list_tpl', $_template_data, TRUE);

			$this->template_lib->set_content($this->load->view('admin/options_tpl', $template_data, TRUE));
		}

		/**
		 * Завантаження списку опцій через ajax запит
		 *
		 * @return string
		 */
		public function options_load()
		{
			$this->init_model->is_admin('json');

			$this->load->model('admin_catalog_options_model');

			$template_data = array(
				'options' => $this->admin_catalog_options_model->get_options($this->input->post('language'))
			);

			$response = array(
				'success' => TRUE,
				'options_list' => $this->load->view('admin/list_tpl', $template_data, TRUE),
			);

			return json_encode($response);
		}

		/**
		 * Додавання опції
		 */
		public function insert()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$parent_id = intval($this->input->post('parent_id'));

			if ($parent_id >= 0)
			{
				$this->load->model('admin_catalog_options_model');

				$option_id = $this->admin_catalog_options_model->option_add($parent_id);

				$response['option_id'] = $option_id;
				$response['success'] = TRUE;

				$this->session->set_userdata('last_option', $option_id);
			}

			$this->_clean_cache();
			return json_encode($response);
		}

		/**
		 * Збереження опції
		 */
		public function update()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$option_id = intval($this->input->post('option_id'));
			$name = $this->db->escape_str(strip_tags($this->input->post('name', TRUE)));
			$language = $this->input->post('language');
			$languages = $this->config->item('languages');

			if ($option_id > 0 AND isset($languages[$language]))
			{
				$this->load->model('admin_catalog_options_model');
				$this->load->helper('form');

				$set = array('name_' . $language => form_prep($name));
				$this->admin_catalog_options_model->update_option($set, array('option_id' => $option_id));

				$this->session->set_userdata('last_option', $option_id);
				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Приховування/відображення опції
		 */
		public function hidden()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$option_id = intval($this->input->post('option_id'));
			$status = intval($this->input->post('status'));

			if ($option_id > 0 AND in_array($status, array(0, 1)))
			{
				$this->load->model('admin_catalog_options_model');
				$this->admin_catalog_options_model->update_option(array('hidden' => $status), array('option_id' => $option_id));

				$this->session->set_userdata('last_option', $option_id);
				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Збереження порядку сортування опцій
		 */
		public function update_position()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$items = $this->input->post('items');
			$option_id = intval($this->input->post('option_id'));

			if (is_array($items) AND $option_id > 0)
			{
				$this->session->set_userdata('last_option', $option_id);

				$this->load->model('admin_catalog_options_model');
				$this->admin_catalog_options_model->update_position($items);

				$this->session->set_userdata('last_option', $option_id);
				$response['success'] = TRUE;
			}

			$this->_clean_cache();
			return json_encode($response);
		}

		/**
		 * Видалення опції
		 */
		public function delete()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$option_id = intval($this->input->post('option_id'));

			if ($option_id > 0)
			{
				$this->load->model('admin_catalog_options_model');
				$this->admin_catalog_options_model->delete_option($option_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Форма прив’язки опцій до компоненту
		 */
		public function component()
		{
			$this->init_model->is_admin('redirect');

			$component_id = intval($this->input->get('component_id'));
			$menu_id = intval($this->input->get('menu_id'));

			if ($component_id < 0 OR $menu_id < 0) show_404();

			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->template_lib->set_title('Прив’язка опцій до компоненту');
			$this->template_lib->set_admin_menu_active('components', 'top_level');
			$this->template_lib->set_admin_menu_active('', 'sub_level');

			$this->template_lib->set_bread_crumbs('', 'Прив’язка опцій до компоненту');

			$this->load->model('admin_catalog_options_model');

			$component_options = $this->admin_catalog_options_model->get_component_options($component_id);

			$tpl_data = array(
				'component_options' => $component_options,
				'options' => $this->admin_catalog_options_model->get_options(LANG),
				'component_id' => $component_id,
				'menu_id' => $menu_id,
				'return_link' => $this->init_model->get_link($menu_id, '{URL}'),
			);
			$this->template_lib->set_content($this->load->view('admin/select_list_tpl', $tpl_data, TRUE));
		}

		/**
		 * Прив’язки опцій до компоненту
		 */
		public function component_save()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$component_id = intval($this->input->post('component_id'));
			$options = $this->input->post('options');

			if (!is_array($options)) $options = array();

			if ($component_id > 0)
			{
				$this->load->model('admin_catalog_options_model');
				$this->admin_catalog_options_model->save_component_options($component_id, $options);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Очистка кешу
		 */
		public function _clean_cache()
		{
			$this->cache->clean();
		}
	}