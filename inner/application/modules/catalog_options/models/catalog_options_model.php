<?php

	if (!defined('BASEPATH')) exit('No direct script access allowed');

	class Options_model extends CI_Model {

		public function total_options($component_id)
		{
			return $this->db->where('parent_id', 0)->where('component_id', $component_id)->count_all_results('options');
		}

		public function get_options($product_id)
		{
			$options = array();
			$parents = array();

			$this->db->select('catalog_product_bind_options.option_id, catalog_product_bind_options.price, catalog_product_bind_options.price, options.parent_id, options.name_' . LANG . ' as name, options.image');
			$this->db->join('options', 'catalog_product_bind_options.option_id = options.option_id');
			$this->db->where('catalog_product_bind_options.product_id', $product_id);
			$this->db->where('options.hidden', 0);
			$this->db->order_by('options.position', 'asc');

			$result = $this->db->get('catalog_product_bind_options')->result_array();

			if (count($result) > 0)
			{
				$_options = array();

				foreach ($result as $row)
				{
					$parents[] = $row['parent_id'];
					$_options[$row['parent_id']][] = array(
						'option_id' => $row['option_id'],
						'price' => $row['price'],
						'name' => $row['name'],
						'image' => $row['image'],
					);
				}

				$this->db->select('option_id, name_' . LANG . ' as name');
				$this->db->where_in('option_id', $parents);
				$this->db->where('hidden', 0);
				$this->db->order_by('options.position');

				$result = $this->db->get('options')->result_array();

				if (count($result) > 0)
				{
					foreach ($result as $row)
					{
						$options[] = array(
							'option_id' => $row['option_id'],
							'name' => $row['name'],
							'children' => $_options[$row['option_id']]
						);
					}
				}
			}

			//echo '<pre>' . print_r($options, TRUE) . '</pre>';
			//exit;

			return $options;
		}

	}