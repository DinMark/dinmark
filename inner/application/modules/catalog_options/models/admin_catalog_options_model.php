<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_catalog_options_model extends CI_Model
	{
		/**
		 * Отримання списку опцій
		 *
		 * @param null $language
		 *
		 * @return array
		 */
		public function get_options($language)
		{
			$options = array();

			$this->db->select('option_id, parent_id, hidden, position, name_' . DEF_LANG . ' as def_name, name_' . $language . ' as name, image');
			$this->db->order_by('position');

			$result = $this->db->get('catalog_options')->result_array();
			foreach ($result as $v) $options[$v['parent_id']][] = $v;

			return $options;
		}

		/**
		 * Додавання нової опції
		 *
		 * @param int $parent_id
		 *
		 * @return int
		 */
		public function option_add($parent_id)
		{
			$this->db->insert('catalog_options', array('parent_id' => $parent_id));
			$option_id = $this->db->insert_id();

			$this->load->dbutil();
			$this->dbutil->optimize_table($this->db->dbprefix . 'catalog_options');

			return $option_id;
		}

		/**
		 * Збереження порядку сортування опцій
		 *
		 * @param $items
		 */
		public function update_position($items)
		{
			if (is_array($items))
			{
				$set = array();

				foreach ($items as $val)
				{
					if (isset($val['option_id']) AND $val['option_id'] > 0)
					{
						$set[] = array(
							'option_id' => $val['option_id'],
							'parent_id' => intval($val['parent_id']),
							'position' => intval($val['position'])
						);
					}

					if (count($set) == 50) {
						$this->db->update_batch('catalog_options', $set, 'option_id');
						$set = array();
					}
				}

				if (count($set) > 0) $this->db->update_batch('catalog_options', $set, 'option_id');
			}
		}

		/**
		 * Отримання інформації про опцію
		 *
		 * @param int $option_id
		 *
		 * @return array
		 */
		public function get_option($option_id)
		{
			return $this->db->get_where('catalog_options', array('option_id' => $option_id))->row_array();
		}

		/**
		 * Оновлення інформації про опцію
		 *
		 * @param $set
		 * @param $where
		 */
		public function update_option($set, $where)
		{
			$this->db->update('catalog_options', $set, $where);
		}

		/**
		 * Видалення опції
		 *
		 * @param int|array $option_id
		 */
		public function delete_option($option_id)
		{
			if (!is_array($option_id)) $option_id = array($option_id);

			foreach ($option_id as $v)
			{
				$this->db->delete('catalog_options', array('option_id' => $v));
				$this->db->delete('catalog_bind_options', array('option_id' => $v));

				//$dir = ROOT_PATH . 'upload/catalog_options/' . $v . '/';
				//if (file_exists($dir)) delete_files($dir, TRUE, FALSE, 1);

				$result = $this->db->select('option_id')->where('parent_id', $v)->get('catalog_options')->result_array();

				if (count($result) > 0)
				{
					$options = array();
					foreach ($result as $_v) $options[] = $_v['option_id'];

					$this->delete_option($options);
				}
			}
		}

		/**
		 * Отримання опцій прив’язаних до компоненту
		 *
		 * @param int $component_id
		 * @return array
		 */
		public function get_component_options($component_id)
		{
			$options = array();

			$result = $this->db->select('option_id')->where('component_id', $component_id)->get('catalog_bind_options')->result_array();
			foreach ($result as $v) $options[] = $v['option_id'];

			return $options;
		}

		/**
		 * Збереження/видалення опцій компоненту
		 *
		 * @param int $component_id
		 * @param array $options
		 */
		public function save_component_options($component_id, $options)
		{
			$component_options = $this->get_component_options($component_id);

			if (count($component_options) > 0)
			{
				$delete_rows = array_diff($component_options, $options);

				if (count($delete_rows) > 0)
				{
					foreach ($delete_rows as $v)
					{
						$this->db->delete('catalog_bind_options', array('option_id' => intval($v), 'component_id' => $component_id));
					}
				}
			}

			if (count($options) > 0)
			{
				foreach ($options as $v)
				{
					$c = $this->db->where('option_id', intval($v))->where('component_id', $component_id)->count_all_results('catalog_bind_options');
					if ($c == 0) $this->db->insert('catalog_bind_options', array('option_id' => intval($v), 'component_id' => $component_id));
				}
			}

			$this->load->dbutil();
			$this->dbutil->optimize_table($this->db->dbprefix . 'catalog_bind_options');
		}

		/**
		 * Отримання меню опцій для форми редагування товару
		 *
		 * @param int $component_id
		 * @return array
		 */
		public function get_options_menu($component_id)
		{
			$options = array();

			$this->db->select('catalog_bind_options.option_id, catalog_options.parent_id, catalog_options.name_' . LANG . ' as name, parent_option.name_' . LANG . ' as parent_name');
			$this->db->join('catalog_options', 'catalog_options.option_id=catalog_bind_options.option_id');
			$this->db->join('catalog_options as parent_option', 'parent_option.option_id=catalog_options.parent_id');
			$this->db->where_in('catalog_bind_options.component_id', $component_id);
			$this->db->order_by('catalog_options.position', 'asc');

			$result = $this->db->get('catalog_bind_options')->result_array();
			foreach ($result as $v)
			{
				$options[$v['parent_id']]['name'] = $v['parent_name'];

				$options[$v['parent_id']]['childs'][] = array(
					'option_id' => $v['option_id'],
					'name' => $v['name'],
				);
			}

			return $options;
		}
	}
