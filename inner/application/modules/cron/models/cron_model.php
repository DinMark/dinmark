<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Cron_model
	 */
	class Cron_model extends CI_Model {
		/**
		 * Перевірка на наявність області від Нової пошти
		 *
		 * @param array $data
		 */
		public function check_np_area(array $data)
		{
			$c = (int)$this->db->where('ref', $data['ref'])->count_all_results('np_areas');

			$this->db->set($data);

			if ($c === 0) {
				$this->db->insert('np_areas');
			} else {
				$this->db
					->where('ref', $data['ref'])
					->update('np_areas');
			}
		}

		/**
		 * Перевірка на наявність міста від Нової пошти
		 *
		 * @param array $data
		 */
		public function check_np_city(array $data)
		{
			$c = (int)$this->db->where('ref', $data['ref'])->count_all_results('np_cities');

			$this->db->set($data);

			if ($c === 0) {
				$this->db->insert('np_cities');
			} else {
				$this->db
					->where('ref', $data['ref'])
					->update('np_cities');
			}
		}

		/**
		 * Отримання міст від Нової пошти
		 */
		public function get_np_cities()
		{
			return $this->db
				->get('np_cities')
				->result_array();
		}

		/**
		 * Перевірка на наявність складу від Нової пошти
		 *
		 * @param array $data
		 */
		public function check_np_warehouse(array $data)
		{
			$c = (int)$this->db->where('ref', $data['ref'])->count_all_results('np_warehouses');

			if ($c === 0) {
				$this->db->set($data);
				$this->db->insert('np_warehouses');
			} else {
//				$this->db
//					->where('ref', $data['ref'])
//					->update('np_warehouses');
			}
		}

		public function clean_np_warehouse()
		{
			$this->db
				->where('flag', 0)
				->delete('np_warehouses');

			$this->db
				->set('flag', 0)
				->where('flag', 1)
				->update('np_warehouses');
		}

		public function catalog_new()
		{
			$this->db
				->set('status', 0)
				->where('status', 2)
				->update('catalog');

			$this->db
				->set('status', 2)
				->order_by('product_id', 'desc')
				->limit(30)
				->update('catalog');
		}
	}