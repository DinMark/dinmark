<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	set_time_limit(0);
	ini_set('memory_limit', '512M');

	/**
	 * @property Admin_seo_model $admin_seo_model
	 * @property Admin_catalog_model $admin_catalog_model
	 */
	class Cron extends MX_Controller
	{

		public function sitemap()
		{
			$this->load->helper('file');
			$this->load->model('seo/admin_seo_model');
			$this->admin_seo_model->update_xml($this->config->item('languages'), $this->config->item('multi_languages'));
			exit;
		}

		public function sale_out()
		{
			$this->load->model('catalog/admin_catalog_model');
			$this->admin_catalog_model->sale_out();
			exit;
		}

		public function status_out()
		{
			$this->load->model('catalog/admin_catalog_model');
			$this->admin_catalog_model->status_out();
			exit;
		}

		public function catalog_new()
		{
			$this->load->model('cron_model');
			$this->cron_model->catalog_new();
		}

		public function exchange()
		{
			if ($this->config->item('auto_exchange') == 1)
			{
				$ch = curl_init('https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5');
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
				curl_exec($ch);
				$result = curl_exec($ch);
				curl_close($ch);

				if ($result)
				{
					$xml = simplexml_load_string($result);

					if (isset($xml->row[0]))
					{
						$vars = array();

						foreach ($xml->row as $v)
						{
							$vars[mb_strtolower(strval($v->exchangerate->attributes()->ccy))] = floatval($v->exchangerate->attributes()->sale);
						}

						$this->load->model('config/admin_config_model');
						$this->admin_config_model->save_exchnage($vars);

						if (!isset($vars['usd'])) $vars['usd'] = 0;
						if (!isset($vars['eur'])) $vars['eur'] = 0;
						//if (!isset($vars['rur'])) $vars['rur'] = 0;

						$this->admin_config_model->change_prices($vars['usd'], $vars['eur'], $vars['rur']);

						$this->cache->delete('db_config');

						$response['success'] = TRUE;
						$response['usd'] = $vars['usd'];
						$response['eur'] = $vars['eur'];
						//$response['rur'] = $vars['rur'];
					}
				}
			}

			exit;
		}

		public function vl() {
			$r = $this->db->select('variant_id, menu_id, component_id, url_' . LANG . ' as url')->get('catalog_variants')->result_array();

			foreach ($r as $v) {
				$c = $this->db->where(array('item_id' => $v['variant_id'], 'method' => 'variant'))->count_all_results('site_links');

				if ($c === 0) {
					$this->db->insert(
						'site_links',
						array(
							'item_id' => $v['variant_id'],
							'component_id' => $v['component_id'],
							'menu_id' => $v['menu_id'],
							'module' => 'catalog',
							'method' => 'variant',
							'hash_' . LANG => md5($v['url']),
						)
					);
				} else {
					$this->db->update(
						'site_links',
						array(
							'hash_' . LANG => md5($v['url']),
						),
						array(
							'item_id' => $v['variant_id'],
							'module' => 'catalog',
							'method' => 'variant',
						)
					);
				}
			}
			exit;
		}

		public function nl() {
			$r = $this->db->select('news_id, menu_id, component_id, url_' . LANG . ' as url')->get('news')->result_array();

			foreach ($r as $v) {
				$c = $this->db->where(array('item_id' => $v['news_id'], 'module' => 'news'))->count_all_results('site_links');

				if ($c === 0) {
					$this->db->insert(
						'site_links',
						array(
							'item_id' => $v['news_id'],
							'component_id' => $v['component_id'],
							'menu_id' => $v['menu_id'],
							'module' => 'news',
							'method' => 'details',
							'hash_' . LANG => md5($v['url']),
						)
					);
				} else {
					$this->db->update(
						'site_links',
						array(
							'hash_' . LANG => md5($v['url']),
						),
						array(
							'item_id' => $v['news_id'],
							'module' => 'news',
							'method' => 'details',
						)
					);
				}
			}
			exit;
		}

		/**
		 * Оновлення списка областей нової пошти
		 */
		public function np_areas()
		{
			$this->load->library(
				'Np_lib',
				array(
					'key' => $this->config->item('np_key'),
				)
			);

			$result = $this->np_lib->get_areas();

			if (isset($result['data'][0])) {
				$this->load->model('cron_model');

				foreach ($result['data'] as $v) {
					$this->cron_model->check_np_area(
						array(
							'ref' => $v['Ref'],
							'name_ua' => $v['Description'],
							'areas_center' => $v['AreasCenter'],
						)
					);
				}
			}

			exit;
		}

		/**
		 * Оновлення списка міст нової пошти
		 */
		public function np_cities()
		{
			$this->load->library(
				'Np_lib',
				array(
					'key' => $this->config->item('np_key'),
				)
			);

			$result = $this->np_lib->get_cities();

			if (isset($result['data'][0])) {
				$this->load->model('cron_model');

				foreach ($result['data'] as $v) {
					$this->cron_model->check_np_city(
						array(
							'ref' => form_prep($v['Ref']),
							'area_ref' => form_prep($v['Area']),
							'city_id' => (int)$v['CityID'],
							'name_ua' => form_prep($v['Description']),
							'name_ru' => form_prep($v['DescriptionRu']),
						)
					);
				}
			}

			exit;
		}

		/**
		 * Оновлення списка складів нової пошти
		 */
		public function np_warehouses()
		{
			$this->load->library(
				'Np_lib',
				array(
					'key' => $this->config->item('np_key'),
				)
			);

			$this->load->model('cron_model');

			$cities = $this->cron_model->get_np_cities();

			foreach ($cities as $city) {
				$result = $this->np_lib->get_warehouses($city['ref']);

				if (isset($result['data'][0])) {
					$this->load->model('cron_model');

					foreach ($result['data'] as $v) {
						$weight = array();

						if ((float)$v['TotalMaxWeightAllowed'] > 0) {
							$weight[] = (float)$v['TotalMaxWeightAllowed'];
						}

						if ((float)$v['PlaceMaxWeightAllowed'] > 0) {
							$weight[] = (float)$v['PlaceMaxWeightAllowed'];
						}

						$weight = count($weight) > 0 ? min($weight) : 0;

						$this->cron_model->check_np_warehouse(
							array(
								'ref' => form_prep($v['Ref']),
								'city_ref' => $city['ref'],
								'name_ua' => form_prep($v['Description']),
								'name_ru' => form_prep($v['DescriptionRu']),
								'weight' => $weight,
								'flag' => 1,
							)
						);
					}
				}
			}

			//$this->cron_model->clean_np_warehouse();
		}

//		public function clean()
//		{
//			$r = $this->db
//					->select('c.product_id')
//					->select('(select count(*) from `' . $this->db->dbprefix('catalog_images') . '` as cp where c.product_id=cp.product_id) as photos', false)
//					->get('catalog as c')
//					->result_array();
//
//			foreach ($r as $v) {
//				if ($v['photos'] > 1) {
//					$_r = $this->db
//						->select('image_id, image')
//						->where('product_id', $v['product_id'])
//						->order_by('position')
//						->get('catalog_images')
//						->result_array();
//
//					foreach ($_r as $_k => $_v) {
//						if ($_k > 0) {
//							$file = ROOT_PATH . 'upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/' . $_v['image'];
//							if (file_exists($file)) {
//								unlink($file);
//							}
//
//							$file = ROOT_PATH . 'upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/t_' . $_v['image'];
//							if (file_exists($file)) {
//								unlink($file);
//							}
//
//							$file = ROOT_PATH . 'upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/s_' . $_v['image'];
//							if (file_exists($file)) {
//								unlink($file);
//							}
//
//							$this->db
//								->where('image_id', $_v['image_id'])
//								->delete('catalog_images');
//						}
//					}
//				}
//			}

//			$r = $this->db
//				->select('sl.*')
//				->select('(select count(*) from `' . $this->db->dbprefix('catalog_variants') . '` as cv where sl.item_id=cv.variant_id) as variant', false)
//				->where('sl.module', 'catalog')
//				->where('sl.method', 'variant')
//				->get('site_links as sl')
//				->result_array();
//
//			foreach ($r as $v) {
//				if ($v['variant'] < 1) {
//					$this->db
//						->where('link_id', $v['link_id'])
//						->delete('site_links');
//				}
//			}
//

//			$this->load->helper('translit');
//
//			$r = $this->db
//				->select('c.product_id, c.title_ua, c.title_ru')
//				->get('catalog as c')
//				->result_array();
//
//			foreach ($r as $v) {
//				$url_ua = translit($v['title_ua']);
//				$url_ru = translit($v['title_ru']);
//
//				$this->db
//					->set('url_ua', $url_ua)
//					->set('url_ru', $url_ru)
//					->where('product_id', $v['product_id'])
//					->update('catalog');
//
//				$this->db
//					->set('hash_ua', md5($url_ua))
//					->set('hash_ru', md5($url_ru))
//					->where('item_id', $v['product_id'])
//					->where('module', 'catalog')
//					->where('method', 'details')
//					->update('site_links');
//			}

//			exit;
//		}

		public function text1c() {
			error_reporting(E_ALL);
			ini_set('display_errors', true);

			ini_set('soap.wsdl_cache_enabled', 0);

			try {
				$client = new SoapClient(
					'http://138.201.140.166:8081/Dinmark/ws/webexchange.1cws?wsdl',
					array(
						'login' => 'Exchange',
						'password' => 'Exchange_7891',
						'trace' => 1,
					)
				);

				echo '<pre>';
				var_dump($client->__getFunctions());
				echo '</pre><hr>';

				$response = $client->Export_Groups();

//				$response = $client->Export_products(array(
//					'BeginID' => 350,
//					'EndID' => 359,
//				));

//				$response = $client->Export_Clients();

				echo '<pre>';
				print_r($response);
				echo '</pre>';

				// $template_data['soap_response'] = $client->GetDetails($params)->return;
			} catch (SoapFault $e) {
				echo 'Error:<hr><pre>';
				var_dump($e->getMessage());
				echo '</pre>';
			}

			exit;
		}
	}