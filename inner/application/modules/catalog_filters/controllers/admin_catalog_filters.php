<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_catalog_filters
	 *
	 * @property Admin_catalog_filters_model $admin_catalog_filters_model
	 */

	class Admin_catalog_filters extends MX_Controller
	{
		/**
		 * Список фільтрів
		 *
		 * @return void
		 */
		public function index()
		{
			$this->init_model->is_admin('redirect');

			$this->template_lib->set_title('Управління фільтрами товарів каталогу');

			$this->template_lib->set_admin_menu_active('catalog_config');
			$this->template_lib->set_admin_menu_active('catalog_filters', 'sub_level');

			$menu_id = intval($this->input->get('menu_id'));
			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->load->model('admin_catalog_filters_model');

			$template_data = array(
				'languages' => $this->config->item('languages'),
				'menu_id' => $menu_id,
				'last' => intval($this->session->userdata('last_filter')),
			);

			$_template_data = array('filters' => $this->admin_catalog_filters_model->get_filters(LANG));
			$template_data['filters'] = $this->load->view('admin/list_tpl', $_template_data, TRUE);

			$this->template_lib->set_content($this->load->view('admin/filters_tpl', $template_data, TRUE));
		}

		/**
		 * Завантаження списку фільтрів через ajax запит
		 *
		 * @return string
		 */
		public function filters_load()
		{
			$this->init_model->is_admin('json');

			$this->load->model('admin_catalog_filters_model');

			$template_data = array(
				'filters' => $this->admin_catalog_filters_model->get_filters($this->input->post('language'))
			);

			$response = array(
				'success' => TRUE,
				'filters_list' => $this->load->view('admin/list_tpl', $template_data, TRUE),
			);

			return json_encode($response);
		}

		/**
		 * Додавання фільтру
		 */
		public function insert()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$parent_id = intval($this->input->post('parent_id'));

			if ($parent_id >= 0)
			{
				$this->load->model('admin_catalog_filters_model');

				$filter_id = $this->admin_catalog_filters_model->filter_add($parent_id);

				$response['filter_id'] = $filter_id;
				$response['success'] = TRUE;

				$this->session->set_userdata('last_filter', $filter_id);
			}

			$this->_clean_cache();
			return json_encode($response);
		}

		/**
		 * Збереження фільтру
		 */
		public function update()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$filter_id = intval($this->input->post('filter_id'));
			$name = $this->db->escape_str(strip_tags($this->input->post('name', TRUE)));
			$language = $this->input->post('language');
			$languages = $this->config->item('languages');

			if ($filter_id > 0 AND isset($languages[$language]))
			{
				$this->load->model('admin_catalog_filters_model');
				$this->load->helper('form');

				$set = array('name_' . $language => form_prep($name));
				$this->admin_catalog_filters_model->update_filter($set, array('filter_id' => $filter_id));

				$this->session->set_userdata('last_filter', $filter_id);
				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Приховування/відображення фільтру
		 */
		public function hidden()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$filter_id = intval($this->input->post('filter_id'));
			$status = intval($this->input->post('status'));

			if ($filter_id > 0 AND in_array($status, array(0, 1)))
			{
				$this->load->model('admin_catalog_filters_model');
				$this->admin_catalog_filters_model->update_filter(array('hidden' => $status), array('filter_id' => $filter_id));

				$this->session->set_userdata('last_filter', $filter_id);
				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Збереження порядку сортування фільтрів
		 */
		public function update_position()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);

			$items = $this->input->post('items');
			$filter_id = intval($this->input->post('filter_id'));

			if (is_array($items) AND $filter_id > 0)
			{
				$this->session->set_userdata('last_filter', $filter_id);
				
				$this->load->model('admin_catalog_filters_model');
				$this->admin_catalog_filters_model->update_position($items);

				$this->session->set_userdata('last_filter', $filter_id);
				$response['success'] = TRUE;
			}

			$this->_clean_cache();
			return json_encode($response);
		}

		/**
		 * Видалення фільтру
		 */
		public function delete()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$filter_id = intval($this->input->post('filter_id'));

			if ($filter_id > 0)
			{
				$this->load->model('admin_catalog_filters_model');
				$this->admin_catalog_filters_model->delete_filter($filter_id);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Форма прив’язки фільтрів до компоненту
		 */
		public function component()
		{
			$this->init_model->is_admin('redirect');

			$component_id = intval($this->input->get('component_id'));
			$menu_id = intval($this->input->get('menu_id'));

			if ($component_id < 0 OR $menu_id < 0) show_404();

			$this->init_model->set_menu_id($menu_id, TRUE);

			$this->template_lib->set_title('Прив’язка фільтрів до компоненту');
			$this->template_lib->set_admin_menu_active('components', 'top_level');
			$this->template_lib->set_admin_menu_active('', 'sub_level');

			$this->template_lib->set_bread_crumbs('', 'Прив’язка фільтрів до компоненту');

			$this->load->model('admin_catalog_filters_model');

			$component_filters = $this->admin_catalog_filters_model->get_component_filters($component_id);

			$tpl_data = array(
				'component_filters' => $component_filters,
				'filters' => $this->admin_catalog_filters_model->get_filters(LANG),
				'component_id' => $component_id,
				'menu_id' => $menu_id,
				'return_link' => $this->init_model->get_link($menu_id, '{URL}'),
			);
			$this->template_lib->set_content($this->load->view('admin/select_list_tpl', $tpl_data, TRUE));
		}

		/**
		 * Прив’язки фільтрів до компоненту
		 */
		public function component_save()
		{
			$this->init_model->is_admin('json');

			$response = array('success' => FALSE);
			$component_id = intval($this->input->post('component_id'));
			$filters = $this->input->post('filters');

			if (!is_array($filters)) $filters = array();

			if ($component_id > 0)
			{
				$this->load->model('admin_catalog_filters_model');
				$this->admin_catalog_filters_model->save_component_filters($component_id, $filters);

				$response['success'] = TRUE;
			}

			return json_encode($response);
		}

		/**
		 * Очистка кешу
		 */
		public function _clean_cache()
		{
			$this->cache->clean();
		}
	}