<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	$this->template_lib->set_js('admin/ui/jquery-ui-1.10.3.custom.min.js');
	$this->template_lib->set_js('admin/jquery.mjs.nestedSortable.js');

	//$this->template_lib->set_css('js/admin/fineuploader/fineuploader-3.5.0.css', TRUE);
	//$this->template_lib->set_js('admin/fineuploader/jquery.fineuploader-3.5.0.min.js');
?>
<div class="admin_component" id="admin_filters_component">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="filters"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Фільтри товарів каталогу</div></div>
			<a class="fm add add_filter" href="#"><b></b>Додати фільтр</a>
			<a class="fm add_bottom" href="#"><b class="active"></b>Додавати внизу</a>
		</div>
		<?php if (count($languages) > 1): ?>
		<div class="fmr component_lang">
			<?php foreach ($languages as $key => $val): ?>
			<a href="#" class="flags <?=$key;?><?=(($key == LANG) ? ' active' : '');?>" data-language="<?=$key;?>"><img src="img/flags_<?=$key;?>.png"></a>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
	<div class="fm admin_menu">
		<ul id="filters_list"><?=$filters;?></ul>
	</div>
</div>

<script type="text/javascript">

	var filter_min_level = 1,
		filter_max_level = 2,
		filter_add_bottom = 1,
		move_filter_id = <?=$last;?>;

	$(function() {

		$('#filter_<?=$last;?>').find('.last_edit').eq(0).addClass('active');

		var $component = $('#admin_filters_component'),
			$loader = $component.find('.component_loader'),
			$filters = $('#filters_list'),
			$languages = $component.find('.component_lang');

		function save_position() {

			component_loader_show($('.component_loader'), '');

			var items = [];

			$('#filter_' + move_filter_id).closest('ul').find('li').each(function (i, val) {
				items[i] = {
					filter_id: $(val).data('filter-id'),
					position: i,
					parent_id: $(val).data('parent-id'),
					level: $(val).data('level')
				}
			});

			$.post(
				'<?=$this->uri->full_url('admin/catalog_filters/update_position');?>',
				{
					filter_id: move_filter_id,
					items : items
				},
				function (response) {
					if (response.success) component_loader_hide($('.component_loader'), '');
				},
				'json'
			);
		}

		function init_sortable() {
			$filters.nestedSortable({
				forcePlaceholderSize: true,
				opacity: .4,
				tabSize: 5,
				listType : 'ul',
				handle: 'a.filter_sorter',
				items: 'li',
				toleranceElement: '> div',
				update : function (event, ui) {
					move_filter_id = $(ui.item).data('filter-id');

					$('.last_edit.active').removeClass('active');
					$('#filter_' + move_filter_id).find('.last_edit').eq(0).addClass('active');

					set_filters_data();
					save_position();
				},
				placeholder: "ui-state-highlight",
				axis : 'y',
				helper : 'clone',
				protectRoot : false,
				maxLevels : filter_max_level
			});
		}

		function set_filters_data() {

			$filters.find('li').map(function (i) {

				var level = $(this).parents('ul').length, parent_id = 0;
				if (level > 0) parent_id = $(this).parents('li').eq(0).data('filter-id');

				$(this)
					.data('level', level - 1).data('parent-id', parent_id)
					.find('.number').eq(0).text(i + 1)
					.end().end()
					.find('.step_left').css('visibility', 'hidden')
					.end()
					.find('.step_right').css('visibility', 'hidden')
					.end()
					.find('.double_step_right').css('visibility', 'hidden');

				if (i % 2 == 0) {
					$(this).addClass('grey');
				} else {
					$(this).removeClass('grey');
				}

				if (level > 1) {
					if ($(this).find('.auto').eq(0).find('b').length === 0) $(this).find('.auto').eq(0).prepend('<b></b>');
					$(this).find('.step_left').eq(0).css('visibility', 'visible');
				} else {
					$(this).find('.auto').eq(0).find('b').remove();
				}

				if ($(this).index() > 0) {
					$(this).find('.double_step_right').eq(0).css('visibility', 'visible');

					if ($(this).find('ul').length > 0 && ((level + 1) != filter_max_level)) $(this).find('.step_right').eq(0).css('visibility', 'visible');
				}

				if ($(this).find('ul').length > 0) {
					$(this).find('.filter_sorter').eq(0).addClass('double_arrows');
				} else {
					$(this).find('.filter_sorter').eq(0).removeClass('double_arrows');
				}

				if (level >= filter_max_level) {
					$(this)
						.find('.step_right').eq(0).css('visibility', 'hidden')
						.end().end()
						.find('.child_add').eq(0).addClass('no_active');

					if (level === filter_max_level) $(this).find('.double_step_right').eq(0).css('visibility', 'hidden');
					if (level > filter_max_level) $(this).find('.auto').eq(0).find('span').addClass('through');
				} else {
					$(this)
						.find('.child_add').eq(0).removeClass('no_active')
						.end()
						.find('.auto').eq(0).find('span').removeClass('through');
				}
			});

			init_sortable();
		}

		set_filters_data();

		$component
			.find('.component_lang').on('click', 'a', function (e) {
				e.preventDefault();

				component_loader_show($loader, '');

				var language = $(this).siblings().removeClass('active').end().addClass('active').data('language');

				$.post(
					'<?=$this->uri->full_url('admin/catalog_filters/filters_load');?>',
					{
						language : language
					},
					function (response) {
						if (response.success) {

							$filters.html(response.filters_list);

							$('#filter_' + move_filter_id).find('.last_edit').eq(0).addClass('active');
							set_filters_data();
							component_loader_hide($loader, '');
						}
					},
					'json'
				);
			})
			.end()
			.on('click', '.add_bottom', function (e) {
				e.preventDefault();

				$(this).find('b').toggleClass('active');

				if ($(this).find('b').hasClass('active')) {
					filter_add_bottom = 1;
				} else {
					filter_add_bottom = 0;
				}
			})
			.on('click', '.add_filter', function (e) {
				e.preventDefault();

				component_loader_show($loader, '');

				$.post(
					'<?=$this->uri->full_url('admin/catalog_filters/insert');?>',
					{
						parent_id: 0
					},
					function (response) {
						if (response.success) {
							move_filter_id = response.filter_id;
							$('.last_edit').removeClass('active');

							var item = $('#filter_template').html();
							item = item.split('%filter_id%').join(response.filter_id);

							filter_add_bottom === 0 ? $filters.prepend(item) : $filters.append(item);

							$.when(set_filters_data()).then(save_position());
						}
					},
					'json'
				);
			});

		$filters
			.on('click', '.up_add', function (e) {
				e.preventDefault();

				component_loader_show($loader, '');

				var $link = $(this),
					$li = $link.closest('li'),
					parent_id = $link.parents('li').eq(1).length > 0 ? $link.parents('li').eq(1).data('menu-id') : 0;

				$.post(
					'<?=$this->uri->full_url('admin/catalog_filters/insert');?>',
					{
						parent_id: parent_id
					},
					function (response) {
						if (response.success) {
							move_filter_id = response.filter_id;
							$('.last_edit').removeClass('active');

							var item = $('#filter_template').html();
							item = item.split('%filter_id%').join(response.filter_id);

							$li.before(item);

							$.when(set_filters_data()).then(save_position());
						}
					},
					'json'
				);
			})
			.on('click', '.child_add', function (e) {
				e.preventDefault();

				component_loader_show($loader, '');

				var $link = $(this),
					$li = $link.closest('li');

				$.post(
					'<?=$this->uri->full_url('admin/catalog_filters/insert');?>',
					{
						filter_id: $li.data('filter-id')
					},
					function (response) {
						if (response.success) {
							move_filter_id = response.filter_id;
							$('.last_edit').removeClass('active');

							if ($li.find('ul').length === 0) $li.append('<ul></ul>');

							var item = $('#filter_template').html();
							item = item.split('%filter_id%').join(response.filter_id);

							filter_add_bottom === 0 ? $li.find('ul').eq(0).prepend(item) : $li.find('ul').eq(0).append(item);

							$.when(set_filters_data()).then(save_position());
						}
					},
					'json'
				);
			})
			.on('click', '.down_add', function (e) {
				e.preventDefault();

				component_loader_show($loader, '');

				var $link = $(this),
					$li = $link.closest('li'),
					parent_id = $link.parents('li').eq(1).length > 0 ? $link.parents('li').eq(1).data('filter-id') : 0;

				$.post(
					'<?=$this->uri->full_url('admin/catalog_filters/insert');?>',
					{
						parent_id : parent_id
					},
					function (response) {
						if (response.success) {
							move_filter_id = response.filter_id;
							$('.last_edit').removeClass('active');

							var item = $('#filter_template').html();
							item = item.split('%filter_id%').join(response.filter_id);

							$li.after(item);

							$.when(set_filters_data()).then(save_position());
						}
					},
					'json'
				);
			})
			.on('click', '.edit', function (e) {
				e.preventDefault();

				var $holder = $(this).closest('.holder'), item;

				if (!$(this).hasClass('active')) {
					item = $holder.find('.auto').find('span.menu_item').text();
					if (item == 'Новий фільтр') item = '';

					$holder.find('.auto').find('span.menu_item').html('<input type="text" name="item" value="' + item + '" placeholder="Введіть назву фільтру товару">');
					$holder.find('.auto').find('span.menu_item').find('input').focus();

					$(this).addClass('active');
				} else {
					$(this).removeClass('active');

					$filters.find('.last_edit.active').removeClass('active');
					$holder.find('.last_edit').addClass('active');

					component_loader_show($('.component_loader'), '');

					item = $holder.find('.auto').find('input').eq(0).val();

					$.post(
						'<?=$this->uri->full_url('admin/catalog_filters/update');?>',
						{
							filter_id : $holder.closest('li').data('filter-id'),
							name : item,
							language : $languages.find('.active').length > 0 ? $languages.find('.active').data('language') : '<?=LANG;?>'
						},
						function (response) {
							if (response.success) {
								$holder.find('.auto').find('span.menu_item').text(item);
								component_loader_hide($('.component_loader'), '');
							}
						},
						'json'
					);
				}
			})
			.on('keydown', '.auto input', function (e) {
				if (e.keyCode == 13) {
					var $holder = $(this).closest('.holder'),
						$link = $holder.find('.edit'), item;

					$filters.find('.last_edit.active').removeClass('active');
					$holder.find('.last_edit').addClass('active');

					$link.removeClass('active');
					component_loader_show($('.component_loader'), '');

					item = $(this).val();

					$.post(
						'<?=$this->uri->full_url('admin/catalog_filters/update');?>',
						{
							filter_id : $holder.closest('li').data('filter-id'),
							name : item,
							language : $languages.find('.active').length > 0 ? $languages.find('.active').data('language') : '<?=LANG;?>'
						},
						function (response) {
							if (response.success) {
								$holder.find('.auto').find('span.menu_item').text(item);
								component_loader_hide($('.component_loader'), '');
							}
						},
						'json'
					);
				}
			})
			.on('click', '.hide-show', function (e) {
				e.preventDefault();

				var $holder = $(this).closest('.holder'),
					status = 0;

				if (!$holder.hasClass('hidden')) status = 1;

				$holder.toggleClass('hidden');
				$(this).toggleClass('active');

				$filters.find('.last_edit.active').removeClass('active');
				$holder.find('.last_edit').addClass('active');

				component_loader_show($('.component_loader'), '');

				$.post(
					'<?=$this->uri->full_url('admin/catalog_filters/hidden');?>',
					{
						filter_id : $holder.closest('li').data('filter-id'),
						status : status
					},
					function (response) {
						if (response.success) component_loader_hide($('.component_loader'), '');
					},
					'json'
				);
			})
			.on('click', '.step_left', function (e) {
				e.preventDefault();

				var $li = $(this).closest('li'),
					$ul = $(this).closest('ul'),
					$li_prev = $(this).parents('li').eq(1),
					$li_clone = $li.clone();

				move_filter_id = $li.data('filter-id');

				$li.remove();
				$li_prev.after($li_clone);

				if ($ul.find('li').length === 0) $li_prev.find('ul').remove();

				$filters.find('.last_edit.active').removeClass('active');
				$li_clone
					.find('.holder').removeClass('active')
					.end()
					.find('.last_edit').eq(0).addClass('active')
					.end().end()
					.find('.add_items').eq(0).hide()
					.end().end()
					.find('.number').eq(0).show();

				set_filters_data();
				save_position();
			})
			.on('click', '.step_right', function (e) {
				e.preventDefault();

				var $parent = $(this).closest('li'),
					$parent_clone = $parent.clone(),
					$prev_item = $parent.prev('li');

				move_filter_id = $parent.data('filter-id');
				$parent.remove();

				$filters.find('.last_edit.active').removeClass('active');
				$parent_clone.find('.last_edit').eq(0).addClass('active');

				if ($prev_item.find('ul').length > 0) {
					$prev_item.find('ul').eq(0).append($parent_clone);
				} else {
					$prev_item.append('<ul></ul>');
					$prev_item.find('ul').eq(0).append($parent_clone);
				}

				set_filters_data();
				save_position();
			})
			.on('click', '.double_step_right', function (e) {
				e.preventDefault();

				var $childs = $(this).closest('li').find('ul').length > 0 ? $(this).closest('li').find('ul').html() : '';
				$(this).closest('li').find('ul').remove();

				var $parent = $(this).closest('li').prev('li'),
					$clone = $(this).closest('li').clone();

				move_filter_id = $(this).closest('li').data('filter-id');
				$(this).closest('li').remove();

				$filters.find('.last_edit.active').removeClass('active');
				$clone.find('.last_edit').eq(0).addClass('active');

				if ($parent.find('ul').length === 0) $parent.append('<ul></ul>');
				$parent.find('ul').eq(0).append($clone);
				if ($childs != '') $parent.find('ul').eq(0).append($childs);

				set_filters_data();
				save_position();
			})
			.on('click', '.delete', function (e) {
				e.preventDefault();

				var $ul = $(this).closest('ul'),
					$li = $(this).closest('li');

				confirmation('Видалити фільтр?<br><span>Якщо є дочірні фільтри, то вони також будуть видалені!</span>', function () {

					component_loader_show($('.component_loader'), '');

					$.post(
						'<?=$this->uri->full_url('admin/catalog_filters/delete');?>',
						{
							filter_id : $li.data('filter-id')
						},
						function (response) {
							if (response.success) {
								$li.remove();

								if ($ul.find('li').length === 0) {
									$ul.remove();
								} else {
									set_filters_data();
								}

								component_loader_hide($('.component_loader'), '');
							}
						},
						'json'
					);
				});
			})
			.on('click', '.filter_sorter', function (e) {
				e.preventDefault();
			})
			.on('mouseenter', '.holder', function () {
				$(this)
					.addClass('active').find('.number_cell').find('.number').hide()
					.end()
					.find('.add_items').show();
			})
			.on('mouseleave', '.holder', function () {
				$(this)
					.removeClass('active').find('.number_cell').find('.add_items').hide()
					.end()
					.find('.number').show();
			});
	});
</script>

<script type="text/template" id="filter_template">
	<li id="filter_%filter_id%" data-filter-id="%filter_id%">
		<div class="holder">
			<div class="cell last_edit active"></div>
			<div class="cell w_20 icon">
				<a href="#" class="hide-show active"></a>
			</div>
			<div class="cell w_30 number_cell">
				<div class="number"></div>
				<div class="add_items">
					<a href="#" class="up_add"></a>
					<a href="#" class="child_add"></a>
					<a href="#" class="down_add"></a>
				</div>
			</div>
			<div class="cell w_20 icon">
				<a href="#" class="edit"></a>
			</div>
			<div class="cell auto">
				<span class="menu_item">Новий фільтр</span>
			</div>
			<div class="cell w_70 icon no_padding">
				<div class="fm step_left">
					<a href="#"></a>
				</div>
				<div class="fm step_right">
					<a href="#"></a>
				</div>
				<div class="fm double_step_right">
					<a href="#"></a>
				</div>
			</div>
			<div class="cell w_20 icon">
				<a href="#" class="single_arrows filter_sorter"></a>
			</div>
			<div class="cell w_20 icon">
				<a href="#" class="delete"></a>
			</div>
		</div>
	</li>
</script>