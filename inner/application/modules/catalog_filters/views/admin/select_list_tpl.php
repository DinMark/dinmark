<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	$this->template_lib->set_js('admin/jquery.form.js');
?>
<div class="admin_component" id="admin_filters_component">
	<div class="component_loader"><span></span></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="filter"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Прив’язка фільтрів до компоненту</div></div>
			<a class="fm save" href="#"><b></b>Зберегти</a>
			<a class="fm apply" href="#"><b></b>Застосувати</a>
			<a class="fm cancel" href="<?=$return_link;?>"><b></b>Повернутись до списку товарів</a>
		</div>
	</div>
	<form id="filters_form" action="<?=$this->uri->full_url('admin/catalog_filters/component_save');?>" method="post" autocomplete="off">
		<input type="hidden" name="component_id" value="<?=$component_id;?>">
		<div class="fm admin_menu">
			<?php if (isset($filters[0])): ?>
				<ul id="filters_list">
					<?php foreach ($filters[0] as $v): ?>
						<li>
							<div class="holder">
								<div class="cell auto"><span class="menu_item"><?=$v['name'];?></span></div>
							</div>
							<?php if (isset($filters[$v['filter_id']])): ?>
								<ul>
									<?php foreach ($filters[$v['filter_id']] as $_v): ?>
										<li>
											<div class="holder">
												<div class="cell w_20">
													<div class="controls">
														<label for="filter_<?=$_v['filter_id'];?>" class="check_label">
															<i></i>
															<input type="checkbox" id="filter_<?=$_v['filter_id'];?>" name="filters[]" value="<?=$_v['filter_id'];?>"<?=in_array($_v['filter_id'], $component_filters) ? ' checked="checked"' : '';?>></label>
													</div>
												</div>
												<div class="cell auto"><span class="menu_item"><?=$_v['name'];?></span></div>
											</div>
										</li>
									<?php endforeach; ?>
								</ul>
							<?php endif; ?>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</div
	</form>
	<div class="fm for_sucsess">
		<div class="fmr save_links">
			<a href="#" class="fm save_adm"><b></b>Зберегти</a>
			<a href="#" class="fm apply_adm"><b></b>Застосувати</a>
			<a href="<?=$return_link;?>" class="fm cansel_adm"><b></b>Повернутись до списку товарів</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {
		var $filters = $('#filters_list');

		$filters
			.on('mouseenter', '.holder', function () { $(this).addClass('active'); })
			.on('mouseleave', '.holder', function () { $(this).removeClass('active'); })
			.find('li:even').addClass('grey');

		/**
		 * Відправка форми
		 */
		$('.apply_adm').add('.component_edit_links .apply').on('click', function (e) {
			e.preventDefault();

			component_loader_show($('.component_loader'), '');

			$('#filters_form').ajaxSubmit({
				success: function (response) {
					if (response.success) component_loader_hide($('.component_loader'), '');
				},
				dataType: 'json'
			});
		});

		$('.save_adm').add('.component_edit_links .save').on('click', function (e) {
			e.preventDefault();

			component_loader_show($('.component_loader'), '');

			$('#filters_form').ajaxSubmit({
				success: function (response) {
					if (response.success) window.location.href = '<?=$return_link;?>';
				},
				dataType: 'json'
			});
		});
	});
</script>