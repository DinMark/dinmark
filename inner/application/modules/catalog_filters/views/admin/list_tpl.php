<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	function build_filters($items, $item)
	{
		foreach ($item as $val)
		{
			echo '<li id="filter_' . $val['filter_id'] . '" data-filter-id="' . $val['filter_id'] . '" data-position="' . $val['position'] . '">';
			echo '<div class="holder' . (($val['hidden'] == 1) ? ' hidden' : '') . '">';
			echo '<div class="cell last_edit"></div>';
			echo '<div class="cell w_20 icon"><a href="#" class="hide-show ' . (($val['hidden'] == 0) ? ' active' : '') . '"></a></div>';
			echo '<div class="cell w_30 number_cell"><div class="number"></div><div class="add_items"><a href="#" class="up_add"></a><a href="#" class="child_add"></a><a href="#" class="down_add"></a></div></div>';
			echo '<div class="cell w_20 icon"><a href="#" class="edit"></a></div>';
			echo '<div class="cell auto"><span class="menu_item">' . (($val['name'] != '') ? $val['name'] : (($val['def_name'] != '') ? $val['def_name'] : 'Новий фільтр')) . '</span></div>';
			echo '<div class="cell w_70 icon no_padding"><div class="fm step_left"><a href="#"></a></div><div class="fm step_right"><a href="#"></a></div><div class="fm double_step_right"><a href="#"></a></div></div>';
			echo '<div class="cell w_20 icon"><a href="#" class="single_arrows filter_sorter"></a></div>';
			echo '<div class="cell w_20 icon"><a href="#" class="delete"></a></div>';
			echo '</div>';

			if (isset($items[$val['filter_id']]))
			{
				echo '<ul>';
				build_filters($items, $items[$val['filter_id']]);
				echo '</ul>';
			}
			echo '</li>';
		}
	}

	if (isset($filters[0])) build_filters($filters, $filters[0]);