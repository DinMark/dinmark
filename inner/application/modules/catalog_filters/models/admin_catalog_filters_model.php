<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_catalog_filters_model extends CI_Model
	{
		/**
		 * Отримання списку фільтрів
		 *
		 * @param null $language
		 *
		 * @return array
		 */
		public function get_filters($language)
		{
			$filters = array();

			$this->db->select('filter_id, parent_id, hidden, position, name_' . DEF_LANG . ' as def_name, name_' . $language . ' as name, image');
			$this->db->order_by('position', 'asc');

			$result = $this->db->get('catalog_filters')->result_array();
			foreach ($result as $v) $filters[$v['parent_id']][] = $v;

			return $filters;
		}

		/**
		 * Додавання нового фільтру
		 *
		 * @param int $parent_id
		 *
		 * @return int
		 */
		public function filter_add($parent_id)
		{
			$this->db->insert('catalog_filters', array('parent_id' => $parent_id));
			$filter_id = $this->db->insert_id();

			$this->load->dbutil();
			$this->dbutil->optimize_table($this->db->dbprefix . 'catalog_filters');

			return $filter_id;
		}

		/**
		 * Збереження порядку сортування фільтрів
		 *
		 * @param $items
		 */
		public function update_position($items)
		{
			if (is_array($items))
			{
				$set = array();

				foreach ($items as $val)
				{
					if (isset($val['filter_id']) AND $val['filter_id'] > 0)
					{
						$set[] = array(
							'filter_id' => $val['filter_id'],
							'parent_id' => intval($val['parent_id']),
							'position' => intval($val['position'])
						);
					}

					if (count($set) == 50) {
						$this->db->update_batch('catalog_filters', $set, 'filter_id');
						$set = array();
					}
				}

				if (count($set) > 0) $this->db->update_batch('catalog_filters', $set, 'filter_id');
			}
		}

		/**
		 * Отримання інформації про фільтер
		 *
		 * @param int $filter_id
		 *
		 * @return array
		 */
		public function get_filter($filter_id)
		{
			return $this->db->get_where('catalog_filters', array('filter_id' => $filter_id))->row_array();
		}

		/**
		 * Оновлення інформації про фільтер
		 *
		 * @param $set
		 * @param $where
		 */
		public function update_filter($set, $where)
		{
			$this->db->update('catalog_filters', $set, $where);
		}

		/**
		 * Видалення фільтру
		 *
		 * @param int|array $filter_id
		 */
		public function delete_filter($filter_id)
		{
			if (!is_array($filter_id)) $filter_id = array($filter_id);

			foreach ($filter_id as $v)
			{
				$this->db->delete('catalog_filters', array('filter_id' => $v));
				$this->db->delete('catalog_bind_filters', array('filter_id' => $v));
				
				$this->db->delete('catalog_product_bind_filters', array('filter_id' => $v));
				$this->db->delete('catalog_product_bind_filters', array('value_id' => $v));

				//$dir = ROOT_PATH . 'upload/catalog_filters/' . $v . '/';
				//if (file_exists($dir)) delete_files($dir, TRUE, FALSE, 1);

				$result = $this->db->select('filter_id')->where('parent_id', $v)->get('catalog_filters')->result_array();

				if (count($result) > 0)
				{
					$filters = array();
					foreach ($result as $_v) $filters[] = $_v['filter_id'];

					$this->delete_filter($filters);
				}
			}
		}

		/**
		 * Отримання фільтрів прив’язаних до компоненту
		 *
		 * @param int $component_id
		 * @return array
		 */
		public function get_component_filters($component_id)
		{
			$filters = array();

			$result = $this->db->select('filter_id')->where('component_id', $component_id)->get('catalog_bind_filters')->result_array();
			foreach ($result as $v) $filters[] = $v['filter_id'];

			return $filters;
		}

		/**
		 * Збереження/видалення фільтрів компоненту
		 *
		 * @param int $component_id
		 * @param array $filters
		 */
		public function save_component_filters($component_id, $filters)
		{
			$component_filters = $this->get_component_filters($component_id);

			if (count($component_filters) > 0)
			{
				$delete_rows = array_diff($component_filters, $filters);

				if (count($delete_rows) > 0)
				{
					foreach ($delete_rows as $v)
					{
						$this->db->delete('catalog_bind_filters', array('filter_id' => intval($v), 'component_id' => $component_id));
					}
				}
			}

			if (count($filters) > 0)
			{
				foreach ($filters as $v)
				{
					$c = $this->db->where('filter_id', intval($v))->where('component_id', $component_id)->count_all_results('catalog_bind_filters');
					if ($c == 0) $this->db->insert('catalog_bind_filters', array('filter_id' => intval($v), 'component_id' => $component_id));
				}
			}

			$this->load->dbutil();
			$this->dbutil->optimize_table($this->db->dbprefix . 'catalog_bind_filters');
		}

		/**
		 * Отримання меню фільтрів для форми редагування товару
		 *
		 * @param int $component_id
		 * @return array
		 */
		public function get_filters_menu($component_id)
		{
			$filters = array();

			$this->db->select('catalog_bind_filters.filter_id, catalog_filters.parent_id, catalog_filters.name_' . LANG . ' as name, parent_filter.name_' . LANG . ' as parent_name');
			$this->db->join('catalog_filters', 'catalog_filters.filter_id=catalog_bind_filters.filter_id');
			$this->db->join('catalog_filters as parent_filter', 'parent_filter.filter_id=catalog_filters.parent_id');
			$this->db->where_in('catalog_bind_filters.component_id', $component_id);
			$this->db->order_by('parent_filter.position', 'asc');
			$this->db->order_by('catalog_filters.position', 'asc');

			$result = $this->db->get('catalog_bind_filters')->result_array();
			foreach ($result as $v)
			{
				$filters[$v['parent_id']]['name'] = $v['parent_name'];

				$filters[$v['parent_id']]['childs'][] = array(
					'filter_id' => $v['filter_id'],
					'name' => $v['name'],
				);
			}

			return $filters;
		}
	}