<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Catalog_filters_model extends CI_Model {

		/**
		 * Отримання фільтрів компоненту
		 *
		 * @param int|array $components
		 * @return array
		 */
		public function get_filters($components)
		{
			$cache_key = is_numeric($components) ? $components : implode('_', $components);

			$filters = $this->cache->get('filters/filters_' . LANG . '_' . $cache_key);

			if ($filters) {
				return $filters;
			}

			$prefix = $this->db->dbprefix;
			$filters = array();

			$this->db->select('catalog_filters.filter_id, catalog_filters.parent_id, catalog_filters.name_' . LANG . ' as value_name');
			$this->db->select('catalog_parent_filters.name_' . LANG . ' as filter_name');

			$this->db->select('(select count(*) from `' . $prefix . 'catalog_product_bind_filters` join `' . $prefix . 'catalog_product_bind_components` on `' . $prefix . 'catalog_product_bind_components`.`product_id` = `' . $prefix . 'catalog_product_bind_filters`.`product_id` where `' . $prefix . 'catalog_product_bind_components`.`component_id` = `' . $prefix . 'catalog_bind_filters`.`component_id` and `' . $prefix . 'catalog_product_bind_filters`.`value_id` = `' . $prefix . 'catalog_filters`.`filter_id`) as products', FALSE);

			$this->db->join('catalog_filters', 'catalog_filters.filter_id = catalog_bind_filters.filter_id');
			$this->db->join('catalog_filters as catalog_parent_filters', 'catalog_parent_filters.filter_id = catalog_filters.parent_id');

			if (is_numeric($components)) {
				$this->db->where('catalog_bind_filters.component_id', $components);
			} else {
				$this->db->where_in('catalog_bind_filters.component_id', $components);
			}

			$this->db->where('catalog_filters.hidden', 0);
			//$this->db->where('catalog_filters.name_' . LANG . ' != ', 0);

			$this->db->order_by('catalog_parent_filters.position', 'asc');
			$this->db->order_by('catalog_filters.position', 'asc');

			$result = $this->db->get('catalog_bind_filters')->result_array();

			foreach ($result as $v)
			{
				if ((!is_numeric($v['value_name']) or $v['value_name'] > 0) and $v['products'] > 0) {
					$filters[$v['parent_id']]['filter_id'] = $v['parent_id'];
					$filters[$v['parent_id']]['name'] = $v['filter_name'];

					$filters[$v['parent_id']]['childs'][$v['value_name']] = array(
						'value_id' => $v['filter_id'],
						'name' => $v['value_name'],
						'products' => $v['products'],
					);

					ksort($filters[$v['parent_id']]['childs'], SORT_NATURAL);
				}
			}

			$this->cache->save('filters_' . LANG . '_' . $cache_key, $filters, 1800, 'filters/');

			return $filters;
		}

		/**
		 * Перевірка активності фільтрів по відношенню до інших параметрів фільтрації
		 *
		 * @param int|array $components
		 * @param array $component_filters
		 * @param array $filters
		 * @return array
		 */
		public function check_filters($components, $component_filters, $filters)
		{
			$checked_filters = array();

			foreach ($component_filters as $k => $v)
			{
				if (!is_array($v)) continue;

				$k = intval($k);
				$v = array_map('intval', $v);

				foreach ($v as $_v)
				{
					$this->db->join('catalog_product_bind_components', 'catalog_product_bind_components.product_id=catalog_product_bind_filters.product_id');
					$this->db->where('catalog_product_bind_filters.value_id', $_v);

					if (is_numeric($components)) {
						$this->db->where('catalog_product_bind_components.component_id', $components);
					} else {
						$this->db->where_in('catalog_product_bind_components.component_id', $components);
					}

					if (isset($filters['min_price']) AND isset($filters['max_price']))
					{
						$this->db->join('catalog', 'catalog.product_id=catalog_product_bind_filters.product_id');
						$this->db->where('catalog.price >=', $filters['min_price']);
						$this->db->where('catalog.price <=', $filters['max_price']);
					}

					if (isset($filters['filters']))
					{
						foreach ($filters['filters'] as $filter_id => $values)
						{
							if ($k != $filter_id)
							{
								$key = array_search($_v, $values);
								if ($key) unset($values[$key]);

								if (count($values) > 0)
								{
									$this->db->join('catalog_product_bind_filters as filter_' . $filter_id, 'filter_' . $filter_id . '.product_id=catalog.product_id');
									$this->db->group_start();
									$this->db->where('filter_' . $filter_id . '.filter_id', $filter_id);

									if (count($values) == 1)
									{
										$this->db->where('filter_' . $filter_id . '.value_id', $values[0]);
									}
									else
									{
										$this->db->where_in('filter_' . $filter_id . '.value_id', $values);
									}
									$this->db->group_end();
								}
							}
						}
					}

					$products = $this->db->count_all_results('catalog_product_bind_filters');

					$checked_filters[] = array($_v, $products);
				}
			}

			return $checked_filters;
		}
	}