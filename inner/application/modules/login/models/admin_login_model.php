<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_login_model extends CI_Model
	{
		public function get_admin($login)
		{
			$r = $this->db->where('login', $login)->get('admin');

			return ($r->num_rows() > 0) ? $r->row_array() : NULL;
		}
	}
