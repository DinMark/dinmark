<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Admin_catalog_slider_model extends CI_Model
	{
		/**
		 * ID активного слайду
		 *
		 * @var null|int
		 */
		protected $slide_id;

		/**
		 * Директорія слайду
		 *
		 * @var null|string
		 */
		protected $dir;

		/**
		 * Отримання списку слайдів
		 *
		 * @param int $component_id
		 * @return array
		 */
		public function get_slides($component_id)
		{
			return $this->db
				->select('slide_id, hidden, title_' . LANG . ' as title, file_name_' . LANG . ' as file_name')
				->where('component_id', $component_id)
				->order_by('position')
				->get('catalog_slider')
				->result_array();
		}

		/**
		 * Встановлення ID активного слайду
		 *
		 * @param int $slide_id
		 * @return bool
		 */
		public function set_slide_id($slide_id)
		{
			$c = (int)$this->db
				->where('slide_id', $slide_id)
				->count_all_results('catalog_slider');

			return $c > 0 ? $this->slide_id = $slide_id : false;
		}

		/**
		 * Отримання ID активного слайду
		 *
		 * @return int
		 */
		public function get_slide_id()
		{
			return $this->slide_id;
		}

		/**
		 * Отримання слайду
		 *
		 * @return array
		 */
		public function get_slide()
		{
			return $this->db
				->where('slide_id', $this->slide_id)
				->get('catalog_slider')
				->row_array();
		}

		/**
		 * Додавання слайду
		 *
		 * @param int $add_top
		 * @param int $menu_id
		 * @param int $component_id
		 */
		public function add_slide($add_top, $menu_id, $component_id)
		{
			$this->db->insert(
				'catalog_slider',
				array(
					'menu_id' => $menu_id,
					'component_id' => $component_id,
					'position' => $this->get_position($component_id, $add_top),
				)
			);

			$this->set_slide_id((int)$this->db->insert_id());
		}

		/**
		 * Максимальна позиція слайду
		 *
		 * @param int $component_id
		 * @param int $add_top
		 * @return int
		 */
		private function get_position($component_id, $add_top)
		{
			if ($add_top === 0) {
				$position = (int)$this->db
					->select_max('position')
					->where('component_id', $component_id)
					->get('catalog_slider')
					->row('position') + 1;
			} else {
				$this->db
					->set('position', '`position` + 1', false)
					->where('component_id', $component_id)
					->update('catalog_slider');

				$position = 1;
			}

			return $position;
		}

		/**
		 * Оновлення слайду
		 *
		 * @param array $set
		 */
		public function update_slide($set)
		{
			$this->db->update(
				'catalog_slider',
				$set,
				array('slide_id' => $this->slide_id)
			);
		}

		/**
		 * Встановлення директорії слайду
		 *
		 * @return Admin_slider_model
		 */
		public function set_dir()
		{
			$this->dir = get_dir_path('upload/catalog_slider/' . $this->slide_id);
			return $this;
		}

		/**
		 * Отримання директорії слайду
		 *
		 * @return null|string
		 */
		public function get_dir()
		{
			if ($this->dir === null) {
				$this->set_dir();
			}

			return $this->dir;
		}

		/**
		 * Отримання назви файлу зображення
		 *
		 * @param string $language
		 * @return null
		 */
		public function get_image($language)
		{
			$result = $this->db
				->select('file_name_' . $language . ' as file_name')
				->where('slide_id', $this->slide_id)
				->get('catalog_slider')
				->row_array();

			return isset($result['file_name']) ? $result['file_name'] : null;
		}

		/**
		 * Оновлення назви файлу зображення
		 *
		 * @param string $file_name
		 * @param string $language
		 */
		public function update_image($file_name, $language)
		{
			$this->db
				->set('file_name_' . $language, $file_name)
				->where('slide_id', $this->slide_id)
				->update('catalog_slider');
		}

		/**
		 * Обробка зображення
		 *
		 * @param string $file_name
		 * @param string $language
		 * @return array
		 */
		public function processing_image($file_name, $language)
		{
			$real_sizes = getimagesize($this->dir . $file_name);
			$big_sizes = $this->config->item('slider_big_sizes');
			$thumb_sizes = $this->config->item('slider_thumb_sizes');

			copy($this->dir . $file_name, $this->dir . 's_' . $file_name);

			$this->image_lib->resize_crop(
				$this->dir . $file_name,
				$this->dir . $file_name,
				$big_sizes[0],
				$big_sizes[1],
				false
			);

			$this->image_lib->resize(
				$this->dir . $file_name,
				$this->dir . 't_' . $file_name,
				$thumb_sizes[0],
				$thumb_sizes[1],
				false
			);

			$this->delete_image($language);
			$this->update_image($file_name, $language);

			return $real_sizes;
		}

		/**
		 * Обрізка зображення
		 *
		 * @param array $coords
		 * @return string
		 */
		public function crop_image(array $coords)
		{
			$image = '';
			$file_name = $this->get_image(LANG);

			if (file_exists($this->dir . 's_' . $file_name)) {
				$real_sizes = getimagesize($this->dir . 's_' . $file_name);
				$big_sizes = $this->config->item('slider_big_sizes');
				$thumb_sizes = $this->config->item('slider_thumb_sizes');

				$w_index = $real_sizes[0] / $thumb_sizes[0];
				$h_index = $real_sizes[1] / round(($thumb_sizes[0] * $real_sizes[1]) / $real_sizes[0]);

				$x = round($coords['x'] * $w_index);
				$y = round($coords['y'] * $h_index);
				$x2 = round($coords['x2'] * $w_index);
				$y2 = round($coords['y2'] * $h_index);

				$this->image_lib->crop(
					$this->dir . 's_' . $file_name,
					$this->dir . $file_name,
					$x2 - $x,
					$y2 - $y,
					$x,
					$y,
					$big_sizes[0],
					$big_sizes[1]
				);

				$this->image_lib->resize(
					$this->dir . $file_name,
					$this->dir . 't_' . $file_name,
					$thumb_sizes[0],
					$thumb_sizes[1],
					false
				);

				$image = '/upload/catalog_slider/' . $this->slide_id . '/t_' . $file_name . '?t' . time() . rand(10000, 1000000);
			}

			return $image;
		}

		/**
		 * Видалення зображення
		 *
		 * @param string $language
		 */
		public function delete_image($language)
		{
			$file_name = $this->get_image($language);

			if ($file_name !== null and $file_name !== '') {
				if (file_exists($this->dir . $file_name)) {
					unlink($this->dir . $file_name);
				}

				if (file_exists($this->dir . 't_' . $file_name)) {
					unlink($this->dir . 't_' . $file_name);
				}

				if (file_exists($this->dir . 's_' . $file_name)) {
					unlink($this->dir . 's_' . $file_name);
				}

				$this->update_image('', $language);
			}
		}

		/**
		 * Видалення слайду
		 */
		public function delete_slide()
		{
			delete_files($this->get_dir(), true, false, 1);

			$this->db->delete(
				'catalog_slider',
				array('slide_id' => $this->slide_id)
			);
		}

		/**
		 * Видалення компоненту
		 *
		 * @param int $component_id
		 */
		public function delete_component($component_id)
		{
			$result = $this->db
				->select('slide_id')
				->where('component_id', $component_id)
				->get('catalog_slider')
				->result_array();

			foreach ($result as $v)
			{
				$this->set_slide_id($v['slide_id']);
				$this->delete_slide();
			}

			$this->db->delete(
				'components',
				array('component_id' => $component_id)
			);
		}
	}