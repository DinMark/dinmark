<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Catalog_slider_model extends CI_Model
	{
		/**
		 * Отримання слайдів
		 *
		 * @param int $component_id
		 * @return array
		 */
		public function get_slides($component_id)
		{
			$result = $this->db
				->select('slide_id')
				->select('title_' . LANG . ' as title, title_' . DEF_LANG . ' as def_title')
				->select('file_name_' . LANG . ' as file_name, file_name_' . DEF_LANG . ' as def_file_name')

				->where('component_id', $component_id)
				->where('hidden', 0)

				->order_by('position')

				->get('catalog_slider')
				->result_array();

			$slides = array();

			foreach ($result as $v) {
				if ($v['file_name'] === '') {
					$v['file_name'] = $v['def_file_name'];
				}

				if ($v['file_name'] === '' and $v['bg_file_name'] === '') {
					continue;
				}

				if ($v['title'] === '') {
					$v['title'] = $v['def_title'];
				}

				$slides[] = array(
					'slide_id' => (int)$v['slide_id'],
					'title' => stripslashes($v['title']),
					'url' => $v['url'],
					'file_name' => $v['file_name'],
				);
			}

			return $slides;
		}

		public function get_menu($menu_id)
		{
			$categories = array();

			$r = $this->db->query('select m.id, m.name_' . LANG . ' as name, m.url_' . LANG . ' as url, (select concat(cs.slide_id, "|", cs.file_name_' . LANG . ') from `' . $this->db->dbprefix('catalog_slider') . '` as cs where cs.menu_id=m.id order by cs.position asc limit 1) as slide from `' . $this->db->dbprefix('menu') . '` as m where m.parent_id = ' . $menu_id . ' and m.hidden=0 order by m.position asc')->result_array();

			foreach ($r as $v) {
				$v['categories'] = $this->get_menu($v['id']);
				$categories[] = $v;
			}

			return $categories;
		}
	}