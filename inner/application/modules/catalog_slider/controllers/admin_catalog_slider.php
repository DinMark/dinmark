<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Admin_catalog_slider
	 *
	 * @property Admin_catalog_slider_model $admin_catalog_slider_model
	 */
	class Admin_catalog_slider extends MX_Controller
	{
		/**
		 * Додавання слайду
		 *
		 * @return string
		 */
		public function add_slide()
		{
			$response = array('success' => false);

			$menu_id = $this->input->post('menu_id');
			$component_id = $this->input->post('component_id');

			if (
				$this->init_model->is_admin()
				and
				$component_id > 0
			) {
				$this->load->model('admin_catalog_slider_model');

				$this->admin_catalog_slider_model->add_slide(
					(int)$this->input->post('add_top'),
					$menu_id,
					$component_id
				);

				$response['slide_id'] = $this->admin_catalog_slider_model->get_slide_id();
				$response['success'] = true;
			}

			return json_encode($response);
		}

		/**
		 * Приховування слайду
		 *
		 * @return string
		 */
		public function visibility_slide()
		{
			$response = array('success' => false);

			$this->load->model('admin_catalog_slider_model');

			$hidden = (int)$this->input->post('hidden');

			if (
				$this->init_model->is_admin()
				and
				$this->admin_catalog_slider_model->set_slide_id((int)$this->input->post('slide_id'))
				and
				in_array($hidden, array(0, 1), true)
			) {
				$this->admin_catalog_slider_model->update_slide(
					array(
						'hidden' => $hidden,
					)
				);

				$this->session->set_userdata(
					'last_catalog_slide',
					$this->admin_catalog_slider_model->get_slide_id()
				);

				$response['success'] = true;
			}

			return json_encode($response);
		}

		/**
		 * Збереження порядку сортування
		 */
		public function slides_position()
		{
			$response = array('success' => false);

			$this->load->model('admin_catalog_slider_model');
			$slides = $this->input->post('slides');

			if (
				$this->init_model->is_admin()
				and
				$this->admin_catalog_slider_model->set_slide_id((int)$this->input->post('slide_id'))
				and
				is_array($slides)
			) {
				$this->session->set_userdata(
					'last_catalog_slide',
					$this->admin_catalog_slider_model->get_slide_id()
				);

				foreach ($slides as $position => $slide_id) {
					if (is_numeric($position) and $this->admin_catalog_slider_model->set_slide_id($slide_id)) {
						$this->admin_catalog_slider_model->update_slide(
							array(
								'position' => (int)$position,
							)
						);
					}
				}

				$response['success'] = true;
			}

			return json_encode($response);
		}

		/**
		 * Редагування слайду
		 */
		public function edit()
		{
			ini_set('display_errors', true);

			$this->load->model('admin_catalog_slider_model');

			$menu_id = (int)$this->input->get('menu_id');

			if (
				$this->init_model->is_admin()
				and
				$this->admin_catalog_slider_model->set_slide_id((int)$this->input->get('slide_id'))
			) {
				$this->init_model->set_menu_id($menu_id, true);

				$this->template_lib->set_title('Управління слайдером каталогу - редагування слайду');
				$this->template_lib->set_admin_menu_active('components');
				$this->template_lib->set_admin_menu_active('', 'sub_level');

				$this->session->set_userdata(
					'last_catalog_slide',
					$this->admin_catalog_slider_model->get_slide_id()
				);

				$this->load->helper('directory');
				$this->load->config('catalog_slider');

				$this->template_lib->set_content(
					$this->load->view(
						'admin/edit_tpl',
						array(
							'menu_id' => $menu_id,
							'slide_id' => $this->admin_catalog_slider_model->get_slide_id(),
							'slide' => $this->admin_catalog_slider_model->get_slide(),
							'thumb' => $this->config->item('slider_thumb_sizes'),
							'big' => $this->config->item('slider_big_sizes'),
							'languages' => $this->config->item('languages'),
						),
						true
					)
				);
			} else {
				redirect($this->init_model->get_link($menu_id, '{URL}'));
			}
		}

		/**
		 * Збереження інформації про слайд
		 *
		 * @return string
		 */
		public function save()
		{
			$response = array('success' => false);

			$this->load->model('admin_catalog_slider_model');

			if (
				$this->init_model->is_admin()
				and
				$this->admin_catalog_slider_model->set_slide_id((int)$this->input->post('slide_id'))
			) {
				$this->load->helper('form');

				$title = $this->input->post('title');
				$url = $this->input->post('url');

				$set = array();

				foreach ($title as $key => $val) {
					$set['title_' . $key] = form_prep($val);
					$set['url_' . $key] = $url[$key];
				}

				$this->admin_catalog_slider_model->update_slide($set);

				$response['success'] = true;
			}

			return json_encode($response);
		}

		/**
		 * Завантаження зображення
		 *
		 * @return string
		 */
		public function upload_image()
		{
			$response = array('success' => false);

			$this->load->model('admin_catalog_slider_model');

			if (
				$this->init_model->is_admin()
				and
				$this->admin_catalog_slider_model->set_slide_id((int)$this->input->post('slide_id'))
			) {
				$this->load->library('image_lib');
				$this->load->helper('translit');
				$this->load->helper('directory');
				$this->load->config('catalog_slider');

				$this->admin_catalog_slider_model->set_dir();

				$this->load->library(
					'upload',
					array(
						'upload_path' => $this->admin_catalog_slider_model->get_dir(),
						'overwrite' => false,
						'file_name' => translit_filename($_FILES['slide_image_' . LANG]['name']),
						'allowed_types' => 'gif|jpg|jpeg|png',
					)
				);

				if ($this->upload->do_upload('slide_image_' . LANG)) {
					$file_name = $this->upload->data('file_name');

					$sizes = $this->admin_catalog_slider_model->processing_image($file_name, LANG);

					$response['success'] = true;
					$response['width'] = $sizes[0];
					$response['height'] = $sizes[1];
					$response['file_name'] = $file_name . '?t=' . time() . rand(10000, 1000000);
				}
			}

			$this->config->set_item('is_ajax_request', true);
			return json_encode($response);
		}

		/**
		 * Обрізка зображення
		 *
		 * @return string
		 */
		public function crop_image()
		{
			$response = array('success' => false);

			$this->load->model('admin_catalog_slider_model');

			$coords = $this->input->post('coords');
			$coords = array_map('floatval', $coords);

			if (
				$this->init_model->is_admin()
				and
				$this->admin_catalog_slider_model->set_slide_id((int)$this->input->post('slide_id'))
				and
				is_numeric($coords['x'])
				and
				is_numeric($coords['y'])
				and
				$coords['w'] >= 0
				and
				$coords['h'] >= 0
			) {
				$file_name = $this->admin_catalog_slider_model->get_image(LANG);

				if ($file_name !== null AND $file_name !== '') {
					$this->load->library('Image_lib');
					$this->load->helper('directory');
					$this->load->config('catalog_slider');

					$this->admin_catalog_slider_model->set_dir();

					$response['image'] = $this->admin_catalog_slider_model->crop_image($coords);
					$response['success'] = true;
				}
			}

			return json_encode($response);
		}

		/**
		 * Видалення зображення
		 *
		 * @return string
		 */
		public function delete_image()
		{
			$response = array('success' => false);

			$this->load->model('admin_catalog_slider_model');

			if (
				$this->init_model->is_admin()
				and
				$this->admin_catalog_slider_model->set_slide_id((int)$this->input->post('slide_id'))
			) {
				$this->admin_catalog_slider_model->delete_image(LANG);

				$response['success'] = true;
			}

			return json_encode($response);
		}

		/**
		 * Видалення слайду
		 *
		 * @return string
		 */
		public function delete_slide()
		{
			$response = array('success' => false);

			$this->load->model('admin_catalog_slider_model');

			if (
				$this->init_model->is_admin()
				and
				$this->admin_catalog_slider_model->set_slide_id((int)$this->input->post('slide_id'))
			) {
				$this->load->helper('directory');
				$this->load->helper('file');

				$this->admin_catalog_slider_model->delete_slide();

				$response['success'] = true;
			}

			return json_encode($response);
		}

		/**
		 * Видалення компоненту
		 *
		 * @return string
		 */
		public function delete_component()
		{
			$response = array(
				'success' => false,
				'error' => 1,
			);

			$menu_id = (int)$this->input->post('menu_id');
			$component_id = (int)$this->input->post('component_id');

			if (
				$this->init_model->is_admin()
				and
				$component_id > 0
			) {
				$this->load->model('admin_catalog_slider_model');

				$this->load->helper('directory');
				$this->load->helper('file');

				$this->admin_catalog_slider_model->delete_component($component_id);

				$response['success'] = true;
				$response['error'] = 0;
			}

			return json_encode($response);
		}
	}