<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Компонент слайдеру
	 *
	 * @property Admin_catalog_slider_model $admin_catalog_slider_model
	 * @property Catalog_slider_model $catalog_slider_model
	 */
	class Catalog_slider extends MX_Controller
	{
		/**
		 * Вивід компоненту слайдеру
		 *
		 * @param int $menu_id
		 * @param int $component_id
		 * @param int $hidden
		 * @param array $config
		 * @return string
		 */
		public function index($menu_id, $component_id, $hidden, $config = array())
		{
			$this->load->helper('directory');

			$component_content = '';

			if ($this->init_model->is_admin()) {
				$this->load->model('admin_catalog_slider_model');
				$this->load->config('catalog_slider');

				$component_content = $this->load->view(
					'admin/slider_tpl',
					array(
						'menu_id' => $menu_id,
						'component_id' => $component_id,
						'hidden' => $hidden,
						'slider_thumb_sizes' => $this->config->item('slider_thumb_sizes'),
						'slides' => $this->admin_catalog_slider_model->get_slides($component_id),
						'last_slide' => $this->session->userdata('last_catalog_slide'),
					),
					true
				);
			} else {
				$this->load->model('catalog_slider_model');

				$slides = $this->catalog_slider_model->get_slides($component_id);
				$total = count($slides);

				if ($total > 0) {
					$component_content = $this->load->view(
						'slider_tpl',
						array(
							'slider_menu' => $this->catalog_slider_model->get_menu($menu_id),
							'slides' => $slides,
						),
						true
					);
				}
			}

			return $component_content;
		}
	}