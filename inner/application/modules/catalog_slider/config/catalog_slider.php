<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	$config['slider_big_sizes'] = array(870, 214);
	$config['slider_thumb_sizes'] = array(300, 74);