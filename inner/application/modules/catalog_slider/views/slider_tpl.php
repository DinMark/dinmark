<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<?php if (count($slides) > 0): ?>
<div class="container">
	<div class="row">
		<div class="categories inner-categories fm">
			<div class="col-md-12">
				<div class="sect-title"><span><?php if (LANG == 'ua') echo 'Категорії'; if (LANG == 'ru') echo 'Категории'; if (LANG == 'en') echo 'Categories'; ?></span></div>
				<div class="row">
					<div class="col-md-3">
						<?php if (isset($slider_menu) and count($slider_menu) > 1): ?>
                            <nav class="categories-list">
                                <ul>
                                    <?php foreach ($slider_menu as $k => $v): ?>
                                        <li<?php if ($k > 2): ?> class="more-category hidden"<?php endif; ?>>
                                            <a href="<?=$this->uri->full_url($v['url']);?>"<?php if (mb_strlen($v['name'], 'UTF-8') > 30): ?> class="long"<?php endif; ?> data-menu-id="<?=$v['id'];?>"><span>&gt;</span><?=$v['name'];?></a>
                                            <?php if (isset($v['categories']) and count($v['categories']) > 0): ?>
                                            <ul>
                                                <?php foreach ($v['categories'] as $_v): ?>
                                                    <li>
                                                        <a href="<?=$this->uri->full_url($_v['url']);?>" data-menu-id="<?=$_v['id'];?>"><span>&gt;</span><?=$_v['name'];?></a>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                            <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>
                                    <?php if (count($slider_menu) > 3): ?>
                                    <li>
                                        <a href="#" class="more-categories"><span>&plus;</span><?php if (LANG == 'ua') echo 'Більше категорій'; if (LANG == 'ru') echo 'Больше категорий'; if (LANG == 'en') echo 'More categories'; ?></a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </nav>
                        <?php endif; ?>
					</div>
					<div class="col-md-9">
						<div class="categories-slider">
							<?php foreach ($slides as $k => $v): ?>
								<figure>
									<?php if ($v['url'] !== ''): ?><a href="<?=$v['url'];?>"><?php endif; ?>
										<img src="<?=base_url('upload/catalog_slider/' . $v['slide_id'] . '/' . $v['file_name']);?>" alt="">
										<?php if ($v['url'] !== ''): ?></a><?php endif; ?>
								</figure>
							<?php endforeach; ?>
						</div>
						<?php foreach ($slider_menu as $k => $v): ?>
                            <?php if ((string)$v['slide'] !== ''): ?>
								<?php $slide = explode('|', $v['slide']); ?>
                                <?php if ($slide[1] !== ''): ?>
                                    <figure id="figure-<?=$v['id'];?>" class="hidden">
                                        <img src="<?=base_url('upload/catalog_slider/' . $slide[0] . '/' . $slide[1]);?>" alt="">
                                    </figure>
                                <?php endif; ?>
                            <?php endif; ?>
							<?php foreach ($v['categories'] as $_v): ?>
								<?php if ((string)$_v['slide'] !== ''): ?>
									<?php $slide = explode('|', $_v['slide']); ?>
									<?php if ($slide[1] !== ''): ?>
                                        <figure id="figure-<?=$_v['id'];?>" class="hidden">
                                            <img src="<?=base_url('upload/catalog_slider/' . $slide[0] . '/' . $slide[1]);?>" alt="">
                                        </figure>
									<?php endif; ?>
								<?php endif; ?>
							<?php endforeach; ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>