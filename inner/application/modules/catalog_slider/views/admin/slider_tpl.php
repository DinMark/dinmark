<?php
	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/** @var int $menu_id */
	/** @var int $component_id */
	/** @var array $slider_thumb_sizes */
	/** @var array $slides */

	$this->template_lib->set_js('admin/ui/jquery-ui-1.10.3.custom.min.js');
	$this->template_lib->set_js('plugins/mustache.min.js');
?>
<div class="fm admin_component" id="admin_component_<?=$component_id;?>" data-component-id="<?=$component_id;?>" data-menu-id="<?=$menu_id;?>" data-module="slider" data-css-class="gallery" data-visibility-url="<?=$this->uri->full_url('admin/components/toggle_visibility');?>" data-delete-url="<?=$this->uri->full_url('admin/catalog_slider/delete_component');?>">
	<div class="component_loader"></div>
	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="gallery"></div>
		</div>
		<div class="fm component_edit_links">
			<div class="fm only_text"><div>Слайдер каталогу</div></div>
			<a class="fm add add_slide" href="#"><b></b>Додати слайд</a>
			<a class="fm add_bottom add_mode" href="#"><b class="active"></b>Додавати внизу</a>
		</div>
		<div class="fmr component_del">
			<a href="#" class="fm delete_component"><b></b></a>
		</div>
		<div class="fmr component_pos">
			<a href="#" class="fm up_component"><b></b></a>
			<a href="#" class="fm down_component"><b></b></a>
		</div>
	</div>
	<div class="fm admin_menu">
		<ul>
			<li class="th">
				<div class="holder">
					<div class="cell last_edit"></div>
					<div class="cell w_20">#</div>
					<div class="cell w_20"></div>
					<div class="cell no_padding" style="width: <?=$slider_thumb_sizes[0];?>px">Зображення</div>
					<div class="cell w_20 icon"></div>
					<div class="cell auto">Назва слайду</div>
					<div class="cell w_20 icon"></div>
					<div class="cell w_20 icon"></div>
				</div>
			</li>
		</ul>
		<ul class="slides_list">
			<?php if (count($slides) > 0): ?>
				<?php foreach ($slides as $slide): ?>
				<li data-slide-id="<?=$slide['slide_id'];?>">
					<div class="holder<?php if ($slide['hidden'] == 1): ?> hidden<?php endif; ?>">
						<div class="cell last_edit<?=((isset($last_slide) AND ($last_slide == $slide['slide_id'])) ? ' active' : '');?>"></div>
						<div class="cell w_20 number"></div>
						<div class="cell w_20 icon"><a href="#" class="hide-show<?=(($slide['hidden'] == 0) ? ' active' : '');?>"></a></div>
						<div class="cell no_padding" style="width: <?=$slider_thumb_sizes[0];?>px">
							<?php if ($slide['file_name'] != ''): ?><img src="<?=base_url('upload/catalog_slider/' . $slide['slide_id'] . '/t_' . $slide['file_name']);?>" alt=""><?php endif; ?>
						</div>
						<div class="cell w_20 icon">
							<a href="<?=$this->uri->full_url('admin/catalog_slider/edit?menu_id=' . $menu_id . '&slide_id=' . $slide['slide_id']);?>" class="edit"></a>
						</div>
						<div class="cell auto"><span class="menu_item"><?=($slide['title'] != '' ? $slide['title'] : 'Новий слайд');?></span></div>
						<div class="cell w_20 icon sorter"><a href="#" class="single_arrows"></a></div>
						<div class="cell w_20 icon"><a href="#" class="delete"></a></div>
					</div>
				</li>
				<?php endforeach; ?>
			<?php else: ?>
				<li class="empty_row">
					<div class="holder">
						<div class="cell auto number" style="text-align: center">Список сладів пустий</div>
					</div>
				</li>
			<?php endif; ?>
		</ul>
	</div>
</div>

<script id="slide_template" type="text/html">
	<li data-slide-id="{{ slide_id }}">
		<div class="holder">
			<div class="cell last_edit active"></div>
			<div class="cell w_20 number"></div>
			<div class="cell w_20 icon"><a href="#" class="hide-show active"></a></div>
			<div class="cell no_padding" style="width: <?=$slider_thumb_sizes[0];?>px"></div>
			<div class="cell w_20 icon"><a href="<?=$this->uri->full_url('admin/catalog_slider/edit?menu_id=' . $menu_id . '&slide_id={{ slide_id }}'); ?>" class="edit"></a></div>
			<div class="cell auto"><span class="menu_item">Новий слайд</span></div>
			<div class="cell w_20 icon sorter"><a href="#" class="single_arrows"></a></div>
			<div class="cell w_20 icon"><a href="#" class="delete"></a></div>
		</div>
	</li>
</script>

<script type="text/javascript">

	$(function () {
		var $component = $('#admin_component_<?=$component_id;?>'),
			$slides_list = $component.find('.slides_list'),
			slide_add_top = 0;

		$component.component();

		$component
			.on('click', '.add_mode', function (e) {
				e.preventDefault();

				$(this).find('b').toggleClass('active');
				slide_add_top = $(this).find('b').hasClass('active') ? 1 : 0;
			})
			.on('click', '.add_slide', function (e) {
				e.preventDefault();

				component_loader_show($component.find('.component_loader'), '');

				$.post(
					full_url('admin/catalog_slider/add_slide'),
					{
						add_top: slide_add_top,
						menu_id: <?=$menu_id;?>,
						component_id: <?=$component_id;?>
					},
					function (response) {
						if (response.success) {

							$slides_list.find('.empty_row').remove();
							$slides_list.find('.last_edit').removeClass('active');

							var slide = Mustache.render($('#slide_template').html(), response);
							(slide_add_top === 1) ? $slides_list.prepend(slide) : $slides_list.append(slide);

							$slides_list.trigger('zebra_list').trigger('sortable_list');
							component_loader_hide($component.find('.component_loader'), '');
						}
					},
					'json'
				);
			});

		$slides_list
			.on('click', '.hide-show', function (e) {
				e.preventDefault();

				var $holder = $(this).closest('.holder'),
					status = 0;

				if ($holder.hasClass('hidden')) {
					$holder.removeClass('hidden');
					$(this).addClass('active');
				} else {
					$holder.addClass('hidden');
					$(this).removeClass('active');
					status = 1;
				}

				component_loader_show($component.find('.component_loader'), '');
				$('#slides_list').find('.last_edit').removeClass('active');
				$holder.find('.last_edit').addClass('active');

				$.post(
					full_url('admin/catalog_slider/visibility_slide'),
					{
						slide_id: $holder.closest('li').data('slide-id'),
						hidden: status
					},
					function (response) {
						if (response.success) {
							component_loader_hide($('.component_loader'), '');
						}
					},
					'json'
				);
			})
			.on('click', '.delete', function (e) {
				e.preventDefault();

				var $link = $(this);

				confirmation('Видалити слайд?', function () {
					component_loader_show($component.find('.component_loader'), '');
					$.post(
						full_url('admin/catalog_slider/delete_slide'),
						{
							slide_id: $link.closest('li').data('slide-id')
						},
						function (response) {
							if (response.success) {

								$link.closest('li').slideUp(function () {

									if ($slides_list.find('li').length === 1) {
										$slides_list.html('<li class="empty_row"><div class="holder"><div class="cell auto number" style="text-align: center">Список сладів пустий</div></div></li>');
									} else {
										$(this).remove();
									}

									$slides_list.trigger('zebra_list');
								});

								component_loader_hide($component.find('.component_loader'), '');
							}
						},
						'json'
					);
				});
			})
			.on('mouseover', '.holder', function () {
				$(this).addClass('active');
			})
			.on('mouseout', '.holder', function () {
				$(this).removeClass('active');
			})
			.on('zebra_list', function () {
				$slides_list
					.find('li').removeClass('grey').each(function (i) {
						if (!$(this).hasClass('empty_row')) $(this).find('.number').text(i + 1);
					})
					.end()
					.find('li:even').addClass('grey');
			})
			.on('sortable_list', function () {
				$slides_list
					.sortable({
						axis:'y',
						handle:'.sorter a',
						scroll: true,
						crollSpeed: 2000,
						forcePlaceholderSize: true,
						placeholder: "ui-state-highlight",
						update: function (e, object) {

							$(this).find('.last_edit').removeClass('active');
							$(object.item).find('.last_edit').addClass('active');

							component_loader_show($component.find('.component_loader'), '');

							var slides = [];

							$slides_list.find('li').each(function (i) {
								slides[i] = $(this).data('slide-id');
								$(this).find('.number').text(i + 1);
							});

							$.post(
								full_url('admin/catalog_slider/slides_position'),
								{
									slide_id: $(object.item).data('slide-id'),
									slides: slides
								},
								function (response) {
									if (response.success) {
										component_loader_hide($component.find('.component_loader'), '');
										$slides_list.trigger('zebra_list');
									}
								},
								'json'
							);
						}
					});
			});

		$slides_list.trigger('zebra_list').trigger('sortable_list');
	});
</script>