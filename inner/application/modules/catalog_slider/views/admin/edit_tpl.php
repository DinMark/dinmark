<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_css('js/admin/fineuploader/fineuploader-3.5.0.css', TRUE);
	$this->template_lib->set_js('admin/fineuploader/jquery.fineuploader-3.5.0.min.js');

	$this->template_lib->set_css('js/admin/jcrop/css/jquery.Jcrop.min.css', TRUE);
	$this->template_lib->set_js('admin/jcrop/js/jquery.Jcrop.min.js');

	$this->template_lib->set_js('admin/jquery.form.js');
	$this->template_lib->set_js('plugins/mustache.min.js');

	$sizes = array();
	$bg_sizes = array();

	$dir = get_dir_path('upload/catalog_slider/' . $slide_id, false);
?>
<div class="fm admin_component">
	<div class="component_loader"></div>

	<div class="fm adcom_panel">
		<div class="fm type_of_component">
			<div class="gallery"></div>
		</div>
		<div class="fm component_edit_links">
			<a href="#" class="fm save"><b></b>Зберегти</a>
			<a href="#" class="fm apply"><b></b>Застосувати</a>
			<a href="<?=$this->init_model->get_link($menu_id, '{URL}');?>" class="fm cancel"><b></b>До списку слайдів</a>
		</div>
		<div class="fmr component_lang" <?php if (count($languages) === 1): ?> style="display:none"<?php endif; ?>>
			<?php foreach ($languages as $key => $val): ?>
				<a href="#" class="flags <?=$key;?><?=(($key === LANG) ? ' active' : '');?>" data-language="<?=$key;?>">
					<img src="<?=base_url('img/flags_' . $key . '.png');?>" alt="">
				</a>
			<?php endforeach; ?>
		</div>
	</div>

	<form id="slide_form" action="<?=$this->uri->full_url('admin/catalog_slider/save');?>" method="post">
		<input type="hidden" name="slide_id" value="<?=$slide_id;?>">
		<?php foreach ($languages as $key => $val): ?>
		<?php
			$sizes[$key] = array(0, 0);

			if ($slide['file_name_' . $key] !== '') {
				$sizes[$key] = getimagesize($dir . '/s_' . $slide['file_name_' . $key]);
			}
		?>
		<div class="lang_tab lang_tab_<?=$key;?>"<?php if (LANG !== $key): ?> style="display:none"<?php endif; ?>>
			<div class="evry_title">
				<label class="block_label">Назва:</label>
				<input type="text" name="title[<?=$key;?>]" value="<?=$slide['title_' . $key];?>">
			</div>

			<div class="evry_title">
				<label class="block_label">Посилання:</label>
				<input type="text" name="url[<?=$key;?>]" value="<?=$slide['url_' . $key];?>">
			</div>
		</div>
		<div class="lang_tab lang_tab_<?=$key;?>"<?php if (LANG !== $key): ?> style="display:none"<?php endif; ?>>
			<div class="evry_title">
				<label class="block_label">Зображення:</label>
				<div class="fm" id="image_uploader_box_<?=$key;?>"<?php if ($slide['file_name_' . $key] !== ''): ?> style="display: none"<?php endif; ?>><div id="image_uploader_<?=$key;?>"></div></div>
				<div class="fm for_photo_cut" id="image_box_<?=$key;?>" style="<?php if ($slide['file_name_' . $key] === ''): ?> display: none<?php endif; ?>">
					<div class="fm photo_cut" id="image_<?=$key;?>">
						<div class="fm photo_holder" style="width:<?=$thumb[0];?>px; height:<?=$thumb[1];?>px;">
							<div style="width:<?=$thumb[0];?>px; height:<?=$thumb[1];?>px;">
								<?php if ($slide['file_name_' . $key] !== ''): ?><img src="upload/catalog_slider/<?=$slide_id;?>/t_<?=$slide['file_name_' . $key];?>" alt=""><?php endif; ?>
							</div>
						</div>
						<div class="links">
							<a href="#" id="crop_image_<?=$key;?>" class="fm fpc_edit" data-width="<?=$sizes[$key][0];?>" data-height="<?=$sizes[$key][1];?>"><b></b>Редагувати</a>
							<a href="#" id="delete_image_<?=$key;?>" class="fm fpc_delete"><b></b>Видалити</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
		<div class="fm for_sucsess">
			<div class="fmr save_links">
				<a href="#" class="fm save_adm"><b></b>Зберегти</a>
				<a href="#" class="fm apply_adm"><b></b>Застосувати</a>
				<a href="<?=$this->init_model->get_link($menu_id, '{URL}');?>" class="fm cansel_adm"><b></b>До списку слайдів</a>
			</div>
		</div>
	</form>
</div>

<script id="slider_image_template" type="text/html">
	<div class="fm photo_holder" style="width:<?=$thumb[0];?>px; height:<?=$thumb[1];?>px;">
		<div style="width:<?=$thumb[0];?>px; height:<?=$thumb[1];?>px;">
			<img src="/upload/catalog_slider/<?=$slide_id;?>/t_{{ file_name }}" alt="">
		</div>
	</div>
	<div class="links">
		<a href="#" id="crop_image_{{ language }}" class="fm fpc_edit" data-width="{{ width }}" data-height="{{ height }}"><b></b>Редагувати</a>
		<a href="#" id="delete_image_{{ language }}" class="fm fpc_delete"><b></b>Видалити</a>
	</div>
</script>

<script id="slider_crop_template" type="text/html">
	<div id="crop_overlay" class="confirm_overlay" style="display: block; opacity: 0.5;"></div>
	<div id="crop_modal" class="crop_modal">
		<div class="fm crop_area">
			<div class="fm ca_panel">
				<a id="crop_cancel" href="#" class="fmr ca_cencel"><b></b>Скасувати</a>
				<a id="crop_save" href="#" class="fmr ca_save"><b></b>Зберегти</a>
				<span class="controls">
					<label class="check_label active"><i><input type="checkbox" name="proportion" checked="checked" value="1"></i>Пропорційно</label>
				</span>
			</div>
			<div id="crop_preview" class="fm crop_review">
				<div style="overflow: hidden" class="crop_prew_border">
					<img src="{{ source }}" alt="">
				</div>
			</div>
			<div id="crop_source" class="fm crop_source">
				<img width="{{ width }}" height="{{ height }}" src="{{ source }}">
			</div>
		</div>
	</div>
</script>

<script type="text/javascript">
	var source = {
			ua: '<?=($slide['file_name_ua'] !== '') ? ('/upload/catalog_slider/' . $slide_id . '/s_' . $slide['file_name_ua']) : '';?>',
			ru: '<?=($slide['file_name_ru'] !== '') ? ('/upload/catalog_slider/' . $slide_id . '/s_' . $slide['file_name_ru']) : '';?>',
			en: '<?=($slide['file_name_en'] !== '') ? ('/upload/catalog_slider/' . $slide_id . '/s_' . $slide['file_name_en']) : '';?>',
			pl: '<?=($slide['file_name_pl'] !== '') ? ('/upload/catalog_slider/' . $slide_id . '/s_' . $slide['file_name_pl']) : '';?>'
		},
		api = {
			ua: null,
			ru: null,
			en: null,
			pl: null
		};

	$(document).ready(function () {
		$('.component_lang').find('a').each(function () {
			var language = $(this).data('language');

			$(this).on('click', function (e) {
				e.preventDefault();

				$(this).addClass('active').siblings().removeClass('active');
				$('.lang_tab').hide();
				$('.lang_tab_' + $(this).data('language')).show();
			});

			/* Slide Image */

			$('#image_uploader_' + language)
				.fineUploader({
					request: {
						endpoint: (language !== DEF_LANG ? '/' + language : '') + '/admin/catalog_slider/upload_image/',
						inputName: 'slide_image_' + language,
						params: {
							slide_id : <?=$slide_id;?>
						}
					},
					multiple: false,
					text: {
						uploadButton: 'Виберіть або перетягніть файл зображення',
						dragZone: '',
						dropProcessing: ''
					},
					validation: {
						allowedExtensions: ['jpeg', 'jpg', 'png', 'gif'],
						sizeLimit: <?=(int)ini_get('upload_max_filesize') * 1048576;?>
					},
					messages: {
						typeError: "Дозволено завантажувати: {extensions}.",
						sizeError: "Розмір файлу не повинен перевищувати {sizeLimit}.",
						tooManyproductsError: "Дозволено завантажувати файлів: {productLimit}."
					}
				})
				.on('complete', function (event, id, fileName, response) {
					if (response.success) {
						$('.qq-upload-success').remove();
						source[language] = '/upload/catalog_slider/<?=$slide_id;?>/s_' + response.file_name;

						$('#image_uploader_box_' + language).hide();

						var image = Mustache.render($('#slider_image_template').html(), $.extend(response, {language: language}));
						$('#image_' + language).html(image);

						$('#image_box_' + language).show();
					}
				});

			$('#image_box_' + language)
				.on('click', '#delete_image_' + language, function (e) {
					e.preventDefault();

					component_loader_show($('.component_loader'), '');

					$.post(
						(language !== DEF_LANG ? '/' + language : '') + '/admin/catalog_slider/delete_image/',
						{
							slide_id: <?=$slide_id;?>
						},
						function (response) {
							if (response.success) {
								component_loader_hide($('.component_loader'), '');
								$('#image_box_' + language).hide('').find('#image_' + language).html('');
								$('#image_uploader_box_' + language).show();
							}
						},
						'json'
					);
				})
				.on('click', '#crop_image_' + language, function (e) {
					e.preventDefault();

					if ($.type(api[language]) === 'object') {
						api[language].destroy();
						api[language] = null;
					}

					var $link = $(this),
						width = $link.data('width') > <?=$thumb[0];?> ? <?=$thumb[0];?> : $link.data('width'),
						height = width * $link.data('height') / $link.data('width'),
						crop_modal = Mustache.render(
							$('#slider_crop_template').html(),
							{
								language: language,
								source: source[language],
								width: width,
								height: height
							}
						);

					$('body').append(crop_modal);
					$('#crop_preview').css('width', <?=$thumb[0];?> + 'px');
					$('#crop_overlay').css('height', $(document).height());
					$('#crop_modal').css('top', $(document).scrollTop() + 50);

					$('#crop_source').find('img').Jcrop({
							keySupport: false,
							aspectRatio: <?=$big[0]/$big[1];?>,
							setSelect: [0, 0, <?=$thumb[0];?>, <?=$thumb[1];?>],
							realSizes: [$link.data('width'), $link.data('height')],
							onChange: function (coords) {
								crop_preview($('#crop_preview').find('div'), coords, <?=$thumb[0];?>, <?=$thumb[1];?>, width, height);
							}
						},
						function () {
							api[language] = this;

							$('[name="proportion"]').off('change').on('change', function () {
								if ($(this).prop('checked')) {
									$(this).closest('label').addClass('active');
									api[language].setOptions({aspectRatio: <?=$big[0]/$big[1];?>});
								} else {
									$(this).closest('label').removeClass('active');
									api[language].setOptions({aspectRatio: 0});
								}
								api[language].focus();
							});

							$('#crop_cancel').off('click').on('click', function (e) {
								e.preventDefault();
								api[language].destroy();
								$('#crop_modal').add('#crop_overlay').remove();
							});

							$('#crop_save').off('click').on('click', function (e) {
								e.preventDefault();

								$.post(
									(language !== DEF_LANG ? '/' + language : '') + '/admin/catalog_slider/crop_image/',
									{
										slide_id: <?=$slide['slide_id'];?>,
										width: width,
										coords: api[language].tellScaled()
									},
									function (response) {
										if (response.success) {
											api[language].destroy();
											$('#crop_modal').add('#crop_overlay').remove();
											$('#image_' + language).find('img').eq(0).attr('src', response.image);
										}
									},
									'json'
								);
							});
						});
				});
		});

		/**
		 * Відправка форми
		 */
		$('.save_adm').add('.component_edit_links .save').on('click', function (e) {
			e.preventDefault();

			$('#slide_form').ajaxSubmit({
				beforeSubmit:function () {
					component_loader_show($('.component_loader'), '');
				},
				success:function (response) {
					if (response.success) {
						component_loader_hide($('.component_loader'), '');
						window.location.href = '<?=$this->init_model->get_link($menu_id, '{URL}');?>';
					}
				},
				dataType: 'json'
			});
		});

		/**
		 * Відправка форми
		 */
		$('a.apply_adm').add('.component_edit_links .apply').on('click', function (e) {
			e.preventDefault();

			$('#slide_form').ajaxSubmit({
				beforeSubmit:function () {
					component_loader_show($('.component_loader'), '');
				},
				success:function (response) {
					if (response.success) {
						component_loader_hide($('.component_loader'), '');
					}
				},
				dataType: 'json'
			});
		});
	});
</script>