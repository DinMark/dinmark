<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	/**
	 * Class Profile
	 *
	 * @property Profile_model $profile_model
	 */
	class Profile extends MX_Controller
	{
		public function __construct()
		{
			parent::__construct();

			if ($this->input->method() === 'get') {
				$segments = $this->uri->segment_array();
				$languages = array_keys($this->config->item('languages'));

				if (array_key_exists(1, $segments) and in_array($segments[1], $languages, true)) {
					array_shift($segments);
				}

				$uri = implode('/', $segments) . ($this->input->server('QUERY_STRING') !== '' ? '?' . ltrim($this->input->server('QUERY_STRING'), '?') : '');

				$links = array();

				foreach ($languages as $lang) {
					$links[$lang] = $this->uri->full_url($uri, $lang !== DEF_LANG ? $lang : '');
				}

				$this->template_lib->set_template_var('language_links', $links);
			}
		}

		/**
		 * Форма реєстрації
		 */
		public function registration()
		{
			$page_title = LANG == 'ua' ? 'Стати партнером' : 'Стать партнером';

			$this->template_lib->set_title($page_title);
			$this->template_lib->set_bread_crumbs('', $page_title);

			$this->load->model('catalog_help/catalog_help_model');

			$template_data = array(
				'help_registration' => $this->catalog_help_model->get_help('registration'),
				'help__registration' => $this->catalog_help_model->get_help('_registration'),
			);

			$c = $this->load->view('registration/registration_tpl', $template_data, TRUE);
			$this->template_lib->set_content($c);
		}

		/**
		 * Реєстрація акаунту
		 *
		 * @return string
		 */
		public function create()
		{
			$response = array('success' => FALSE);

			$this->load->library('form_validation');

			$this->form_validation->set_rules('name', '1', 'required|trim|xss_clean|form_prep');
			$this->form_validation->set_rules('email', '2', 'required|valid_email|trim|xss_clean|form_prep');
			$this->form_validation->set_rules('phone', '6', 'required|trim|xss_clean|form_prep');
			$this->form_validation->set_rules('password', '3', 'required|trim|xss_clean|strip_tags');
			$this->form_validation->set_rules('reply_password', '4', 'required|matches[password]');

			if ($this->form_validation->run())
			{
				$this->load->model('profile_model');
				$this->load->helper('form');

				if ($this->profile_model->check_email($this->input->post('email')))
				{
					$response['errors'][] = 5;
				}
				else
				{
					$salt = md5($this->profile_model->get_random_password());

					$set = array(
						'name' => $this->input->post('name'),
						'email' => $this->input->post('email'),
						'phone' => $this->input->post('phone'),
						'password' => md5($salt . base64_encode($this->input->post('password'))),
						'salt' => $salt,
						'tmp_key' => md5($salt . $this->config->item('encryption_key') . $this->input->post('email')),
					);
					$this->profile_model->create_profile($set);

					$this->load->library('Email_lib');

					$domain_name = str_replace('www.', '', $this->input->server('HTTP_HOST'));
					$this->email_lib->SetFrom('info@' . $domain_name, $domain_name);
					$this->email_lib->AddAddress($set['email'], $set['name']);

					$subject = '';
					if (LANG == 'ua') $subject = 'Активація користувача';
					if (LANG == 'ru') $subject = 'Активация пользователя';

					$this->email_lib->Subject = $subject;
					$this->email_lib->Body = $this->load->view('registration/register_letter_tpl', $set, TRUE);

					$this->email_lib->Send();

					$response['success'] = TRUE;
				}
			}

			return json_encode($response);
		}

		/**
		 * Активація аккаунту
		 */
		public function activate()
		{
			$page_title = LANG == 'ua' ? 'Активація профілю' : 'Активация профиля';
			$this->template_lib->set_title($page_title);
			$this->template_lib->set_bread_crumbs('', $page_title);

			$key = $this->input->get('key');

			if ($key === NULL OR mb_strlen($key) != 32)
			{
				$content = $this->load->view('activate/activate_fail_tpl', '', TRUE);
				$this->template_lib->set_content($content);
			}
			else
			{
				$this->load->model('profile_model');

				$user = $this->profile_model->activate_profile($key);

				if ($user)
				{
					$this->profile_model->login($user);
					$this->profile_model->bind_orders($user['user_id'],  $user['email']);

					$content = $this->load->view('activate/activate_success_tpl', '', TRUE);
					$this->template_lib->set_content($content);
				}
				else
				{
					$content = $this->load->view('activate/activate_fail_tpl', '', TRUE);
					$this->template_lib->set_content($content);
				}
			}
		}

		/**
		 * Форма входу
		 */
		public function login()
		{
			$page_title = LANG == 'ua' ? 'Вхід' : 'Вход';

			$this->template_lib->set_title($page_title);
			$this->template_lib->set_bread_crumbs('', $page_title);

			$c = $this->load->view('enter_tpl', '', TRUE);
			$this->template_lib->set_content($c);
		}

		/**
		 * Вхід
		 *
		 * @return string
		 */
		public function enter()
		{
			$response = array('success' => FALSE);

			$this->load->library('form_validation');

			$this->form_validation->set_rules('email', '1', 'required|valid_email|trim|xss_clean|strip_tags');
			$this->form_validation->set_rules('password', '1', 'required|trim|xss_clean|strip_tags');

			if ($this->form_validation->run())
			{
				$this->load->model('profile_model');

				$user = $this->profile_model->get_user(NULL, $this->input->post('email'), 'user_id, name, password, salt, email, phone');

				if ($user)
				{
					if (md5($user['salt'] . base64_encode($this->input->post('password'))) == $user['password'])
					{
						$this->profile_model->login($user);
						$response['success'] = TRUE;
					}
				}
			}

			return json_encode($response);
		}

		/**
		 * Форма відновлення паролю
		 */
		public function remember()
		{
			$this->template_lib->set_title('Відновлення паролю');

			$c = $this->load->view('remember/remember_tpl', '', TRUE);
			$this->template_lib->set_content($c);
		}

		/**
		 * Відправка листа з запитом на відновлення паролю
		 */
		public function send_remember()
		{
			$response = array('errors' => array());

			$this->load->library('captcha_lib');
			if (!$this->captcha_lib->validate($this->input->post('captcha', TRUE), 'remember'))
			{
				$response['errors'] = array(1);
			}
			else
			{
				$this->load->library('form_validation');
				$this->form_validation->set_rules('email', '', 'required|valid_email');

				if (!$this->form_validation->run())
				{
					$response['errors'] = array(2);
				}
				else
				{
					$this->load->model('profile_model');

					$email = $this->input->post('email');
					$user = $this->profile_model->get_user(NULL, $email, 'user_id, name, salt');

					if (!$user)
					{
						$response['errors'] = array(2);
					}
					else
					{
						$key = md5($user['salt'] . rand(100000, 999999) . $this->config->item('encryption_key') . $this->input->post('email'));

						$set = array('tmp_key' => $key);
						$this->profile_model->update_user($user['user_id'], $set);

						$subject = '';
						if (LANG == 'ua') $subject = 'Запит на відновлення паролю';
						if (LANG == 'ru') $subject = 'Запрос на восстановление пароля';

						$letter_data = array(
							'key' => $key,
							'name' => $user['name'],
							'email' => $email,
							'subject' => $subject,
						);

						$this->load->library('Email_lib');

						$domain_name = str_replace('www.', '', $this->input->server('HTTP_HOST'));
						$this->email_lib->SetFrom('info@' . $domain_name, $domain_name);
						$this->email_lib->AddAddress($email, $user['name']);

						$this->email_lib->Subject = $subject;
						$this->email_lib->Body = $this->load->view('remember/remember_letter_step_one_tpl', $letter_data, TRUE);

						$this->email_lib->Send();
					}
				}
			}

			return json_encode($response);
		}

		/**
		 * Формування нового паролю користувача
		 */
		public function email_remember()
		{
			$this->template_lib->set_title('Відновлення паролю');

			$key = $this->input->get('key');

			if (mb_strlen($key) == 32)
			{
				$this->load->model('profile_model');

				$user = $this->profile_model->reset_password($key);

				if ($user)
				{
					$letter_data = array('user' => $user);

					$this->load->library('Email_lib');

					$domain_name = str_replace('www.', '', $this->input->server('HTTP_HOST'));
					$this->email_lib->SetFrom('info@' . $domain_name, $domain_name);
					$this->email_lib->AddAddress($user['email'], $user['name']);

					if (LANG == 'ua') $letter_data['subject'] = 'Новий пароль';
					if (LANG == 'ru') $letter_data['subject'] = 'Новый пароль';

					$this->email_lib->Subject = $letter_data['subject'];
					$this->email_lib->Body = $this->load->view('remember/remember_letter_step_two_tpl', $letter_data, TRUE);

					$this->email_lib->Send();

					$c = $this->load->view('remember/remember_success_tpl', '', TRUE);
					$this->template_lib->set_content($c);
				}
				else
				{
					$c = $this->load->view('remember/remember_fail_tpl', '', TRUE);
					$this->template_lib->set_content($c);
				}
			}
			else
			{
				$c = $this->load->view('remember/remember_fail_tpl', '', TRUE);
				$this->template_lib->set_content($c);
			}
		}

		/**
		 * Форма редагування аккаунту
		 */
		public function data()
		{
			if (!$this->init_model->is_user()) redirect($this->uri->full_url('profile/login'));

			$page_title = LANG == 'ua' ? 'Персональні дані' : 'Персональные данные';

			$this->template_lib->set_title($page_title);
			$this->template_lib->set_bread_crumbs('', $page_title);

			$this->load->model('profile_model');

			$user_id = $this->session->userdata('user_id');
			$user = $this->profile_model->get_user($user_id);

			if ($user['price_type'] === 'p8') {
				$user['price'] = 1.4;
			} elseif ($user['price_type'] === 'p7') {
				$user['price'] = 1.45;
			} elseif ($user['price_type'] === 'p6') {
				$user['price'] = 1.5;
			} elseif ($user['price_type'] === 'p5') {
				$user['price'] = 1.6;
			} elseif ($user['price_type'] === 'p4') {
				$user['price'] = 1.7;
			} elseif ($user['price_type'] === 'p3') {
				$user['price'] = 1.8;
			} elseif ($user['price_type'] === 'p2') {
				$user['price'] = 1.9;
			} else {
				$user['price'] = 2;
			}

			$template_data = array('user' => $user);
			$c = $this->load->view('edit/edit_tpl', $template_data, TRUE);
			$this->template_lib->set_content($c);
		}

		/**
		 * Збереження основних даних користувача
		 *
		 * @return string
		 */
		public function save()
		{
			if (!$this->init_model->is_user()) die();

			$response = array('success' => FALSE, 'error_code' => 0);

			$this->load->library('form_validation');

			$this->form_validation->set_rules('name', '1', 'required|trim|xss_clean|strip_tags|form_prep');
			$this->form_validation->set_rules('email', '2', 'required|valid_email|trim|xss_clean|strip_tags|form_prep');

			if ($this->input->post('password') != '')
			{
				$this->form_validation->set_rules('old_password', '3', 'required');
				$this->form_validation->set_rules('password', '4', 'required');
				$this->form_validation->set_rules('repeat_password', '5', 'required|matches[password]');
			}

			if ($this->form_validation->run())
			{
				$this->load->model('profile_model');
				$this->load->helper('form');

				if ($this->profile_model->check_email($this->input->post('email'), $this->session->userdata('user_id')))
				{
					$response['error_code'] = 1;
				}
				else
				{
					if ($this->input->post('password') != '')
					{
						if (!$this->profile_model->check_password($this->input->post('old_password'), $this->session->userdata('user_id'))) $response['error_code'] = 2;
					}

					if (count($response['errors']) == 0)
					{
						$set = array(
							'name' => $this->input->post('name'),
							'email' => $this->input->post('email'),
							'phone' => form_prep(strip_tags($this->input->post('phone', TRUE))),
						);

						if ($this->input->post('password') != '')
						{
							$salt = md5($this->profile_model->get_random_password());
							$set['password'] = md5($salt . base64_encode($this->input->post('password')));
							$set['salt'] = $salt;
						}

						$this->profile_model->update_user($this->session->userdata('user_id'), $set);

						$session = array(
							'user_name' => $this->input->post('name'),
							'user_email' => $this->input->post('email'),
							'user_phone' => form_prep(strip_tags($this->input->post('phone', TRUE))),
						);

						$this->session->set_userdata($session);
						$response['success'] = TRUE;
					}
				}
			}

			return json_encode($response);
		}

		/**
		 * Форма редагування доставки
		 */
		public function delivery()
		{
			if (!$this->init_model->is_user()) redirect($this->uri->full_url('profile/login'));

			$page_title = 'Доставка';

			$this->template_lib->set_title($page_title);
			$this->template_lib->set_bread_crumbs('', $page_title);

			$this->load->model('profile_model');
			$this->load->model('catalog_help/catalog_help_model');

			$user_id = $this->session->userdata('user_id');
			$user = $this->profile_model->get_user($user_id, null, 'delivery');
			$delivery = (isset($user['delivery']) and $user['delivery'] !== '') ? json_decode(stripslashes($user['delivery']), true) : array();

			$template_data = array(
				'helps' => $this->catalog_help_model->get_helps(),
				'delivery' => $delivery,
			);

			if (isset($delivery['np_type']) and (int)$delivery['np_type'] === 1) {
				$this->load->model('cart/cart_model');

				if (isset($delivery['np_city']) and $delivery['np_city'] !== '') {
					$city = $this->cart_model->get_city($delivery['np_city']);

					if ($city !== null) {
						$template_data['np_city'] = $city['name'];
						$template_data['np_warehouses'] = $this->cart_model->get_warehouses($delivery['np_city']);
 					}
				}
			}

			$this->template_lib->set_content(
				$this->load->view('edit/delivery_tpl', $template_data, TRUE)
			);
		}

		/**
		 * Збереження налаштувань доставки користувача
		 *
		 * @return string
		 */
		public function save_delivery()
		{
			error_reporting(E_ALL);
			ini_set('display_errors', true);

			if (!$this->init_model->is_user()) die();

			$response = array(
				'success' => FALSE,
			);


			$delivery = array(
				'type' => (int)$this->input->post('type'),
			);

			if ($delivery['type'] === 2) {
				$delivery['np_type'] = (int)$this->input->post('np_type');
			}

			if ($delivery['type'] > 1) {
				if (isset($delivery['np_type']) and $delivery['np_type'] === 1) {
					$delivery['np_city'] = $this->input->post('np_city', true);
					$delivery['np_warehouse'] = $this->input->post('np_warehouse', true);
				} else {
					$delivery['address_1'] = $this->input->post('address_1', true);
					$delivery['address_2'] = $this->input->post('address_2', true);
					$delivery['region'] = $this->input->post('region', true);
					$delivery['city'] = $this->input->post('city', true);
				}
			}

			$this->load->model('profile/profile_model');

			$this->profile_model->update_user(
				$this->session->userdata('user_id'),
				array(
					'delivery' => $this->db->escape_str(json_encode($delivery)),
				)
			);


			$response['success'] = TRUE;
			return json_encode($response);
		}

		/**
		 * Замовлення
		 */
		public function orders()
		{
			if (!$this->init_model->is_user()) redirect($this->uri->full_url('profile/login'));

			$page_title = LANG == 'ua' ? 'Замовлення' : 'Заказы';

			$this->template_lib->set_title($page_title);
			$this->template_lib->set_bread_crumbs('', $page_title);

			$this->load->model('profile_model');

			$template_data = array('orders' => $this->profile_model->get_orders());
			$c = $this->load->view('edit/orders_tpl', $template_data, TRUE);
			$this->template_lib->set_content($c);
		}

		/**
		 * Закінчення сеансу
		 */
		public function logout()
		{
			$this->load->model('profile_model');
			$this->profile_model->logout();

			redirect($this->uri->full_url());
		}

		/**
		 * Оновлення каптчі відновлення паролю
		 */
		public function remember_captcha()
		{
			$this->load->library('captcha_lib');
			$this->captcha_lib->get_image('remember');
			$this->config->set_item('is_ajax_request', TRUE);
		}

		/**
		 * Порівняння
		 */
		public function compare()
		{
			$page_title = LANG == 'ua' ? 'Порівняння' : 'Сравнение';

			$products = explode(',', (string)$this->input->get('products'));
			$products = array_map('intval', $products);

			if (count($products) == 0) {
				redirect($this->uri->full_url());
			}

			$this->template_lib->set_title($page_title);
			$this->template_lib->set_bread_crumbs('', $page_title);

			$this->load->model('catalog/catalog_model');

			$variants = $this->catalog_model->get_variants(0, $products);

			foreach ($variants as &$v) {
				$v['filters'] = $this->catalog_model->get_variant_filters($v['variant_id']);
				$v['product'] = $this->catalog_model->get_simple($v['product_id']);
			}

			$template_data = array(
				'variants' => $variants
			);
			$c = $this->load->view('compare_tpl', $template_data, TRUE);
			$this->template_lib->set_content($c);
		}

		/**
		 * Обрані
		 */
		public function favorite()
		{
			$page_title = LANG == 'ua' ? 'Вибрані товари' : 'Выбранные товары';

			$this->template_lib->set_title($page_title);
			$this->template_lib->set_bread_crumbs('', $page_title);

			$template_data = array();

			$products = explode(',', (string)$this->input->get('products'));
			$products = array_map('intval', $products);

			if (count($products) > 0) {
				$this->load->model('catalog/catalog_model');

				$variants = $this->catalog_model->get_variants(0, $products);

				foreach ($variants as &$v) {
					$v['filters'] = $this->catalog_model->get_variant_filters($v['variant_id']);
					$v['product'] = $this->catalog_model->get_simple($v['product_id']);
				}

				$template_data['variants'] = $variants;
			}

			$c = $this->load->view('favorite_tpl', $template_data, TRUE);
			$this->template_lib->set_content($c);
		}
	}