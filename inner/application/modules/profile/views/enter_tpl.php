<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>

<div class="login-page fm">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="sect-title fm"><span><? if(LANG=='ua')echo'Вхід';if(LANG=='ru')echo'Вход';if(LANG=='en')echo'Login';?></span></div>
				<div class="login-wrapper fm">
					<form id="login_form" action="<?=$this->uri->full_url('profile/enter');?>" method="post">
		                <div class="fm input-item">
		                    <label for="login_email">Email:</label>
		                    <input type="text" id="login_email" name="login_email" value="">
		                </div>
		                <div class="fm input-item">
		                    <label for="login_password"><? if(LANG=='ua')echo'Пароль';if(LANG=='ru')echo'Пароль';if(LANG=='en')echo'Password';?>:</label>
		                    <input type="password" id="login_password" name="login_password" value="">
		                </div>
		                <div class="fm input-item box-c">
		                	<a href="#" id="login_send" class="common-btn send_adm"><? if(LANG=='ua')echo'Увійти';if(LANG=='ru')echo'Войти';if(LANG=='en')echo'Login';?></a>
		                </div>
		                <div class="fm input-item box-c">
		                	<a class="remind_me" href="<?=$this->uri->full_url('profile/remember');?>"><? if(LANG=='ua')echo'Забули пароль';if(LANG=='ru')echo'Забыли пароль';if(LANG=='en')echo'Forgot your password';?>?</a>
		                </div>
		            </form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	function login() {
		var $login_fields = {email: $('#login_email'), password: $('#login_password')},
			$login_send = $('#login_send'),
			login_request = {email: $.trim($login_fields.email.val()), password: $.trim($login_fields.password.val())},
			login_error = false;

		if (!emailRegex.test(login_request.email)) {
			$login_fields.email.addClass('error');
			login_error = true;
		}

		if (!ruleRegex.test(login_request.password)) {
			$login_fields.password.addClass('error');
			login_error = true;
		}

		if (!login_error) {
			var msg_4 = '';
			if (LANG == 'ua') msg_4 = 'Відправка запиту...';
			if (LANG == 'ru') msg_4 = 'Отправка запроса...';
			if (LANG == 'en') msg_4 = 'Sending request...';
			$login_send.text(msg_4);

			$.post(
				'<?=$this->uri->full_url('profile/enter');?>',
				login_request,
				function (response) {
					if (response.success) {
						window.location.href = '<?=$this->uri->full_url();?>';
					} else {
						$login_fields.email.addClass('error');
						$login_fields.password.addClass('error');
						var msg_5 = '';
						if (LANG == 'ua') msg_5 = 'Увійти';
						if (LANG == 'ru') msg_5 = 'Войти';
						if (LANG == 'en') msg_5 = 'Login';
						$login_send.text(msg_5);
					}
				},
				'json'
			);
		}
	}

	$(function () {
		$('#login_send').on('click', function (e) {
			e.preventDefault();
			$(this).closest('form').submit();
		});

		$('#login_form')
			.on('keyup blur paste', 'input', function (e) {
				if (e.keyCode == 13) {
					$(this).closest('form').submit();
				} else {
					var val = $.trim($(this).val());
					if (ruleRegex.test(val)) $(this).removeClass('error');
				}
			})
			.on('submit', function (e) {
				e.preventDefault();
				login();
			});
	});
</script>