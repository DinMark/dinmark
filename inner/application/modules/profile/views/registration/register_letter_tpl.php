<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title><?php echo $subject; ?></title>
	<style type="text/css">
		body, table tr td, table tr th {font: 12px/18px Verdana, Arial, Tahoma, sans-serif;}
		table tr td, table tr th {padding: 5px 0 5px 0;empty-cells: show;}
	</style>
</head>
<body>
<?php if (LANG == 'ua'): ?>
<table align="center" width="900" cellpadding="10" cellspacing="0" border="0" style="border-collapse: collapse;">
	<tr>
		<td align="left" style="padding-bottom: 30px">
			<span style="font-size: 14px">Ви зареєструвались на сайті <a href="<?php echo base_url(); ?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319"><?php echo $this->config->item('site_name_' . LANG); ?></span></a>.</span>
		</td>
	</tr>
	<tr>
		<td align="left" style="padding-bottom: 30px">
			Шановний(а) <b><?php echo $name; ?></b> перейдіть по даному <a href="<?php echo $this->uri->full_url('profile/activate?key=' . $tmp_key); ?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319">лінку активації</span></a>, після цього Ви зможете залогуватись на сайті. Активація одноразова.
			<br><br>
			На цей лист відповідати не потрібно - він згенерований автоматично.
		</td>
	</tr>
	<tr>
		<td style="left">
			&copy; <?php echo date('Y'); ?> <?php echo $this->config->item('site_name_' . LANG); ?><br/>
			<a href="http://www.websufix.com" style="color:#0061b3; font:10px Tahoma; text-decoration:none; margin:0 0 0 17px;"><span style="color:#0061b3;">SUFIX web design</span></a>
		</td>
	</tr>
</table>
<?php endif; ?>
<?php if (LANG == 'ru'): ?>
<table align="center" width="900" cellpadding="10" cellspacing="0" border="0" style="border-collapse: collapse;">
	<tr>
		<td align="left" style="padding-bottom: 30px">
			<span style="font-size: 14px">Вы зарегистрировались на сайте <a href="<?php echo base_url(); ?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319"><?php echo $this->config->item('site_name_' . LANG); ?></span></a>.</span>
		</td>
	</tr>
	<tr>
		<td align="left" style="padding-bottom: 30px">
			Уважаемый(ая) <b><?php echo $name; ?></b> перейдите по данной <a href="<?php echo $this->uri->full_url('profile/activate?key=' . $tmp_key); ?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319">ссылке активации</span></a>, после этого Вы сможете войти на сайт. Активация одноразовая.
			<br><br>
			На это письмо отвечать не нужно - оно сгенерировано автоматически.
		</td>
	</tr>
	<tr>
		<td style="left">
			&copy; <?php echo date('Y'); ?> <?php echo $this->config->item('site_name_' . LANG); ?><br/>
			<a href="http://www.websufix.com" style="color:#0061b3; font:10px Tahoma; text-decoration:none; margin:0 0 0 17px;"><span style="color:#0061b3;">SUFIX web design</span></a>
		</td>
	</tr>
</table>
<?php endif; ?>
<?php if (LANG == 'en'): ?>
<table align="center" width="900" cellpadding="10" cellspacing="0" border="0" style="border-collapse: collapse;">
	<tr>
		<td align="left" style="padding-bottom: 30px">
			<span style="font-size: 14px">You have registered on the site <a href="<?php echo base_url(); ?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319"><?php echo $this->config->item('site_name_' . LANG); ?></span></a>.</span>
		</td>
	</tr>
	<tr>
		<td align="left" style="padding-bottom: 30px">
			Dear <b><?php echo $name; ?></b> click on this <a href="<?php echo $this->uri->full_url('profile/activate?key=' . $tmp_key); ?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319">activation link</span></a>, then you can log in the site. Enabling single.
			<br><br>
			This letter is not required to answer - it is generated automatically.
		</td>
	</tr>
	<tr>
		<td style="left">
			&copy; <?php echo date('Y'); ?> <?php echo $this->config->item('site_name_' . LANG); ?><br/>
			<a href="http://www.websufix.com" style="color:#0061b3; font:10px Tahoma; text-decoration:none; margin:0 0 0 17px;"><span style="color:#0061b3;">SUFIX web design</span></a>
		</td>
	</tr>
</table>
<?php endif; ?>
</body>
</html>