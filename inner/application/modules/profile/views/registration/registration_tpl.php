<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>

<div class="login-page fm">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/yLyERXwDkMY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="col-md-6">
				<div class="sect-title fm"><span><? if (LANG == 'ua') echo 'Стати партнером'; if (LANG == 'ru') echo 'Стать партнером'; if (LANG == 'en') echo 'Partner with'; ?></span></div>
				<div class="login-wrapper fm">
					<form id="reg_form" action="<?= $this->uri->full_url('profile/create'); ?>" method="post">
						<div class="fm input-item">
							<label for="reg_name"><? if (LANG == 'ua') echo 'Ім’я'; if (LANG == 'ru') echo 'Имя'; if (LANG == 'en') echo 'Name'; ?>:</label>
							<input type="text" id="reg_name" name="reg_name" value="">
						</div>
						<div class="fm input-item">
							<label for="reg_email">Email:</label>
							<input type="text" id="reg_email" name="reg_email" value="">
						</div>
						<div class="fm input-item">
							<label for="reg_phone"><?php if(LANG=='ua')echo'Телефон';if(LANG=='ru')echo'Телефон';if(LANG=='en')echo'Phone';?>:</label>
							<input type="text" id="reg_phone" name="reg_phone" value="">
						</div>
						<div class="fm input-item">
							<label for="reg_password"><? if (LANG == 'ua') echo 'Пароль'; if (LANG == 'ru') echo 'Пароль'; if (LANG == 'en') echo 'Password'; ?>:</label>
							<input type="password" id="reg_password" name="reg_password" value="">
						</div>
						<div class="fm input-item">
							<label for="reg_repeat_password"><? if (LANG == 'ua') echo 'Повторіть пароль'; if (LANG == 'ru') echo 'Повторите пароль'; if (LANG == 'en') echo 'Confirm Password'; ?>:</label>
							<input type="password" id="reg_repeat_password" name="reg_repeat_password" value="">
						</div>
						<div class="fm input-item box-c">
							<a href="#" id="reg_send"
							   class="common-btn send_adm"><? if (LANG == 'ua') echo 'Зареєструватись'; if (LANG == 'ru') echo 'Регистрация'; if (LANG == 'en') echo 'Register'; ?></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="sect-article fm">
    <div class="container">
        <div class="article-wrapper fm">
            <article>
                <?=$help_registration;?>
            </article>
        </div>
    </div>
</section>

<section class="sect-article fm after_create hidden">
    <div class="container">
        <div class="article-wrapper fm">
            <article>
                <div class="sect-title fm"><span><? if (LANG == 'ua') echo 'Ваш обліковий запис успішно створено!'; if (LANG == 'ru') echo 'Ваш аккаунт успешно созданСтать партнером'; if (LANG == 'en') echo 'Your account was successfully created'; ?></span></div>
				<?=$help__registration;?>
            </article>
        </div>
    </div>
</section>

<script type="text/javascript">

	function registration() {
		var $reg_fields = {
				name: $('#reg_name'),
				email: $('#reg_email'),
				phone: $('#reg_phone'),
				password: $('#reg_password'),
				reply_password: $('#reg_repeat_password')
			},
			$reg_send = $('#reg_send'),
			reg_request = {
				name: $.trim($reg_fields.name.val()),
				email: $.trim($reg_fields.email.val()),
				phone: $.trim($reg_fields.phone.val()),
				password: $.trim($reg_fields.password.val()),
				reply_password: $.trim($reg_fields.reply_password.val())
			},
			reg_error = false;

		if (!ruleRegex.test(reg_request.name)) {
			$reg_fields.name.addClass('error');
			reg_error = true;
		}

		if (!emailRegex.test(reg_request.email)) {
			$reg_fields.email.addClass('error');
			reg_error = true;
		}

        if (!ruleRegex.test(reg_request.phone)) {
            $reg_fields.phone.addClass('error');
            reg_error = true;
        }

		if (!ruleRegex.test(reg_request.password)) {
			$reg_fields.password.addClass('error');
			reg_error = true;
		}

		if (!ruleRegex.test(reg_request.reply_password)) {
			$reg_fields.reply_password.addClass('error');
			reg_error = true;
		}

		if (reg_request.password != reg_request.reply_password) {
			$reg_fields.password.addClass('error');
			$reg_fields.reply_password.addClass('error');
			reg_error = true;
		}
		if (!reg_error) {
			var msg_1 = '';
			if (LANG == 'ua') msg_1 = 'Відправка запиту...';
			if (LANG == 'ru') msg_1 = 'Отправка запроса...';
			if (LANG == 'en') msg_1 = 'Sending request...';
			$reg_send.text(msg_1);

			$.post(
				'<?=$this->uri->full_url('profile/create');?>',
				reg_request,
				function (response) {
					if (response.success) {
//						var msg_2 = '';
//						if (LANG == 'ua') msg_2 = 'Профіль створено. На Ваш email надіслано код активації';
//						if (LANG == 'ru') msg_2 = 'Профиль создан. На Ваш email отправлен код активации';
//						if (LANG == 'en') msg_2 = 'Profile created. At Your email was sent an activation code.';
//						$('#reg_form').html(msg_2);

                        $('.login-page').add('.sect-article').addClass('hidden');
                        $('.after_create').removeClass('hidden');
					} else {
						$reg_fields.email.addClass('error');
						var msg_3 = '';
						if (LANG == 'ua') msg_3 = 'Зареєструватись';
						if (LANG == 'ru') msg_3 = 'Регистрация';
						if (LANG == 'en') msg_3 = 'Register';
						$reg_send.text(msg_3);
					}
				},
				'json'
			);
		}
	}

	$(function () {
		$('#reg_send').on('click', function (e) {
			e.preventDefault();
			$(this).closest('form').submit();
		});

		$('#reg_form')
			.on('keyup blur paste', 'input', function (e) {
				if (e.keyCode == 13) {
					$(this).closest('form').submit();
				} else {
					var val = $.trim($(this).val());
					if (ruleRegex.test(val)) $(this).removeClass('error');
				}
			})
			.on('submit', function (e) {
				e.preventDefault();
				registration();
			});
	});
</script>