<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="login-page fm">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="sect-title fm"><span><? if(LANG=='ua')echo'Відновлення паролю';if(LANG=='ru')echo'Восстановление пароля';if(LANG=='en')echo'Password recovery';?></span></div>
				<div class="login-wrapper fm" id="remember_form">
					<div class="fm input-item">
						<label for="remember_email">Email:</label>
						<input type="text" id="remember_email" name="remember_email" value="">
					</div>
					<div class="fm input-item">
						<label for="remember_captcha"><? if(LANG=='ua')echo'Захисний код';if(LANG=='ru')echo'Защитный код';if(LANG=='en')echo'Security code';?>:</label>
						<div class="for_input cap">
							<input type="text" id="remember_captcha" name="remember_captcha" value="" maxlength="4">
							<a id="remember_captcha_reload" href="#" class="fm reload">
								<img id="remember_captcha_img" src="<?=$this->uri->full_url('profile/remember_captcha');?>" alt="">
							</a>
						</div>
					</div>
					<div class="fm input-item box-c">
						<a href="#" id="remember_send" class="common-btn send_adm"><? if(LANG=='ua')echo'Відновити пароль';if(LANG=='ru')echo'Восстановить пароль';if(LANG=='en')echo'Recover password';?></a>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="sect-title fm"><span><? if(LANG=='ua')echo'Вже зареєстровані';if(LANG=='ru')echo'Уже зарегистрированы';if(LANG=='en')echo'Already registered';?>?</span></div>
				<div class="input-item box-c fm">
					<a href="<?=$this->uri->full_url('profile/login');?>" class="common-btn"><? if(LANG=='ua')echo'Увійти';if(LANG=='ru')echo'Войти';if(LANG=='en')echo'Login';?></a>
				</div>
				<div class="sect-title fm"><span><? if(LANG=='ua')echo'Ще не зареєструвались';if(LANG=='ru')echo'Еще не зарегистрированы';if(LANG=='en')echo'Not yet registered';?>?</span></div>
				<div class="input-item box-c fm">
					<a href="<?=$this->uri->full_url('profile/login');?>" class="common-btn"><? if(LANG=='ua')echo'Зареєструватись';if(LANG=='ru')echo'Зарегистрироваться';if(LANG=='en')echo'Register';?></a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$('#remember_form').on('keyup blur paste', 'input', function () {
			var val = $.trim($(this).val());
			if (ruleRegex.test(val)) $(this).removeClass('error');
		});

		$('#remember_send').on('click', function (e) {
			e.preventDefault();

			var $remember_fields = {
					email: $('#remember_email'),
					captcha: $('#remember_captcha')
				},
				$remember_send = $('#remember_send'),
				remember_request = {
					email: $.trim($remember_fields.email.val()),
					captcha: $.trim($remember_fields.captcha.val())
				},
				remember_error = false;

			if (!emailRegex.test(remember_request.email)) {
				$remember_fields.email.addClass('error');
				remember_error = true;
			}

			if (!ruleRegex.test(remember_request.captcha)) {
				$remember_fields.captcha.addClass('error');
				remember_error = true;
			}

			if (!remember_error) {
				var msg_1 = '';
				if (LANG == 'ua') msg_1 = 'Відправка запиту...';
				if (LANG == 'ru') msg_1 = 'Отправка запроса...';
				if (LANG == 'en') msg_1 = 'Sending request...';
				$remember_send.text(msg_1);

				$.post(
					'<?=$this->uri->full_url('profile/send_remember');?>',
					remember_request,
					function (response) {
						if (response.errors.length === 0) {
							var msg = '';
							if (LANG == 'ua') msg = 'Лист з подальшими інструкціями надісланий на Вашу пошту!';
							if (LANG == 'ru') msg = 'Письмо с дальнейшими инструкциями отослано на Ваш почту!';
							if (LANG == 'en') msg = 'Letter with next instructions sent to your e-mail!';
							$('#remember_form').text(msg);
						} else {
							$('#remember_captcha_img').fadeTo(500, 0, function () {
								$(this).attr('src', '<?=$this->uri->full_url('profile/remember_captcha?t='); ?>' + Math.random()).fadeTo(500, 1);
							});

							for (var i = 0; i < response.errors.length; i++) {
								if (response.errors[i] == 1) $remember_fields.captcha.addClass('error');
								if (response.errors[i] == 2) $remember_fields.email.addClass('error');
							}
							var msg_2 = '';
							if (LANG == 'ua') msg_2 = 'Відновити пароль';
							if (LANG == 'ru') msg_2 = 'Восстановить пароль';
							if (LANG == 'en') msg_2 = 'Recover password';
							$remember_send.text(msg_2);
						}
					},
					'json'
				);
			}
		});

		$('#remember_captcha_reload').on('click', function (e) {
			e.preventDefault();

			$('#remember_captcha_img').fadeTo(500, 0, function () {
				$(this).attr('src', '<?=$this->uri->full_url('profile/remember_captcha?t='); ?>' + Math.random()).fadeTo(500, 1);
			});
		});
	});
</script>