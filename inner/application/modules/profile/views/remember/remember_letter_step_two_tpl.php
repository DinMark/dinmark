<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title><?=$subject;?></title>
	<style type="text/css">
		body, table tr td, table tr th {font: 12px/18px Verdana, Arial, Tahoma, sans-serif;}
		table tr td, table tr th {padding: 5px 0 5px 0;empty-cells: show;}
	</style>
</head>
<body>
<?php if (LANG == 'ua'): ?>
	<table align="center" width="900" cellpadding="10" cellspacing="0" border="0" style="border-collapse: collapse;">
		<tr>
			<td align="left" style="padding-bottom: 30px">
				<span style="font-size: 14px">Новий пароль на сайті <a href="<?=$this->uri->full_url();?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319"><?=$this->config->item('site_name_' . LANG);?></span></a>.</span>
			</td>
		</tr>
		<tr>
			<td align="left" style="padding-bottom: 30px">
				Ваш логін: <?=$user['email'];?>
				<br>
				Ваш новий пароль: <?=$user['password'];?>
			</td>
		</tr>
		<tr>
			<td style="left">
				&copy; <?=date('Y');?> <?=$this->config->item('site_name_' . LANG);?><br>
				<a href="http://www.websufix.com" style="color:#0061b3; font:10px Tahoma; text-decoration:none; margin:0 0 0 17px;"><span style="color:#0061b3;">SUFIX web design</span></a>
			</td>
		</tr>
	</table>
<?php endif; ?>
<?php if (LANG == 'ru'): ?>
	<table align="center" width="900" cellpadding="10" cellspacing="0" border="0" style="border-collapse: collapse;">
		<tr>
			<td align="left" style="padding-bottom: 30px">
				<span style="font-size: 14px">Новий пароль на сайті <a href="<?=$this->uri->full_url();?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319"><?=$this->config->item('site_name_' . LANG);?></span></a>.</span>
			</td>
		</tr>
		<tr>
			<td align="left" style="padding-bottom: 30px">
				Ваш логин: <?=$user['email'];?>
				<br>
				Ваш новый пароль: <?=$user['password'];?>
			</td>
		</tr>
		<tr>
			<td style="left">
				&copy; <?=date('Y');?> <?=$this->config->item('site_name_' . LANG);?><br>
				<a href="http://www.websufix.com" style="color:#0061b3; font:10px Tahoma; text-decoration:none; margin:0 0 0 17px;"><span style="color:#0061b3;">SUFIX web design</span></a>
			</td>
		</tr>
	</table>
<?php endif; ?>
<?php if (LANG == 'en'): ?>
	<table align="center" width="900" cellpadding="10" cellspacing="0" border="0" style="border-collapse: collapse;">
		<tr>
			<td align="left" style="padding-bottom: 30px">
				<span style="font-size: 14px">The new password on the site <a href="<?=$this->uri->full_url();?>" style="color: #693319 !important; text-decoration: underline;"><span style="color: #693319"><?=$this->config->item('site_name_' . LANG);?></span></a>.</span>
			</td>
		</tr>
		<tr>
			<td align="left" style="padding-bottom: 30px">
				Your login: <?=$user['email'];?>
				<br>
				Your new password: <?=$user['password'];?>
			</td>
		</tr>
		<tr>
			<td style="left">
				&copy; <?=date('Y');?> <?=$this->config->item('site_name_' . LANG);?><br>
				<a href="http://www.websufix.com" style="color:#0061b3; font:10px Tahoma; text-decoration:none; margin:0 0 0 17px;"><span style="color:#0061b3;">SUFIX web design</span></a>
			</td>
		</tr>
	</table>
<?php endif; ?>
</body>
</html>