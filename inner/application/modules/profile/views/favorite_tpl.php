<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<div class="container fav">
    <div class="row">
        <?php if (count($variants) > 0): ?><?php foreach ($variants as $v): ?>
        <div class="col-md-4">
            <div class="fm one-good"><?php $url = $this->uri->full_url($v['product']['url']); ?>
                <div class="one-good_wrapper fm">
                    <?php if ($v['product']['status'] == 1): ?><div class="one-good_status promo"><b></b><span><? if(LANG=='ua')echo'Акція';if(LANG=='ru')echo'Акция';if(LANG=='en')echo'Share';?></span><i></i></div><?php endif; ?>
                    <?php if ($v['product']['status'] == 3): ?><div class="one-good_status discount"><b></b><span><? if(LANG=='ua')echo'Знижка';if(LANG=='ru')echo'Скидка';if(LANG=='en')echo'Discount';?></span><i></i></div><?php endif; ?>
                    <?php if ($v['product']['status'] == 4): ?><div class="one-good_status hit"><b></b><span><? if(LANG=='ua')echo'Топ продаж';if(LANG=='ru')echo'Топ продаж';if(LANG=='en')echo'Top sales';?></span><i></i></div><?php endif; ?>
                    <div class="fm one-good_img">
                        <div class="cell">
                            <a href="<?=$url;?>">
                                <?php if (isset($v['product']['image']) and $v['product']['image'] != ''): ?><img src="<?=base_url('upload/catalog/' . $this->init_model->dir_by_id($v['product_id']) . '/' . $v['product_id'] . '/t_' . $v['product']['image']);?>" alt="<?=$v['title'];?>"><?php endif; ?>
                            </a>
                        </div>
                    </div>
                    <div class="fm one-good_head">
                        <a href="<?=$this->uri->full_url($v['url']);?>" class="one-good_name"><?=$v['title'];?></a>
                    </div>
                    <div class="one-good_options fm">
                        <?php foreach($v['filters'] as $_v): ?>
                            <div class="one-good_options_row">
                                <div class="one-good_options_cell"><?=$_v['filter_name'];?>:</div>
                                <div class="one-good_options_cell"><?=implode(', ', $_v['childs']);?></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="box-l fm">
                        <div class="fm one-good_icons">
                            <a href="#" class="one-good_compare select_compare" data-product-id="<?=$v['variant_id'];?>"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>
                            <a href="#" class="one-good_favorite select_favorite active" data-product-id="<?=$v['variant_id'];?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                        </div>
                        <a href="<?=$url;?>" class="fmr one-good_btn"><? if(LANG=='ua')echo'купити';if(LANG=='ru')echo'купить';if(LANG=='en')echo'buy';?></a>
                    </div>
                </div>
                <a href="#" class="ct_delete select_favorite active rem" data-product-id="<?=$v['variant_id'];?>"></a>
            </div>
        </div>
        <?php endforeach; ?><?php else: ?>
            <p>Ви ще не обрали улюблених товарів</p>
        <?php endif; ?>
    </div>
</div>