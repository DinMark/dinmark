<?php

	if (!defined('BASEPATH')) exit('No direct script access allowed');

	if (LANG == 'ua') echo '<div class="centre"><div class="fm good">Ви успішно активували свій обліковий запис!</div></div>';
	if (LANG == 'ru') echo '<div class="centre"><div class="fm good">Вы успешно активировали свою учетную запись!</div></div>';
	if (LANG == 'en') echo '<div class="centre"><div class="fm good">You have successfully activated your account!</div></div>';