<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<div class="fm my_user_page">
	<div class="container">
	<div class="fm mup_title_links">
		<ul>
			<li><a href="<?=$this->uri->full_url('profile/data');?>"><? if(LANG=='ua')echo'Персональні дані';if(LANG=='ru')echo'Персональные данные';if(LANG=='en')echo'Personal Information';?></a></li>
			<li><a href="<?=$this->uri->full_url('profile/orders');?>" class="active"><? if(LANG=='ua')echo'Замовлення';if(LANG=='ru')echo'Заказы';if(LANG=='en')echo'Orders';?></a></li>
            <li><a href="<?=$this->uri->full_url('profile/delivery');?>"><?php if (LANG == 'ua') echo 'Доставка'; if (LANG == 'ru') echo 'Доставка'; if (LANG == 'en') echo 'Delivery'; ?></a></li>
			<li><a id="favorite_link_2" href="<?=$this->uri->full_url('profile/favorite');?>"><?php if (LANG == 'ua') echo 'Мої улюбленні'; if (LANG == 'ru') echo 'Мои любимые'; if (LANG == 'en') echo 'My favorite'; ?></a></li>
		</ul>
	</div>
	<div class="fm mup_table">
		<div class="fm mup_tab_th">
			<div class="fm mtt_cell w_143"><? if(LANG=='ua')echo'№ замовлення';if(LANG=='ru')echo'№ заказа';if(LANG=='en')echo'№ order';?></div>
			<div class="fm mtt_cell w_169"><? if(LANG=='ua')echo'Дата замовлення';if(LANG=='ru')echo'Дата заказа';if(LANG=='en')echo'Order date';?></div>
			<div class="fm mtt_cell w_316"><? if(LANG=='ua')echo'Деталі замовлення';if(LANG=='ru')echo'Детали заказа';if(LANG=='en')echo'Order details';?></div>
			<div class="fm mtt_cell w_542"><? if(LANG=='ua')echo'Відстеження';if(LANG=='ru')echo'Отслеживание';if(LANG=='en')echo'Tracking';?></div>
		</div>
		<?php if (isset($orders) AND count($orders) > 0): ?>
			<?php foreach ($orders as $order): ?>
				<div class="fm mtt_big_row">
					<div class="fm mup_tab_td">
						<div class="fm mtt_cell w_143"><?=$order['order_id'];?></div>
						<div class="fm mtt_cell w_169"><?=date('d.m.Y H:i', $order['order_date']);?></div>
						<div class="fm mtt_cell w_316"><a href="#" class="order_details"><i class="fa fa-eye"></i></a></div>
						<div class="fm mtt_cell w_542">
							<form action="" class="tracking-form">
								<label for="">ТТН номер</label>
								<input type="text">
								<input type="submit" value="<? if(LANG=='ua')echo'Відстежити';if(LANG=='ru')echo'Отследить';if(LANG=='en')echo'Track';?>" class="common-btn">
							</form>
						</div>
					</div>
					<div class="fm mtt_details close">
						<div class="fm mtd_left">
							<div class="mtt_details_table">
								<?php foreach ($order['products'] as $key => $row): ?>
									<div class="fm mtt_details_row">

										<div class="mtt_details_cell"><?php echo $key + 1; ?>.</div>

										<div class="mtt_details_cell">
											<?php if ((string)$row['variant_url'] !== ''): ?>
                                                <a href="<?php echo $this->uri->full_url($row['variant_url']); ?>" target="_blank"><?php echo $row['title']; ?></a>
                                            <?php elseif ((string)$row['url'] !== ''): ?>
                                                <a href="<?php echo $this->uri->full_url($row['url']); ?>" target="_blank"><?php echo $row['title']; ?></a>
                                            <?php else: ?>
												<?php echo $row['title']; ?>
                                            <?php endif; ?>
										</div>

										<div class="mtt_details_cell"><?php echo $row['price']; ?> <? if(LANG=='ua')echo'грн';if(LANG=='ru')echo'грн';if(LANG=='en')echo'uah';?></div>

										<div class="mtt_details_cell"><?php echo $row['total']; ?> <? if(LANG=='ua')echo'шт.';if(LANG=='ru')echo'шт.';if(LANG=='en')echo'items';?></div>

										<div class="mtt_details_cell"><?php echo $row['total']; ?> <? if(LANG=='ua')echo'шт.';if(LANG=='ru')echo'шт.';if(LANG=='en')echo'items';?> х <?php echo $row['price']; ?> <? if(LANG=='ua')echo'грн';if(LANG=='ru')echo'грн';if(LANG=='en')echo'uah';?></div>

										<div class="mtt_details_cell"><?php echo $row['price'] * $row['total']; ?> <? if(LANG=='ua')echo'грн';if(LANG=='ru')echo'грн';if(LANG=='en')echo'uah';?></div>

										<div class="mtt_details_cell"><?php if ($row['code'] != ''): ?>
											<? if(LANG=='ua')echo'Код';if(LANG=='ru')echo'Код';if(LANG=='en')echo'Code';?>: <?php echo $row['code']; ?>
										<?php endif; ?></div>

									</div>
								<?php endforeach; ?>
							</div>
							<p class="fm ord_total"><? if(LANG=='ua')echo'Всього';if(LANG=='ru')echo'Итого';if(LANG=='en')echo'Total';?>: <b><?php echo $order['total']; ?></b> <? if(LANG=='ua')echo'шт.';if(LANG=='ru')echo'шт.';if(LANG=='en')echo'items';?> <b><?php echo $order['cost']; ?></b> <? if(LANG=='ua')echo'грн';if(LANG=='ru')echo'грн';if(LANG=='en')echo'uah';?></p>
						</div>
						<div class="fm mtd_right">
							<p style="margin:0;">
								Оплата:
								<b>
									<?php
										if ($order['payment'] == 1) echo 'оплати при отриманні';
										if ($order['payment'] == 2) echo 'банкіський переказ';
										if ($order['payment'] == 3) echo 'безготівковий переказ для ЮО';
										if ($order['payment'] == 4) echo 'Приват 24';
									?>
								</b>
							</p>
							<p style="margin:0;">
								<? if(LANG=='ua')echo'Доставка';if(LANG=='ru')echo'Доставка';if(LANG=='en')echo'Delivery';?>:
								<b>
									<?=$order['delivery']?>
								</b>
							</p>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {
		$('.order_details').on('click', function (e) {
			e.preventDefault();

			if ($(this).closest('.mtt_big_row').find('.mtt_details').hasClass('close')) {
				$(this).closest('.mtt_big_row').find('.mtt_details').removeClass('close');
				// var msg_1 = '';
				// if (LANG == 'ua') msg_1 = 'Згорнути';
				// if (LANG == 'ru') msg_1 = 'Свернуть';
				// if (LANG == 'en') msg_1 = 'Close';
				// $(this).text(msg_1);
			} else {
				var msg_2 = '';
				// if (LANG == 'ua') msg_2 = 'Розгорнути';
				// if (LANG == 'ru') msg_2 = 'Развернуть';
				// if (LANG == 'en') msg_2 = 'Open';
				// $(this).text(msg_2);
				$(this).closest('.mtt_big_row').find('.mtt_details').addClass('close');
			}
		});
	});
</script>