<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<div class="fm my_user_page">
	<div class="container">
	<div class="fm mup_title_links">
		<ul>
			<li><a href="<?=$this->uri->full_url('profile/data');?>" class="active"><? if(LANG=='ua')echo'Персональні дані';if(LANG=='ru')echo'Персональные данные';if(LANG=='en')echo'Personal Information';?></a></li>
			<li><a href="<?=$this->uri->full_url('profile/orders');?>"><? if(LANG=='ua')echo'Замовлення';if(LANG=='ru')echo'Заказы';if(LANG=='en')echo'Orders';?></a></li>
			<li><a href="<?=$this->uri->full_url('profile/delivery');?>"><?php if (LANG == 'ua') echo 'Доставка'; if (LANG == 'ru') echo 'Доставка'; if (LANG == 'en') echo 'Delivery'; ?></a></li>
			<li><a id="favorite_link_2" href="<?=$this->uri->full_url('profile/favorite');?>"><?php if (LANG == 'ua') echo 'Мої улюбленні'; if (LANG == 'ru') echo 'Мои любимые'; if (LANG == 'en') echo 'My favorite'; ?></a></li>
		</ul>
	</div>
	<div class="fm mup_table">
		<div class="fm black_my_date">
			<div class="row">
			<form id="profile_form" action="<?=$this->uri->full_url('profile/save');?>" method="post">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="fm input-item">
						<label for="profile_name"><? if(LANG=='ua')echo'Ім’я';if(LANG=='ru')echo'Имя';if(LANG=='en')echo'Name';?>:</label>
						<input id="profile_name" type="text" value="<?=stripslashes($user['name']);?>">
					</div>
					<div class="fm input-item">
						<label for="profile_email">Email:</label>
						<input id="profile_email" type="text" value="<?=stripslashes($user['email']);?>">
					</div>
					<div class="fm input-item">
						<label for="profile_phone"><? if(LANG=='ua')echo'Телефон';if(LANG=='ru')echo'Телефон';if(LANG=='en')echo'Phone';?>:</label>
						<input id="profile_phone" type="text" value="<?=stripslashes($user['phone']);?>">
					</div>
					<div class="fm input-item">
						<? if(LANG=='ua')echo'Код клієнта';if(LANG=='ru')echo'Код клиента';if(LANG=='en')echo'Customer code';?>:
						<?=($user['code'] > 0 ? $user['code'] : '-');?>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="fm input-item">
						<label for="profile_old_password"><? if(LANG=='ua')echo'Старий пароль';if(LANG=='ru')echo'Старый пароль';if(LANG=='en')echo'Old password';?>:</label>
						<input id="profile_old_password" type="password" value="">
					</div>
					<div class="fm input-item">
						<label for="profile_password"><? if(LANG=='ua')echo'Новий пароль';if(LANG=='ru')echo'Новый пароль';if(LANG=='en')echo'New password';?>:</label>
						<input id="profile_password" type="password" value="">
					</div>
					<div class="fm  input-item">
						<label for="profile_repeat_password"><? if(LANG=='ua')echo'Повторіть новий пароль';if(LANG=='ru')echo'Повторите новый пароль';if(LANG=='en')echo'Repeat new password';?>:</label>
						<input id="profile_repeat_password" type="password" value="">
					</div>
					<div class="fm input-item">
						<a id="profile_send" href="#" class="save_min common-btn"><? if(LANG=='ua')echo'Зберегти зміни';if(LANG=='ru')echo'Сохранить изменения';if(LANG=='en')echo'Save changes';?></a>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {

		$('#profile_send').on('click', function (e) {
			e.preventDefault();
			$(this).closest('form').submit();
		});

		$('#profile_form')
			.on('keyup blur paste', 'input', function (e) {
				if (e.keyCode == 13) {
					$(this).closest('form').submit();
				} else {
					var val = $.trim($(this).val());
					if (ruleRegex.test(val)) $(this).removeClass('error');
				}
			})
			.on('submit', function (e) {
				e.preventDefault();

				$('#profile_form').find('input').removeClass('error');

				var $profile_fields = {
						name: $('#profile_name'),
						email: $('#profile_email'),
						phone: $('#profile_phone'),
						old_password: $('#profile_old_password'),
						password: $('#profile_password'),
						repeat_password: $('#profile_repeat_password')
					},
					$profile_send = $('#profile_send'),
					profile_request = {
						name: $.trim($profile_fields.name.val()),
						email: $.trim($profile_fields.email.val()),
						phone: $.trim($profile_fields.phone.val()),
						old_password: $.trim($profile_fields.old_password.val()),
						password: $.trim($profile_fields.password.val()),
						repeat_password: $.trim($profile_fields.repeat_password.val())
					},
					profile_error = false;

				if (!ruleRegex.test(profile_request.name)) {
					$profile_fields.name.addClass('error');
					profile_error = true;
				}

				if (!emailRegex.test(profile_request.email)) {
					$profile_fields.email.addClass('error');
					profile_error = true;
				}

				if (profile_request.password != '') {
					if (!ruleRegex.test(profile_request.old_password)) {
						$profile_fields.old_password.addClass('error');
						profile_error = true;
					}

					if (!ruleRegex.test(profile_request.password)) {
						$profile_fields.password.addClass('error');
						profile_error = true;
					}

					if (!ruleRegex.test(profile_request.repeat_password)) {
						$profile_fields.repeat_password.addClass('error');
						profile_error = true;
					}

					if (profile_request.password != profile_request.repeat_password) {
						$profile_fields.password.addClass('error');
						$profile_fields.repeat_password.addClass('error');
						profile_error = true;
					}
				}

				if (!profile_error) {
					var msg_1 = '';
					if (LANG == 'ua') msg_1 = 'Збереження...';
					if (LANG == 'ru') msg_1 = 'Сохранение...';
					if (LANG == 'en') msg_1 = 'Saving...';
					$profile_send.text(msg_1);

					$.post(
						'<?=$this->uri->full_url('profile/save');?>',
						profile_request,
						function (response) {
							if (response.success) {
								$profile_fields.old_password.val('');
								$profile_fields.password.val('');
								$profile_fields.repeat_password.val('');
								var msg_2 = '';
								if (LANG == 'ua') msg_2 = 'Зміни збережено';
								if (LANG == 'ru') msg_2 = 'Изменения сохранены';
								if (LANG == 'en') msg_2 = 'Changes saved';
								var msg_3 = '';
								if (LANG == 'ua') msg_3 = 'Зберегти зміни';
								if (LANG == 'ru') msg_3 = 'Сохранить изменения';
								if (LANG == 'en') msg_3 = 'Save changes';
								$profile_send.text(msg_2);
								setTimeout(function () { $profile_send.text(msg_3); }, 2000);
							} else {
								if (response.error_code == 1) $profile_fields.email.addClass('error');
								if (response.error_code == 2) $profile_fields.password.addClass('error');
								var msg_4 = '';
								if (LANG == 'ua') msg_4 = 'Зберегти зміни';
								if (LANG == 'ru') msg_4 = 'Сохранить изменения';
								if (LANG == 'en') msg_4 = 'Save changes';
								$profile_send.text(msg_4);
							}
						},
						'json'
					);
				}
			});

	});
</script>