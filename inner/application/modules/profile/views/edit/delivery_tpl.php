<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	$this->template_lib->set_js('autocomplete/jquery.autocomplete.min.js');

?>
<div class="fm my_user_page">
	<div class="container">
	<div class="fm mup_title_links">
		<ul>
			<li><a href="<?=$this->uri->full_url('profile/data');?>"><? if(LANG=='ua')echo'Персональні дані';if(LANG=='ru')echo'Персональные данные';if(LANG=='en')echo'Personal Information';?></a></li>
			<li><a href="<?=$this->uri->full_url('profile/orders');?>"><? if(LANG=='ua')echo'Замовлення';if(LANG=='ru')echo'Заказы';if(LANG=='en')echo'Orders';?></a></li>
			<li><a href="<?=$this->uri->full_url('profile/delivery');?>" class="active"><?php if (LANG == 'ua') echo 'Доставка'; if (LANG == 'ru') echo 'Доставка'; if (LANG == 'en') echo 'Delivery'; ?></a></li>
			<li><a id="favorite_link_2" href="<?=$this->uri->full_url('profile/favorite');?>"><?php if (LANG == 'ua') echo 'Мої улюбленні'; if (LANG == 'ru') echo 'Мои любимые'; if (LANG == 'en') echo 'My favorite'; ?></a></li>
		</ul>
	</div>
	<div class="fm mup_table">
		<div class="fm black_my_date">
			<div class="row">
				<form id="profile_form" action="<?=$this->uri->full_url('profile/save_delivery');?>" method="post">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div id="delivery_form">
							<div class="box_title"><? if(LANG=='ua')echo'Спосіб доставки';if(LANG=='ru')echo'Способ доставки';if(LANG=='en')echo'Shipping Method';?></div>
							<div class="border_box">
								<div class="field">
									<input type="radio" name="delivery_selector" id="delivery_selector_1" value="2"<?php if (!isset($delivery['type']) or $delivery['type'] === 2): ?> checked="checked"<?php endif; ?>>
									<label for="delivery_selector_1"><? if(LANG=='ua')echo'Нова пошта';if(LANG=='ru')echo'Новая почта';if(LANG=='en')echo'Nova Poshta';?></label>
									<div class="delivery-type np_selector<?php if (isset($delivery['type']) and $delivery['type'] !== 2): ?> hidden<?php endif; ?>">
										<div class="field">
											<input type="radio" name="np_selector" id="department" value="1"<?php if (!isset($delivery['np_type']) or $delivery['np_type'] === 1): ?> checked="checked"<?php endif; ?>>
											<label for="department" class="custom-label"><? if(LANG=='ua')echo'на відділення';if(LANG=='ru')echo'доставка на отделение';if(LANG=='en')echo'to the office';?></label>
										</div>
										<div class="field">
											<input type="radio" name="np_selector" id="courier" value="2"<?php if (isset($delivery['np_type']) and $delivery['np_type'] === 2): ?> checked="checked"<?php endif; ?>>
											<label for="courier" class="custom-label"><? if(LANG=='ua')echo'кур`єр';if(LANG=='ru')echo'курьер';if(LANG=='en')echo'courier';?></label>
										</div>
									</div>
								</div>
								<div class="field">
									<input type="radio" name="delivery_selector" id="delivery_selector_2" value="3"<?php if (isset($delivery['type']) and $delivery['type'] === 3): ?> checked="checked"<?php endif; ?>>
									<label for="delivery_selector_2">ІнТайм</label>
								</div>
								<div class="field">
									<input type="radio" name="delivery_selector" id="delivery_selector_3" value="4"<?php if (isset($delivery['type']) and $delivery['type'] === 4): ?> checked="checked"<?php endif; ?>>
									<label for="delivery_selector_3">Деливери</label>
								</div>
								<div class="field">
									<input type="radio" name="delivery_selector" id="delivery_selector_4" value="5"<?php if (isset($delivery['type']) and $delivery['type'] === 5): ?> checked="checked"<?php endif; ?>>
									<label for="delivery_selector_4">Транспортна компанія</label>
								</div>
								<div class="field">
									<input type="radio" name="delivery_selector" id="delivery_selector_5" value="1"<?php if (isset($delivery['type']) and $delivery['type'] === 1): ?> checked="checked"<?php endif; ?>>
									<label for="delivery_selector_5"><? if(LANG=='ua')echo'Самовивіз';if(LANG=='ru')echo'Самовывоз';if(LANG=='en')echo'Pickup';?></label>
								</div>
							</div>
						</div>
					</div>
					<div id="delivery_details" class="col-md-6 col-sm-12 col-xs-12">
						<div id="delivery_details_1" class="delivery_details<?php if (!isset($delivery['type']) or $delivery['type'] !== 1): ?> hidden<?php endif; ?>"><p><?=$helps['self_delivery'];?></p></div>
						<div id="delivery_details_2_1" class="delivery_details<?php if (isset($delivery['type']) and ($delivery['type'] !== 2 or (isset($delivery['np_type']) and $delivery['np_type'] !== 1))): ?> hidden<?php endif; ?>">
							<p><?=$helps['new_post_warehouse'];?></p>
							<input type="text" name="np_city" placeholder="Місто" value="<?php if (isset($np_city)): ?><?=$np_city;?><?php endif; ?>">
							<input type="hidden" name="np_city_id" placeholder="Місто" value="<?php if (isset($delivery['np_city'])): ?><?=$delivery['np_city'];?><?php endif; ?>">
							<select name="np_warehouse"<?php if (!isset($np_warehouses)): ?> disabled="disabled"<?php endif; ?>>
								<option value=""><? if(LANG=='ua')echo'Відділення';if(LANG=='ru')echo'Отделение';if(LANG=='en')echo'Department';?></option>
								<?php if (isset($np_warehouses) and count($np_warehouses) > 0): ?>
									<?php foreach ($np_warehouses as $v): ?>
									    <option value="<?=$v['ref'];?>"<?php if (isset($delivery['np_warehouse']) and $delivery['np_warehouse'] === $v['ref']): ?> selected="selected"<?php endif; ?>><?=$v['name'];?></option>
									<?php endforeach; ?>
								<?php endif; ?>
							</select>
						</div>
						<div id="delivery_details_2_2" class="delivery_details<?php if (isset($delivery['type']) and ($delivery['type'] !== 2 or (isset($delivery['np_type']) and $delivery['np_type'] !== 2))): ?> hidden<?php endif; ?>">
							<p><?=$helps['new_post_courier'];?></p>
							<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 1: </label><input name="delivery_address_1" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 2 and isset($delivery['np_type']) and $delivery['np_type'] === 2 and isset($delivery['address_1'])): ?><?=$delivery['address_1'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 2: </label><input name="delivery_address_2" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 2 and isset($delivery['np_type']) and $delivery['np_type'] === 2 and isset($delivery['address_2'])): ?><?=$delivery['address_2'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Область';if(LANG=='ru')echo'Область';if(LANG=='en')echo'Region';?>: </label><input name="delivery_region" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 2 and isset($delivery['np_type']) and $delivery['np_type'] === 2 and isset($delivery['region'])): ?><?=$delivery['region'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Місто';if(LANG=='ru')echo'Город';if(LANG=='en')echo'City';?>: </label><input name="delivery_city" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 2 and isset($delivery['np_type']) and $delivery['np_type'] === 2 and isset($delivery['city'])): ?><?=$delivery['city'];?><?php endif; ?>">
						</div>
						<div id="delivery_details_3" class="delivery_details<?php if (!isset($delivery['type']) or $delivery['type'] !== 3): ?> hidden<?php endif; ?>">
							<p><?=$helps['intime'];?></p>
							<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 1: </label><input name="delivery_address_1" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 3 and isset($delivery['address_1'])): ?><?=$delivery['address_1'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 2: </label><input name="delivery_address_2" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 3 and isset($delivery['address_2'])): ?><?=$delivery['address_2'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Область';if(LANG=='ru')echo'Область';if(LANG=='en')echo'Region';?>: </label><input name="delivery_region" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 3 and isset($delivery['region'])): ?><?=$delivery['region'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Місто';if(LANG=='ru')echo'Город';if(LANG=='en')echo'City';?>: </label><input name="delivery_city" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 3 and isset($delivery['city'])): ?><?=$delivery['city'];?><?php endif; ?>">
						</div>
						<div id="delivery_details_4" class="delivery_details<?php if (!isset($delivery['type']) or $delivery['type'] !== 4): ?> hidden<?php endif; ?>">
							<p><?=$helps['delivery'];?></p>
							<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 1: </label><input name="delivery_address_1" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 4 and isset($delivery['address_1'])): ?><?=$delivery['address_1'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 2: </label><input name="delivery_address_2" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 4 and isset($delivery['address_2'])): ?><?=$delivery['address_2'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Область';if(LANG=='ru')echo'Область';if(LANG=='en')echo'Region';?>: </label><input name="delivery_region" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 4 and isset($delivery['region'])): ?><?=$delivery['region'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Місто';if(LANG=='ru')echo'Город';if(LANG=='en')echo'City';?>: </label><input name="delivery_city" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 4 and isset($delivery['city'])): ?><?=$delivery['city'];?><?php endif; ?>">
						</div>
						<div id="delivery_details_5" class="delivery_details<?php if (!isset($delivery['type']) or $delivery['type'] !== 5): ?> hidden<?php endif; ?>">
							<p><?=$helps['transport_company'];?></p>
							<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 1: </label><input name="delivery_address_1" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 5 and isset($delivery['address_1'])): ?><?=$delivery['address_1'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Адреса';if(LANG=='ru')echo'Адрес';if(LANG=='en')echo'Address';?> 2: </label><input name="delivery_address_2" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 5 and isset($delivery['address_2'])): ?><?=$delivery['address_2'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Область';if(LANG=='ru')echo'Область';if(LANG=='en')echo'Region';?>: </label><input name="delivery_region" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 5 and isset($delivery['region'])): ?><?=$delivery['region'];?><?php endif; ?>">
							<p class="field req"><label><? if(LANG=='ua')echo'Місто';if(LANG=='ru')echo'Город';if(LANG=='en')echo'City';?>: </label><input name="delivery_city" type="text" value="<?php if (isset($delivery['type']) and $delivery['type'] === 5 and isset($delivery['city'])): ?><?=$delivery['city'];?><?php endif; ?>">
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<a id="profile_send" href="#" class="save_min common-btn"><? if(LANG=='ua')echo'Зберегти зміни';if(LANG=='ru')echo'Сохранить изменения';if(LANG=='en')echo'Save changes';?></a>
					</div>
				</form>
			</div>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {
		var $delivery_type = $('#delivery_form'),
			$delivery = $('#delivery_details');

		$delivery_type.on('change', '[name="delivery_selector"]', function () {
			var type = $(this).val(),
				np_type = type == 2 ? $delivery_type.find('[name="np_selector"]:checked').val() : '';

			if (type == 2) {
				$delivery_type.find('.np_selector').removeClass('hidden');
			} else {
				$delivery_type.find('.np_selector').addClass('hidden');
			}

			$delivery.find('.delivery_details').addClass('hidden');
			$('#delivery_details_' + type + (np_type !== '' ? '_' + np_type : '')).removeClass('hidden');
		});

		$delivery_type.on('change', '[name="np_selector"]', function () {
			var np_type = $(this).val(),
				type = $delivery_type.find('[name="delivery_selector"]:checked').val();

			$delivery.find('.delivery_details').addClass('hidden');
			$('#delivery_details_' + type + (np_type !== '' ? '_' + np_type : '')).removeClass('hidden');
		});

		$delivery.find('[name="np_city"]')
			.on('keydown', function () {
				$delivery.find('[name="np_warehouse"]').val('').prop('disabled', true);
				$delivery.find('[name="np_city_id"]').val('');
			})
			.autocomplete({
				minChars: 2,
				ajaxSettings: {
					method: 'POST'
				},
				serviceUrl: full_url('cart/get_cities'),
				onSelect: function (suggestion) {
					$delivery.find('[name="np_city_id"]').val(suggestion.data);

					$.post(
						full_url('cart/get_warehouses'),
						{
							ref: suggestion.data
						},
						function (response) {
							if (response.success) {
								var options = '<option value="">' + $delivery.find('[name="np_warehouse"]').find('option').eq(0).text() + '</option>';

								$.each(response.warehouses, function (i, v) {
									options += '<option value="' + v.ref + '">' + v.name + '</option>';
								});

								$delivery.find('[name="np_warehouse"]').html(options).prop('disabled', false);
							}
						},
						'json'
					);
				}
			});


		$('#profile_send').on('click', function (e) {
			e.preventDefault();
			$(this).closest('form').submit();
		});

		$('#profile_form')
			.on('keyup blur paste', 'input', function (e) {
				if (e.keyCode == 13) {
					$(this).closest('form').submit();
				}
			})
			.on('submit', function (e) {
				e.preventDefault();

				var $profile_send = $('#profile_send'),
					profile_request = {},
					type = $delivery_type.find('[name="delivery_selector"]:checked').val(),
					np_type = $delivery_type.find('[name="np_selector"]:checked').val();

				profile_request['type'] = type;

				if (type == 2) {
					profile_request['np_type'] = np_type;
				}

				if (type > 1) {
					var $delivery_details = $('#delivery_details_' + type + (type == 2 ? '_' + np_type : ''));

					if (type == 2 && np_type == 1) {
						profile_request['np_city'] = $delivery_details.find('[name="np_city_id"]').val();
						profile_request['np_warehouse'] = $delivery_details.find('[name="np_warehouse"]').val();
					} else {
						profile_request['address_1'] = $delivery_details.find('[name="delivery_address_1"]').val();
						profile_request['address_2'] = $delivery_details.find('[name="delivery_address_2"]').val();
						profile_request['region'] = $delivery_details.find('[name="delivery_region"]').val();
						profile_request['city'] = $delivery_details.find('[name="delivery_city"]').val();
					}
				}

				var msg_1 = '';
				if (LANG == 'ua') msg_1 = 'Збереження...';
				if (LANG == 'ru') msg_1 = 'Сохранение...';
				if (LANG == 'en') msg_1 = 'Saving...';
				$profile_send.text(msg_1);

				$.post(
					'<?=$this->uri->full_url('profile/save_delivery');?>',
					profile_request,
					function (response) {
						if (response.success) {
							var msg_2 = '';
							if (LANG == 'ua') msg_2 = 'Зміни збережено';
							if (LANG == 'ru') msg_2 = 'Изменения сохранены';
							if (LANG == 'en') msg_2 = 'Changes saved';
							var msg_3 = '';
							if (LANG == 'ua') msg_3 = 'Зберегти зміни';
							if (LANG == 'ru') msg_3 = 'Сохранить изменения';
							if (LANG == 'en') msg_3 = 'Save changes';
							$profile_send.text(msg_2);
							setTimeout(function () { $profile_send.text(msg_3); }, 2000);
						} else {
							var msg_4 = '';
							if (LANG == 'ua') msg_4 = 'Зберегти зміни';
							if (LANG == 'ru') msg_4 = 'Сохранить изменения';
							if (LANG == 'en') msg_4 = 'Save changes';
							$profile_send.text(msg_4);
						}
					},
					'json'
				);
			});

	});
</script>