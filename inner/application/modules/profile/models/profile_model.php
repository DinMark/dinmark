<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	class Profile_model extends CI_Model
	{
		/**
		 * Перевірка на наявність email адреси в базі даних
		 *
		 * @param string $email
		 * @param bool $user_id
		 *
		 * @return bool
		 */
		public function check_email($email = '', $user_id = FALSE)
		{
			if ($email == '') return TRUE;
			if ($user_id) $this->db->where('user_id !=', $user_id);
			return $this->db->where('email', $email)->count_all_results('users') > 0 ? TRUE : FALSE;
		}

		/**
		 * Перевірка співпадіння паролю
		 *
		 * @param string $password
		 * @param int $user_id
		 *
		 * @return bool
		 */
		public function check_password($password, $user_id)
		{
			$user = $this->db->select('password, salt')->where('user_id', $user_id)->get('users')->row_array();
			return $user['password'] === md5($user['salt'] . base64_encode($password));
		}

		/**
		 * Створення нового профілю
		 *
		 * @param array $set
		 */
		public function create_profile($set)
		{
			$this->db->insert('users', $set);
			return $this->db->insert_id();
		}

		/**
		 * Активація профілю
		 *
		 * @param string $key
		 *
		 * @return bool|array
		 */
		public function activate_profile($key)
		{
			$user_id = intval($this->db->select('user_id')->where('tmp_key', $key)->get('users')->row('user_id'));
			if ($user_id == 0) return FALSE;

			$this->db->set('status', 1)->set('tmp_key', '')->where('user_id', $user_id)->update('users');

			return $this->get_user($user_id, NULL, 'user_id, status, name, email, phone');
		}

		/**
		 * Прив’язка замовлень до нового користувача по email
		 *
		 * @param int $user_id
		 * @param string $email
		 */
		public function bind_orders($user_id, $email)
		{
			$this->db->set('user_id', $user_id);
			$this->db->where('user_id', 0);
			$this->db->where('email', $email);
			$this->db->update('orders');
		}

		/**
		 * Отримання інформації про користувача
		 *
		 * @param null|int $user_id
		 * @param null|string $email
		 * @param string $fields
		 *
		 * @return bool|array
		 */
		public function get_user($user_id = NULL, $email = NULL, $fields = '*')
		{
			if ($user_id === NULL AND $email === NULL) return FALSE;

			$fields = explode(',', $fields);

			foreach ($fields as &$v) {
				if (!mb_strpos($v, '.')) {
					$v = 'users.' . $v;
				}
			}

			$this->db->select(implode(',', $fields));

			if ($user_id != NULL) $this->db->where('users.user_id', $user_id);
			if ($email != NULL) $this->db->where('md5(`' . $this->db->dbprefix . 'users`.`email`)', md5($email));

			$this->db
				->select('clients.price_type')
				->join('clients', 'users.code=clients.id', 'left');

			$user = $this->db->get('users')->row_array();
			return count($user) > 0 ? $user : FALSE;
		}

		/**
		 * Відновлення паролю
		 *
		 * @param string $key
		 *
		 * @return bool|array
		 */
		public function reset_password($key)
		{
			$user = $this->db->select('user_id, name, email')->where('tmp_key', $key)->get('users')->row_array();
			if (count($user) == 0) return FALSE;

			$password = $this->get_random_password();
			$user['password'] = $password;
			$salt = $this->get_random_password();

			$set = array(
				'password' => md5($salt . base64_encode($password)),
				'salt' => $salt,
				'tmp_key' => '',
			);
			$this->update_user($user['user_id'], $set);

			return $user;
		}

		/**
		 * Оновлення інформації про користувача
		 *
		 * @param int $user_id
		 * @param array $set
		 */
		public function update_user($user_id, $set)
		{
			$this->db->set($set)->where('user_id', $user_id)->update('users');
		}

		/**
		 * Фомування випадкового паролю
		 *
		 * @param int $chars_min
		 * @param int $chars_max
		 * @param bool $use_upper_case
		 * @param bool $include_numbers
		 * @param bool $include_special_chars
		 *
		 * @return string
		 */
		public function get_random_password($chars_min = 6, $chars_max = 8, $use_upper_case = FALSE, $include_numbers = TRUE, $include_special_chars = FALSE)
		{
			$length = rand($chars_min, $chars_max);

			$selection = 'aeuoyibcdfghjklmnpqrstvwxz';
			if ($include_numbers) $selection .= "1234567890";
			if ($include_special_chars) $selection .= "!@\"#$%&[]{}?|";

			$password = "";
			for ($i = 0; $i < $length; $i++)
			{
				$current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
				$password .= $current_letter;
			}

			return $password;
		}

		/**
		 * Встановлення даних сеансу
		 *
		 * @param array $user
		 */
		public function login($user)
		{
			$session = array(
				'user_id' => $user['user_id'],
				'user_name' => $user['name'],
				'user_phone' => $user['phone'],
				'user_email' => $user['email'],
			);
			$this->session->set_userdata($session);
		}

		/**
		 * Видалення даних сеансу
		 */
		public function logout()
		{
			$this->session->sess_destroy();
		}

		/**
		 * Отримання замовлень користувача
		 *
		 * @return array
		 */
		public function get_orders()
		{
			$result = $this->db->where('user_id', $this->session->userdata('user_id'))->order_by('order_date', 'desc')->get('orders')->result_array();

			foreach ($result as $k => $v)
			{
				$result[$k]['products'] = $this->db
					->select('orders_products.*')
					->where('orders_products.order_id', $v['order_id'])

					->select('catalog.url_' . LANG . ' as url')
					->join('catalog', 'orders_products.product_id = catalog.product_id', 'left')

					->select('catalog_variants.url_' . LANG . ' as variant_url')
					->join('catalog_variants', 'orders_products.variant_id = catalog_variants.variant_id', 'left')

					->get('orders_products')
					->result_array();
			}

			return $result;
		}
	}