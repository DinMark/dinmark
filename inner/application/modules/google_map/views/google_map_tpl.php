<?php defined('ROOT_PATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<article class="fm google_map">
			<?php if (isset($map) AND count($map) > 0 AND $map['center_lat'] > 0 AND $map['center_lng'] > 0): ?>
				<header><?php if (!$h1): ?><h1><?=$map['title'];?></h1><?php else: ?><h2><?=$map['title'];?></h2><?php endif; ?></header>
			    <div id="google_map_<?=$component_id;?>" class="fm map_area"></div>
				<script type="text/javascript">
					$(function () {
						var mapOptions = {
							center: new google.maps.LatLng(<?=$map['center_lat'];?>, <?=$map['center_lng'];?>),
							zoom: <?=$map['zoom'];?>,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						};
						var map = new google.maps.Map(document.getElementById("google_map_<?=$component_id;?>"), mapOptions);

						<?php if ($map['marker_lat'] != '' AND $map['marker_lng'] != ''): ?>
						var marker = new google.maps.Marker({
							map: map,
							position: new google.maps.LatLng(<?=$map['marker_lat'];?>, <?=$map['marker_lng'];?>)
						});
						var infowindow = new google.maps.InfoWindow({
							content: '<?=$map['description'];?>'
						});
						google.maps.event.addListener(marker, 'click', function() {
							infowindow.open(map,marker);
						});
						<?php endif; ?>
					});
				</script>
			<?php else: ?>
				<header><h2><? if(LANG=='ua')echo'Нова мапа';if(LANG=='ru')echo'Новая карта';if(LANG=='en')echo'New map';?></h2></header>
			<?php endif; ?>
			</article>
		</div>
	</div>
</div>