<?php defined('ROOT_PATH') OR exit('No direct script access allowed');

	if (!function_exists('get_dir_code')) {
		/**
		 * Отримання назви директориї для кожних ста записів
		 *
		 * @param int $item_id
		 * @return int
		 */
		function get_dir_code($item_id)
		{
			return ceil($item_id / 100) * 100;
		}
	}

	if (!function_exists('get_dir_path')) {
		/**
		 * Отримання повного шляху директорії
		 *
		 * @param string $path
		 * @param bool $create
		 * @return int
		 */
		function get_dir_path($path, $create = true)
		{
			if ($create) {
				$_path = explode('/', trim($path, '/'));
				$path = ROOT_PATH;

				foreach ($_path as $dir) {
					$path .= $dir . '/';

					if (!file_exists($path)) {
						mkdir($path);
					}
				}
			} else {
				$path = ROOT_PATH . trim($path, '/') . '/';
			}

			return $path;
		}
	}