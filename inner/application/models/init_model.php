<?php

	defined('ROOT_PATH') OR exit('No direct script access allowed');

	class init_model extends CI_Model
	{
		/**
		 * ID активного пункту меню
		 *
		 * @var null|int
		 */
		private $menu_id = 0;
		/**
		 * ID активних пунктів меню
		 *
		 * @var array
		 */
		private $menu_parents = array();
		/**
		 * Перевірка валідації адміна
		 *
		 * @var bool
		 */
		private $admin_validate = FALSE;
		private $user_validate = FALSE;
		/**
		 * Головна сторінка
		 *
		 * @var bool
		 */
		private $is_main = FALSE;
		/**
		 * Назва активного пункту меню
		 *
		 * @var string
		 */
		private $menu_name = '';

		/**
		 * Встановлення конфігураційних змінних з бази даних
		 */
		public function set_config()
		{
			$config = $this->cache->get('db_config');

			if ($config AND !$this->is_admin())
			{
				foreach ($config as $row)
				{
					if ($row['key'] != 'percent')
					{
						if ($row['key'] != 'languages')
						{
							$this->config->set_item($row['key'], $row['val']);
						}
						else
						{
							$this->config->set_item('database_' . $row['key'], $row['val'] != '' ? unserialize($row['val']) : array());
						}
					}
				}
			}
			else
			{
				$result = $this->db->get('config');

				if ($result->num_rows() > 0)
				{
					$result_array = $result->result_array();
					$this->cache->save('db_config', $result_array, 3600);

					foreach ($result_array as $row)
					{
						if ($row['key'] != 'languages')
						{
							$this->config->set_item($row['key'], $row['val']);
						}
						else
						{
							$this->config->set_item('database_' . $row['key'], $row['val'] != '' ? unserialize($row['val']) : array());
						}
					}
				}
			}
		}

		/**
		 * Перевірка залогованості адміністратора
		 *
		 * @param string $return
		 *
		 * @return bool
		 */
		public function is_admin($return = 'boolean')
		{
			if (!$this->admin_validate AND defined('ADMIN_VALIDATE')) exit;

			if (!defined('ADMIN_VALIDATE'))
			{
				$flag = FALSE;
				$is_admin = intval($this->session->userdata('is_admin'));
				$key_admin = $this->session->userdata('key_admin');

				if ($is_admin > 0)
				{
					$result = $this->db->select('id, password')->where('id', $is_admin)->get('admin')->row_array();

					if (count($result) > 0)
					{
						if ($key_admin === md5($result['id'] . $result['password'] . $this->config->item('encryption_key') . $this->input->ip_address() . $this->input->user_agent()))
						{
							$flag = TRUE;
						}
					}
				}

				$this->admin_validate = TRUE;
				define('ADMIN_VALIDATE', $flag);
			}

			if (!ADMIN_VALIDATE)
			{
				if ($return == 'redirect')
				{
					redirect($this->uri->full_url('admin/login/index'));
				}
				elseif ($return == 'json')
				{
					die(json_encode(array(
						'is_admin' => 0
					)));
				}
				else
				{
					return FALSE;
				}
			}

			$cookie = array(
				'name' => 'manager_bridge',
				'value' => 'The Value',
				'expire' => 0,
				'path' => '/',
				'httponly' => TRUE
			);

			$this->input->set_cookie($cookie);

			return TRUE;
		}

		/**
		 * Визначення компоненту за посланням
		 *
		 * @return array
		 */
		public function get_routing()
		{
			$url = $this->uri->clean_url();
			$clean_url = $this->uri->clean_url(array(LANG => 0));

			if ($clean_url == '')
			{
				$this->db->select('id, name_' . LANG . ' as name');
				$this->db->where('main', 1);
				$result = $this->db->get('menu');

				if ($result->num_rows() == 0) show_404();

				$row = $result->row_array();
				$this->set_menu_id($row['id']);

				$this->set_main();
				$this->menu_name = $row['name'];

				return array('components', 'get_components', $row['id']);
			}
			else
			{
				$this->load->helper('form');

				$clean_url = form_prep($clean_url);
				$url = form_prep($url);

				$this->db->select('item_id, menu_id, module, method');
				$this->db->where_in('hash_' . LANG, array(md5($url), md5($clean_url)));
				$result = $this->db->get('site_links');

				if ($result->num_rows() == 0) {
					show_404();
				}

				$row = $result->row_array();
				$this->set_menu_id($row['menu_id'], TRUE, $clean_url);

				return array($row['module'], $row['method'], $row['item_id']);
			}
		}

		/**
		 * Встановлення активного пункту меню
		 *
		 * @param int $menu_id
		 * @param bool $get_info
		 * @param string $url
		 */
		public function set_menu_id($menu_id = 0, $get_info = FALSE, $url = '')
		{
			$this->menu_id = intval($menu_id);

			if ($get_info)
			{
                if (!$this->is_admin()) {
                    $this->db->where('hidden', 0);
                }

				$this->db->select('parent_id, level, menu_index, position, main, name_' . LANG . ' as name, url_' . LANG . ' as url, static_url_' . LANG . ' as static_url');
				$this->db->where('id', $this->menu_id);

				$result = $this->db->get('menu')->row_array();

				if ($result['static_url'] !== '' and $result['url'] !== '' and $result['url'] === $url) {
				    show_404();
                }

				if ($result['parent_id'] > 0) {
					$this->set_menu_parents((int)$result['parent_id']);
				}

				$this->menu_parents[] = $this->menu_id;
				$this->menu_name = $result['name'];

				if ($result['static_url'] != '')
				{
					if (mb_substr($result['static_url'], mb_strlen($result['static_url']) - 5) == '.html')
					{
						$result['url'] = base_url($result['static_url']);
					}
					else
					{
						$result['url'] = $this->uri->full_url($result['static_url']);
					}
				}
				else
				{
					$result['url'] = $this->uri->full_url($result['url']);
				}

				$this->template_lib->set_bread_crumbs($result['url'], $result['name']);
			}
		}

		/**
		 * Встановлення активних пунктів меню
		 *
		 * @param int $parent_id
		 */
		public function set_menu_parents($parent_id)
		{
			array_unshift($this->menu_parents, $parent_id);
			//$this->menu_parents[] = $parent_id;

			$this->db->select('parent_id, name_' . LANG . ' as name, url_' . LANG . ' as url, static_url_' . LANG . ' as static_url');
			$this->db->where('id', $parent_id);

			$result = $this->db->get('menu')->row_array();

			if (is_array($result)) {
				if ($result['static_url'] != '')
				{
					if (mb_substr($result['static_url'], mb_strlen($result['static_url']) - 5) == '.html')
					{
						$result['url'] = base_url($result['static_url']);
					}
					else
					{
						$result['url'] = $this->uri->full_url($result['static_url']);
					}
				}
				else
				{
					$result['url'] = $this->uri->full_url($result['url']);
				}

				$this->template_lib->set_bread_crumbs($result['url'], $result['name'], 'prepend');

				if ($result['parent_id'] > 0) {
					$this->set_menu_parents((int)$result['parent_id']);
				}
			}
		}

		/**
		 * Встановлення ознаки головної сторінки
		 */
		public function set_main()
		{
			$this->is_main = TRUE;
		}

		/**
		 * Отримання активного пункту меню
		 *
		 * @return int
		 */
		public function get_menu_id()
		{
			if ($this->menu_id == 0)
			{
				$this->menu_id = intval($this->input->get('menu_id'));
			}

			return $this->menu_id;
		}

		/**
		 * Отримання активних пунктів меню
		 *
		 * @return array
		 */
		public function get_menu_parents()
		{
			return $this->menu_parents;
		}

		/**
		 * Перевірка на головну сторінку
		 *
		 * @return bool
		 */
		public function is_main()
		{
			return $this->is_main;
		}

		/**
		 * Перевірка наявності компонента на сторінці
		 *
		 * @param int $menu_id
		 * @param string $module
		 * @param null|string $method
		 *
		 * @return bool
		 */
		public function check_component($menu_id, $module, $method = NULL)
		{
			if ($menu_id > 0) $this->db->where('menu_id', $menu_id);
			$this->db->where('module', $module);
			if ($method !== NULL) $this->db->where('method', $method);

			return (bool)$this->db->count_all_results('components');
		}

		#######################################################################################################

		/**
		 * Отримання посилання на сторінку з потрібним компонентом
		 *
		 * @param string $module
		 * @return string
		 */
		public function get_component_link($module)
		{
			$link = '#';

			$this->db->select('menu.main, menu.url_path_' . LANG . ' as url');
			$this->db->join('menu', 'menu.id = components.menu_id');
			$this->db->where('components.hidden', 0);
			$this->db->where('components.module', $module);
			$this->db->where('menu.hidden', 0);

			$result = $this->db->get('components')->row_array();
			if (count($result) > 0) $link = ($result['main'] == 1) ? $this->uri->full_url() : $this->uri->full_url($result['url']);

			return $link;
		}

		#######################################################################################################

		/**
		 * Отримання сегментів візуальної навігації
		 */
		public function set_bread_crumbs()
		{
			$this->db->select('main, name_' . LANG . ' as name, url_path_' . LANG . ' as url');
			$this->db->where('main', 1);

			$result = $this->db->get('menu');

			if ($result->num_rows() > 0)
			{
				$row_array = $result->row_array();
				$this->template_lib->set_bread_crumbs($this->uri->full_url(), $row_array['name'], 'prepend');
			}
		}

		#######################################################################################################

		/**
		 * Отримання посилання через ID пункту меню
		 *
		 * @param $menu_id
		 * @param string $template
		 * @param bool $br
		 * @param bool $hidden
		 * @param null|string $language
		 *
		 * @return mixed|string
		 */
		function get_link($menu_id, $template = '', $br = FALSE, $hidden = FALSE, $language = NULL)
		{
			if ($language === '' OR ($language === NULL AND !defined('LANG_SEGMENT')))
			{
				$language = $this->config->item('def_lang');
				$_language = '';
			}
			else if ($language === NULL AND defined('LANG_SEGMENT'))
			{
				$language = LANG_SEGMENT;
				$_language = LANG_SEGMENT;
			}
			else
			{
				$_language = $language;
			}

			$this->db->select('main, target, name_' . $language . ' as name, url_' . $language . ' as url, static_url_' . $language . ' as static_url');
			$this->db->where('id', $menu_id);
			if ($hidden === FALSE) $this->db->where('hidden', 0);
			$r = $this->db->get('menu');

			if ($r->num_rows() > 0)
			{
				$row = $r->row_array();

				if ($br === TRUE) $row['name'] = str_replace(" ", '<br>', $row['name']);

				if ($row['main'] == 1)
				{
				    $url = $this->uri->full_url('', $_language);
				}
				else
				{
					if ($row['static_url'] != '')
					{
						$url = ($row['target'] == 0) ? $this->uri->full_url($row['static_url'], $_language) : $row['static_url'];
					}
					else
					{
						$url = $this->uri->full_url($row['url'], $_language);
					}
				}

				return str_replace(array('{URL}', '{NAME}'), array($url, $row['name']), $template);
			} else {
                return $this->uri->full_url('', $_language);
            }

			//return '';
		}

		#######################################################################################################

		/**
		 * Встановлення метатегів сторінки
		 */
		public function set_metatags()
		{
			$is_set = FALSE;

			if ($this->db->where('menu_id', $this->menu_id)->count_all_results('seo_tags') == 0)
			{
				$set = array(
					'item_id' => 0,
					'component_id' => 0,
					'menu_id' => $this->menu_id
				);
				$this->db->insert('seo_tags', $set);
			}

			$this->db->select('type_' . LANG . ' as type, title_' . LANG . ' as title, description_' . LANG . ' as description, keywords_' . LANG . ' as keywords');
			$this->db->select('cache_title_' . LANG . ' as cache_title, cache_description_' . LANG . ' as cache_description, cache_keywords_' . LANG . ' as cache_keywords, cache_' . LANG . ' as cache');
			$this->db->where('menu_id', $this->menu_id);
			$this->db->where('item_id', 0);

			$result = $this->db->get('seo_tags');

			if ($result->num_rows() > 0)
			{
				$row = $result->row_array();

				if ($row['type'] == 1)
				{
					$this->template_lib->set_title($row['title']);
					$this->template_lib->set_description($row['description']);
					$this->template_lib->set_keywords($row['keywords']);

					$is_set = TRUE;
				}
				else
				{
					if ($row['cache'] == 1 AND $row['cache_title'] != '' AND !$this->is_admin())
					{
						$this->template_lib->set_title($row['cache_title']);
						$this->template_lib->set_description($row['cache_description']);
						$this->template_lib->set_keywords($row['cache_keywords']);

						$is_set = TRUE;
					}
					else
					{
						$this->db->select('component_article.title_' . LANG . ' as title, component_article.text_' . LANG . ' as text');
						$this->db->join('components', 'components.component_id = component_article.component_id');
						$this->db->where('component_article.menu_id', $this->menu_id);
						$this->db->where('components.hidden', 0);
						$this->db->order_by('components.position');

						$result = $this->db->get('component_article');

						if ($result->num_rows() > 0)
						{
							$this->load->library('seo_lib');

							$title = '';
							$text = '';
							$result_array = $result->result_array();

							foreach ($result_array as $key => $row)
							{
								if ($key == 0) $title = $row['title'];
								$text .= $row['title'] . ' ' . $row['text'] . ' ';
							}

							$description = $this->seo_lib->generate_description($text);
							$keywords = $this->seo_lib->generate_keywords($text);

							$this->template_lib->set_title($title);
							$this->template_lib->set_description($description);
							$this->template_lib->set_keywords($keywords);

							$set = array(
								'cache_title_' . LANG => $title,
								'cache_description_' . LANG => $description,
								'cache_keywords_' . LANG => $keywords,
								'cache_' . LANG => 1
							);
							$this->db->update('seo_tags', $set, array('menu_id' => $this->menu_id, 'item_id' => 0));

							$is_set = TRUE;
						}
					}
				}
			}

			if ($is_set === FALSE)
			{
				if ($this->menu_name != '')
				{
					$this->template_lib->set_title($this->menu_name);
				}
				else
				{
					$this->template_lib->set_title($this->config->item('site_name_' . LANG));
				}
			}
		}

		#######################################################################################################

		/**
		 * @param int $id
		 * @return int
		 */
		public function dir_by_id($id)
		{
			return ceil($id / 100) * 100;
		}

		#######################################################################################################

		/**
		 * Перевірка залогованості користувача
		 *
		 * @return bool
		 */
		public function is_user()
		{
			if (!$this->user_validate AND defined('USER_VALIDATE')) exit;

			if (!defined('USER_VALIDATE'))
			{
				$user_id = intval($this->session->userdata('user_id'));

				if ($user_id > 0) {
					$user = $this->db
						->select('users.price_type')
						->where('users.user_id', $user_id)

						->select('clients.price_type as client_price_type')
						->join('clients', 'users.code = clients.id', 'left')

						->get('users')
						->row_array();

					if ($user !== null) {
						if ($user['price_type'] !== '' and $user['price_type'] !== '0328a9a7-c76c-11e6-8965-00155d139102') {
							$this->config->set_item('price_type', $user['price_type']);
						} elseif ((string)$user['client_price_type'] !== '' and $user['client_price_type'] !== '0328a9a7-c76c-11e6-8965-00155d139102') {
							$this->config->set_item('price_type', $user['client_price_type']);
						}

						return TRUE;
					}
				}
			}

			return FALSE;
		}

		#######################################################################################################

		public function get_gag()
		{
			$text = $this->db->select('text')->where('lang', LANG)->get('gag')->row('text');

			if (is_string($text))
			{
				$text = stripslashes($text);
			}
			else
			{
				$text = '';
			}

			return $text;
		}

		#######################################################################################################

		/**
		 * Перевірка на максимальну знижку
		 *
		 * @return bool
		 */
		public function check_percents() {
			return !in_array($this->config->item('price_type'), array('p7', 'p8'));
		}

		public function get_price($price, $currency, $percent = 0, $price_type = false) {
			$currency = mb_strtolower($currency, 'UTF-8');

			if ($currency === 'usd') {
				$price = $price * $this->config->item('usd');
			} elseif ($currency === 'eur') {
				$price = $price * $this->config->item('eur');
			} elseif ($currency === 'pln') {
				$price = $price * $this->config->item('pln');
            } elseif ($currency === 'UAH') {
                $price = $price;
			}

			if (!$price_type) {
				$price_type = $this->config->item('price_type');
			}

			if ($price_type === 'p8') {
				$price = $price * 1.4;
			} elseif ($price_type === 'p7') {
				$price = $price * 1.45;
			} elseif ($price_type === 'p6') {
				$price = $price * 1.5;
			} elseif ($price_type === 'p5') {
				$price = $price * 1.6;
			} elseif ($price_type === 'p4') {
				$price = $price * 1.7;
			} elseif ($price_type === 'p3') {
				$price = $price * 1.8;
			} elseif ($price_type === 'p2') {
				$price = $price * 1.9;
			} elseif ($price_type === 'p1') {
				$price = $price * 1.94;
			} else {
				$price = $price * 2;
			}

			if ($percent > 0 and !in_array($price_type, array('p7', 'p8'))) {
				$price = $price - ($price * $percent / 100);
			}

			return round($price, 2);
		}

		/**
		 * Отримання порогу ліміту в штуках
		 *
		 * @param int $box
		 * @param float|int $percent
		 * @return float|int
		 */
		public function get_limit_items($box, $percent)
		{
			$items = 0;
			$limit = (float)$this->config->item('l' . $percent);

			if ($box > 0 and $limit > 0) {
				$items = ceil($limit / $box) * $box;
			}

			return $items;
		}

		/**
		 * Приведення ціни за коробку
		 *
		 * @param int|float $price
		 * @param int $box
		 * @return float
		 */
		public function price_to_box($price, $box)
		{
			$box = (int)$box;

			if ($box !== 100 and $box > 0) {
				$price = round($box * $price / 100, 2);
			}

			return $price;
		}
	}