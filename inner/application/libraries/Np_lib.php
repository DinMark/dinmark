<?php defined('ROOT_PATH') or exit('No direct script access allowed');

	/**
	 * Class Np_lib
	 */
	class Np_lib {
		/**
		 * @var
		 */
		protected $key;

		/**
		 * @var
		 */
		protected $lang;

		/**
		 *
		 */
		const api_url = 'https://api.novaposhta.ua/v2.0/json/';

		/**
		 *
		 */
		const content_type = 'application/json';

		/**
		 * Конструктор класу
		 *
		 * @param array $params
		 */
		public function __construct(array $params)
		{
			if (array_key_exists('key', $params)) {
				$this->key = $params['key'];
			}

			if (array_key_exists('lang', $params)) {
				$this->lang = $params['lang'];
			}
		}

		// Address methods

		/**
		 * Отримання списка областей
		 *
		 * @param int $page
		 * @param string $ref
		 *
		 * @return array
		 */
		public function get_areas($page = 1, $ref = '')
		{
			return $this->request(
				'Address',
				'getAreas',
				array(
					'page' => $page,
					'Ref' => $ref,
				)
			);
		}

		/**
		 * Пошук населених пунктів за назвою
		 *
		 * @param string $city
		 *
		 * @return array
		 */
		public function get_cities($city = '')
		{
			return $this->request(
				'Address',
				'getCities',
				array(
					'FindByString' => $city,
				)
			);
		}

		/**
		 * Отримання списка відділень
		 *
		 * @param string $city_ref
		 * @param int $page
		 *
		 * @return array
		 */
		public function get_warehouses($city_ref = '', $page = 1)
		{
			return $this->request(
				'Address',
				'getWarehouses',
				array(
					'CityRef' => $city_ref,
					'page' => $page,
				)
			);
		}

		// Library methods

		/**
		 * Отримання списку видів зворотньої доставки ваниажу
		 *
		 * @return array
		 */
		public function get_backward_delivery_cargo_types()
		{
			return $this->request('Common', 'getBackwardDeliveryCargoTypes');
		}

		/**
		 * Отримання описів вантажу
		 *
		 * @param string $search_string
		 *
		 * @return array
		 */
		public function get_cargo_description($search_string = '')
		{
			return $this->request(
				'Common',
				'getCargoDescriptionList',
				array(
					'FindByString' => $search_string,
				)
			);
		}

		/**
		 * Отримання видів вантажу
		 *
		 * @return array
		 */
		public function get_cargo_types()
		{
			return $this->request('Common', 'getCargoTypes');
		}

		/**
		 * Отримання статусів документів
		 *
		 * @return array
		 */
		public function get_document_statuses()
		{
			return $this->request('Common', 'getDocumentStatuses');
		}

		/**
		 * Отримання списку форм власності
		 *
		 * @return array
		 */
		public function get_ownerships_forms()
		{
			return $this->request('Common', 'getOwnershipFormsList');
		}

		/**
		 * Отримання форм оплати
		 *
		 * @return array
		 */
		public function get_payment_forms()
		{
			return $this->request('Common', 'getPaymentForms');
		}

		/**
		 * Отримання технологій доставки
		 *
		 * @return array
		 */
		public function get_service_types()
		{
			return $this->request('Common', 'getServiceTypes');
		}

		/**
		 * Отримання списка контрагентів відправників
		 *
		 * @return array
		 */
		public function get_counterparties_types()
		{
			return $this->request('Common', 'getTypesOfCounterparties');
		}

		/**
		 * Отримання видів платників
		 *
		 * @return array
		 */
		public function get_payer_types()
		{
			return $this->request('Common', 'getTypesOfPayers');
		}

		/**
		 * Отримання видів платників зворотньої доставки
		 *
		 * @return array
		 */
		public function get_payer_types_redelivery()
		{
			return $this->request('Common', 'getTypesOfPayersForRedelivery');
		}

		// Documents methods

		/**
		 * Створення документу
		 *
		 * @param array $params
		 *
		 * @return array
		 */
		public function create_document($params)
		{
			return $this->request('InternetDocument', 'save', $params);
		}

		/**
		 * Видалення документу
		 *
		 * @param string|array $ref
		 *
		 * @return array
		 */
		public function delete_document($ref)
		{
			return $this->request(
				'InternetDocument',
				'delete',
				array(
					'DocumentRefs' => is_array($ref) ? $ref : array($ref),
				)
			);
		}

		/**
		 * Отримання списка документів
		 *
		 * @param string $ref
		 * @param string $barcode
		 *
		 * @return array
		 */
		public function get_documents($ref = '', $barcode = '')
		{
			$params = null;

			if ($ref !== '' or $barcode !== '') {
				$params = array();

				if ($ref !== '') {
					$params['Ref'] = $ref;
				}

				if ($barcode !== '') {
					$params['IntDocNumber'] = $barcode;
				}
			}

			return $this->request('InternetDocument', 'getDocumentList', $params);
		}

		/**
		 * Отримання документу
		 *
		 * @param string $ref
		 *
		 * @return array
		 */
		public function get_document($ref = '')
		{
			if ($ref !== '') {
				return $this->request(
					'InternetDocument',
					'getDocument',
					array(
						'Ref' => $ref,
					)
				);
			} else {
				return false;
			}
		}

		/**
		 * Трекінг документа
		 *
		 * @param string|array $barcodes
		 *
		 * @return array
		 */
		public function documents_traking($barcodes)
		{
			return $this->request(
				'InternetDocument',
				'documentsTracking',
				array(
					'Documents' => !is_array($barcodes) ? explode(',', $barcodes) : $barcodes,
				)
			);
		}

		/**
		 * Друк документу
		 *
		 * @param string $document_refs
		 * @param string $type
		 *
		 * @return array
		 */
		public function print_document($document_refs = '', $type = 'html')
		{
			return 'https://my.novaposhta.ua/orders/printDocument/orders%5B%5D/' . $document_refs . '/type/' . $type . '/apiKey/' . $this->key;
		}

		/**
		 * Друк маркування
		 *
		 * @param string $document_refs
		 * @param string $type
		 *
		 * @return string
		 */
		public function print_marking($document_refs = '', $type = 'html')
		{
			return 'https://my.novaposhta.ua/orders/printMarkings/orders%5B%5D/' . $document_refs . '/type/' . $type . '/apiKey/' . $this->key;
		}

		/**
		 * Розрахунок вартості доставки
		 *
		 * @param string $sender_city
		 * @param string $recipient_city
		 * @param string $sevice_type
		 * @param int|float $weight
		 * @param int|float $cost
		 *
		 * @return array|bool
		 */
		public function get_delivery_price($sender_city, $recipient_city, $sevice_type, $weight, $cost)
		{
			if (
				$sender_city !== ''
				and $recipient_city !== ''
				and $sender_city !== ''
				and $weight !== ''
				and $cost !== ''
			) {
				return $this->request(
					'InternetDocument',
					'getDocumentPrice',
					array(
						'CitySender' => $sender_city,
						'CityRecipient' => $recipient_city,
						'ServiceType' => $sevice_type,
						'Weight' => $weight,
						'Cost' => $cost,
					)
				);
			} else {
				return false;
			}
		}

		// Counterparties methods

		/**
		 * Збереження контрагента
		 *
		 * @param string $counterparty_property
		 * @param string $city_ref
		 * @param string $counterparty_type
		 * @param string $first_name
		 * @param string $middle_name
		 * @param string $last_name
		 * @param string $phone
		 *
		 * @return array|bool
		 */
		public function save_counterparty($counterparty_property, $city_ref, $counterparty_type, $first_name, $middle_name, $last_name, $phone)
		{
			if (
				$counterparty_property !== ''
				and $city_ref !== ''
				and $counterparty_type !== ''
				and $first_name !== ''
				and $middle_name !== ''
				and $last_name !== ''
				and $phone !== ''
			) {
				return $this->request(
					'Counterparty',
					'save',
					array(
						'CounterpartyProperty' => $counterparty_property,
						'CityRef' => $city_ref,
						'CounterpartyType' => $counterparty_type,
						'FirstName' => $first_name,
						'MiddleName' => $middle_name,
						'LastName' => $last_name,
						'Phone' => $phone,
					)
				);
			} else {
				return false;
			}
		}

		/**
		 * Отримання списка контрагентів
		 *
		 * @param string $property
		 * @param int $page
		 * @param string $find_string
		 *
		 * @return array
		 */
		public function get_counterparties($property = 'Recipient', $page = 1, $find_string = '')
		{
			$params = array(
				'CounterpartyProperty' => $property,
				'Page' => $page,
			);

			if ($find_string !== '') {
				$params['FindByString'] = $find_string;
			}

			return $this->request('Counterparty', 'getCounterparties', $params);
		}

		/**
		 * Отримання списка адрес контрагента
		 *
		 * @param string $ref
		 * @param string $type
		 *
		 * @return array|bool
		 */
		public function get_counterparty_address($ref = '', $type = '')
		{
			if ($ref !== '' and $type !== '') {
				return $this->request(
					'Counterparty',
					'getCounterpartyAddresses',
					array(
						'Ref' => $ref,
						'Page' => 0,
					)
				);
			} else {
				return false;
			}
		}

		/**
		 * Отримання списку контактних осіб контрагента
		 *
		 * @param string $ref
		 *
		 * @return array|bool
		 */
		public function get_counterparty_contact_persons($ref = '')
		{
			if ($ref !== '') {
				return $this->request(
					'Counterparty',
					'getCounterpartyContactPersons',
					array(
						'Ref' => $ref,
					)
				);
			} else {
				return false;
			}
		}

		/**
		 * Отримання опцій контрагента
		 *
		 * @param string $ref
		 *
		 * @return array|bool
		 */
		public function get_counterparty_options($ref = '')
		{
			if ($ref !== '') {
				return $this->request(
					'Counterparty',
					'getCounterpartyOptions',
					array(
						'Ref' => $ref,
					)
				);
			} else {
				return false;
			}
		}

		/**
		 * Виконання запиту
		 *
		 * @param string $model
		 * @param string $method
		 * @param null|array $params
		 *
		 * @return array
		 */
		public function request($model, $method, $params = null)
		{
			$ch = curl_init(self::api_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: ' . self::content_type));
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			curl_setopt(
				$ch,
				CURLOPT_POSTFIELDS,
				json_encode(
					array(
						'apiKey' => $this->key,
						'modelName' => $model,
						'calledMethod' => $method,
						'language' => $this->lang,
						'methodProperties' => $params,
					)
				)
			);

			$result = curl_exec($ch);
			curl_close($ch);

			return json_decode($result, 1);
		}
	}